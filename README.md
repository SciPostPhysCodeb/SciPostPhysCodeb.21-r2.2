# Codebase release 2.2 for HEJ

by Jeppe R. Andersen, Bertrand Ducloué, Conor Elrick, Hitham Hassan, Andreas Maier, Graeme Nail, Jérémy Paltrinieri, Andreas Papaefstathiou, Jennifer M. Smillie

SciPost Phys. Codebases 21-r2.2 (2023) - published 2023-12-05

[DOI:10.21468/SciPostPhysCodeb.21-r2.2](https://doi.org/10.21468/SciPostPhysCodeb.21-r2.2)

This repository archives a fixed release as a citable refereed publication. Please refer to the [publication page](https://scipost.org/SciPostPhysCodeb.21-r2.2) to view the full bundle and get information on how to cite it.

For a link to the latest/development version, check the Resources section below.

Copyright Jeppe R. Andersen, Bertrand Ducloué, Conor Elrick, Hitham Hassan, Andreas Maier, Graeme Nail, Jérémy Paltrinieri, Andreas Papaefstathiou, Jennifer M. Smillie.

This README is published under the terms of the CC BY 4.0 license. For the license to the actual codebase, please refer to the license specification in the codebase folder.

## Resources:

* Live (external) repository at [https://phab.hepforge.org/source/hej/browse/v2.2/;2.2.2](https://phab.hepforge.org/source/hej/browse/v2.2/;2.2.2)
* Codebase release version (archive) repository at [https://git.scipost.org/SciPostPhysCodeb/SciPostPhysCodeb.21-r2.2](https://git.scipost.org/SciPostPhysCodeb/SciPostPhysCodeb.21-r2.2)

# Changelog

This is the log for changes to the HEJ program. Further changes to the HEJ API
are documented in [`Changes-API.md`](Changes-API.md). If you are using HEJ as a
library, please also read the changes there.

## Version 2.2

This release adds support for the new processes:

* Wp+Wp with jets.
* Wm+Wm with jets.

In addition, there are new options when running HEJ:

* Enable NLO truncation for use in HEJ@NLO predictions.
* Require a lowpt jet, used for running lowpt separately.

This release includes minor changes which affect users of HEJ as a library (see
[`Changes-API.md`](Changes-API.md)). Compilation now requires a
compiler supporting C++17.

### 2.2.2

* Fixed calculation of unordered gluon emission in Z+jets production.

### 2.2.1

* Fixed calculation of extremal qqbar contribution to W+jets production.

### 2.2.0

#### New Processes

* Resummation for WpWp/WmWm with jets, including interference between
  configurations.
* Significantly improved description of Higgs boson plus jet production:
  - Processes with extremal Higgs boson emission are now treated as
    leading-log.
  - Resummation is now enabled for Higgs boson production with a
    single jet.
* `HEJFOG` can generate multiple jets together with a charged
  lepton-antilepton pair via a virtual Z boson or photon.

#### Updates to configuration file

* With the new option `off-shell tolerance` HEJ repairs the momenta
  of massless particles that are slightly off-shell and incoming
  particles with small but non-vanishing transverse momenta.
* Introduced new event types `unknown` and `invalid` under the `event
  treatment` option. `unknown` indicates processes that are not
  recognised by HEJ. `invalid` is reserved for unphysical processes.
* Introduced new event treatment `abort` for aborting the program when
  an undesired event type is encountered.
* Removed the deprecated settings `min extparton pt` and
  `max ext soft pt fraction`.
* Removed the deprecated `analysis` setting.

#### Changes to input/output
* Event reader supports event input from named pipes (fifo)

## Version 2.1

This release adds support for two new processes:

* W boson with jets.
* Jets with a charged lepton-antilepton pair via a virtual Z boson or photon.

In addition, the complete set of first subleading processes (unordered
gluon, central and extremal quark-antiquark pair) is implemented for
pure jets and W + jets, see
[arXiv:2012.10310](https://arxiv.org/abs/2012.10310). Unordered gluon emission
is also supported for Higgs boson + jets and Z boson/photon + jets.

This release include many changes to the code, which affect users of
HEJ as a library (see [`Changes-API.md`](Changes-API.md)).

### 2.1.3

* Updated documentation.

### 2.1.2

* Updated `cxxopts.hpp` dependency.

### 2.1.1

* Fixed invalid iterator accesses.
* Reorganised automated tests.
* Updated documentation.

### 2.1.0

#### New Processes
* Resummation for W bosons with jets
  - New subleading processes `extremal qqbar` & `central qqbar` for a quark and
    anti-quark in the final state, e.g. `g g => u d_bar Wm g` (the other
    subleading processes also work with W's)
  - `HEJFOG` can generate multiple jets together with a (off-shell) W bosons
    decaying into lepton & neutrino
* Resummation for jets with a charged lepton-antilepton pair via a
  virtual Z boson or photon. Includes the `unordered` subleading process.
* Resummation can now be performed on all subleading processes within pure
  jets also. This includes `unordered`, `extremal qqbar` and `central
  qqbar` processes.

#### More Physics implementation
* Partons now have a Colour charge
  - Colours are read from and written to LHE files
  - For reweighted events the colours are created according to leading colour in
    the FKL limit
* Use relative fraction for soft transverse momentum in tagging jets (`soft pt
  regulator`) as new (optional) parameter.
  - This supersedes `min extparton pt`, which is marked **deprecated** and will
    be removed in version 2.2.0
  - This is a direct replacement for the old `max ext soft pt fraction`, which
    is also **deprecated**.
* Changed the redistribution of the momenta of soft emissions. Now also bosons
  can take part of the recoil (previously only jets).

#### Updates to Runcard
* Allow multiplication and division of multiple scale functions e.g.
  `H_T/2*m_j1j2`
* Grouped `event treatment` for subleading channels together in runcard
  - Rename `non-HEJ` processes to `non-resummable`
* Read electro-weak constants from input
  - new mandatory setting `vev` to change vacuum expectation value
  - new mandatory settings `particle properties` to specify mass & width of
    bosons
  - `HEJFOG`: decays are now specified in `decays` setting (previously under
    `particle properties`)
* Allow loading multiple analyses with `analyses`. The old `analysis` (with "i")
  is marked **deprecated**.
* Optional setting to specify maximal number of Fixed Order events (`max
  events`, default is all)
* Allow changing the regulator lambda in input (`regulator parameter`, only for
  advanced users)

#### Changes to Input/Output
* Added support to read & write `hdf5` event files suggested in
  [arXiv:1905.05120](https://arxiv.org/abs/1905.05120) (needs
  [HighFive](https://github.com/BlueBrain/HighFive))
* Support input with average weight equal to the cross section (`IDWTUP=1 or 4`)
* Support unmodified Les Houches Event Files written by Sherpa with
  `cross section = sum(weights)/sum(trials)`
* Analyses now get general run information (`LHEF::HEPRUP`) in the
  constructor. **This might break previously written, external analyses!**
  - external analyses should now be created with
    `make_analysis(YAML::Node const & config, LHEF::HEPRUP const & heprup)`
* Support `rivet` version 3 with both `HepMC` version 2 and 3
  - Multiple weights with `rivet 3` will only create one `.yoda` file (instead
    of one per weight/scale)
* Added option to unweight only resummation events
  (`unweight: {type: resummation}`)
* Added option for partially unweighting resummation events, similar to
  the fixed-order generator.
* Improved unweighting algorithm.
* Follow HepMC convention for particle Status codes: incoming = 11,
  decaying = 2, outgoing = 1 (unchanged)

#### Miscellaneous
* Print cross sections at end of run
* Added example analysis & scale to `examples/`. Everything in `examples/` will
  be build when the flag `-DBUILD_EXAMPLES=TRUE` is set in `cmake`.
* Dropped support for HepMC 3.0.0, either HepMC version 2 or >3.1 is required
  - It is now possible to write out both HepMC 2 and HepMC 3 events at the same
    time
* Require LHADPF version 6. Dropped support for all other versions.
* Use `git-lfs` for raw data in test (`make test` now requires `git-lfs`)
* Currents are now generated with [`FORM`](https://github.com/vermaseren/form)
  - `FORM` is included as a `git submodule`, use `git submodule update --init`
    to download `FORM`
* Create [Sphinx](http://sphinx-doc.org/) and [Doxygen](http://doxygen.org/)
  documentation by `make sphinx` or `make doxygen` in your `build/` folder

## Version 2.0

First release of HEJ 2. Complete code rewrite compared to HEJ 1. Improved
matching to Fixed Order ([arXiv:1805.04446](https://arxiv.org/abs/1805.04446)).
Implemented processes: Higgs boson with jets (FKL and unordered gluon emission,
with finite quark mass loop,
[arXiv:1812.08072](https://arxiv.org/abs/1812.08072)), and pure jets (only FKL).
See [arXiv:1902.08430](https://arxiv.org/abs/1902.08430)

## 2.0.7

* Added missing `#include` directives.

### 2.0.6

* Fixed compiling rivet when YODA headers are _outside_ of rivet directory.

### 2.0.5

* Fixed event classification for input not ordered in rapidity.

### 2.0.4

* Fixed wrong path of `HEJ_INCLUDE_DIR` in `hej-config.cmake`.

### 2.0.3

* Fixed parsing of (numerical factor) * (base scale) in configuration.
* Don't change scale names, but sanitise Rivet output file names instead.

### 2.0.2

* Changed scale names to `"_over_"` and `"_times_"` for proper file names (was
  `"/"` and `"*"` before).

### 2.0.1

* Fixed name of fixed-order generator in error message.

### 2.0.0

* First release.

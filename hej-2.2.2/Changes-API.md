# Changelog for HEJ API

This log lists only changes on the HEJ API. These are primarily code changes
relevant for calling HEJ as an API. This file should only be read as an addition
to [`Changes.md`](Changes.md), where the main features are documented.

## Version 2.2

### 2.2.0

#### New and updated functions
* New functions to decide whether particles are massive, charged,
  charged leptons, antiparticles.
* Added `to_string` function for `Event`.
* Updated function `Event::EventData.reconstruct_intermediate()`
  - Now requires an `EWConstants` argument
  - Added support for WpWp and WmWm events.
  - In the case of WW same-flavour the reconstruction will minimise the total
    off-shell momentum.
* Replaced `no_2_jets` and `bad_final_state` enumerators in `EventType` with
  `invalid` and `unknown`.
* Added helper functions `is_backward_g_to_h`, `is_forward_g_to_h` to
  detect incoming gluon to outgoing Higgs transitions.
* Updated function `implemented_types()` to now be in HEJ namespace.
* `EventWriter` has a new `set_xs_scale` member function to set the
  ratio between the sum of event weights and the cross section.

#### Removed previously deprecated interfaces
* Removed the `UnclusteredEvent` class.
* Removed the `Weights.hh` header file.
* Removed the `min_extparton_pt` and `max_ext_soft_pt_fraction` settings.

### Deprecation
The following functionality is deprecated and will be **removed** in
version 2.3.0.
* The `EmptyAnalysis` class.
* `HEJ::optional`. Use `std::optional` instead.

## Version 2.1

### 2.1.0

#### Changes to Event class
* Restructured `Event` class
  - `Event` can now only be build from a (new) `Event::EventData` class
  - Removed default constructor for `Event`
  - `Event::EventData` replaces the old `UnclusteredEvent` struct.
  - `UnclusteredEvent` is now **deprecated**, and will be removed in version
    2.2.0
  - Removed `Event.unclustered()` function
  - Added new member function `Events.parameters()`, to directly access
    (underlying) `Parameters<EventParameters>`
  - New member functions `begin_partons`, `end_partons` (`rbegin_partons`,
    `rend_partons`) with aliases `cbegin_partons`, `cend_partons`
    (`crbegin_partons`, `crend_partons`) for constant (reversed) iterators over
    outgoing partons.
* New function `Event::EventData.reconstruct_intermediate()` to reconstruct
  bosons from decays, e.g. `positron + nu_e => Wp`
* Added optional Colour charges to particles (`Particle.colour`)
  - Colour connection in the HEJ limit can be generated via
    `Event.generate_colours` (automatically done in the resummation)
  - Colour configuration of input events can be checked with
    `Event.is_leading_colour`
* Added function `Event.valid_hej_state` to check if event _could have_ been
  produced by `HEJ` according to the `soft pt regulator` cut on the jets

#### New and updated functions
* Renamed `EventType::nonHEJ` to `EventType::non_resummable` and `is_HEJ()`
  to `is_resummable()` such that run card is consistent with internal workings
* Made `MatrixElement.tree_kin(...)` and `MatrixElement.tree_param(...)` public
* New `EventReweighter` member function `treatment` to query the
  treatment with respect to resummation for the various event types.
* Added auxiliary functions to obtain a `std::string` from `EventDescription`
  (`to_string` for human readable, and `to_simple_string` for easy parsable
  string)
* New `get_analyses` function to read in multiple `HEJ::Analysis` at once,
  similar to `get_analysis`
* New `get_ew_parameters` function to extract electroweak parameters from
  YAML configuration.

#### New classes
* New class `Unweighter` to do unweighting
* New class `CrossSectionAccumulator` to keep track of Cross Section of the
  different subprocess
* New template struct `Parameters` similar to old `Weights`
  - `Weights` are now an alias for `Parameters<double>`. Calling `Weights` did
    not change
  - `Weights.hh` was replaced by `Parameters.hh`. The old `Weights.hh` header
    will be removed in version 2.2.0
* Function to multiplication and division of `EventParameters.weight` by double
  - This can be combined with `Parameters`, e.g.
    `Parameters<EventParameters>*Weights`, see also `Events.parameters()`
  - Moved `EventParameters` to `Parameters.hh` header
* new class `EWConstants` replaces previously hard coded `vev`
  - `EWConstants` have to be set in the general `Config` and the
    `MatrixElementConfig`

#### Input/Output
* New abstract `EventReader` class, as base for reading events from files
  - Moved LHE file reader to `HEJ::LesHouchesReader`
* New (optional) function `finish()` in abstract class `EventWriter`. `finish()`
  is called _after_ all events are written.
* Support reading (`HDF5Reader`) and writing (`HDF5Writer`) `hdf5` files
* New `BufferedEventReader` class that allows to put events back into
  the reader.
* New `SherpaLHEReader` to read Sherpa LHE files with correct weights
* `get_analysis` now requires `YAML::Node` and `LHEF::HEPRUP` as arguments
* Replaced `HepMCInterface` and `HepMCWriter` by `HepMCInterfaceX` and
  `HepMCWriterX` respectively, with `X` being the major version of HepMC (2 or
  3)
  - Renamed `HepMCInterfaceX::init_kinematics` to `HepMCInterfaceX::init_event`
    and protected it, use `HepMCInterfaceX::operator()` instead
  - Removed redundant `HepMCInterfaceX::add_variation` function

#### Linking
* Export cmake target `HEJ::HEJ` to link directly against `libHEJ`
* Preprocessor flags (`HEJ_BUILD_WITH_XYZ`) for enabled/disabled dependencies
  are now written to `ConfigFlags.hh`
* Provide links to version specific object files, e.g. `libHEJ.so ->
  libHEJ.so.2.1 (soname) -> libHEJ.so.2.1.0`
* Removed `LHAPDF` from public interface

#### Miscellaneous
* Capitalisation of `Config.hh` now matches class `Config` (was `config.hh`)
* Renamed `Config::max_ext_soft_pt_fraction` to `Config::soft_pt_regulator`.
  The new `Config::soft_pt_regulator` parameter is optional and the old
  `Config::max_ext_soft_pt_fraction` is **deprecated**.
* Replaced redundant member `EventReweighterConfig::jet_param` with getter
  function `EventReweighter.jet_param()` (`JetParameters` are already in
  `PhaseSpacePointConfig`)
* Avoid storing reference to the Random Number Generator inside classes
  - Constructors of `EventReweighter` now expect `std::shared_ptr<HEJ::RNG>`
    (was reference)
  - Moved reference to `HEJ::RNG` from constructor of `JetSplitter` to
    `JetSplitter.split`

## Version 2.0

### 2.0.4

* Fixed wrong path of `HEJ_INCLUDE_DIR` in `hej-config.cmake`

### 2.0.0

* First release

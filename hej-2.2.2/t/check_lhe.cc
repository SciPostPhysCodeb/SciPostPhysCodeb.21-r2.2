/**
 *  \authors   The HEJ collaboration (see AUTHORS for details)
 *  \date      2019-2023
 *  \copyright GPLv2 or later
 */
#include "hej_test.hh"

#include <cstdlib>
#include <iostream>
#include <unordered_map>

#include "LHEF/LHEF.h"

#include "fastjet/JetDefinition.hh"

#include "HEJ/Event.hh"
#include "HEJ/event_types.hh"
#include "HEJ/EventReader.hh"

namespace {
  constexpr double EP = 1e-3;
  const fastjet::JetDefinition JET_DEF{fastjet::kt_algorithm, 0.4};
  constexpr double MIN_JET_PT = 30;
}


int main(int argn, char** argv) {
  if(argn != 2 && argn != 3){
    std::cerr << "Usage: " << argv[0] << " lhe_file [XS test value]\n";
    return EXIT_FAILURE;
  }

  auto reader{ HEJ::make_reader(argv[1]) };

  // version should be overwritten
  ASSERT(reader->heprup().version==LHEF::HEPRUP().version);

  std::unordered_map<int, double> xsec_ref;
  for(int i=0; i < reader->heprup().NPRUP; ++i)
      xsec_ref[reader->heprup().LPRUP[i]] = 0.;

  while(reader->read_event()){
    auto const & hepeup = reader->hepeup();
    ASSERT(hepeup.NUP > 2); // at least 3 particles (2 in + 1 out)
    // first incoming has positive pz
    ASSERT(hepeup.PUP[0][2] > hepeup.PUP[1][2]);
    // test that we can trasform IDPRUP to event type
    (void) name(static_cast<HEJ::event_type::EventType>(hepeup.IDPRUP));
    xsec_ref[hepeup.IDPRUP] += hepeup.weight();
    // test that a HEJ event can be transformed back to the original HEPEUP
    auto hej_event = HEJ::Event::EventData(hepeup).cluster(JET_DEF, MIN_JET_PT);
    // there are two different weight infos, which should be the same
    ASSERT(hej_event.central().weight == hepeup.weight());
    ASSERT(hej_event.central().weight == hepeup.XWGTUP);
    // reader->heprup() is const, we can't use it to create a hepeup
    auto cp_heprup = reader->heprup();
    auto new_event = HEJ::to_HEPEUP(hej_event, &cp_heprup);
    ASSERT_PROPERTY(new_event, hepeup, weight());
    ASSERT_PROPERTY(new_event, hepeup, XWGTUP);
    ASSERT_PROPERTY(new_event, hepeup, SCALUP);
    ASSERT_PROPERTY(new_event, hepeup, NUP);
  }
  double xstotal = 0;
  for(std::size_t i = 0; i < xsec_ref.size(); ++i){
    std::cout << "[+]" << xsec_ref.size() << std::endl;
    double const ref = xsec_ref[reader->heprup().LPRUP[i]];
    double const calc = reader->heprup().XSECUP[i];
    xstotal += calc;
    std::cout << ref << '\t' << calc << '\n';
    if(std::abs(calc-ref) > EP*calc){
      std::cerr << "Cross sections deviate substantially";
      return EXIT_FAILURE;
    }
  }
  if (argn == 3){
      const double xstest = std::stod(argv[2]);
      if(std::abs(xstotal-xstest) > EP*xstotal){
      std::cerr << "Total XS deviates substantially from ref value: "
      << xstotal << " vs " << xstest << "\n";
      return EXIT_FAILURE;
      }
    }


  return EXIT_SUCCESS;
}

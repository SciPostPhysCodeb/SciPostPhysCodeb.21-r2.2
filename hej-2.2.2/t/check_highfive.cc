/**
 *  \authors   The HEJ collaboration (see AUTHORS for details)
 *  \date      2019-2023
 *  \copyright GPLv2 or later
 */
#include <cstdlib>
#include <iostream>

#include <highfive/H5File.hpp>

#include "HEJ/exceptions.hh"

using namespace HighFive;

int main(int argn, char** argv) {
  constexpr double EP = 1e-3;

  if(argn != 3){
    std::cerr << "Usage: check_highfive hdf5_file XS_test_value\n";
    return EXIT_FAILURE;
  }
  const double xstest = std::stod(argv[2]);
  File file(argv[1], File::ReadOnly);
  auto dataset = file.getDataSet("procInfo/xSection");
  std::vector<double> result;
  // hdf data in form
  // {  0,   1,     2,              3,...}
  // {fkl, uno, qqbar, non-resummable,...}
  const std::vector<size_t> fkl_xs_idx{0};
  dataset.select(fkl_xs_idx).read(result);
  double xs = result[0];
  const std::vector<size_t> nonresum_xs_idx{3};
  dataset.select(nonresum_xs_idx).read(result);
  xs += result[0];
  if(std::abs(xs-xstest) > EP*xs){
      std::cerr << "Total XS deviates substantially from ref value: "
                << xs << " vs " << xstest << "\n";
      return EXIT_FAILURE;
  }
  return EXIT_SUCCESS;
}

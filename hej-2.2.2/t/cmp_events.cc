/**
 *  \authors   The HEJ collaboration (see AUTHORS for details)
 *  \date      2019-2020
 *  \copyright GPLv2 or later
 */
#include "HEJ/Event.hh"
#include "HEJ/EWConstants.hh"
#include "HEJ/stream.hh"
#include "HEJ/utility.hh"

#include "LHEF/LHEF.h"

namespace {
  const HEJ::ParticleProperties Wprop{80.385, 2.085};
  const HEJ::ParticleProperties Zprop{91.187, 2.495};
  const HEJ::ParticleProperties Hprop{125,    0.004165};
  constexpr double vev = 246.2196508;
  const HEJ::EWConstants ew_parameters{vev, Wprop, Zprop, Hprop};
}

int main(int argn, char** argv) {
  if(argn != 3){
    std::cerr << "Usage: cmp_events eventfile eventfile_no_boson ";
    return EXIT_FAILURE;
  }
  HEJ::istream in{argv[1]};
  LHEF::Reader reader{in};
  HEJ::istream in_no_boson{argv[2]};
  LHEF::Reader reader_no_boson{in_no_boson};

  while(reader.readEvent()) {
    if(! reader_no_boson.readEvent()) {
      std::cerr << "wrong number of events in " << argv[2] << '\n';
      return EXIT_FAILURE;
    }
    const auto is_AWZH = [](HEJ::Particle const & p) {
      return is_AWZH_boson(p);
    };
    const HEJ::Event::EventData event_data{reader.hepeup};
    const auto boson = std::find_if(
        begin(event_data.outgoing), end(event_data.outgoing), is_AWZH
    );
    if(boson == end(event_data.outgoing)) {
      std::cerr << "no boson in event\n";
      return EXIT_FAILURE;
    }
    HEJ::Event::EventData data_no_boson{reader_no_boson.hepeup};
    data_no_boson.reconstruct_intermediate(ew_parameters);
    const auto new_boson = std::find_if(
        begin(event_data.outgoing), end(event_data.outgoing), is_AWZH
    );
    if(new_boson == end(data_no_boson.outgoing)) {
      std::cerr << "failed to find reconstructed boson\n";
      return EXIT_FAILURE;
    }

    if(new_boson->type != boson->type) {
      std::cerr << "reconstructed wrong boson type\n";
      return EXIT_FAILURE;
    }

    if(!HEJ::nearby(new_boson->p, boson->p)) {
      std::cerr << "reconstructed boson momentum is off\n";
      return EXIT_FAILURE;
    }
  }
  return EXIT_SUCCESS;
}

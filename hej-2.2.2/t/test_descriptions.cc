/**
 *  \authors   The HEJ collaboration (see AUTHORS for details)
 *  \date      2019-2020
 *  \copyright GPLv2 or later
 */
#include "hej_test.hh"

#include <cstdlib>
#include <iostream>
#include <utility>

#include "HEJ/Event.hh"
#include "HEJ/Parameters.hh"
#include "HEJ/PDG_codes.hh"
#include "HEJ/ScaleFunction.hh"

#include "fastjet/JetDefinition.hh"
#include "fastjet/PseudoJet.hh"

int main() {
  constexpr double mu = 125.;
  HEJ::ScaleFunction fun{"125", HEJ::FixedScale{mu}};
  ASSERT(fun.name() == "125");

  HEJ::ScaleGenerator scale_gen{
    {std::move(fun)}, {0.5, 1, 2.}, 2.1
  };
  HEJ::Event::EventData tmp;
  tmp.outgoing.push_back(
      {HEJ::ParticleID::gluon, fastjet::PtYPhiM(50., -1., 0.3, 0.), {}}
  );
  tmp.outgoing.push_back(
      {HEJ::ParticleID::gluon, fastjet::PtYPhiM(30., 1., -0.3, 0.), {}}
  );
  HEJ::Event ev{
    tmp.cluster(
      fastjet::JetDefinition{fastjet::kt_algorithm, 0.4}, 20.
    )
  };

  auto rescaled = scale_gen(std::move(ev));
  ASSERT(rescaled.central().description->scale_name == "125");
  for(auto const & var: rescaled.variations()) {
    ASSERT(var.description->scale_name == "125");
  }
  ASSERT(rescaled.central().description->mur_factor == 1.);
  ASSERT(rescaled.central().description->muf_factor == 1.);

  ASSERT(rescaled.variations(0).description->mur_factor == 1.);
  ASSERT(rescaled.variations(0).description->muf_factor == 1.);

  ASSERT(rescaled.variations(1).description->mur_factor == 0.5);
  ASSERT(rescaled.variations(1).description->muf_factor == 0.5);

  ASSERT(rescaled.variations(2).description->mur_factor == 0.5);
  ASSERT(rescaled.variations(2).description->muf_factor == 1.);

  ASSERT(rescaled.variations(3).description->mur_factor == 1.);
  ASSERT(rescaled.variations(3).description->muf_factor == 0.5);

  ASSERT(rescaled.variations(4).description->mur_factor == 1.);
  ASSERT(rescaled.variations(4).description->muf_factor == 2.);

  ASSERT(rescaled.variations(5).description->mur_factor == 2.);
  ASSERT(rescaled.variations(5).description->muf_factor == 1.);

  ASSERT(rescaled.variations(6).description->mur_factor == 2.);
  ASSERT(rescaled.variations(6).description->muf_factor == 2.);
  return EXIT_SUCCESS;
}

# Script for running HEJ with FIFO

set -eu

FIFO=input.lhe.gz

# Copy config file and change output file name
cp analysis_config_simple.yml config_rivet.yml
sed -i "s/plugin:.*/  rivet: MC_XS\n      output: analysis_rivet/g" config_rivet.yml

# run fifo
mkfifo $FIFO
cat $1 >> $FIFO &
$2 config_rivet.yml $FIFO
rm $FIFO
mv analysis_rivet.yoda analysis_fifo.yoda

# run w/o fifo
$2 config_rivet.yml $1
mv analysis_rivet.yoda analysis_nofifo.yoda

# make sure fails
#sed -i "s/YODA/GROGU/g" analysis_fifo.yoda

#compare
diff analysis_fifo.yoda analysis_nofifo.yoda

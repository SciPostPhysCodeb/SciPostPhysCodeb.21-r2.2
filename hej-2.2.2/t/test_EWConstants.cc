/**
 *  \authors   The HEJ collaboration (see AUTHORS for details)
 *  \date      2020
 *  \copyright GPLv2 or later
 */
#include "hej_test.hh"

#include "HEJ/EWConstants.hh"

namespace {
  constexpr double VEV = 38.;
  constexpr HEJ::ParticleProperties WPROP{VEV, 11.};
  constexpr HEJ::ParticleProperties ZPROP{VEV*2., 13.};
  constexpr HEJ::ParticleProperties HPROP{19.,    17.};
  constexpr double AWEAK = 2.*WPROP.mass/VEV*WPROP.mass/VEV;
  constexpr double COS = WPROP.mass/ZPROP.mass;
  inline bool operator==(
      HEJ::ParticleProperties const & p1, HEJ::ParticleProperties const & p2
  ){
    return p1.mass==p2.mass && p1.width == p2.width;
  }
  inline bool operator==(
      HEJ::EWConstants const & p1, HEJ::EWConstants const & p2
  ){
    return p1.vev()==p2.vev()
          && p1.Wprop() == p2.Wprop()
          && p1.Zprop() == p2.Zprop()
          && p1.Hprop() == p2.Hprop();
  }
}

int main() {
  constexpr HEJ::EWConstants EWCONST{VEV, WPROP, ZPROP, HPROP};
  // check that everything is copied correctly
  ASSERT(EWCONST.vev() == VEV);
  ASSERT(EWCONST.prop(HEJ::ParticleID::Wp)==EWCONST.Wprop());
  ASSERT(EWCONST.prop(HEJ::ParticleID::Wm)==EWCONST.Wprop());
  ASSERT(EWCONST.prop(HEJ::ParticleID::Z) ==EWCONST.Zprop());
  ASSERT(EWCONST.prop(HEJ::ParticleID::h) ==EWCONST.Hprop());
  ASSERT(EWCONST.alpha_w() == AWEAK);
  ASSERT(EWCONST.cos_tw() == COS);
  ASSERT(EWCONST.cos2_tw()+EWCONST.sin2_tw() == 1.);

  // check that default is not set
  HEJ::EWConstants ew2;
  ASSERT_THROW(ew2.vev(), std::invalid_argument);
  ew2.set_vevWZH(EWCONST.vev(), EWCONST.Wprop(), EWCONST.Zprop(), EWCONST.Hprop());
  ASSERT(ew2 == EWCONST);

  return EXIT_SUCCESS;
}

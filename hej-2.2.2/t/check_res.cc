/**
 *  \authors   The HEJ collaboration (see AUTHORS for details)
 *  \date      2019-2020
 *  \copyright GPLv2 or later
 */
#include "hej_test.hh"

#include <algorithm>
#include <cmath>
#include <cstdlib>
#include <iostream>
#include <iterator>
#include <memory>
#include <string>
#include <utility>

#include "HEJ/Config.hh"
#include "HEJ/CrossSectionAccumulator.hh"
#include "HEJ/Event.hh"
#include "HEJ/event_types.hh"
#include "HEJ/EventReweighter.hh"
#include "HEJ/EWConstants.hh"
#include "HEJ/Fraction.hh"
#include "HEJ/HiggsCouplingSettings.hh"
#include "HEJ/make_RNG.hh"
#include "HEJ/Mixmax.hh"
#include "HEJ/Parameters.hh"
#include "HEJ/ScaleFunction.hh"
#include "HEJ/EventReader.hh"
#include "HEJ/YAMLreader.hh"

#include "fastjet/JetDefinition.hh"

#include "LHEF/LHEF.h"

namespace HEJ { struct RNG; }

namespace {
  using EventTreatment = HEJ::EventTreatment;
  using namespace HEJ::event_type;
  HEJ::EventTreatMap TREAT{
    {non_resummable, EventTreatment::discard},
    {unof, EventTreatment::discard},
    {unob, EventTreatment::discard},
    {qqbar_exb, EventTreatment::discard},
    {qqbar_exf, EventTreatment::discard},
    {qqbar_mid, EventTreatment::discard},
    {FKL, EventTreatment::reweight}
  };

  bool correct_colour(HEJ::Event const & ev){
    if(!HEJ::event_type::is_resummable(ev.type()))
      return true;
    return ev.is_leading_colour();
  }
} // namespace

int main(int argn, char** argv) {

  if(argn == 6 && std::string(argv[5]) == "unof"){
    --argn;
    TREAT[unof] = EventTreatment::reweight;
    TREAT[unob] = EventTreatment::discard;
    TREAT[FKL] = EventTreatment::discard;
  }
  else if(argn == 6 && std::string(argv[5]) == "unob"){
    --argn;
    TREAT[unof] = EventTreatment::discard;
    TREAT[unob] = EventTreatment::reweight;
    TREAT[FKL] = EventTreatment::discard;
  }
  else if(argn == 6 && std::string(argv[5]) == "splitf"){
    --argn;
    TREAT[qqbar_exb] = EventTreatment::discard;
    TREAT[qqbar_exf] = EventTreatment::reweight;
    TREAT[FKL] = EventTreatment::discard;
  }
  else if(argn == 6 && std::string(argv[5]) == "splitb"){
    --argn;
    TREAT[qqbar_exb] = EventTreatment::reweight;
    TREAT[qqbar_exf] = EventTreatment::discard;
    TREAT[FKL] = EventTreatment::discard;
  }
  else if(argn == 6 && std::string(argv[5]) == "qqbar_mid"){
    --argn;
    TREAT[qqbar_mid] = EventTreatment::reweight;
    TREAT[FKL] = EventTreatment::discard;
  }
  if(argn != 5){
    std::cerr << "Usage: check_res yaml eventfile xsection tolerance [uno] \n";
    return EXIT_FAILURE;
  }

  HEJ::Config config = HEJ::load_config(argv[1]);
  auto reader = HEJ::make_reader(argv[2]);
  const double xsec_ref = std::stod(argv[3]);
  const double tolerance = std::stod(argv[4]);

  reader->read_event();

  HEJ::EventReweighterConfig conf = HEJ::to_EventReweighterConfig(config);
  conf.treat = TREAT;
  HEJ::MatrixElementConfig ME_conf = HEJ::to_MatrixElementConfig(config);
  HEJ::PhaseSpacePointConfig psp_conf = HEJ::to_PhaseSpacePointConfig(config);
  const fastjet::JetDefinition BORN_JET_DEF = config.fixed_order_jets.def;
  const double BORN_JETPTMIN = config.fixed_order_jets.min_pt;
  const int NUM_TRIES = config.trials;
  HEJ::ScaleGenerator scale_gen{
    config.scales.base,
    config.scales.factors,
    config.scales.max_ratio
  };
  std::shared_ptr<HEJ::RNG> ran{
    HEJ::make_RNG(config.rng.name,config.rng.seed)
  };


  HEJ::EventReweighter hej{reader->heprup(), std::move(scale_gen), conf, ran};

  HEJ::CrossSectionAccumulator xs;
  do{
    auto ev_data = HEJ::Event::EventData{reader->hepeup()};
    shuffle_particles(ev_data);
    ev_data.reconstruct_intermediate(ME_conf.ew_parameters);
    HEJ::Event ev{
      ev_data.cluster(
        BORN_JET_DEF, BORN_JETPTMIN
      )
    };
    auto resummed_events = hej.reweight(ev, NUM_TRIES);
    for(auto const & res_ev: resummed_events) {
      ASSERT(correct_colour(res_ev));
      ASSERT(std::isfinite(res_ev.central().weight));
      // we fill the xs uncorrelated since we only want to test the uncertainty
      // of the resummation
      xs.fill(res_ev);
    }
  } while(reader->read_event());
  xs.scale(reader->scalefactor());
  const double xsec = xs.total().value;
  const double xsec_err = std::sqrt(xs.total().error);
  const double significance =
    std::abs(xsec - xsec_ref) / std::sqrt( xsec_err*xsec_err + tolerance*tolerance );
  std::cout << xsec_ref << " +/- " << tolerance << " ~ "
    << xsec << " +- " << xsec_err << " => " << significance << " sigma\n";

  if(significance > 3.){
    std::cerr << "Cross section is off by over 3 sigma!\n";
    return EXIT_FAILURE;
  }
  return EXIT_SUCCESS;
}

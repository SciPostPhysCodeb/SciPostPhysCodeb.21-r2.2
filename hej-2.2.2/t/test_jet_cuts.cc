/**
 *  \authors   The HEJ collaboration (see AUTHORS for details)
 *  \date      2019-2020
 *  \copyright GPLv2 or later
 */
#include "hej_test.hh"

#include <array>
#include <cstdlib>
#include <iostream>
#include <string>
#include <vector>

#include "HEJ/Event.hh"
#include "HEJ/event_types.hh"

#include "fastjet/JetDefinition.hh"

namespace {
  const fastjet::JetDefinition JET_DEF{fastjet::JetAlgorithm::antikt_algorithm, 0.4};
  const double MIN_JET_PT{30.};
  const double SOFT_PT_REGULATOR{0.4};

  bool correct( bool const expected,
    std::array<std::string,2> const & in, std::vector<std::string> const & out,
      int const overwrite_boson
  ){
    HEJ::Event ev{ parse_configuration(
                      in,out,overwrite_boson ).cluster(JET_DEF, MIN_JET_PT)};
    ASSERT(is_resummable(ev.type())); // only test jet configurations
    if(ev.valid_hej_state(SOFT_PT_REGULATOR) != expected){
      std::cerr << "Expected " << (expected?"valid":"invalid")
                << " but found "
                << (ev.valid_hej_state(SOFT_PT_REGULATOR)?"valid":"invalid")
                << " jets (" << overwrite_boson << ")\n" << ev;
      auto jet_idx{ ev.particle_jet_indices() };
      std::cout << "Particle Jet indices: ";
      for(int const i: jet_idx)
        std::cout << i << " ";
      std::cout << std::endl;
      return false;
    }
    return true;
  }

  // valid jet configurations
  bool valid_jets(){
    // FKL
    // extremal parton inside jet
    for(int i=-3; i>-13;--i){
      std::array<std::string,2> base_in{"g","g"};
      std::vector<std::string> base_out(7, "g");
      if(!correct(true, base_in, base_out, i))
        return false;
    }
    // replace one of the extremal with a boson
    if( !( correct(true,{"g","d"},{"g","g","g","g","g","d","h"}, -13)
        && correct(true,{"d","d"},{"d","g","g","g","g","d","h"}, -13)
        && correct(true,{"2","2"},{"1","g","g","g","g","2","Wp"},-14)
    ))
      return false;

    // uno
    if( !( correct(true,{"g","3"},{"h","g","g","g","g","3","g"},  -1)
        && correct(true,{"3","2"},{"Wp","g","3","g","g","g","1"}, -1)
        && correct(true,{"1","2"},{"Wm","g","2","g","g","g","2"}, -1)
        && correct(true,{"1","3"},{"1","g","g","g","g","3","g"},  -3)
        && correct(true,{"-1","3"},{"-1","g","g","g","g","3","g"},-5)
        && correct(true,{"3","-2"},{"g","3","g","g","g","g","-2"},-5)
        && correct(true,{"-3","3"},{"g","-3","g","g","g","g","3"},-6)
        && correct(true,{"-4","3"},{"-4","g","g","g","g","3","g"},-7)
        && correct(true,{"3","-5"},{"g","3","g","g","g","g","-5"},-7)
        && correct(true,{"g","3"},{"g","g","g","g","g","3","g"},  -8)
        && correct(true,{"3","g"},{"g","3","g","g","g","g","g"},  -8)
        && correct(true,{"2","3"},{"g","1","g","Wp","g","g","3"}, -9)
        && correct(true,{"2","3"},{"1","g","g","g","3","Wp","g"}, -9)
        && correct(true,{"2","3"},{"1","g","Wp","g","g","3","g"}, -10)
        && correct(true,{"3","g"},{"g","3","g","g","g","g","g"},  -11)
        && correct(true,{"1","3"},{"1","g","g","g","g","3","g"},  -12)
        && correct(true,{"3","2"},{"g","3","g","g","g","g","2"},  -12)
        && correct(true,{"3","g"},{"g","3","g","g","g","g","h"},  -13)
        && correct(true,{"2","3"},{"1","g","g","g","3","g","Wp"}, -13)
        && correct(true,{"2","3"},{"g","1","g","g","g","3","Wp"}, -14)
    ))
      return false;

    // extremal qqbar
    if( !( correct(true,{"2","g"},{"1","Wp","g","g","g","1","-1"}, -3)
        && correct(true,{"2","g"},{"1","Wp","g","g","g","1","-1"}, -5)
        && correct(true,{"g","2"},{"1","-1","g","Wp","g","g","1"}, -5)
        && correct(true,{"2","g"},{"1","g","g","g","Wp","1","-1"}, -7)
        && correct(true,{"g","2"},{"1","-1","g","g","g","Wp","1"}, -7)
        && correct(true,{"2","g"},{"1","Wp","g","g","g","1","-1"}, -8)
        && correct(true,{"g","2"},{"1","-1","Wp","g","g","g","1"}, -8)
        && correct(true,{"g","2"},{"1","-1","g","Wp","g","g","1"}, -9)
        && correct(true,{"g","3"},{"-2","1","g","Wp","g","g","3"}, -9)
        && correct(true,{"2","g"},{"1","g","g","g","3","Wp","-3"}, -9)
        && correct(true,{"2","g"},{"1","g","Wp","g","g","-3","3"}, -10)
        && correct(true,{"2","g"},{"1","g","g","g","Wp","1","-1"}, -12)
        && correct(true,{"g","2"},{"1","-1","g","Wp","g","g","1"}, -12)
        && correct(true,{"2","g"},{"1","g","g","g","-3","3","Wp"}, -13)
        && correct(true,{"g","g"},{"-2","1","g","g","g","g","Wp"}, -14)
    // pure jets
        && correct(true,{"-3","g"},{"-3","g","g","g","g","-3","3"}, -3)
        && correct(true,{"-3","g"},{"-3","g","g","g","g","-3","3"}, -5)
        && correct(true,{"g","-3"},{"-3","3","g","g","g","g","-3"}, -5)
        && correct(true,{"g","-3"},{"-3","3","g","g","g","g","-3"}, -6)
        && correct(true,{"-3","g"},{"-3","g","g","g","g","-3","3"}, -7)
        && correct(true,{"g","-3"},{"-3","3","g","g","g","g","-3"}, -7)
        && correct(true,{"-3","g"},{"-3","g","g","g","g","-3","3"}, -8)
        && correct(true,{"g","-3"},{"-3","3","g","g","g","g","-3"}, -8)
        && correct(true,{"g","-3"},{"-3","3","g","g","g","g","-3"}, -11)
        && correct(true,{"-3","g"},{"-3","g","g","g","g","-3","3"}, -12)
        && correct(true,{"g","-3"},{"-3","3","g","g","g","g","-3"}, -12)
    ))
      return false;

    // central qqbar
    if( !( correct(true,{"1","g"},{"2","g","-2","Wm","2","g","g"}, -3)
        && correct(true,{"1","g"},{"2","g","g","-2","2","Wm","g"}, -4)
        && correct(true,{"1","g"},{"2","g","g","Wm","-2","2","g"}, -5)
        && correct(true,{"1","g"},{"2","g","g","Wm","-2","2","g"}, -8)
        && correct(true,{"1","g"},{"2","Wm","g","-2","2","g","g"}, -9)
        && correct(true,{"1","g"},{"2","Wm","-2","2","g","g","g"}, -9)
        && correct(true,{"1","g"},{"2","g","g","-2","2","Wm","g"}, -10)
        && correct(true,{"1","g"},{"2","g","Wm","2","-2","g","g"}, -11)
        && correct(true,{"1","g"},{"2","g","g","-2","2","g","Wm"}, -12)
        && correct(true,{"1","g"},{"2","2","-2","g","g","g","Wm"}, -13)
        && correct(true,{"1","g"},{"2","2","-2","g","g","g","Wm"}, -14)
    // pure jets
        && correct(true,{"1","-1"},{"1","g","g","-1","1","g","-1"}, -4)
        && correct(true,{"1","-1"},{"1","g","g","-1","1","g","-1"}, -10)
    ))
      return false;

    return true;
  }

  // invalid jet configurations
  bool invalid_jets(){

    // processes failing jet cuts
    // FKL
    if( !( true
        && correct(false,{"1","g"}, {"g","1","g","g","g","g","g"},   -1)
        // FKL not in own jet
        && correct(false,{"1","1"}, {"1","g","g","g","g","g","1"},   -1)
        && correct(false,{"g","g"}, {"g","g","g","g","g","g","g"},   -2)
        && correct(false,{"1","-3"},{"1","g","g","g","g","-3","g"},  -1)
        && correct(false,{"2","g"}, {"g","2","g","g","g","g","g"},   -2)
        // FKL below pt
        && correct(false,{"1","1"}, {"1","g","g","g","g","g","1"},  -13)
        && correct(false,{"g","3"},{"h","g","g","g","g","3","g"},  -13)
        && correct(false,{"g","1"},{"Wm","g","g","g","g","2","g"}, -13)
        && correct(false,{"3","1"},{"Wm","g","3","g","g","g","2"}, -13)
        // uno in same jet as FKL
        && correct(false,{"-1","1"},{"-1","g","g","g","g","1","g"}, -9)
        && correct(false,{"3","3"}, {"g","3","g","g","g","g","3"},  -9)
        && correct(false,{"-1","1"},{"g","-1","g","g","g","g","1"}, -10)
        && correct(false,{"-1","1"},{"-1","g","g","g","g","1","g"}, -13)
        && correct(false,{"-1","1"},{"g","-1","g","g","g","g","1"}, -13)
        // FKL not in jet & uno other side
        && correct(false,{"2","3"},{"Wp","1","g","g","g","3","g"}, -15)
    ))
      return false;

    // uno
    if( !( true
        // uno backward not in jet
        && correct(false,{"1","2"}, {"g","1","g","g","g","g","2"},  -1)
        // uno forward not in jet
        && correct(false,{"3","3"}, {"3","g","g","g","g","3","g"},   -2)
        // uno backward in same jet
        && correct(false,{"1","g"}, {"g","1","g","g","g","g","g"},   -3)
        // uno forward in same jet
        && correct(false,{"3","2"}, {"3","g","g","g","g","2","g"},   -4)
        // uno backward below pt
        && correct(false,{"3","g"}, {"g","3","g","g","g","g","g"},   -4)
        // uno forward below pt
        && correct(false,{"4","3"}, {"4","g","g","g","g","3","g"},  -10)
        && correct(false,{"2","3"}, {"1","g","g","g","3","g","Wp"}, -14)
        // uno forward not in jet
        && correct(false,{"5","3"}, {"5","g","g","g","g","3","g"},  -11)
    ))
      return false;

    // extremal qqbar
    if( !( true
        // qqbarf in same jet
        && correct(false,{"g","g"}, {"Wm","g","g","g","g","-1","2"},  -4)
        // qqbarb below pt
        && correct(false,{"g","2"},{"1","-1","g","Wp","g","g","1"},  -4)
        // central qqbar not in jet
        && correct(false,{"1","2"}, {"1","g","-2","2","Wp","g","1"},  -5)
        // central qqbar in same jet
        && correct(false,{"1","2"}, {"1","-2","2","g","Wp","g","1"},  -6)
        // central qqbar in same jet
        && correct(false,{"1","2"}, {"1","Wm","g","g","2","-1","2"},  -6)
        // qqbarf below pt
        && correct(false,{"2","g"},{"1","g","Wp","g","g","1","-1"}, -6)
        // central qqbar in same jet
        && correct(false,{"1","2"}, {"1","-1","1","g","g","Wp","1"},  -7)
        // central qqbar in same jet
        && correct(false,{"1","2"}, {"1","Wp","g","3","-3","g","1"},  -7)
        // central qqbar in same jet
        && correct(false,{"g","3"}, {"g","Wp","-2","1","g","g","3"},  -8)
        // central qqbar in same jet
        && correct(false,{"g","-2"},{"Wm","g","g","2","-1","g","-2"}, -8)
        // qqbarf below pt
        && correct(false,{"2","g"},{"1","g","g","g","Wp","1","-1"}, -10)
        // qqbarf outside jet
        && correct(false,{"2","g"},{"1","g","g","g","Wp","1","-1"}, -11)
        // extremal qqbar in same jet as FKL
        && correct(false,{"-1","g"},{"-1","g","Wm","g","g","2","-1"},-9)
        && correct(false,{"g","1"}, {"2","-1","g","g","g","Wm","1"}, -10)
        // extraml qqbar below pt
        && correct(false,{"-1","g"},{"-1","Wm","g","g","g","-1","2"},-13)
        && correct(false,{"g","g"}, {"g","g","g","g","-1","Wm","2"}, -14)
        && correct(false,{"g","g"}, {"-1","2","g","g","g","g","Wm"}, -15)
    // pure jets
        && correct(false,{"2","g"}, {"2","g","g","g","g","2","-2"},  -1)
        && correct(false,{"g","2"}, {"2","-2","g","g","g","g","2"},  -1)
        && correct(false,{"g","-2"},{"4","-4","g","g","g","g","-2"}, -2)
        // qqbar backward not in jet
        && correct(false,{"g","g"}, {"g","g","g","g","g","-2","2"},  -2)
        // qqbar backward in same jet
        && correct(false,{"g","2"}, {"-4","4","g","g","g","g","2"},  -3)
        && correct(false,{"-3","g"},{"-3","g","g","g","g","-3","3"}, -4)
        && correct(false,{"g","-3"},{"-3","3","g","g","g","g","-3"}, -4)
        && correct(false,{"-3","g"},{"-3","g","g","g","g","-3","3"}, -6)
        && correct(false,{"g","-3"},{"-3","3","g","g","g","g","-3"}, -9)
        && correct(false,{"-3","g"},{"-3","g","g","g","g","-3","3"}, -9)
        && correct(false,{"g","-3"},{"-3","3","g","g","g","g","-3"}, -10)
        && correct(false,{"-3","g"},{"-3","g","g","g","g","-3","3"}, -10)
        && correct(false,{"-3","g"},{"-3","g","g","g","g","-3","3"}, -11)
        && correct(false,{"-3","g"},{"-3","g","g","g","g","-3","3"}, -13)
        && correct(false,{"g","-3"},{"-3","3","g","g","g","g","-3"}, -13)
        && correct(false,{"-3","g"},{"-3","g","g","g","g","-3","3"}, -14)
        && correct(false,{"g","-3"},{"-3","3","g","g","g","g","-3"}, -14)
        && correct(false,{"-3","g"},{"-3","g","g","g","g","-3","3"}, -15)
        && correct(false,{"g","-3"},{"-3","3","g","g","g","g","-3"}, -15)
    ))
      return false;

    // central qqbar
    if( !( true
        && correct(false,{"1","g"}, {"2","Wm","-2","2","g","g","g"},  -3)
        && correct(false,{"1","g"}, {"2","g","Wm","-2","2","g","g"},  -3)
        && correct(false,{"1","g"}, {"2","-2","2","g","Wm","g","g"},  -4)
        && correct(false,{"-1","g"},{"-1","g","g","Wp","1","-2","g"}, -4)
        && correct(false,{"-1","g"},{"-1","1","-2","g","g","Wp","g"}, -5)
        && correct(false,{"-1","g"},{"-1","g","1","-2","g","Wp","g"}, -5)
        && correct(false,{"-1","g"},{"-1","g","g","1","-2","Wp","g"}, -5)
        && correct(false,{"1","g"}, {"2","g","g","Wm","2","-2","g"},  -6)
        && correct(false,{"1","g"}, {"2","g","Wm","-2","2","g","g"},  -6)
        && correct(false,{"g","4"}, {"g","g","-4","4","g","Wp","3"},  -6)
        && correct(false,{"1","g"}, {"2","g","Wm","g","-2","2","g"},  -7)
        && correct(false,{"g","4"}, {"g","g","-4","4","g","Wp","3"},  -7)
        && correct(false,{"g","4"}, {"g","Wp","g","-4","4","g","3"},  -7)
        && correct(false,{"1","g"}, {"2","-2","2","Wm","g","g","g"},  -8)
        && correct(false,{"g","4"}, {"g","-4","4","g","Wp","g","3"},  -8)
        && correct(false,{"g","g"}, {"g","g","g","g","-2","1","Wp"},  -9)
        && correct(false,{"1","g"}, {"2","-2","2","g","Wm","g","g"},  -9)
        && correct(false,{"1","g"}, {"2","g","-2","2","g","Wm","g"},  -9)
        && correct(false,{"1","g"}, {"2","g","g","Wm","-2","2","g"},  -10)
        && correct(false,{"g","g"}, {"g","1","-2","Wp","g","g","g"},  -10)
        && correct(false,{"g","1"}, {"g","g","Wp","g","-2","1","1"},  -11)
        && correct(false,{"1","g"}, {"2","g","Wm","g","-2","2","g"},  -11)
        && correct(false,{"1","g"}, {"2","Wm","g","-2","2","g","g"},  -12)
        && correct(false,{"3","2"}, {"3","g","-1","2","Wm","g","2"},  -12)
        && correct(false,{"3","-2"},{"3","-1","2","g","g","Wm","-2"}, -13)
        && correct(false,{"3","-2"},{"3","-1","2","g","g","Wm","-2"}, -14)
        && correct(false,{"3","-2"},{"3","g","g","-1","2","Wm","-2"}, -14)
        && correct(false,{"1","g"}, {"Wm","2","2","-2","g","g","g"},  -15)
        && correct(false,{"3","-2"},{"3","-1","2","g","g","-2","Wm"}, -15)
    // pure jets
        && correct(false,{"-1","g"},{"-1","1","-1","g","g","g","g"}, -1)
        && correct(false,{"4","-3"},{"4","g","g","1","-1","g","-3"}, -2)
        && correct(false,{"1","-1"},{"1","g","-1","1","g","g","-1"}, -3)
        && correct(false,{"1","-1"},{"1","g","g","-1","1","g","-1"}, -3)
        && correct(false,{"1","-1"},{"1","g","g","g","-1","1","-1"}, -3)
        && correct(false,{"1","-1"},{"1","g","g","g","-1","1","-1"}, -4)
        && correct(false,{"1","-1"},{"1","-1","1","g","g","g","-1"}, -4)
        && correct(false,{"1","-1"},{"1","-1","1","g","g","g","-1"}, -5)
        && correct(false,{"1","-1"},{"1","g","-1","1","g","g","-1"}, -5)
        && correct(false,{"1","-1"},{"1","g","g","-1","1","g","-1"}, -5)
        && correct(false,{"1","-1"},{"1","g","g","-1","1","g","-1"}, -6)
        && correct(false,{"1","-1"},{"1","g","-1","1","g","g","-1"}, -7)
        && correct(false,{"1","-1"},{"1","g","g","g","-1","1","-1"}, -7)
        && correct(false,{"1","-1"},{"1","-1","1","g","g","g","-1"}, -8)
        && correct(false,{"1","-1"},{"1","g","g","g","-1","1","-1"}, -8)
        && correct(false,{"1","-1"},{"1","-1","1","g","g","g","-1"}, -9)
        && correct(false,{"1","-1"},{"1","g","-1","1","g","g","-1"}, -9)
        && correct(false,{"1","-1"},{"1","g","g","g","-1","1","-1"}, -10)
        && correct(false,{"1","-1"},{"1","g","g","g","-1","1","-1"}, -11)
        && correct(false,{"1","-1"},{"1","g","g","-1","1","g","-1"}, -12)
        && correct(false,{"1","-1"},{"1","g","g","g","-1","1","-1"}, -12)
    ))
      return false;

    return true;
  }
} // namespace

int main() {
  if(!valid_jets()) return EXIT_FAILURE;
  if(!invalid_jets()) return EXIT_FAILURE;
  return EXIT_SUCCESS;
}

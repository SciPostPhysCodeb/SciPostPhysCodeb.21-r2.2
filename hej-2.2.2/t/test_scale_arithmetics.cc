/**
 *  \authors   The HEJ collaboration (see AUTHORS for details)
 *  \date      2019-2020
 *  \copyright GPLv2 or later
 */
#include "hej_test.hh"

#include <algorithm>
#include <iomanip>
#include <iostream>
#include <memory>
#include <cstdlib>
#include <utility>

#include "HEJ/Config.hh"
#include "HEJ/Event.hh"
#include "HEJ/EventReweighter.hh"
#include "HEJ/make_RNG.hh"
#include "HEJ/Parameters.hh"
#include "HEJ/RNG.hh"
#include "HEJ/stream.hh"
#include "HEJ/YAMLreader.hh"

#include "LHEF/LHEF.h"

namespace {
  constexpr double EP = 1e-13;

  void dump(HEJ::Event const & ev){
    {
      LHEF::Writer writer{std::cout};
      std::cout << std::setprecision(6);
      writer.hepeup = to_HEPEUP(ev, nullptr);
      writer.writeEvent();
    }
    std::cout << "Rapidity ordering:\n";
    for(auto const & part: ev.outgoing()){
      std::cout << std::setw(2) << part.type << ": "<<  std::setw(7) << part.rapidity() << std::endl;
    }
  }
}

int main(int argn, char** argv){
  if(argn != 3){
    std::cerr << "\n# Usage:\n."<< argv[0] <<" config.yml input_file.lhe\n\n";
    return EXIT_FAILURE;
  }
  HEJ::Config config = HEJ::load_config(argv[1]);
  config.scales = HEJ::to_ScaleConfig(
      YAML::Load("scales: [H_T, 1 * H_T, 2/2 * H_T, 2*H_T/2, H_T/2*2, H_T/2/2*4, H_T*H_T/H_T]")
  );
  config.treat[HEJ::event_type::unknown] = HEJ::EventTreatment::keep;

  HEJ::istream in{argv[2]};
  LHEF::Reader reader{in};

  std::shared_ptr<HEJ::RNG> ran{
    HEJ::make_RNG(config.rng.name, config.rng.seed)};

  HEJ::ScaleGenerator scale_gen{
    config.scales.base,
    config.scales.factors,
    config.scales.max_ratio
  };

  HEJ::EventReweighter resum{
    reader.heprup,
    std::move(scale_gen),
    to_EventReweighterConfig(config),
    ran
  };

  std::size_t i = 0;
  while(reader.readEvent()){
    ++i;

    HEJ::Event::EventData data{reader.hepeup};
    shuffle_particles(data);

    HEJ::Event event{
      data.cluster(
        config.resummation_jets.def,
        config.resummation_jets.min_pt
      )
    };

    auto resummed = resum.reweight(event, config.trials);
    for(auto && ev: resummed) {
      for(auto &&var: ev.variations()) {
        if(std::abs(var.muf - ev.central().muf) > EP) {
          std::cerr
            << std::setprecision(15)
            << "unequal scales: " << var.muf
            << " != " << ev.central().muf << '\n'
            << "in resummed event:\n";
          dump(ev);
          std::cerr << "\noriginal event:\n";
          dump(event);
          return EXIT_FAILURE;
        }
      }
    }
  }
  return EXIT_SUCCESS;
}

/**
 *  \authors   The HEJ collaboration (see AUTHORS for details)
 *  \date      2019-2020
 *  \copyright GPLv2 or later
 */
#include "hej_test.hh"

#include <cstdlib>
#include <iostream>

#include "HEJ/stream.hh"
#include "HEJ/Config.hh"
#include "HEJ/event_types.hh"
#include "HEJ/Event.hh"
#include "HEJ/PhaseSpacePoint.hh"
#include "HEJ/Ranlux64.hh"

#include "LHEF/LHEF.h"

#include "fastjet/JetDefinition.hh"

namespace {
  constexpr int MAX_TRIALS = 100;
  const fastjet::JetDefinition JET_DEF{fastjet::kt_algorithm, 0.4};
  constexpr double MIN_JET_PT = 50;
}

int main(int argn, char** argv) {
  if(argn != 2){
    std::cerr << "Usage: " << argv[0] << " eventfile";
    return EXIT_FAILURE;
  }

  HEJ::istream in{argv[1]};
  LHEF::Reader reader{in};
  LHEF::Writer writer{std::cerr};
  writer.heprup = reader.heprup;

  HEJ::PhaseSpacePointConfig conf;
  conf.jet_param = HEJ::JetParameters{JET_DEF, MIN_JET_PT};

  HEJ::Ranlux64 ran{};

  while(reader.readEvent()){
    HEJ::Event::EventData ev_data{ reader.hepeup };
    shuffle_particles(ev_data);
    const HEJ::Event ev{ ev_data( JET_DEF, MIN_JET_PT ) };
    for(int trial = 0; trial < MAX_TRIALS; ++trial){
      HEJ::PhaseSpacePoint psp{ev, conf, ran};
      if(psp.weight() != 0){
        HEJ::Event::EventData tmp_ev;
        tmp_ev.incoming = psp.incoming();
        tmp_ev.outgoing = psp.outgoing();
        tmp_ev.parameters.central = {0,0,0};
        shuffle_particles(tmp_ev);
        HEJ::Event out_ev{ tmp_ev(JET_DEF, MIN_JET_PT) };
        if(out_ev.type() != ev.type()){
          using HEJ::event_type::name;
          std::cerr << "Wrong class of phase space point:\n"
            "original event of class " << name(ev.type()) << ":\n";
          writer.hepeup = reader.hepeup;
          writer.writeEvent();
          std::cerr << "Phase space point of class " << name(out_ev.type()) << ":\n";
          writer.hepeup = to_HEPEUP(out_ev, &writer.heprup);
          writer.writeEvent();
          return EXIT_FAILURE;
        }
      }
    }
  }
  return EXIT_SUCCESS;
}

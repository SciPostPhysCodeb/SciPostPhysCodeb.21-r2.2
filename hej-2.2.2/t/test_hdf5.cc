/**
 *  \authors   The HEJ collaboration (see AUTHORS for details)
 *  \date      2019-2020
 *  \copyright GPLv2 or later
 */
#include <cstdlib>
#include <iostream>
#include <utility>

#include "HEJ/EventReader.hh"

#include "LHEF/LHEF.h"

namespace {
  constexpr int NUM_PARTICLES = 13;
  constexpr int NUM_EVENTS = 51200;
  const std::pair<double, double> ENERGY{7000., 7000.};
  const std::pair<int, int> PDF_ID{13000, 13000};
}

int main(int argc, char** argv) {
  if(argc != 2) {
    std::cerr << "Usage: " << argv[0] << " file.hdf5\n";
    return EXIT_FAILURE;
  }
  auto reader = HEJ::make_reader(argv[1]);
  if(
      reader->heprup().EBMUP != ENERGY
      || reader->heprup().PDFSUP != PDF_ID
  ) {
    std::cerr << "Read incorrect init parameters\n";
    return EXIT_FAILURE;
  }
  int nevent = 0;
  while(reader->read_event()) {
    ++nevent;
    if(reader->hepeup().NUP != NUM_PARTICLES) {
      std::cerr << "Read wrong number of particles: "
                << reader->hepeup().NUP << " != " << NUM_PARTICLES
                << " in event " << nevent;
      return EXIT_FAILURE;
    }
    for(std::size_t i = 0; i < 2; ++i) {
      for(std::size_t j = 0; j < 2; ++j) {
        if(reader->hepeup().PUP[i][j] != 0) {
          std::cerr << "Non-vanishing transverse momentum in incoming particle"
                    " in event " << nevent;
          return EXIT_FAILURE;
        }
      }
    }
  }
  if(nevent != NUM_EVENTS) {
    std::cerr << "Wrong number of events "
      << nevent << " != " << NUM_EVENTS << "\n";
    return EXIT_FAILURE;
  }
  return EXIT_SUCCESS;
}

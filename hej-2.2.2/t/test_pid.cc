/**
 *  \authors   The HEJ collaboration (see AUTHORS for details)
 *  \date      2019-2020
 *  \copyright GPLv2 or later
 */
#include "hej_test.hh"

#include "HEJ/PDG_codes.hh"

namespace {
  void consistent_name(int const id){
    ASSERT(HEJ::to_ParticleID(name(HEJ::ParticleID(id))) == id);
    ASSERT(HEJ::to_ParticleID(std::to_string(id)) == id);
  }
}

int main() {
  using namespace HEJ;
  for(int i=1; i<7; ++i){
    // quarks
    consistent_name(i);
    ASSERT(is_quark(ParticleID(i)));
    consistent_name(-i);
    ASSERT(is_antiquark(ParticleID(-i)));
    // leptons
    consistent_name(i+10);
    ASSERT(is_lepton(ParticleID(i+10)));
    consistent_name(-i-10);
    ASSERT(is_antilepton(ParticleID(-i-10)));
    // negative tests
    ASSERT(!is_anylepton(ParticleID(i)));
    ASSERT(!is_AWZH_boson(ParticleID(-i)));
    ASSERT(!is_anyneutrino(ParticleID(i)));
    ASSERT(!is_anyquark(ParticleID(i+10)));
    ASSERT(!is_parton(ParticleID(-i-10)));
  }
  ASSERT(!is_parton(ParticleID(-6)));
  // bosons
  consistent_name(21);
  ASSERT(is_parton(ParticleID(21)));
  ASSERT(is_gluon(ParticleID(21)));
  for(int i=22; i<25; ++i){
    consistent_name(i);
    ASSERT(is_AWZ_boson(ParticleID(i)));
  }
  consistent_name(-24);
  ASSERT(is_AWZH_boson(ParticleID(-24)));
  consistent_name(25);
  ASSERT(is_AWZH_boson(ParticleID(25)));
  consistent_name(81);
  ASSERT(is_AWZ_boson(ParticleID(81)));
  // proton
  consistent_name(2212);
  consistent_name(-2212);
  // unspecified
  ASSERT(!is_parton(pid::unspecified));
  ASSERT(!is_anyquark(pid::unspecified));
  ASSERT(!is_AWZH_boson(pid::unspecified));
  ASSERT(!is_anylepton(pid::unspecified));
  ASSERT(!is_anyneutrino(pid::unspecified));
  ASSERT_THROW(HEJ::to_ParticleID("unspecified"), std::invalid_argument);
  ASSERT_THROW(HEJ::to_ParticleID("0"), std::invalid_argument);
  // not existing
  ASSERT_THROW(name(HEJ::ParticleID(39)), std::logic_error);
  ASSERT_THROW(HEJ::to_ParticleID("gluino"), std::invalid_argument);
  return EXIT_SUCCESS;
}

/**
 *  \authors   The HEJ collaboration (see AUTHORS for details)
 *  \date      2019-2023
 *  \copyright GPLv2 or later
 */
#include <cstdlib>
#include <iostream>
#include <fstream>

#include <boost/algorithm/string.hpp>

int main(int argn, char** argv) {
  constexpr double EP = 1e-3;

  if(argn != 3){
    std::cerr << "Usage: check_hepmc2 hepmc2_file XS_test_value\n";
    return EXIT_FAILURE;
  }
  const double xstest = std::stod(argv[2]);
  double xs = 0;

  //Doing it manually as that is basically what hepmc2 does anyway!
  //https://gitlab.cern.ch/hepmc/HepMC/-/blob/master/src/GenCrossSection.cc
  std::ifstream istr( argv[1] );

  std::string line;
  std::vector<std::string> vals;

  while (std::getline(istr, line)) {
    if (line.find("C ") == std::string::npos)
        continue;
    boost::algorithm::split(vals, line, boost::is_any_of(" "));
    xs = std::stod(vals[1]);
  }
  if(std::abs(xs-xstest) > EP*xs){
    std::cerr << "Total XS deviates substantially from ref value: "
              << xs << " vs " << xstest << "\n";
    return EXIT_FAILURE;
  }
  return EXIT_SUCCESS;
}

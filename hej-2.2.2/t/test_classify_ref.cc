/**
 *  \authors   The HEJ collaboration (see AUTHORS for details)
 *  \date      2019-2020
 *  \copyright GPLv2 or later
 */
#include "hej_test.hh"

#include <cstdlib>
#include <fstream>
#include <iostream>
#include <string>

#include <fastjet/JetDefinition.hh>

#include "HEJ/event_types.hh"
#include "HEJ/Event.hh"
#include "HEJ/EventReader.hh"
#include "HEJ/EWConstants.hh"

namespace {
  // this is deliberately chosen bigger than in the generation,
  // to cluster multiple partons in one jet
  constexpr double MIN_JET_PT = 40.;
  const fastjet::JetDefinition JET_DEF{fastjet::kt_algorithm, 0.6};
  constexpr std::size_t MAX_EVENTS = 4000;

  const HEJ::ParticleProperties Wprop{80.385, 2.085};
  const HEJ::ParticleProperties Zprop{91.187, 2.495};
  const HEJ::ParticleProperties Hprop{125,    0.004165};
  constexpr double vev = 246.2196508;
  const HEJ::EWConstants ew_parameters{vev, Wprop, Zprop, Hprop};

}

int main(int argn, char** argv) {
  if(argn != 3 && argn != 4){
    std::cerr << "Usage: " << argv[0]
      << " reference_classification input_file.lhe\n";
    return EXIT_FAILURE;
  }
  bool OUTPUT_MODE = false;
  if(argn == 4 && std::string("OUTPUT")==std::string(argv[3]))
      OUTPUT_MODE = true;

  std::fstream ref_file;
  if ( OUTPUT_MODE ) {
    std::cout << "_______________________USING OUTPUT MODE!_______________________" << std::endl;
    ref_file.open(argv[1], std::fstream::out);
  } else {
    ref_file.open(argv[1], std::fstream::in);
  }

  auto reader{ HEJ::make_reader(argv[2]) };

  std::size_t nevent{0};
  while(reader->read_event()){
    ++nevent;
    // We don't need to test forever, the first "few" are enough
    if(nevent>MAX_EVENTS) break;
    HEJ::Event::EventData data{ reader->hepeup() };
    shuffle_particles(data);
    data.reconstruct_intermediate(ew_parameters);
    const HEJ::Event ev{
      data.cluster(
        JET_DEF, MIN_JET_PT
      )
    };
    if ( OUTPUT_MODE ) {
      ref_file << ev.type() << std::endl;
    } else {
      std::string line;
      if(!std::getline(ref_file,line)) break;
      const auto expected{static_cast<HEJ::event_type::EventType>(std::stoi(line))};

      if(ev.type() != expected){
        std::cerr << "wrong classification of event " << nevent << ":\n" << ev
                  << "classified as " << name(ev.type())
                  << ", expected " << name(expected) << "\nJet indices: ";
        for(auto const & idx: ev.particle_jet_indices())
          std::cerr << idx << " ";
        std::cerr << "\n";
        return EXIT_FAILURE;
      }
    }
  }
  return EXIT_SUCCESS;
}

/**
 *  \authors   The HEJ collaboration (see AUTHORS for details)
 *  \date      2019-2020
 *  \copyright GPLv2 or later
 */
#include "hej_test.hh"

#include <cstdlib>
#include <memory>

#include "HEJ/Parameters.hh"

namespace {
  bool same_description(
    HEJ::EventParameters const & par1, HEJ::EventParameters const & par2
  ){
    if( par1.mur!=par2.mur || par1.muf!=par2.muf ) return false;
    auto const & des1 = par1.description;
    auto const & des2 = par2.description;
    if(bool(des1) != bool(des2)) return false; // only one set
    if(!des1) return true; // both not set
    return   (des1->mur_factor == des2->mur_factor)
          && (des1->muf_factor == des2->muf_factor)
          && (des1->scale_name == des2->scale_name);
  }

  void same_description(
    HEJ::Parameters<HEJ::EventParameters> const & par1,
    HEJ::Parameters<HEJ::EventParameters> const & par2
  ){
    ASSERT(same_description(par1.central, par2.central));
    ASSERT(par1.variations.size() == par2.variations.size());
    for(std::size_t i=0; i<par1.variations.size(); ++i)
      ASSERT( same_description(par1.variations[i], par2.variations[i]) );
  }
} // namespace

int main() {
  HEJ::Parameters<HEJ::EventParameters> ev_param;
  ev_param.central = HEJ::EventParameters{ 1,1,1.1,
    std::make_shared<HEJ::ParameterDescription>("a", 1.,1.) };
  ev_param.variations.emplace_back(
    HEJ::EventParameters{ 2,2,2.2,
      std::make_shared<HEJ::ParameterDescription>("b", 2.,2.)
    });
  ev_param.variations.emplace_back(
    HEJ::EventParameters{ 3,3,3.3,
      std::make_shared<HEJ::ParameterDescription>("c", 3.,3.)
    });

  HEJ::Weights const weights{4.4, {5.5, 6.6}};
  HEJ::Weights const wrong_wgt{7.7, {8.8}};
  double const factor = 9.9;

  // multiply
  HEJ::Parameters<HEJ::EventParameters> param = ev_param*weights;
  same_description(ev_param, param);
  ASSERT(param.central.weight == ev_param.central.weight*weights.central);
  for(std::size_t i=0; i<weights.variations.size(); ++i)
    ASSERT(param.variations[i].weight
      == ev_param.variations[i].weight*weights.variations[i]);

  param = ev_param*factor;
  same_description(ev_param, param);
  ASSERT(param.central.weight == ev_param.central.weight*factor);
  for(std::size_t i=0; i<weights.variations.size(); ++i)
    ASSERT(param.variations[i].weight
      == ev_param.variations[i].weight*factor);

  ASSERT_THROW(param = ev_param*wrong_wgt, std::invalid_argument);

  // divide
  param = ev_param/weights;
  same_description(ev_param, param);
  ASSERT(param.central.weight == ev_param.central.weight/weights.central);
  for(std::size_t i=0; i<weights.variations.size(); ++i)
    ASSERT(param.variations[i].weight
      == ev_param.variations[i].weight/weights.variations[i]);

  param = ev_param/factor;
  same_description(ev_param, param);
  ASSERT(param.central.weight == ev_param.central.weight/factor);
  for(std::size_t i=0; i<weights.variations.size(); ++i)
    ASSERT(param.variations[i].weight
      == ev_param.variations[i].weight/factor);

  ASSERT_THROW(param = ev_param/wrong_wgt, std::invalid_argument);

  // add
  param = ev_param+weights;
  same_description(ev_param, param);
  ASSERT(param.central.weight == ev_param.central.weight+weights.central);
  for(size_t i=0; i<weights.variations.size(); ++i)
    ASSERT(param.variations[i].weight
      == ev_param.variations[i].weight+weights.variations[i]);

  ASSERT_THROW(param = ev_param+wrong_wgt, std::invalid_argument);

  // subtract
  param = ev_param-weights;
  same_description(ev_param, param);
  ASSERT(param.central.weight == ev_param.central.weight-weights.central);
  for(size_t i=0; i<weights.variations.size(); ++i)
    ASSERT(param.variations[i].weight
      == ev_param.variations[i].weight-weights.variations[i]);

  ASSERT_THROW(param = ev_param-wrong_wgt, std::invalid_argument);

  return EXIT_SUCCESS;
}

/**
 *  \authors   The HEJ collaboration (see AUTHORS for details)
 *  \date      2019-2023
 *  \copyright GPLv2 or later
 */
#include <cstdlib>
#include <iostream>

#include "HepMC3/GenEvent.h"
#include "HepMC3/ReaderAscii.h"
#include "HepMC3/Print.h"

#include "HEJ/exceptions.hh"

int main(int argn, char** argv) {
  constexpr double EP = 1e-3;

  if(argn != 2 && argn != 3){
    std::cerr << "Usage: check_hepmc hepmc_file [XS test value]\n";
    return EXIT_FAILURE;
  }

  HepMC3::ReaderAscii input{argv[1]};
  if(input.failed()) throw std::runtime_error{"failed to open HepMC file"};
  HepMC3::GenEvent ev{};
  std::shared_ptr<HepMC3::GenCrossSection> cs;
  while(true){
    if ( !input.read_event(ev) || ev.event_number() == 0 ) break;
    if(input.failed()) throw std::runtime_error{"failed to read HepMC event"};
    cs = ev.attribute<HepMC3::GenCrossSection>("GenCrossSection");
  }
  const double xs = cs->xsec();

  if (argn == 3){
      const double xstest = std::stod(argv[2]);
      if(std::abs(xs-xstest) > EP*xs){
          std::cerr << "Total XS deviates substantially from ref value: "
          << xs << " vs " << xstest << "\n";
      return EXIT_FAILURE;
    }
  }

  return EXIT_SUCCESS;
}

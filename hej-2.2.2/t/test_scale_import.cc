/**
 *  \authors   The HEJ collaboration (see AUTHORS for details)
 *  \date      2019-2020
 *  \copyright GPLv2 or later
 */
#include <cstdlib>

#include "fastjet/JetDefinition.hh"
#include "fastjet/PseudoJet.hh"

#include "HEJ/Config.hh"
#include "HEJ/Event.hh"
#include "HEJ/exceptions.hh"
#include "HEJ/PDG_codes.hh"
#include "HEJ/YAMLreader.hh"

int main(int argc, char** argv) {
  constexpr double ep = 1e-7;
  if (argc != 2) {
    throw std::logic_error{"wrong number of args"};
  }
  const HEJ::Config config = HEJ::load_config(argv[1]);

  HEJ::Event::EventData tmp;
  tmp.outgoing.push_back(
      {HEJ::ParticleID::gluon, fastjet::PtYPhiM(50., -1., 0.3, 0.), {}}
  );
  tmp.outgoing.push_back(
      {HEJ::ParticleID::gluon, fastjet::PtYPhiM(30., 1., -0.3, 0.), {}}
  );
  HEJ::Event ev{
    tmp(fastjet::JetDefinition{fastjet::kt_algorithm, 0.4}, 20.)
  };

  const double softest_pt = config.scales.base[0](ev);
  if(std::abs(softest_pt-30.) > ep){
    throw std::logic_error{"wrong softest pt"};
  }
  return EXIT_SUCCESS;
}

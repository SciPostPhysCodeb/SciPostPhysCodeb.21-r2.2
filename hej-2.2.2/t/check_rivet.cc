/**
 *  \authors   The HEJ collaboration (see AUTHORS for details)
 *  \date      2019-2020
 *  \copyright GPLv2 or later
 */
/**
 * small test to see if we can rivet interface, and if rivet accepts our beams
 */
#include <array>
#include <cstdlib>
#include <string>
#include <vector>

#include "fastjet/JetDefinition.hh"

#include "HEJ/Event.hh"
#include "HEJ/exceptions.hh"
#include "HEJ/PDG_codes.hh"
#include "HEJ/RivetAnalysis.hh"

#include "LHEF/LHEF.h"

#include "yaml-cpp/yaml.h"

namespace {
  const fastjet::JetDefinition JET_DEF{fastjet::JetAlgorithm::antikt_algorithm, 0.4};
  const double MIN_JET_PT{30.};

  void test_analysis(
    std::vector<std::string> const & analysis,
    std::array<double,2> const & energy,
    std::array<HEJ::ParticleID,2> const & beam
  ){
    HEJ::Event::EventData ev;
    ev.outgoing.push_back({HEJ::ParticleID::gluon, {  -5,  -78,  -54,   95}, {}});
    ev.outgoing.push_back({HEJ::ParticleID::gluon, {  23,  -64,  -44,   81}, {}});
    ev.outgoing.push_back({HEJ::ParticleID::gluon, {  68,   87,  -24,  113}, {}});
    ev.outgoing.push_back({HEJ::ParticleID::gluon, { -11,   92,   -8,   93}, {}});
    ev.outgoing.push_back({HEJ::ParticleID::gluon, { -12,   99,   44,  109}, {}});
    ev.outgoing.push_back({HEJ::ParticleID::gluon, { -15,  -52,   36,   65}, {}});
    ev.outgoing.push_back({HEJ::ParticleID::gluon, { -48,  -84,   97,  137}, {}});
    ev.incoming[0] = {HEJ::ParticleID::gluon, {   0,    0, -323,  323}, {}};
    ev.incoming[1] = {HEJ::ParticleID::gluon, {   0,    0,  370,  370}, {}};
    auto const event{ ev.cluster(JET_DEF, MIN_JET_PT) };

    std::string yaml_setup{ "{output: rivet_test, rivet: [" };
    for(auto const & ana: analysis)
      yaml_setup+=ana+",";
    yaml_setup.pop_back();
    yaml_setup+="]}";
    auto ana_conf{ YAML::Load(yaml_setup) };

    LHEF::HEPRUP heprup;
    heprup.IDBMUP.first  = beam[0];
    heprup.IDBMUP.second = beam[1];
    heprup.EBMUP.first  = energy[0];
    heprup.EBMUP.second = energy[1];

    HEJ::RivetAnalysis rivet{ ana_conf, heprup };
    if(!rivet.pass_cuts(event, event))
      throw std::logic_error{"Rivet should not induce additional cuts!"};
    rivet.fill(event, event);
    // analyse twice since the first fill is special
    if(!rivet.pass_cuts(event, event))
      throw std::logic_error{"Rivet should not induce additional cuts!"};
    rivet.fill(event, event);
    rivet.finalise();
  }

} // namespace

int main(){
  using namespace HEJ;
  test_analysis({"MC_XS"},{6500,6500},{ParticleID::e_bar,ParticleID::e}); // MC_XS accepts everything
  test_analysis({"MC_XS","ATLAS_2016_I1419652"},{6500,6500},{ParticleID::p,ParticleID::p});
  test_analysis({"ATLAS_2014_I1319490"},{3500,3500},{ParticleID::p,ParticleID::p});
  return EXIT_SUCCESS;
}

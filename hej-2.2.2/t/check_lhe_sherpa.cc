/**
 *  \authors   The HEJ collaboration (see AUTHORS for details)
 *  \date      2019-2020
 *  \copyright GPLv2 or later
 */
#include "hej_test.hh"

#include <cstdlib>
#include <iostream>
#include <memory>
#include <string>

#include "HEJ/EventReader.hh"

#include "LHEF/LHEF.h"

namespace {
  constexpr double EP = 1e-5;
}

int main(int argn, char** argv) {
  if(argn != 3){
    std::cerr << "Usage: " << argv[0] << " lhe_file xs\n";
    return EXIT_FAILURE;
  }

  auto reader{ HEJ::make_reader(argv[1])};

  const double ref_xs = std::stod(argv[2]);

  std::cerr<<"IDWTUP : "<<reader->heprup().IDWTUP<<std::endl;
  if(std::abs(reader->heprup().IDWTUP) < 3||std::abs(reader->heprup().IDWTUP) >4){
    std::cerr << "Bang: This seems to be a PYTHIA file read with SHERPA interpreter!\n";
    return EXIT_FAILURE;
  }
  // version should be overwritten
  ASSERT(reader->heprup().version==LHEF::HEPRUP().version);


  double xs { 0. };
  std::size_t n_evts { 0 };
  while(reader->read_event()){
    ++n_evts;
    xs += reader->hepeup().weight();
    ASSERT(reader->hepeup().weight() == reader->hepeup().XWGTUP);
  }
  ASSERT(std::abs(reader->heprup().XSECUP.front()-ref_xs) < EP*ref_xs);
  // scale to account for num_trials of sherpa
  double scalefactor(reader->scalefactor());
  xs*=scalefactor;

  if(std::abs(xs-ref_xs) > EP*xs){
    std::cerr << "Cross sections deviate substantially!\n"
      <<"Found "<< xs <<" but expected "<< ref_xs <<" -> "<< xs/ref_xs <<"\n";
    return EXIT_FAILURE;
  }
  if(reader->number_events() && *(reader->number_events()) != n_evts){
    std::cerr << "Number of Event not correctly set for Sherpa LHE reader\n";
    return EXIT_FAILURE;
  }
  return EXIT_SUCCESS;
}

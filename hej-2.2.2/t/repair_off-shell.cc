/**
 *  \authors   The HEJ collaboration (see AUTHORS for details)
 *  \date      2022
 *  \copyright GPLv2 or later
 */
#include <cstddef>
#include <iostream>
#include <limits>

#include "hej_test.hh"

#include "HEJ/Event.hh"
#include "HEJ/Particle.hh"
#include "HEJ/EventReader.hh"

namespace {
  constexpr double TOLERANCE = 0.1;
  constexpr double EP = 1e-5;

  void test_repair(LHEF::HEPEUP const & hepeup, const double tolerance) {
    HEJ::Event::EventData ev{hepeup};
    ev.repair_momenta(tolerance);
    for(auto const & in: ev.incoming) {
      ASSERT(in.px() == 0. || std::abs(in.px()) > tolerance);
      ASSERT(in.py() == 0. || std::abs(in.py()) > tolerance);
      ASSERT(in.m() < EP || in.m() > tolerance);
    }
    for(auto const & out: ev.outgoing) {
      ASSERT(is_massive(out) || out.m() < EP || out.m() > tolerance);
    }
    for(auto const & decay: ev.decays) {
      for(auto const & out: decay.second) {
        ASSERT(is_massive(out) || out.m() < EP || out.m() > tolerance);
      }
    }
  }
}

int main(int argc, char** argv) {
  if(argc != 2) {
    std::cerr << "Usage: repair_off-shell EVENTS.LHE\n";
    return EXIT_FAILURE;
  }

  auto reader = HEJ::make_reader(argv[1]);
  while(reader->read_event()) {
    for(auto tolerance: {0., TOLERANCE, std::numeric_limits<double>::max()}) {
      test_repair(reader->hepeup(), tolerance);
    }
  }

}

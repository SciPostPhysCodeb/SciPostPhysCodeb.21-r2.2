/**
 *  \authors   The HEJ collaboration (see AUTHORS for details)
 *  \date      2019-2020
 *  \copyright GPLv2 or later
 */
#include "hej_test.hh"

#include <algorithm>
#include <array>
#include <cstdlib>
#include <iostream>
#include <memory>
#include <vector>

#include "HEJ/Event.hh"
#include "HEJ/EventReader.hh"
#include "HEJ/EventWriter.hh"
#include "HEJ/HDF5Reader.hh"
#include "HEJ/make_writer.hh"
#include "HEJ/output_formats.hh"
#include "HEJ/Parameters.hh"
#include "HEJ/Particle.hh"
#include "HEJ/utility.hh"

#include "fastjet/JetDefinition.hh"

namespace {
  const fastjet::JetDefinition JET_DEF{fastjet::JetAlgorithm::antikt_algorithm, 0.4};
  constexpr double MIN_JET_PT = 20.;
  constexpr std::size_t MAX_NEVENT = 15299;
}

int main(int argc, char** argv) {
  if(argc != 3) {
    std::cerr << "Usage: " << argv[0] << " in_file.hdf5 out_file.hdf5\n";
    return EXIT_FAILURE;
  }

  std::vector<HEJ::Event> events;

  auto reader = HEJ::make_reader(argv[1]);

  auto writer = HEJ::make_format_writer(
      HEJ::FileFormat::HDF5, argv[2], reader->heprup()
  );

  std::size_t nevent = 0;
  while(reader->read_event()) {
    ++nevent;
    const auto event = HEJ::Event::EventData{reader->hepeup()}.cluster(
      JET_DEF, MIN_JET_PT
    );
    events.emplace_back(event);
    writer->write(event);
    if(nevent>=MAX_NEVENT)
      break;
  }
  writer->finish();
  // throw on second call to finish
  ASSERT_THROW(writer->finish(), std::ios_base::failure);

  HEJ::HDF5Reader new_reader{argv[2]};
  std::size_t new_nevent = 0;
  for(auto const & event: events) {
    ++new_nevent;
    new_reader.read_event();
    const auto ev = HEJ::Event::EventData{new_reader.hepeup()}.cluster(
        JET_DEF, MIN_JET_PT
    );
    ASSERT_PROPERTY(ev, event, incoming().size());
    for(std::size_t i = 0; i < ev.incoming().size(); ++i) {
      ASSERT(HEJ::nearby(ev.incoming()[i].p, event.incoming()[i].p));
    }
    ASSERT_PROPERTY(ev, event, outgoing().size());
    for(std::size_t i = 0; i < ev.outgoing().size(); ++i) {
      ASSERT(HEJ::nearby(ev.outgoing()[i].p, event.outgoing()[i].p));
    }
    ASSERT_PROPERTY(ev, event, decays().size());
    ASSERT_PROPERTY(ev, event, type());
    ASSERT_PROPERTY(ev, event, central().weight);
  }
  ASSERT(new_nevent == nevent);

  return EXIT_SUCCESS;
}

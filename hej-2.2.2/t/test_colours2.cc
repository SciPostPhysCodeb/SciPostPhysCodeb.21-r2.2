/**
 *  \authors   The HEJ collaboration (see AUTHORS for details)
 *  \date      2020
 *  \copyright GPLv2 or later
 */
#include "hej_test.hh"

#include <array>
#include <bitset>
#include <cstdlib>
#include <iostream>
#include <string>
#include <utility>
#include <vector>

#include "HEJ/Constants.hh"
#include "HEJ/Event.hh"
#include "HEJ/exceptions.hh"
#include "HEJ/Mixmax.hh"
#include "HEJ/Particle.hh"

#include "fastjet/JetDefinition.hh"

namespace {
  const fastjet::JetDefinition JET_DEF{fastjet::JetAlgorithm::antikt_algorithm, 0.4};
  const double MIN_JET_PT{30.};

  HEJ::Particle & get_particle(
      HEJ::Event::EventData & event, char const name
  ){
    if(name == 'a')
      return event.incoming[0];
    if(name == 'b')
      return event.incoming[1];
    std::size_t idx = name-'0';
    ASSERT(idx<event.outgoing.size());
    return event.outgoing[idx];
  }

  struct colour_flow {
    std::array<std::string,2> incoming;
    std::vector<std::string> outgoing;
    std::string colour;

    HEJ::Event to_event(){
      using namespace HEJ;
      auto event = parse_configuration(incoming, outgoing);
      if(colour.empty()) // no colour -> only parse
        return event.cluster(JET_DEF,MIN_JET_PT);

      ASSERT(colour.front()=='a' && colour.back()=='a'); // only closed loops
      event.sort();

      // fill colours with dummy value
      for(auto & p: event.incoming)
        p.colour = Colour{-1, -1};
      for(auto & p: event.outgoing)
        p.colour = Colour{-1, -1};

      int current_colour = COLOUR_OFFSET;
      int backup_colour = current_colour;
      Particle* last_part = &event.incoming.front();
      // loop connections
      for(auto const & entry: colour){
        if(entry == '_'){ // '_' -> skip connection
          backup_colour = current_colour;
          current_colour = 0;
          continue;
        }
        auto & part = get_particle(event, entry);
        part.colour->first = last_part->colour->second = current_colour;

        current_colour = ++backup_colour;
        last_part = &part;
      }

      for(auto & in: event.incoming)
        std::swap(in.colour->first, in.colour->second);
      // reset untouched colours
      for(auto & out: event.outgoing)
        if(out.colour->first == -1 &&  out.colour->second ==-1)
          out.colour = {};

      shuffle_particles(event);
      return event.cluster(JET_DEF,MIN_JET_PT);
    }
  };

  const colour_flow FKL_GGG{{"g","g"},{"g","g","g"},{}};
  const colour_flow FKL_QGQ{{"1","2"},{"1","g","2"},{}};
  const colour_flow FKL_QBARGQBAR{{"-1","-2"},{"-1","g","-2"},{}};
  const colour_flow FKL_QHGQ{{"1","2"},{"1","h","g","2"},{}};

  const colour_flow UNO_GQGQ{{"1","2"},{"g","1","g","2"},{}};
  const colour_flow UNO_QGQG{{"1","2"},{"1","g","2","g"},{}};
  const colour_flow UNO_WGQQ{{"2","2"},{"W+","g","1","2"},{}};
  const colour_flow UNO_GWQQ{{"2","2"},{"g","W+","1","2"},{}};
  const colour_flow UNO_GQWQ{{"2","2"},{"g","1","W+","2"},{}};
  const colour_flow UNO_QWQG{{"2","2"},{"1","W+","2","g"},{}};
  const colour_flow UNO_QQWG{{"2","2"},{"1","2","W+","g"},{}};
  const colour_flow UNO_QQGW{{"2","2"},{"1","2","g","W+"},{}};

  const colour_flow QQBAR_QBARQGQ{{"g","2"},{"-1","1","g","2"},{}};
  const colour_flow QQBAR_QGQQBAR{{"1","g"},{"1","g","2","-2"},{}};
  const colour_flow QQBAR_QGQBARQ{{"1","g"},{"1","g","-2","2"},{}};
  const colour_flow QQBAR_WQBARQQ{{"g","2"},{"W+","-2","1","2"},{}};
  const colour_flow QQBAR_QBARWQQ{{"g","2"},{"-2","W+","1","2"},{}};
  const colour_flow QQBAR_QBARQWQ{{"g","2"},{"-2","1","W+","2"},{}};
  const colour_flow QQBAR_QWQQBAR{{"2","g"},{"1","W+","2","-2"},{}};
  const colour_flow QQBAR_QQWQBAR{{"2","g"},{"1","2","W+","-2"},{}};
  const colour_flow QQBAR_QQQBARW{{"2","g"},{"1","2","-2","W+"},{}};

  const colour_flow QQBAR_GQQBARQ{{"g","2"},{"g","3","-3","2"},{}};
  const colour_flow QQBAR_QQBARQG{{"1","g"},{"1","-3","3","g"},{}};
  const colour_flow QQBAR_QQBARQQBAR{{"1","-1"},{"1","-1","1","-1"},{}};
  const colour_flow QQBAR_QWQBARQQBAR{{"2","-1"},{"1","W+","-1","1","-1"},{}};
  const colour_flow QQBAR_QQBARWQQBAR{{"2","-1"},{"1","-1","W+","1","-1"},{}};
  const colour_flow QQBAR_QQBARQWQBAR{{"2","-1"},{"1","-1","1","W+","-1"},{}};
  HEJ::Mixmax RNG;

  void verify_colour(colour_flow configuration, std::string line,
      bool const expectation = true
  ){
    configuration.colour = std::move(line);
    auto const event = configuration.to_event();
    if(event.is_leading_colour() != expectation){
      std::cerr << "Expected "<< (expectation?"":"non-") <<"leading colour\n"
        << event
        << "\nwith connection: " << configuration.colour << "\n";
      throw std::logic_error("Colour verification failed");
    }
  }

  int find_colour(HEJ::Event const & evt, int const colour){
    if(evt.incoming()[0].colour->second==colour)
      return -1;
    if(evt.incoming()[1].colour->second==colour)
      return -2;
    for(std::size_t i=0;i<evt.outgoing().size();++i){
      HEJ::Particle const & out = evt.outgoing()[i];
      if(!out.colour)
        continue;
      if(out.colour->first == colour)
        return i;
    }
    return -3;
  }

  int find_anticolour(HEJ::Event const & evt, int const anticolour){
    if(evt.incoming()[0].colour->first==anticolour)
      return -1;
    if(evt.incoming()[1].colour->first==anticolour)
      return -2;
    for(std::size_t i=0;i<evt.outgoing().size();++i){
      HEJ::Particle const & out = evt.outgoing()[i];
      if(!out.colour)
        continue;
      if(out.colour->second == anticolour)
        return i;
    }
    return -3;
  }

  bool matching_colours(HEJ::Event const & evt1, HEJ::Event const & evt2){
    ASSERT(evt1.outgoing().size()==evt2.outgoing().size());
    for(std::size_t i=0; i<2;++i){
      std::optional<HEJ::Colour> col1 = evt1.incoming()[i].colour;
      std::optional<HEJ::Colour> col2 = evt2.incoming()[i].colour;
      ASSERT(col1);
      ASSERT(col2);
      int idx1 = find_colour(evt1, col1->first);
      int idx2 = find_colour(evt2, col2->first);
      if(idx1 == -3 || idx2 == -3)
        return false;
      if(idx1!=idx2){
        return false;
      }
      idx1 = find_anticolour(evt1, col1->second);
      idx2 = find_anticolour(evt2, col2->second);
      if(idx1 == -3 || idx2 == -3)
        return false;
      if(idx1!=idx2)
        return false;
    }
    for(std::size_t i=0; i<evt1.outgoing().size();++i){
      std::optional<HEJ::Colour> col1 = evt1.outgoing()[i].colour;
      std::optional<HEJ::Colour> col2 = evt2.outgoing()[i].colour;
      if(!col1){
        if(!col2) continue;
        return false;
      }
      if(!col2)
        return false;
      int idx1 = find_colour(evt1, col1->second);
      int idx2 = find_colour(evt2, col2->second);
      if(idx1 == -3 || idx2 == -3)
        return false;
      if(idx1!=idx2)
        return false;
      idx1 = find_anticolour(evt1, col1->first);
      idx2 = find_anticolour(evt2, col2->first);
      if(idx1 == -3 || idx2 == -3)
        return false;
      if(idx1!=idx2)
        return false;
    }
    return true;
  }

  void all_colours_possible(
      colour_flow momenta, std::vector<std::string> allowed
  ){
    std::vector<HEJ::Event> possible;
    for(auto const & line: allowed){
      momenta.colour = line;
      possible.push_back(momenta.to_event());
      if(!possible.back().is_leading_colour()){
        std::cerr << "Expected leading colour\n"
          << possible.back()
          << "\nwith connection: " << momenta.colour << "\n";
        throw std::logic_error("Colour verification failed");
      }
    }
    momenta.colour = "";

    ASSERT(possible.size()<16); // sooo magic
    std::bitset<16> missing = (1<<(possible.size()))-1;
    std::size_t max_tries = possible.size()*10;
    // brute force generation of specific colour flow
    while(missing != 0 && max_tries>0){
      --max_tries;
      HEJ::Event test_evt = momenta.to_event();
      test_evt.generate_colours(RNG);
      std::size_t i=0;
      for(; i<possible.size();++i){
        if(matching_colours(test_evt, possible[i])){
          missing[i]=false;
          break;
        }
      }
      if(i==possible.size()){
        std::cerr << "Could not find allowed connection for\n"
          << test_evt << std::endl;
        throw std::logic_error("Unknown colour flow");
      }

    }
    if(max_tries==0){
      std::cerr << "Not all connections found missing:" << std::endl;
      for(std::size_t i=0; i<16; ++i){
        if(missing[i])
          std::cerr << allowed[i] << "\n" <<  possible[i] << std::endl;
      }
      throw std::logic_error("Missing colour flow");
    }
  }
} // namespace

int main() {
  // FKL
  all_colours_possible(FKL_GGG,   {"a012ba","a01b2a","a02b1a","a0b21a",
                                   "a12b0a","a1b20a","a2b10a","ab210a"});
  all_colours_possible(FKL_QGQ,   {"a12_b0_a", "a2_b10_a"});
  all_colours_possible(FKL_QBARGQBAR, {"a_01b_2a", "a_0b_21a"});
  all_colours_possible(FKL_QHGQ,  {"a23_b0_a", "a3_b20_a"});
  // uno
  all_colours_possible(UNO_GQGQ, {"a023_b1_a","a03_b21_a",
                                  "a23_b01_a","a3_b201_a"}); // u-channel
  all_colours_possible(UNO_QGQG, {"a12_b30_a","a2_b310_a",
                                  "a132_b0_a","a32_b10_a"}); // u-channel
  all_colours_possible(UNO_WGQQ, {"a13_b2_a","a3_b12_a"});
  all_colours_possible(UNO_GWQQ, {"a03_b2_a","a3_b02_a"});
  all_colours_possible(UNO_GQWQ, {"a03_b1_a","a3_b01_a"});
  all_colours_possible(UNO_QWQG, {"a2_b30_a","a32_b0_a"});
  all_colours_possible(UNO_QQWG, {"a1_b30_a","a31_b0_a"});
  all_colours_possible(UNO_QQGW, {"a1_b20_a","a21_b0_a"});

  // extremal qqbar
  all_colours_possible(QQBAR_QGQQBAR, {"a12_3b0_a","a2_3b10_a",
                                       "a1b2_30_a","ab2_310_a"}); // u-channel
  all_colours_possible(QQBAR_QGQBARQ, {"a1b3_20_a", "ab3_210_a",
                                       "a13_2b0_a","a3_2b10_a"}); // u-channel
  all_colours_possible(QQBAR_QBARQGQ, {"a23_b1_0a","a3_b21_0a",
                                       "a1_023_ba","a1_03_b2a"}); // u-channel
  all_colours_possible(QQBAR_WQBARQQ, {"a3_b2_1a", "a2_13_ba"});
  all_colours_possible(QQBAR_QBARWQQ, {"a3_b2_0a", "a2_03_ba"});
  all_colours_possible(QQBAR_QBARQWQ, {"a3_b1_0a", "a1_03_ba"});
  all_colours_possible(QQBAR_QWQQBAR, {"a2_3b0_a", "ab2_30_a"});
  all_colours_possible(QQBAR_QQWQBAR, {"a1_3b0_a", "ab1_30_a"});
  all_colours_possible(QQBAR_QQQBARW, {"a1_2b0_a", "ab1_20_a"});

  // central qqbar
  all_colours_possible(QQBAR_GQQBARQ, {"a01_23_ba","a1_23_b0a",
                                       "a03_b1_2a","a3_b1_20a"}); // u-channel
  all_colours_possible(QQBAR_QQBARQG, {"a3b2_10_a","ab32_10_a",
                                       "a2_13b0_a","a2_1b30_a"}); // u-channel
  all_colours_possible(QQBAR_QQBARQQBAR, {"ab_32_10_a","a2_1b_30_a"});
  all_colours_possible(QQBAR_QWQBARQQBAR, {"ab_43_20_a", "a3_2b_40_a"});
  all_colours_possible(QQBAR_QQBARWQQBAR, {"ab_43_10_a", "a3_1b_40_a"});
  all_colours_possible(QQBAR_QQBARQWQBAR, {"ab_42_10_a", "a2_1b_40_a"});

  // forbidden
  // crossed FKL
  verify_colour(FKL_GGG, "a021ba",false);
  verify_colour(FKL_GGG, "a0b12a",false);
  verify_colour(FKL_GGG, "a10b2a",false);
  verify_colour(FKL_GGG, "a1b02a",false);
  verify_colour(FKL_GGG, "a20b1a",false);
  verify_colour(FKL_GGG, "a21b0a",false);
  verify_colour(FKL_GGG, "a2b01a",false);
  verify_colour(FKL_GGG, "ab120a",false);
  // quark with anti-colour
  verify_colour(FKL_QGQ, "a_01b_2a",false);
  verify_colour(FKL_QGQ, "a_0b_21a",false);
  verify_colour(FKL_QBARGQBAR, "a12_b0_a",false);
  verify_colour(FKL_QBARGQBAR, "a2_b10_a",false);
  // higgs with colour
  verify_colour(FKL_QHGQ, "a123_b0_a",false);
  verify_colour(FKL_QHGQ, "a3_1_b20_a",false);
  verify_colour(FKL_QHGQ, "a3_11_b20_a",false);
  // not-connected
  verify_colour(FKL_GGG, "a012a",false);
  verify_colour(FKL_GGG, "a012aa",false);
  verify_colour(FKL_GGG, "a01ba",false);
  verify_colour(FKL_GGG, "a0b2a",false);
  verify_colour(FKL_GGG, "a_01b2a",false);
  verify_colour(FKL_GGG, "a01_b2a",false);
  verify_colour(FKL_GGG, "a0b_12a",false);
  verify_colour(FKL_GGG, "a012b_a",false);
  verify_colour(UNO_GQGQ, "a_1023_ba",false);
  // uno
  verify_colour(UNO_GQGQ, "a203_b1_a",false);
  verify_colour(UNO_QGQG, "a312_b0_a",false);
  // extremal qqbar
  verify_colour(QQBAR_QGQQBAR, "a10_3b2_a",false);
  verify_colour(QQBAR_QGQQBAR, "a2_31b0_a",false);
  verify_colour(QQBAR_QGQQBAR, "a2_31b0_a",false);
  verify_colour(QQBAR_QGQBARQ, "ab13_20_a",false);
  verify_colour(QQBAR_QBARQGQ, "a3_b1_02a",false);
  verify_colour(QQBAR_QBARQGQ, "a21_b3_0a",false);
  // central qqbar
  verify_colour(QQBAR_GQQBARQ, "a1_203_ba",false);
  verify_colour(QQBAR_GQQBARQ, "a3_21_b0a",false);
  verify_colour(QQBAR_QQBARQG, "ab2_130_a",false);
  verify_colour(QQBAR_QQBARQG, "a3b0_12_a",false);
  verify_colour(QQBAR_QQBARQQBAR, "a0_1b_32_a",false);
  verify_colour(QQBAR_QQBARQQBAR, "a0_3b_12_a",false);
  verify_colour(QQBAR_QQBARQQBAR, "a2_3b_10_a",false);
  verify_colour(QQBAR_QQBARQQBAR, "ab_12_30_a",false);

  return EXIT_SUCCESS;
}

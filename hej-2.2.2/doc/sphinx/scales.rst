.. _`Custom scales`:

Custom scales
=============

HEJ 2 comes with a small selection of built-in renormalisation
and factorisation scales, as described in the :ref:`scales <scales>` setting. In
addition to this, user-defined scales can be imported from custom
libraries.

Writing the library
-------------------

Custom scales are defined through C++ functions that take an event and
compute the corresponding scale. As an example, let's consider a
function returning the transverse momentum of the softest jet in an
event. To make it accessible from HEJ 2, we have to prevent C++
name mangling with :code:`extern "C"`:

.. literalinclude:: ../../examples/softestptScale.cc

After saving this code to some file :code:`myscales.cc`, we can compile
it to a shared library. With the :code:`g++` compiler this can be done
with the command

.. code-block:: sh

   g++ $(HEJ-config --cxxflags) -fPIC -shared -O2 -fvisibility=hidden -Wl,-soname,libmyscales.so -o libmyscales.so myscales.cc

If :code:`g++` is used and the library also contains other definitions,
it is recommended to add :code:`-fvisibility=hidden` to the compiler
flags and :code:`__attribute__((visibility("default")))` after
:code:`extern "C"` for each exported function in order to avoid possible
name clashes.

Importing the scale into HEJ 2
-------------------------------------

Our custom scale can now be imported into HEJ 2 by adding the
following lines to the `YAML <http://yaml.org/>`_ configuration file

.. code-block:: YAML

   import scales:
      /path/to/libmyscales.so: softest_jet_pt

It is also possible to import several scales from one or more libraries:

.. code-block:: YAML

   import scales:
      /path/to/libmyscales1.so: [first_scale, second_scale]
      /path/to/libmyscales2.so: [another_scale, yet_another_scale]

The custom scales can then be used as usual in the :ref:`scales <scales>`
setting, for example

.. code-block:: YAML

   scales: [H_T, softest_jet_pt, 2*softest_jet_pt]

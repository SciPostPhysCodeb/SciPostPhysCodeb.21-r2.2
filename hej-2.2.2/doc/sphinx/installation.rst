.. _Installation:

Installation
============

Download
--------

A tar archive of the HEJ 2 source code can be downloaded and
decompressed with the command::

  curl https://hej.hepforge.org/downloads?f=HEJ_2.2.tar.gz | tar -xz

To obtain the latest stable HEJ version, `HEJ_2.2.tar.gz` should be
replaced by `HEJ.tar.gz`.

Alternatively, the HEJ source code can be obtained by installing the
`git version control system <https://git-scm.com/>`_. and running:

.. code-block:: sh

  git clone https://phab.hepforge.org/source/hej.git

We also provide a `Docker image <https://hub.docker.com/r/hejdock/hej>`_
containing a HEJ 2 installation (including the HEJ Fixed Order Generator).
This image can be pulled with::

  docker pull hejdock/hej

When using the Docker image the remaining installation steps can be
skipped.

In addition to working with Docker, these images will also work with
`apptainer <https://apptainer.org/>`_/ `singularity
<https://docs.sylabs.io/guides/latest/user-guide/>`_. This comes with
a caveat that you may need to source the software inside the image
before running HEJ::

  source /cvmfs/pheno.egi.eu/HEJV2/HEJ_env.sh

Users can also make use of the MPI libraries included in the images for
efficient
fixed order event generation for HEJ input using Sherpa which is configured
with MPI support. Note that HEJ itself does not provide support for MPI.


Prerequisites
-------------

Before installing HEJ 2, you need the following programs and
libraries:

- `CMake <https://cmake.org/>`_ version 3.1
- A compiler supporting the C++17 standard, for example
  `gcc <https://gcc.gnu.org/>`_ 7 or later
- `FastJet <http://fastjet.fr/>`_
- `CLHEP <https://gitlab.cern.ch/CLHEP/CLHEP>`_ version 2.3
- `LHAPDF <https://lhapdf.hepforge.org/>`_ version 6
- The `IOStreams` and `uBLAS` `boost <https://www.boost.org>`_ libraries
- `yaml-cpp <https://github.com/jbeder/yaml-cpp>`_
- `autoconf` and `automake` for `FORM <https://github.com/vermaseren/form>`_

In addition, some optional features have additional dependencies:

- `Version 2 of QCDLoop <https://github.com/scarrazza/qcdloop>`_ is
  required to include finite top mass corrections in Higgs boson + jets
  production.
- `HepMC versions 2 and 3 <https://hepmc.web.cern.ch/hepmc/>`_ enable
  event output in the respective format.
- `Rivet <https://rivet.hepforge.org/>`_ version 3.1.4 or later
  together with HepMC 2 or 3 allow using Rivet analyses.
- `HighFive <https://github.com/BlueBrain/HighFive>`_ has to be
  installed in order to read and write event files in the
  `HDF5 <https://www.hdfgroup.org/>`_-based format suggested in
  `arXiv:1905.05120 <https://arxiv.org/abs/1905.05120>`_.

We strongly recommend to install these programs and libraries to
standard locations:

- The executable files should be inside one of the directories listed in
  the `PATH` environment variable. This concerns `cmake`, the C++
  compiler, and the executables contained in `autoconf` and
  `automake`.

- The library header files ending with `.h`, `.hh`, or `.hpp` should be
  in a directory where they are found by the C++ compiler. For `gcc`
  or `clang`, custom locations can be specified using the
  `CPLUS_INCLUDE_PATH` environment variable.

- The compiled library files ending with `.a`, `.so`, or `.dylib`
  should be in a directory where they are found by the linker. Custom
  locations can be set via the `LIBRARY_PATH` environment
  variable. For shared object libraries (`.so` or `.dylib`) custom
  locations should also be part of `LD_LIBRARY_PATH` on linux and
  `DYLD_FALLBACK_LIBRARY_PATH` or `DYLD_LIBRARY_PATH` on macOS.

Compilation
-----------

To compile and install HEJ 2 run::

  cmake source/directory -DCMAKE_INSTALL_PREFIX=target/directory
  make install

:file:`source/directory` is the directory containing the file
:file:`CMakeLists.txt`. If you omit
:code:`-DCMAKE_INSTALL_PREFIX=target/directory` HEJ 2 will be
installed to some default location.

In case some of the aforementioned prerequisites are not found by
:code:`cmake` you can give a hint by adding an additional argument
:code:`-Dlibname_ROOT_DIR=/directory/with/library`, where
:code:`libname` should be replaced by the name of the library in
question. For example, if `FastJet` is installed in the subdirectory
`.local` of your home directory with the `libfastjet.*` library files
in `.local/lib` and the header files ending with `.hh` in
`.local/include/fastjet` you can pass
:code:`-Dfastjet_ROOT_DIR=$HOME/.local` to `cmake`.

If :code:`cmake` fails to find (the correct) boost path,
try setting :code:`-DBOOST_ROOT=/path/to/boost`, this will force
:code:`cmake` to search for boost only in :code:`/path/to/boost`.

To not include specific packages one can add
:code:`-DEXCLUDE_packagename=TRUE` to :code:`cmake`, e.g. by
setting :code:`-DEXCLUDE_rivet=TRUE` HEJ 2 will not be interfaced
to Rivet even if it is available on the system.

Testing
-------

To test your installation, download the NNPDF 2.3 PDF set with::

  lhapdf install NNPDF23_nlo_as_0119

and run::

  make test

The test data of HEJ are stored in a
`Git Large File Storage <https://git-lfs.github.com/>`_ format.
:code:`git clone` therefore requires :code:`git-lfs` to download the data
correctly.

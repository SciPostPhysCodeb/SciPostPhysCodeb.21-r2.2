.. _`Running HEJ 2`:

Running HEJ 2
=============

Quick start
-----------

In order to run HEJ 2, you need a configuration file and a file
containing fixed-order events. A sample configuration is given by the
:file:`config.yml` file distributed together with HEJ 2. Events in the
Les Houches Event File format can be generated with standard Monte Carlo
generators like `MadGraph5_aMC@NLO <https://launchpad.net/mg5amcnlo>`_
or `Sherpa <https://sherpa.hepforge.org/trac/wiki>`_. If HEJ 2 was
compiled with `HDF5 <https://www.hdfgroup.org/>`_ support, it can also
read and write event files in the format suggested in
`arXiv:1905.05120 <https://arxiv.org/abs/1905.05120>`_.
HEJ 2 assumes that the cross section is given by the sum of the event
weights. Depending on the fixed-order generator it may be necessary to
adjust the weights in the Les Houches Event File accordingly.

The processes supported by HEJ 2 are

- Pure multijet production
- Production of a Higgs boson with jets
- Production of a W boson with jets
- Production of jets with a charged lepton-antilepton pair, via a
  virtual Z boson and/or photon
- Production of two same-sign W bosons with jets

where at least two jets are required in each case. For the time being,
only leading-order input events are supported.

After generating an event file :file:`events.lhe` adjust the parameters
under the `fixed order jets`_ setting in :file:`config.yml` to the
settings in the fixed-order generation. Resummation can then be added by
running::

  HEJ config.yml events.lhe

Alternatively it can be more efficient to run HEJ with input events piped from a
fixed order generator. To do this with e.g. Sherpa (where the event output file is
specified in the Sherpa runcard as :file:`events.lhe`), run::

  mkfifo events.lhe
  Sherpa -f Run.dat &
  HEJ config.yml events.lhe


Using the default settings, this will produce an output event file
:file:`HEJ.lhe` with events including high-energy resummation.

When using the `Docker image <https://hub.docker.com/r/hejdock/hej>`_,
HEJ can be run with:

.. code-block:: bash

  docker run -v $PWD:$PWD -w $PWD hejdock/hej HEJ config.yml events.lhe

.. _`HEJ 2 settings`:

Settings
--------

HEJ 2 configuration files follow the `YAML <http://yaml.org/>`_
format. The following configuration parameters are supported:

.. _`trials`:

**trials**
  High-energy resummation is performed by generating a number of
  resummation phase space configurations corresponding to an input
  fixed-order event. This parameter specifies how many such
  configurations HEJ 2 should try to generate for each input
  event. Typical values vary between 10 and 100.

.. _`fixed order jets`:

**fixed order jets**
  This tag collects a number of settings specifying the jet definition
  in the event input. The settings should correspond to the ones used in
  the fixed-order Monte Carlo that generated the input events.

   .. _`fixed order jets: min pt`:

   **min pt**
      Minimum transverse momentum in GeV of fixed-order jets.

   .. _`fixed order jets: algorithm`:

   **algorithm**
      The algorithm used to define jets. Allowed settings are
      :code:`kt`, :code:`cambridge`, :code:`antikt`,
      :code:`cambridge for passive`. See the `FastJet
      <http://fastjet.fr/>`_ documentation for a description of these
      algorithms.

   .. _`fixed order jets: R`:

   **R**
      The R parameter used in the jet algorithm, roughly corresponding
      to the jet radius in the plane spanned by the rapidity and the
      azimuthal angle.

.. _`resummation jets`:

**resummation jets**
  This tag collects a number of settings specifying the jet definition
  in the observed, i.e. resummed events. These settings are optional, by
  default the same values as for the `fixed order jets`_ are assumed.

   .. _`resummation jets: min pt`:

   **min pt**
      Minimum transverse momentum in GeV of resummation jets. This
      should be between 25% and 50% larger than the minimum transverse
      momentum of fixed order jets set by `fixed order jets: min pt`_. The
      contribution from the lower min pt fixed order jets can also be
      calculated separately using the `require low pt jet`_ option.

   .. _`resummation jets: algorithm`:

   **algorithm**
      The algorithm used to define jets. The HEJ 2 approach to
      resummation relies on properties of :code:`antikt` jets, so this
      value is strongly recommended. For a list of possible other
      values, see the `fixed order jets: algorithm`_ setting.

   .. _`resummation jets: R`:

   **R**
      The R parameter used in the jet algorithm.

.. _`event treatment`:

**event treatment**
  Specify how to treat different event types. The different event types
  contribute to different orders in the high-energy limit. The types are:

  .. _`FKL`:

  **FKL**
     Specifies how to treat events respecting FKL rapidity ordering, where all
     but the two partons extremal in rapidity have to be gluons, e.g.
     :code:`u d => u g d`. These configurations are dominant in the high-energy
     limit.

  .. _`unordered`:

  **unordered**
     Specifies how to treat events with one gluon emission that does not respect
     FKL ordering, e.g. :code:`u d => g u d`. In the high-energy limit, such
     configurations are logarithmically suppressed compared to FKL
     configurations.

  .. _`extremal qqbar`:

  **extremal qqbar**
     Specifies how to treat events with a quark-antiquark pair as extremal
     partons in rapidity, e.g. :code:`g d => u u_bar d`. In the high-energy
     limit, such configurations are logarithmically suppressed compared to FKL
     configurations.

  .. _`central qqbar`:

  **central qqbar**
     Specifies how to treat events with a quark-antiquark pair central in
     rapidity, e.g. :code:`g g => g u u_bar g`. In the high-energy limit, such
     configurations are logarithmically suppressed compared to FKL
     configurations.

  .. _`non-resummable`:

  **non-resummable**
     Specifies how to treat events where resummation is currently not
     supported for the given rapidity ordering and/or assignment of
     parton flavours.

  .. _`unknown`:

  **unknown**
     Specifies how to treat events for processes that are not
     implemented, e.g. di-photon production.

  .. _`invalid`:

  **invalid**
     Specifies how to treat events that are considered
     unphysical. Events in this category violate one or more of the
     following requirements:

     - Charge conservation.
     - Four-momentum conservation.
     - Momenta of massless particles must be lightlike.
     - Incoming particles must not have transverse momentum.

  The possible treatments are

  - :code:`reweight` to enable resummation
  - :code:`keep` to keep the events as they are up to a possible
    change of renormalisation and factorisation scale
  - :code:`discard` to discard these events
  - :code:`abort` to abort the program if one of these events is encountered

  The settings :code:`keep`, :code:`discard`, and :code:`abort` are
  supported for all event types. The :code:`reweight` treatment is
  implemented for different event types depending on the considered
  process:

  .. csv-table::
    :header: , "FKL", "unordered", "extremal qqbar", "central qqbar"
    :widths: auto
    :align: center
    :stub-columns: 1

    "pure jets", "Yes", "Yes", "Yes", "Yes"
    "Higgs + jets", "Yes", "Yes", "No", "No"
    "W + jets", "Yes", "Yes", "Yes", "Yes"
    "Z/γ + jets", "Yes", "Yes", "No", "No"
    "same-sign W + jets", "Yes", "No", "No", "No"



.. _`scales`:

**scales**
   Specifies the renormalisation and factorisation scales for the output
   events. This can either be a single entry or a list :code:`[scale1,
   scale2, ...]`. For the case of a list the first entry defines the
   central scale. Possible values are fixed numbers to set the scale in
   GeV or the following:

   - :code:`H_T`: The sum of the scalar transverse momenta of all
     final-state particles
   - :code:`max jet pperp`: The maximum transverse momentum of all jets
   - :code:`jet invariant mass`: Sum of the invariant masses of all jets
   - :code:`m_j1j2`: Invariant mass between the two hardest jets.

   Scales can be multiplied or divided by overall factors, e.g. :code:`H_T/2`.

   It is also possible to import scales from an external library, see
   :ref:`Custom scales`

.. _`scale factors`:

**scale factors**
   A list of numeric factors by which each of the `scales`_ should be
   multiplied. Renormalisation and factorisation scales are varied
   independently. For example, a list with entries :code:`[0.5, 2]`
   would give the four scale choices (0.5μ\ :sub:`r`, 0.5μ\ :sub:`f`);
   (0.5μ\ :sub:`r`, 2μ\ :sub:`f`); (2μ\ :sub:`r`, 0.5μ\ :sub:`f`); (2μ\
   :sub:`r`, 2μ\ :sub:`f`) in this order. The ordering corresponds to
   the order of the final event weights.

.. _`max scale ratio`:

**max scale ratio**
   Specifies the maximum factor by which renormalisation and
   factorisation scales may difer. For a value of :code:`2` and the
   example given for the `scale factors`_ the scale choices
   (0.5μ\ :sub:`r`, 2μ\ :sub:`f`) and (2μ\ :sub:`r`, 0.5μ\ :sub:`f`)
   will be discarded.

.. _`log correction`:

**log correction**
   Whether to include corrections due to the evolution of the strong
   coupling constant in the virtual corrections. Allowed values are
   :code:`true` and :code:`false`.

.. _`NLO truncation`:

**NLO truncation**
   Options to truncate the HEJ resummation at next-to-leading order. Used for
   bin-by-bin NLO reweighting.

   .. _`NLO truncation: enabled`:

   **enabled**
      Enable truncation. Allowed values are :code:`true` and
      :code:`false` (default).

   .. _`NLO truncation: nlo order`:

   **nlo order**
      Set the (base) number of jets in the NLO sample.
      Allowed values are integers (default: 2).

.. _`unweight`:

**unweight**
   Settings for unweighting events. Unweighting can greatly reduce the
   number of resummation events, speeding up analyses and shrinking
   event file sizes.

   .. _`type`:

   **type**
      How to unweight events. The supported settings are

      - :code:`weighted`: Generate weighted events. Default, if nothing
        else specified.
      - :code:`resummation`: Unweight only resummation events. Each set
        of resummation events coming from *a single fixed order event*
        are unweighted separately according to the largest weight in the
        current chunk of events.
      - :code:`partial`: Unweight only resummation events with weights
        below a certain threshold. The weight threshold is determined
        automatically in a calibration run prior to the usual event
        generation.

   .. _`unweight: trials`:

   **trials**
      Maximum number of trial resummation events generated in the
      calibration run for partial unweighting. This option should only
      be set for partial unweighting.

      If possible, each trial is generated from a different input
      fixed-order event. If there are not sufficiently many input
      events, more than one trial event may be generated for each of
      them and the actual number of trial events may be smaller than
      requested.

      Increasing the number of trials generally leads to better
      unweighting calibration but increases the run time. Between 1000
      and 10000 trials are usually sufficient.

   .. _`unweight: max deviation`:

   **max deviation**
       Controls the range of events to which unweighting is
       applied. This option should only be set for partial unweighting.

       A larger value means that a larger fraction of events are
       unweighted.  Typical values are between -1 and 1.

.. _`event output`:

**event output**
   Specifies the name of a single event output file or a list of such
   files. The file format is either specified explicitly or derived from
   the suffix. For example, :code:`events.lhe` or, equivalently
   :code:`Les Houches: events.lhe` generates an output event file
   :code:`events.lhe` in the Les Houches format. The supported formats
   are

   - :code:`file.lhe` or :code:`Les Houches: file`: The Les Houches
     event file format.
   - :code:`file.hepmc2` or :code:`HepMC2: file`: HepMC format version 2.
   - :code:`file.hepmc3` or :code:`HepMC3: file`: HepMC format version 3.
   - :code:`file.hepmc` or :code:`HepMC: file`: The latest supported
     version of the HepMC format, currently version 3.
   - :code:`file.hdf5` or :code:`HDF5: file`: The HDF5-based format of
     `arXiv:1905.05120 <https://arxiv.org/abs/1905.05120>`_.

.. _`random generator`:

**random generator**
   Sets parameters for random number generation.

   .. _`random generator: name`:

   **name**
      Which random number generator to use. Currently, :code:`mixmax`
      and :code:`ranlux64` are supported. Mixmax is recommended. See
      the `CLHEP documentation
      <http://proj-clhep.web.cern.ch/proj-clhep/index.html#docu>`_ for
      details on the generators.

   .. _`random generator: seed`:

   **seed**
      The seed for random generation. This should be a single number for
      :code:`mixmax` and the name of a state file for :code:`ranlux64`.

.. _`analyses`:

**analyses**
   Names and settings for one or more custom and Rivet event
   analyses.

   Entries containing the :code:`rivet` key are interpreted as Rivet analyses;
   the values corresponding to this key should be the analyses names. In
   addition, there is a mandatory :code:`output` key which determines the prefix
   for the yoda output file.

   For a custom analysis the :code:`plugin` sub-entry
   should be set to the analysis file path. All further entries are passed on
   to the analysis. See :ref:`Writing custom analyses` for details.

.. _`vev`:

**vev**
   Higgs vacuum expectation value in GeV. All electro-weak constants are derived
   from this together with the `particle properties`_.

.. _`particle properties`:

**particle properties**

   Specifies various properties of the different particles (Higgs, W or Z). All
   electro-weak constants are derived from these together with the :ref:`vacuum
   expectation value<vev>`.

.. _`particle properties: particle`:

   **Higgs, W or Z**
      The particle (Higgs, |W+| or |W-|, Z) for which the following properties
      are defined.

      .. |W+| replace:: W\ :sup:`+`
      .. |W-| replace:: W\ :sup:`-`

   .. _`particle properties: particle: mass`:

      **mass**
         The mass of the particle in GeV.

   .. _`particle properties: particle: width`:

      **width**
         The total decay width of the particle in GeV.

.. _`Higgs coupling`:

**Higgs coupling**
   This collects a number of settings concerning the effective coupling
   of the Higgs boson to gluons. This is only relevant for the
   production process of a Higgs boson with jets and only supported if
   HEJ 2 was compiled with `QCDLoop
   <https://github.com/scarrazza/qcdloop>`_ support.

.. _`Higgs coupling: use impact factors`:

   **use impact factors**
      Whether to use impact factors for the coupling to the most forward
      and most backward partons. Impact factors imply the infinite
      top-quark mass limit.

.. _`Higgs coupling: mt`:

   **mt**
      The value of the top-quark mass in GeV. If this is not specified,
      the limit of an infinite mass is taken.

.. _`Higgs coupling: include bottom`:

   **include bottom**
      Whether to include the Higgs coupling to bottom quarks.

.. _`Higgs coupling: mb`:

   **mb**
      The value of the bottom-quark mass in GeV. Only used for the Higgs
      coupling, external bottom-quarks are always assumed to be massless.

.. _`require low pt jet`:

**require low pt jet**
    Restrict output to only include events which have fixed order jets with pT
    less than the `resummation jets: min pt`_. Used to calculate the
    contribution from a lower min pt in the fixed order jets. This option allows
    this calculation to be done separately from the calculation where the fixed
    order min pt is equal to the resummation jets min pt. This is usually a small
    correction.

.. _`off-shell tolerance`:

**off-shell tolerance**
     Tolerance for numerical inaccuracies in input momenta. Momenta of
     massless particles with an invariant mass below the given value
     are rescaled to be on-shell. Transverse momentum components of
     incoming particles that are smaller than the given tolerance are
     set to zero. The default value is 0, leaving input momenta unchanged.


Advanced Settings
~~~~~~~~~~~~~~~~~

All of the following settings are optional. Please **do not set** any of the
following options, unless you know exactly what you are doing. The default
behaviour gives the most reliable results for a wide range of observables.

.. _`soft pt regulator`:

**soft pt regulator**
  Specifies the maximum fraction that soft radiation can contribute to the
  transverse momentum of each the tagging jets, i.e. any jet that affects the
  event classification, like the most forward and most backward jet or the jets
  of the central qqbar pair. This setting is needed to regulate an otherwise
  cancelled divergence. Default is 0.1.

.. _`max events`:

**max events**
    Maximal number of (input) Fixed Order events. HEJ will stop after processing
    `max events` many events. Default considers all events.

.. _`regulator parameter`:

**regulator parameter**
    Slicing parameter to regularise the subtraction term, called :math:`\lambda`
    in `arxiv:1706.01002 <https://arxiv.org/abs/1706.01002>`_. Default is 0.2.

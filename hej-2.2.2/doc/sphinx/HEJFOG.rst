The HEJ Fixed Order Generator
=============================

For high jet multiplicities event generation with standard fixed-order
generators becomes increasingly cumbersome. For example, the
leading-order production of a Higgs Boson with five or more jets is
computationally prohibitively expensive.

To this end, HEJ 2 provides the ``HEJFOG`` fixed-order generator
that allows to generate events with high jet multiplicities. To
facilitate the computation the limit of Multi-Regge Kinematics with
large invariant masses between all outgoing particles is assumed in the
matrix elements. The typical use of the ``HEJFOG`` is to supplement
low-multiplicity events from standard generators with high-multiplicity
events before using the HEJ 2 program to add high-energy
resummation.

Installation
------------

The ``HEJFOG`` comes bundled together with HEJ 2 and the
installation is very similar. After downloading HEJ 2 and
installing the prerequisites as described in :ref:`Installation` the
``HEJFOG`` can be installed with::

  cmake /path/to/FixedOrderGen -DCMAKE_INSTALL_PREFIX=target/directory
  make install

where :file:`/path/to/FixedOrderGen` refers to the :file:`FixedOrderGen`
subdirectory in the HEJ 2 directory. If HEJ 2 was
installed to a non-standard location, it may be necessary to specify the
directory containing :file:`HEJ-config.cmake`. If the base installation
directory is :file:`/path/to/HEJ`, :file:`HEJ-config.cmake` should be
found in :file:`/path/to/HEJ/lib/cmake/HEJ` and the commands for
installing the ``HEJFOG`` would read::

  cmake /path/to/FixedOrderGen -DHEJ_DIR=/path/to/HEJ/lib/cmake/HEJ -DCMAKE_INSTALL_PREFIX=target/directory
  make install

The installation can be tested with::

  make test

provided that the ``CT10nlo`` PDF sets is installed.

Running the fixed-order generator
---------------------------------

After installing the ``HEJFOG`` you can modify the provided
configuration file :file:`configFO.yml` and run the generator with::

  HEJFOG configFO.yml

When using the ``HEJ`` docker image, the corresponding command is:

.. code-block:: bash

  docker run -v $PWD:$PWD -w $PWD hejdock/hej HEJFOG configFO.yml

The resulting event file, by default named :file:`HEJFO.lhe`, can then be
fed into HEJ 2 like any event file generated from a standard
fixed-order generator, see :ref:`Running HEJ 2`.

Settings
--------

Similar to HEJ 2, the ``HEJFOG`` uses a `YAML
<http://yaml.org/>`_ configuration file. The settings are

.. _`process`:

**process**
   The scattering process for which events are being generated. The
   format is

   :code:`in1 in2 => out1 out2 ...`

   The incoming particles, :code:`in1`, :code:`in2` can be

   - quarks: :code:`u`, :code:`d`, :code:`u_bar`, and so on
   - gluons: :code:`g`
   - protons :code:`p` or antiprotons :code:`p_bar`

   The outgoing particles :code:`out1`, :code:`out2`, etc. can be

   - jets: At least two final state jets :code:`j` are
     required. Multiple jets can be grouped together, e.g. :code:`2j`.
   - bosons: At most a single Higgs (:code:`h`), :code:`W+`, or
     :code:`W-` boson, which might further :ref:`decay<decays>`. Final
     states with more than one boson are curently not supported.
   - leptons: If there is no Higgs or W boson the final state may
     contain a single lepton pair. Flavours have to be chosen such
     that the leptons can originate from the decay of a single virtual
     W or Z boson.

     In the case of a Z boson only final states with two *charged*
     leptons are supported. The interference between a virtual Z boson
     and photon is taken into account, but the invariant mass of the
     leptons will be chosen close to the Z boson mass.

     Valid examples include :code:`e+ nu_e` and :code:`e+ e-`.

   Instead of the leptons, decays of the bosons can be added
   through the :ref:`decays<decays>`. The latter is required to decay a Higgs
   boson. So :code:`p p => W+ j j` is the same as :code:`p p => e+ nu_e 2j`, if
   the decay :code:`W+ => e+ nu_e` is specified in `decays`_.

.. _`events`:

**events**
   Specifies the number of events to generate.

.. _`jets`:

**jets**
   Defines the properties of the generated jets.

   .. _`jets: min pt`:

   **min pt**
      Minimum jet transverse momentum in GeV.

   .. _`jets: peak pt`:

   **peak pt**
      Optional setting to specify the dominant jet transverse momentum
      in GeV. If the generated events are used as input for HEJ
      resummation, this should be set to the minimum transverse momentum
      of the resummation jets. In all cases it has to be larger than
      :code:`min pt`. The effect is that only a small fraction of jets
      will be generated with a transverse momentum below the value of
      this setting.

   .. _`jets: algorithm`:

   **algorithm**
      The algorithm used to define jets. Allowed settings are
      :code:`kt`, :code:`cambridge`, :code:`antikt`,
      :code:`cambridge for passive`. See the `FastJet
      <http://fastjet.fr/>`_ documentation for a description of these
      algorithms.

   .. _`jets: R`:

   **R**
      The R parameter used in the jet algorithm.

   .. _`jets: max rapidity`:

   **max rapidity**
      Maximum absolute value of the jet rapidity.

.. _`beam`:

**beam**
   Defines various properties of the collider beam.

   .. _`beam: energy`:

   **energy**
      The beam energy in GeV. For example, the 13
      TeV LHC corresponds to a value of 6500.

   .. _`beam: particles`:

   **particles**
      A list :code:`[p1, p2]` of two beam particles. The only supported
      entries are protons :code:`p` and antiprotons :code:`p_bar`.

.. _`pdf`:

**pdf**
   The `LHAPDF number <https://lhapdf.hepforge.org/pdfsets>`_ of the PDF set.
   For example, 230000 corresponds to an NNPDF 2.3 NLO PDF set.

.. _`subleading fraction`:

**subleading fraction**
   This setting is related to the fraction of events that are not a FKL
   configuration. All possible subleading process are listed in
   :ref:`subleading channels<subleading channels>`. This value must be
   positive and not larger than 1. It should typically be chosen between
   0.01 and 0.1. Note that while this parameter influences the chance of
   generating subleading configurations, it generally does not
   correspond to the actual fraction of subleading events.

.. _`subleading channels`:

**subleading channels**
   Optional parameters to select a specific subleading process, multiple
   channels can be selected at once. If multiple subleading configurations are
   possible one will be selected at random for each event, thus each final state
   will include at most one subleading process, e.g. either :code:`unordered` or
   :code:`central qqbar`. Only has an effect if :code:`subleading fraction` is
   non-zero. Subleading processes requested here also have to be available from
   HEJ, for a list of implemented processes see :ref:`event treatment<event
   treatment>`. The following values are allowed:

   - :code:`all`: All channels allowed. This is the default.
   - :code:`none`: No subleading contribution, only the leading (FKL)
     configurations are allowed.  This is equivalent to
     :code:`subleading fraction: 0`.
   - :code:`unordered`: Unordered emission allowed.
     Unordered emission are any rapidity ordering where exactly one gluon is
     emitted outside the rapidity ordering required in FKL events. More
     precisely, if at least one of the incoming particles is a quark or
     antiquark and there are more than two jets in the final state,
     :code:`subleading fraction` states the probability that the flavours
     of the outgoing particles are assigned in such a way that an unordered
     configuration arises.
   - :code:`extremal qqbar`: Production of a quark-antiquark pair as extremal
     partons in rapidity. If the :code:`central qqbar` channel is allowed,
     :code:`subleading fraction` gives the probability of emitting a
     quark-antiquark pair in place of two gluons adjacent in rapidity at the most forward or backwards quarks.
   - :code:`central qqbar`: Production of a central quark-antiquark pair. This
     setting is similar to :code:`extremal qqbar`, but :code:`subleading fraction`
     gives the probability of emitting a quark-antiquark pair in place of two
     gluons adjacent in rapidity at any point *inside* the :code:`FKL` gluon
     chain.

.. _`unweight`:

**unweight**
   This setting defines the parameters for the partial unweighting of
   events. You can disable unweighting by removing this entry from the
   configuration file.

.. _`unweight: sample size`:

   **sample size**
      The number of weighted events used to calibrate the unweighting.
      A good default is to set this to the number of target
      `events`_. If the number of `events`_ is large this can
      lead to significant memory consumption and a lower value should be
      chosen. Contrarily, for large multiplicities the unweighting
      efficiency becomes worse and the sample size should be increased.

.. _`unweight: max deviation`:

   **max deviation**
     Controls the range of events to which unweighting is applied. A
     larger value means that a larger fraction of events are unweighted.
     Typical values are between -1 and 1.

.. _`decays`:

**decays**
   Optional setting specifying the decays of the particle. Only the decay
   into two particles is implemented. Each decay has the form

   :code:`boson: {into: [p1,p2], branching ratio: r}`

   where :code:`boson` is the corresponding boson, :code:`p1` and :code:`p2` are
   the particle names of the decay product (e.g. :code:`photon`) and :code:`r`
   is the branching ratio. The branching ratio is optional (:code:`1` by
   default).

   Decays of a Higgs boson are treated as the production and subsequent
   decay of an on-shell Higgs boson, so decays into e.g. Z bosons are not
   supported. In contrast W bosons are decayed off-shell, thus the
   branching ratio should not be specified or set to :code:`1`.

.. _`FOG scales`:

**scales**
   Specifies the renormalisation and factorisation scales for the output events.
   For details, see the corresponding entry in the HEJ 2
   :ref:`settings<scales>`. Note that this should usually be a single value, as
   the weights resulting from additional scale choices will simply be ignored
   when adding high-energy resummation with HEJ 2.

.. _`FOG event output`:

**event output**
   Specifies the name of a single event output file or a list of such files. See
   the corresponding entry in the HEJ 2 :ref:`settings<event output>` for
   details.

.. _`RanLux init`:

.. _`FOG random generator`:

**random generator**
   Sets parameters for random number generation. See the HEJ 2
   :ref:`settings<random generator>` for details.

.. _`FOG analyses`:

**analyses**
   Specifies the name and settings for one or more analyses library. This
   can be useful to specify cuts at the fixed-order level. See the
   corresponding entry in the HEJ 2 :ref:`settings<analyses>`
   for details.

.. _`FOG vev`:

**vev**
   Higgs vacuum expectation value in GeV. All electro-weak constants are derived
   from this together with the :ref:`particle properties <FOG particle
   properties>`.

.. _`FOG particle properties`:

**particle properties**
   Specifies various properties of the different particles (Higgs, W or Z).
   These have to be specified for all bosons. See the corresponding entry in the
   HEJ 2 :ref:`settings<particle properties>` for details.

.. _`FOG Higgs coupling`:

**Higgs coupling**
   This collects a number of settings concerning the effective coupling of the
   Higgs boson to gluons. See the corresponding entry in the HEJ 2
   :ref:`settings<Higgs coupling>` for details.

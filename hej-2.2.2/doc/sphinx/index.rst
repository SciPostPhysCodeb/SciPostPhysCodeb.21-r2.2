.. HEJ 2 documentation master file, created by
   sphinx-quickstart on Fri Sep 15 16:13:57 2017.

Welcome to the HEJ 2 documentation!
========================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   intro
   installation
   HEJ
   HEJFOG
   analyses
   scales

* :ref:`search`

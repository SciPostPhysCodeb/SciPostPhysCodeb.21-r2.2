import feynman;
defaultpen(fontsize(8));

real h = 17;
real w = sqrt(3)*h;
real len = 10;
real dist = 5;

pair[] v = {
  (0,h), (0,-h), (w,0)
};

draw(Label("$\mu$", BeginPoint, W), gluon((-w,h)--v[0]));
draw(Label("$q_1$",MidPoint,  S), ((-w-len)/2,h-dist)-- ((-w+len)/2,h-dist), Arrow);
draw(Label("$\nu$", EndPoint, W), gluon(v[1]--(-w,-h)));
draw(Label("$q_2$",MidPoint,  N), ((-w+len)/2,-h+dist)-- ((-w-len)/2,-h+dist), Arrow);
draw(v[0]--v[1], MidArrow(6));
draw(v[1]--v[2], MidArrow(6));
draw(v[2]--v[0], MidArrow(6));
draw(v[2]--(2w,0), dashed);
//draw(Label("$p_H$",MidPoint,  N), ((v[2].x+w-len)/2,5)-- ((v[2].x+w+len)/2,5), Arrow);
dot(v, currentpen + 4);

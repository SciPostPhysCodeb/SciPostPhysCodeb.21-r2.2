import feynman;
usepackage("fourier");

currentpen = fontsize(9);

real w = 50;
real h = 25;

draw(Label("a", BeginPoint, W), (-w,h)--(0,h), MidArrow(5));
draw(Label("2", EndPoint, E), (0,h)--(w,h), MidArrow(5));
draw(Label("b", BeginPoint, W), (-w,-h)--(0,-h), MidArrow(5));
draw(Label("3", EndPoint, E), (0,-h)--(w,-h), MidArrow(5));
draw(gluon((0,-h)--(0,h)));
draw(Label("W, Z, $\gamma$", EndPoint,N), photon((-3w/4,h)--(-w/4,3h/2)));
draw(Label("1", EndPoint, N), gluon((-w/4,h)--(3w/4,3h/2)));

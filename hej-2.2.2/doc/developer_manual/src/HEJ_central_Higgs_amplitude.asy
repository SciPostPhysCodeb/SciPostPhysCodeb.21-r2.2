import feynman;
usepackage("fourier");
// usepackage("sansmathfonts");
// texpreamble("\renewcommand{\familydefault}{\sfdefault}");

defaultpen(fontsize(8));

path zigzag(path g, real step=2, real distance=2) {
    real len = arclength(g);
    int state = 0;
    path zig;
    for (real u = 0; u < len; u += step) {
        real t = arctime(g, u);
        pair p = point(g, t);
        pair norm = unit(rotate(90) * dir(g, t));
        if (state == 1)
            p = p + distance * norm;
        else if (state == 3)
            p = p - distance * norm;
        zig = zig -- p;
        state = (state + 1) % 4;
    }
    zig = zig -- point(g, length(g));
    return zig;
}

real h = 80;
real w = 60;
real arrsize = 15;
real arrdist = 7;
real vxsize=7;

pair[] v = {
  (0,h), (0,3h/5), (0,h/5), (0,-h/5), (0,-3h/5), (0,-h)
};

path box = scale(vxsize)*shift(SW/sqrt(2))*unitsquare;
path triangle = scale(vxsize)*(E -- N/2 -- S/2 --cycle);

path pa = (-w,h)--(-2*vxsize,h);
draw(pa, currentpen+1);
draw(gluon(pa));
draw("$p_a$", shift(arrdist*S)*((midpoint(pa)+arrsize/2*W) -- (midpoint(pa)+arrsize/2*E)), Arrow(4));
path p1 = (w,h)--(2*vxsize,h);
draw(p1, currentpen+1);
draw(gluon(p1));
draw("$p_1$", shift(arrdist*S)*((midpoint(p1)+arrsize/2*W) -- (midpoint(p1)+arrsize/2*E)), Arrow(4));
path pb = (-w,-h)--(-2*vxsize,-h);
draw(pb, currentpen+1);
draw(gluon(pb));
draw(Label("$p_b$", MidPoint, N), shift(arrdist*N)*((midpoint(pb)+arrsize/2*W) -- (midpoint(pb)+arrsize/2*E)), Arrow(4));
path pn = (w,-h)--(2*vxsize,-h);
draw(pn, currentpen+1);
draw(gluon(pn));
draw(Label("$p_n$", MidPoint, N), shift(arrdist*N)*((midpoint(pn)+arrsize/2*W) -- (midpoint(pn)+arrsize/2*E)), Arrow(4));
path q1 = v[0]-(0,vxsize/2)--v[1]+(0,vxsize/2);
draw(zigzag(q1), red);
draw(Label("$q_1$", MidPoint, E), shift(arrdist*E)*((midpoint(q1)+arrsize/2*N) -- (midpoint(q1)+arrsize/2*S)), red, Arrow(4));
draw(
     zigzag(v[1]-(0,vxsize/2)--v[1]-(0,vxsize/2)+7S),
     red
     );
draw(v[1]-(0,vxsize/2)+7S --v[2]+(0,vxsize/2)+7N, red+Dotted);
draw(
     zigzag(v[2]+vxsize/2*N--v[2]+(vxsize/2+7)*N),
     red
     );
path qjp1 = v[2]-(0,vxsize/2)--v[3]+(0,vxsize/2);
draw(zigzag(qjp1), red);
draw(Label("$q_{j+1}$", MidPoint, E), shift(arrdist*E)*((midpoint(qjp1)+arrsize/2*N) -- (midpoint(qjp1)+arrsize/2*S)), red, Arrow(4));
draw(v[3]-(0,vxsize/2)+7S --v[4]+(0,vxsize/2)+7N, red+Dotted);
draw(
     zigzag(v[3]-(0,vxsize/2)--v[3]-(0,vxsize/2)+7S),
     red
     );
draw(
     zigzag(v[4]+(0,vxsize/2)+7N--v[4]+(0,vxsize/2)),
     red
     );
path qn = v[4]-(0,vxsize/2)--v[5]+(0,vxsize/2);
draw(zigzag(qn), red);
draw(Label("$q_n$", MidPoint, E), shift(arrdist*E)*((midpoint(qn)+arrsize/2*N) -- (midpoint(qn)+arrsize/2*S)), red, Arrow(4));

draw(gluon(v[1]+(vxsize/2,0)--(w,v[1].y)));
filldraw(shift(v[1])*box,grey);
path pH = v[2]+(vxsize/2,0)--(w,v[2].y);
draw(pH, dashed);
draw("$p_H$", shift(arrdist*S)*((midpoint(pH)+arrsize/2*W) -- (midpoint(pH)+arrsize/2*E)), Arrow(4));
filldraw(shift(v[2])*triangle,green);
draw(gluon(v[3]+(vxsize/2,0)--(w,v[3].y)));
filldraw(shift(v[3])*box,grey);
draw(gluon(v[4]+(vxsize/2,0)--(w,v[4].y)));
filldraw(shift(v[4])*box,grey);

label("$\Delta y_1$", (w, (v[0].y+v[1].y)/2), red);
label("$\Delta y_{j+1}$", (w, (v[2].y+v[3].y)/2), red);
label("$\Delta y_{n}$", (w, (v[4].y+v[5].y)/2), red);

draw(rotate(90)*"Increasing rapidity", (1.8w,h)--(1.8w,-h),Arrow);

filldraw(shift(0, h)*xscale(7)*box, blue);
label("Current", (0,h+vxsize/2), N, blue);

filldraw(shift(0, -h)*xscale(7)*box, blue);

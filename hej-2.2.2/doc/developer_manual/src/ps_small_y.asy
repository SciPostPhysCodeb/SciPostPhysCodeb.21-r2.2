import graph;

real goldenratio=(1+sqrt(5))/2;
size(4.5cm*goldenratio,4.5cm,IgnoreAspect);

real ymin = -5;
real ymax = 5;
real phimin = 0;
real phimax = 2*pi;

real yb = 0.647031;
real yf = 0.80701;
real yj = 0.758564;
real phib = 3.18605;
real phif = 4.05933;
real phij = 0.614849;
real R = 0.4;

fill(
     (yb,phimin)--(yb,phimax)--(yf,phimax)--(yf,phimin)--cycle,
     blue+opacity(0.3)
     );
fill(
     (yb,phib-R)--(yb,phib+R)--(yf,phib+R)--(yf,phib-R)--cycle,
     black+opacity(0.3)
     );
fill(
     (yb,phif-R)--(yb,phif+R)--(yf,phif+R)--(yf,phif-R)--cycle,
     black+opacity(0.3)
     );
fill(
     (yb,phij-R)--(yb,phij+R)--(yf,phij+R)--(yf,phij-R)--cycle,
     black+opacity(0.3)
     );
draw(circle((yb, phib), R));
draw(circle((yf, phif), R));
draw(circle((yj, phij), R));

xaxis(Label("$y$",0.5),YEquals(phimin),ymin,ymax);
xaxis(YEquals(phimax),ymin,ymax);
yaxis(Label("$\phi$",0.5),XEquals(ymin),phimin,phimax);
yaxis(XEquals(ymax),phimin,phimax);

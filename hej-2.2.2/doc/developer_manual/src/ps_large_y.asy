import graph;

real goldenratio=(1+sqrt(5))/2;
size(4.5cm*goldenratio,4.5cm,IgnoreAspect);

real ymin = -5;
real ymax = 5;
real phimin = 0;
real phimax = 2*pi;

real yb = -4;
real phib = 1.76814;
real phif = 3.347;
real yf = 4.4;
real R = 0.4;

fill((yb,phimin)--(yb,phimax)--(yf,phimax)--(yf,phimin)--cycle, blue+opacity(0.3));
fill(arc((yb, phib), R, -90, 90)--cycle, black + opacity(0.3));
fill(arc((yf, phif), R, 90, 270)--cycle, black + opacity(0.3));
draw(circle((yb, phib), R));
draw(circle((yf, phif), R));
filldraw(circle((-0.540897, 5.23749), R), black+opacity(0.3), black);

xaxis(Label("$y$",0.5),YEquals(phimin),ymin,ymax);
xaxis(YEquals(phimax),ymin,ymax);
yaxis(Label("$\phi$",0.5),XEquals(ymin),phimin,phimax);
yaxis(XEquals(ymax),phimin,phimax);

import feynman;

real w = 100;
real h = 100;

real arr_len = 7;
real dist = 7;

path arr = arr_len*W--arr_len*E;

draw((-w,h)--(0,h), MidArrow);
draw(Label("$p_a$", MidPoint, N), shift(-w/2, h+dist)*arr, Arrow);
draw((0,h)--(w,h), MidArrow);
draw(Label("$p_1$", MidPoint, N), shift(w/2, h+dist)*arr, Arrow);
draw((-w,-h)--(0,-h), MidArrow);
draw(Label("$p_b$", MidPoint, S), shift(-w/2, -h-dist)*arr, Arrow);
draw((0,-h)--(w,-h), MidArrow);
draw(Label("$p_n$", MidPoint, S), shift(w/2, -h-dist)*arr, Arrow);

draw(gluon((0,h)--(0,h/2)));
draw(gluon((0,h/2)--(0,0)));
draw(Label("$q_1$", MidPoint, W), shift(-dist, h/4)*rotate(-90)*arr, Arrow);
draw(gluon((0,-h/2)--(0,-h)));
draw(Label("$q_3$", MidPoint, W), shift(-dist, -3h/4)*rotate(-90)*arr, Arrow);

draw(gluon((0,h/2)--(w,h/2)));
draw((0,0)--(w,0), MidArrow);
draw(Label("$p_q$", MidPoint, N), shift(w/2, dist)*arr, Arrow);
draw((0,-h/2)--(0,0), MidArrow);
draw((w,-h/2)--(0,-h/2), MidArrow);
draw(Label("$p_{\bar{q}}$", MidPoint, S), shift(w/2, -h/2-dist)*arr, Arrow);

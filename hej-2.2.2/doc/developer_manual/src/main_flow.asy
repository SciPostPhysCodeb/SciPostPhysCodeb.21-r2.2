import flowchart;

usepackage("fourier");

size(0, 400);
defaultpen(fontsize(8));

pen code = font("T1", "DejaVuSansMono-TLF", "m", "n") + fontsize(6);

block read = rectangle(Label("LHEF::Reader::readEvent", code), Label("read event"),(0,1));
block cluster = rectangle(Label("Event constructor", code), Label("perform jet clustering"), (0,0.5));
block resum = rectangle(Label("EventReweighter::reweight", code), Label("perform resummation"), (0,0));
block cut = rectangle(Label("Analysis::pass\_cuts", code), Label("apply cuts"),(0,-0.5));
block fill = rectangle(Label("Analysis::fill", code), Label("analyse"), (-0.3,-1));
block write = rectangle(Label("CombinedEventWriter::write", code), Label("write out event"), (0.5, -1));

draw(read);
draw(cluster);
draw(resum);
draw(cut);
draw(fill);
draw(write);
add(new void(picture pic, transform t) {
    blockconnector operator --=blockconnector(pic,t);
    read--Down--Label("LHEF::HEPEUP", code)--Arrow--cluster;
    cluster--Down--Label("Event", code)--Arrow--resum;
    resum--Down--Arrow--cut;
    resum--Down--Label("Event", code)--Arrow--cut;
    cut--Down--Label("Event", code)--Left--Arrow--fill;
    cut--Down--Right--Arrow--write;
  });

/**
 *  \authors   The HEJ collaboration (see AUTHORS for details)
 *  \date      2019-2020
 *  \copyright GPLv2 or later
 */
#include "HEJ/HepMC3Writer.hh"

#include "LHEF/LHEF.h"

#include "HEJ/ConfigFlags.hh"
#include "HEJ/exceptions.hh"

#ifdef HEJ_BUILD_WITH_HepMC3

#include <utility>

#include "HepMC3/Attribute.h"
#include "HepMC3/WriterAscii.h"

#include "HEJ/HepMC3Interface.hh"

#else

#include <cassert>

#endif

#ifdef HEJ_BUILD_WITH_HepMC3

namespace HEJ {

  struct HepMC3Writer::HepMC3WriterImpl: EventWriter{
    HepMC3Interface HepMC3_;

    std::unique_ptr<HepMC3::WriterAscii> writer_;
    std::string const file_;

    HepMC3WriterImpl(
        std::string file, LHEF::HEPRUP && heprup
    ):
      HepMC3_{std::forward<LHEF::HEPRUP>(heprup)},
      file_{std::move(file)}
    {}

    void init_writer(){
      writer_ = std::make_unique<HepMC3::WriterAscii>(file_, HepMC3_.run_info());
    }

    void finish() override {
      if(finished())
        throw std::ios_base::failure("HepMC3 writer already finished.");
      EventWriter::finish();
      if(!writer_) // make sure that we always write something
        init_writer();
      writer_->close();
    }

    void write(Event const & ev) override {
      auto out_ev = HepMC3_(ev);
      //! weight names are only available after first event
      if(!writer_)
        init_writer();
      writer_->write_event(out_ev);
    }

    void set_xs_scale(const double scale) override {
      HepMC3_.set_xs_scale(scale);
    }

    ~HepMC3WriterImpl() override {
      finish_or_abort(this, "HepMC3Writer");
    }
  };

  HepMC3Writer::HepMC3Writer(std::string const & file, LHEF::HEPRUP heprup):
    impl_{ std::make_unique<HepMC3WriterImpl>(file, std::move(heprup)) }
  {}

  void HepMC3Writer::write(Event const & ev){
    impl_->write(ev);
  }

  void HepMC3Writer::set_xs_scale(const double scale) {
    impl_->set_xs_scale(scale);
  }

  void HepMC3Writer::finish(){
    impl_->finish();
  }
} // namespace HEJ

#else // no HepMC3

namespace HEJ {

  struct HepMC3Writer::HepMC3WriterImpl{};

  HepMC3Writer::HepMC3Writer(
      std::string const & /*file*/, LHEF::HEPRUP /*heprup*/
  ){
      throw std::invalid_argument(
          "Failed to create HepMC3 writer: "
          "HEJ 2 was built without HepMC3 support"
      );
  }

  void HepMC3Writer::write(Event const & /*ev*/){
    assert(false);
  }

  void HepMC3Writer::set_xs_scale(double /* scale */) {
    assert(false);
  }

  void HepMC3Writer::finish(){
    assert(false);
  }

}
#endif

namespace HEJ {
  HepMC3Writer::~HepMC3Writer() = default;
}

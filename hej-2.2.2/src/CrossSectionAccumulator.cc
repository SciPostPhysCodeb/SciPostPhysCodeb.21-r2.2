/**
 *  \authors   The HEJ collaboration (see AUTHORS for details)
 *  \date      2019-2020
 *  \copyright GPLv2 or later
 */

#include "HEJ/CrossSectionAccumulator.hh"

#include <cmath>
#include <iomanip>
#include <string>
#include <utility>

#include "HEJ/Event.hh"
#include "HEJ/Parameters.hh"

namespace HEJ {
  void CrossSectionAccumulator::fill( double const wt, double const err,
      event_type::EventType const type
  ){
    total_.value += wt;
    total_.error += err;
    auto & entry = xs_[type];
    entry.value += wt;
    entry.error += err;
  }

  void CrossSectionAccumulator::fill(
    double const wt, event_type::EventType const type
  ){
    fill(wt, wt*wt, type);
  }
  void CrossSectionAccumulator::fill(Event const & ev){
    fill(ev.central().weight, ev.type());
  }

  void CrossSectionAccumulator::fill_correlated(
    double const sum, double const sum2, event_type::EventType const type
  ){
    fill(sum, sum*sum+sum2, type);
  }
  void CrossSectionAccumulator::fill_correlated(std::vector<Event> const & evts){
    if(evts.empty()) return;
    fill_correlated(evts.cbegin(), evts.cend());
  }

  // method to scale total_ and xs_ after they have been filled
  void CrossSectionAccumulator::scale(double const xsscale) {
    // scale total cross section:
    total_.value*=xsscale;
    total_.error*=xsscale*xsscale;

    for(auto & temp : xs_) {
      temp.second.value*=xsscale;
      temp.second.error*=xsscale*xsscale;
    }
  }

  std::ostream& operator<<(std::ostream& os, const CrossSectionAccumulator& xs){
    constexpr size_t name_width = 25;
    constexpr size_t precision = 3;
    const std::streamsize orig_prec = os.precision();
    os << std::scientific << std::setprecision(precision)
      << "    " << std::left << std::setw(name_width)
      << "Cross section: " << xs.total().value
      << " +- " << std::sqrt(xs.total().error) << " (pb)\n";
    for(auto const & xs_type: xs) {
      os << "    " << std::left << std::scientific <<std::setw(name_width)
        << (name(xs_type.first) + std::string(": "));
      os << xs_type.second.value << " +- "
        << std::sqrt(xs_type.second.error) << " (pb) "
        << std::fixed << std::setprecision(precision)
        << "[" <<std::setw(precision*2) <<std::right
        << ((xs_type.second.value)/xs.total().value)*100. << "%]"
        << std::endl;
    }
    os << std::defaultfloat;
    os.precision(orig_prec);
    return os;
  }
} // namespace HEJ

/**
 *  \authors   The HEJ collaboration (see AUTHORS for details)
 *  \date      2019-2020
 *  \copyright GPLv2 or later
 */
#include "HEJ/stream.hh"

#include <array>

#include <boost/iostreams/filter/gzip.hpp>

namespace HEJ {

  namespace {
    bool is_gzip(std::ifstream & file){
      static constexpr std::array<char,2> magic_bytes = {'\x1f', '\x8b'};
      if(file.peek() != magic_bytes[0]) return false;
      file.get();
      const char second = file.peek();
      file.unget();
      return second == magic_bytes[1];
    }
  }

  istream::istream(std::string const & filename):
    file_{filename, std::ios_base::in | std::ios_base::binary},
    stream_{std::make_unique<boost_istream>()}
  {
    if(is_gzip(file_)){
      stream_->push(boost::iostreams::gzip_decompressor{});
    }
    stream_->push(file_);
  }
} // namespace HEJ

/**
 *  \authors   The HEJ collaboration (see AUTHORS for details)
 *  \date      2019-2020
 *  \copyright GPLv2 or later
 */
#include "HEJ/kinematics.hh"

#include "fastjet/PseudoJet.hh"

#include "HEJ/Particle.hh"

namespace HEJ {
  //reconstruct incoming momenta from momentum conservation
    std::tuple<fastjet::PseudoJet, fastjet::PseudoJet> incoming_momenta(
        std::vector<Particle> const & outgoing
    ){
      double xa(0.);
      double xb(0.);
      for(auto const & out: outgoing){
        xa += out.p.e() - out.p.pz();
        xb += out.p.e() + out.p.pz();
      }
      return std::tuple<fastjet::PseudoJet, fastjet::PseudoJet>{
        {0,0,-xa/2.,xa/2.},
        {0,0,xb/2.,xb/2.}
      };
    }
} // namespace HEJ

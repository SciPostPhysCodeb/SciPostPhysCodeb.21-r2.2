/**
 *  \authors   The HEJ collaboration (see AUTHORS for details)
 *  \date      2022
 *  \copyright GPLv2 or later
 */
#include "HEJ/LesHouchesReader.hh"
#include "HEJ/Version.hh"

#include <vector>

namespace HEJ {
  LesHouchesReader::Generator LesHouchesReader::get_generator(
    LHEF::HEPRUP const & heprup, std::string const & header
  ){
    // try getting generator name from specific tag
    if(!heprup.generators.empty()){
      std::string const & gen_name = heprup.generators.back().name;
      if(gen_name == "HEJ" || gen_name == HEJ::Version::String())
        return Generator::HEJ;
      if(gen_name == "HEJ Fixed Order Generation")
        return Generator::HEJFOG;
      if(gen_name == "MadGraph5_aMC@NLO") return Generator::MG;
      std::cerr << "Unknown LHE Generator " << gen_name
        << " using default LHE interface.\n";
      return Generator::unknown;
    }
    // The generator tag is not always used -> check by hand
    if(header.find("generated with HEJ")!=std::string::npos)
      return Generator::HEJ;
    if(header.find("# created by SHERPA")!=std::string::npos)
      return Generator::Sherpa;
    if(header.find("<MGVersion>")!=std::string::npos)
      return Generator::MG;
    std::cerr<<"Could not determine LHE Generator using default LHE interface.\n";
    return Generator::unknown;
  }


  LesHouchesReader::LesHouchesReader(
    std::string const & filename
  ):
    stream_{filename},
    reader_{stream_},
    num_trials_(reader_.heprup.NPRUP, 0),
    generator_{get_generator(reader_.heprup, reader_.headerBlock)},
    // calculate cross section if it was not set to > 0.
    calculate_XSECUP_{reader_.heprup.XSECUP.front() <= 0.}
  {
    // always use the newest LHE version
    reader_.heprup.version = LHEF::HEPRUP().version;

    reader_.heprup.XSECUP.resize(reader_.heprup.NPRUP);

    // For IDWTUP == 1 or 4 we assume avg(weight)=xs
    // With the modified weights we have in Sherpa sum(weight)=xs
    // -> overwrite IDWTUP to "something neutral"
    if(generator_ == Generator::Sherpa) {
      reader_.heprup.IDWTUP = reader_.heprup.IDWTUP>0?3:-3;
    }

  }

  bool LesHouchesReader::read_event() {
    if(!reader_.readEvent()) return false;

    int nprup=reader_.heprup.NPRUP;
    int position=std::max(1,std::min(hepeup().IDPRUP,nprup))-1; // 0..nprup-1

    if (generator_ == Generator::Sherpa) {
      reader_.heprup.XSECUP.at(position) *= num_trials_.at(position);
      num_trials_.at(position) +=
          std::stod(hepeup().attributes.at("trials"));
      reader_.heprup.XSECUP.at(position) += hepeup().XWGTUP;
      reader_.heprup.XSECUP.at(position) /=
          num_trials_.at(position);
    } else if (LesHouchesReader::calculate_XSECUP()) {
      reader_.heprup.XSECUP.at(position) += hepeup().XWGTUP;
    }
    return true;
  }
} // namespace HEJ

/**
 *  \authors   The HEJ collaboration (see AUTHORS for details)
 *  \date      2019-2020
 *  \copyright GPLv2 or later
 */
#include "HEJ/make_writer.hh"

#include "HEJ/HDF5Writer.hh"
#include "HEJ/HepMC2Writer.hh"
#include "HEJ/HepMC3Writer.hh"
#include "HEJ/LesHouchesWriter.hh"
#include "HEJ/exceptions.hh"

namespace HEJ {
  std::unique_ptr<EventWriter> make_format_writer(
      FileFormat format, std::string const & outfile,
      LHEF::HEPRUP const & heprup
  ){
    switch(format){
    case FileFormat::Les_Houches:
      return std::make_unique<LesHouchesWriter>(outfile, heprup);
    case FileFormat::HepMC2:
      return std::make_unique<HepMC2Writer>(outfile, heprup);
    case FileFormat::HepMC3:
      return std::make_unique<HepMC3Writer>(outfile, heprup);
    case FileFormat::HDF5:
      return std::make_unique<HDF5Writer>(outfile, heprup);
    default:
      throw std::logic_error("unhandled file format");
    }
  }

} // namespace HEJ

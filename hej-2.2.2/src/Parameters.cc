/**
 *  \authors   The HEJ collaboration (see AUTHORS for details)
 *  \date      2019-2020
 *  \copyright GPLv2 or later
 */
#include "HEJ/Parameters.hh"

#include <iostream>
#include <sstream>

namespace HEJ {

  namespace {
    void replace(
        std::string & str,
        char to_replace, std::string const & replacement
    ) {
      for( auto pos = str.find(to_replace); pos != std::string::npos;
          pos = str.find(to_replace, pos)
      ){
        str.replace(pos, 1, replacement);
        pos += replacement.size();
      }
    }

    // remove "special" characters from scale name
    // so that we can more easily use it as part of a file name
    std::string sanitise_scalename(std::string scalename) {
      replace(scalename, '/', "_over_");
      replace(scalename, '*', "_times_");
      replace(scalename, ' ', "_");
      return scalename;
    }
  } // namespace

  std::string to_string(ParameterDescription const & p) {
    // use ostringstream over std::to_string to remove trailing 0s
    std::ostringstream stream;
    stream << "\\mu_r = ";
    if(p.mur_factor != 1.) stream << p.mur_factor << '*';
    stream << p.scale_name << ", "
      "\\mu_f = ";
    if(p.muf_factor != 1.) stream << p.muf_factor << '*';
    stream << p.scale_name;
    return stream.str();
  }

  std::string to_simple_string(ParameterDescription const & p) {
    // use ostringstream over std::to_string to remove trailing 0s
    std::ostringstream stream;
    stream << "Scale_" << sanitise_scalename(p.scale_name)
      << "_MuR" << p.mur_factor << "_MuF" << p.muf_factor;
    return stream.str();
  }

} // namespace HEJ

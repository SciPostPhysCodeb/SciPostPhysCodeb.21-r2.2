/**
 *  \authors   The HEJ collaboration (see AUTHORS for details)
 *  \date      2019-2020
 *  \copyright GPLv2 or later
 */
#include "HEJ/PDG_codes.hh"

#include <boost/rational.hpp>
#include <map>

#include "HEJ/exceptions.hh"

namespace HEJ {
  ParticleID to_ParticleID(std::string const & name){
    using namespace HEJ::pid;
    static const std::map<std::string, ParticleID> known = {
      {"d", d}, {"down", down}, {"1",static_cast<ParticleID>(1)},
      {"u", u}, {"up", up}, {"2",static_cast<ParticleID>(2)},
      {"s", s}, {"strange", strange}, {"3",static_cast<ParticleID>(3)},
      {"c", c}, {"charm", charm}, {"4",static_cast<ParticleID>(4)},
      {"b", b}, {"bottom", bottom}, {"5",static_cast<ParticleID>(5)},
      {"t", t}, {"top", top}, {"6",static_cast<ParticleID>(6)},
      {"e", e}, {"electron", electron}, {"e-", e}, {"11",static_cast<ParticleID>(11)},
      {"nu_e", nu_e}, {"electron_neutrino", electron_neutrino},
        {"electron-neutrino", electron_neutrino}, {"12",static_cast<ParticleID>(12)},
      {"mu", mu}, {"muon", muon}, {"mu-", mu}, {"13",static_cast<ParticleID>(13)},
      {"nu_mu", nu_mu}, {"muon_neutrino", muon_neutrino},
         {"muon-neutrino", muon_neutrino}, {"14",static_cast<ParticleID>(14)},
      {"tau", tau}, {"tau-", tau}, {"15",static_cast<ParticleID>(15)},
      {"nu_tau", nu_tau}, {"tau_neutrino", tau_neutrino},
         {"tau-neutrino", tau_neutrino}, {"16",static_cast<ParticleID>(16)},
      {"d_bar", d_bar}, {"antidown", antidown}, {"-1",static_cast<ParticleID>(-1)},
      {"u_bar", u_bar}, {"antiup", antiup}, {"-2",static_cast<ParticleID>(-2)},
      {"s_bar", s_bar}, {"antistrange", antistrange}, {"-3",static_cast<ParticleID>(-3)},
      {"c_bar", c_bar}, {"anticharm", anticharm}, {"-4",static_cast<ParticleID>(-4)},
      {"b_bar", b_bar}, {"antibottom", antibottom}, {"-5",static_cast<ParticleID>(-5)},
      {"t_bar", t_bar}, {"antitop", antitop}, {"-6",static_cast<ParticleID>(-6)},
      {"e_bar", e_bar}, {"antielectron", antielectron}, {"positron", positron},
        {"e+", e_bar}, {"-11",static_cast<ParticleID>(-11)},
      {"nu_e_bar", nu_e_bar}, {"electron_antineutrino", electron_antineutrino},
        {"electron-antineutrino", electron_antineutrino},
        {"-12",static_cast<ParticleID>(-12)},
      {"mu_bar", mu_bar}, {"mu+", mu_bar}, {"antimuon", antimuon},
        {"-13",static_cast<ParticleID>(-13)},
      {"nu_mu_bar", nu_mu_bar}, {"muon_antineutrino", muon_antineutrino},
        {"muon-antineutrino", muon_antineutrino},
        {"-14",static_cast<ParticleID>(-14)},
      {"tau_bar", tau_bar}, {"tau+", tau_bar}, {"antitau", antitau},
        {"-15",static_cast<ParticleID>(-15)},
      {"nu_tau_bar", nu_tau_bar}, {"tau_antineutrino", tau_antineutrino},
        {"tau-antineutrino", tau_antineutrino},
        {"-16",static_cast<ParticleID>(-16)},
      {"gluon", gluon}, {"g", g}, {"21",static_cast<ParticleID>(21)},
      {"photon", photon}, {"gamma", gamma}, {"22",static_cast<ParticleID>(22)},
      {"Z", Z}, {"23",static_cast<ParticleID>(23)},
      {"Z_photon_mix", Z_photon_mix}, {"Z_gamma_mix", Z_gamma_mix},
        {"Z/photon superposition",Z_photon_mix}, {"81",static_cast<ParticleID>(81)},
      {"Wp", Wp}, {"W+", Wp}, {"24",static_cast<ParticleID>(24)},
      {"Wm", Wm}, {"W-", Wm}, {"-24",static_cast<ParticleID>(-24)},
      {"h", h}, {"H", h}, {"Higgs", Higgs}, {"higgs", higgs},
        {"25",static_cast<ParticleID>(25)},
      {"p", p}, {"proton", proton}, {"2212",static_cast<ParticleID>(2212)},
      {"p_bar", p_bar}, {"antiproton", antiproton}, {"-2212",static_cast<ParticleID>(-2212)}
    };
    const auto res = known.find(name);
    if(res == known.end()){
      throw std::invalid_argument("Unknown particle " + name);
    }
    return res->second;
  }

namespace pid {
  std::string name(ParticleID id) {
    using namespace HEJ::pid;
    switch (id) {
    case unspecified: return "unspecified";
    case down: return "down";
    case up: return "up";
    case strange: return "strange";
    case charm: return "charm";
    case bottom: return "bottom";
    case top: return "top";
    case electron: return "electron";
    case muon: return "muon";
    case tau: return "tau";
    case electron_neutrino: return "electron-neutrino";
    case muon_neutrino: return "muon-neutrino";
    case tau_neutrino: return "tau-neutrino";
    case antidown: return "antidown";
    case antiup: return "antiup";
    case antistrange: return "antistrange";
    case anticharm: return "anticharm";
    case antibottom: return "antibottom";
    case antitop: return "antitop";
    case positron: return "positron";
    case antimuon: return "antimuon";
    case antitau: return "antitau";
    case electron_antineutrino: return "electron-antineutrino";
    case muon_antineutrino: return "muon-antineutrino";
    case tau_antineutrino: return "tau-antineutrino";
    case gluon: return "gluon";
    case photon: return "photon";
    case Z: return "Z";
    case Z_photon_mix: return "Z/photon superposition";
    case Wp: return "W+";
    case Wm: return "W-";
    case Higgs: return "Higgs";
    case proton: return "proton";
    case antiproton: return "antiproton";
    }
    throw std::logic_error{"unreachable"};
  }

  ParticleID anti(ParticleID const id){
      return static_cast<ParticleID>(-id);
  }
} // namespace pid

  boost::rational<int> charge(ParticleID id) {
    using Ratio = boost::rational<int>;
    using namespace pid;
    if(is_antiparticle(id)) return -charge(anti(id));
    switch(id) {
    case electron:
    case muon:
    case tau:
      return Ratio{-1};

    case down:
    case strange:
    case bottom:
      return Ratio{-1, 3};

    case up:
    case charm:
    case top:
      return Ratio{2, 3};

    case Wp:
    case proton:
      return Ratio{1};
    default:
      return Ratio{0};
    }
  }

} // namespace HEJ

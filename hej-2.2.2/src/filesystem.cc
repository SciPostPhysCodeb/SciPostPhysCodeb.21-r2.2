#include "HEJ/filesystem.hh"

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

namespace HEJ {
  bool is_regular(char const * filename) {
    struct stat buf;
    const int err = stat(filename, &buf);
    if(err) {
      return false;
    }
    return S_ISREG(buf.st_mode);
  }
}

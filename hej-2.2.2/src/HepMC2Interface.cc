/**
 *  \authors   The HEJ collaboration (see AUTHORS for details)
 *  \date      2019-2020
 *  \copyright GPLv2 or later
 */
#include "HEJ/HepMC2Interface.hh"

#include "HEJ/ConfigFlags.hh"
#include "HEJ/exceptions.hh"

#ifdef HEJ_BUILD_WITH_HepMC2

#include <cassert>
#include <cmath>
#include <utility>

#include "LHEF/LHEF.h"

#include "HepMC/GenCrossSection.h"
#include "HepMC/GenEvent.h"
#include "HepMC/GenParticle.h"
#include "HepMC/GenVertex.h"
#include "HepMC/SimpleVector.h"
#include "HepMC/Units.h"

#include "HEJ/Event.hh"
#include "HEJ/Particle.hh"
#include "HEJ/detail/HepMCInterface_common.hh"

#else // no HepMC2

#include "HEJ/utility.hh"

#endif

#ifdef HEJ_BUILD_WITH_HepMC2

namespace HEJ {

  namespace detail_HepMC {
    template<>
    struct HepMCVersion<2> {
      using GenEvent = HepMC::GenEvent;
      using Beam = std::array<HepMC::GenParticle*,2>;
    };

    template<>
    auto make_particle_ptr<2> (
        Particle const & sp, int status
    ) {
      return new HepMC::GenParticle(
          to_FourVector<HepMC::FourVector>(sp),
          static_cast<int> (sp.type),
          status
      );
    }

    template<>
    auto make_vx_ptr<2>() {
      return new HepMC::GenVertex();
    }
  } // namespace detail_HepMC

  HepMC2Interface::HepMC2Interface(LHEF::HEPRUP const & heprup):
    beam_particle_{static_cast<ParticleID>(heprup.IDBMUP.first),
                   static_cast<ParticleID>(heprup.IDBMUP.second)},
    beam_energy_{heprup.EBMUP.first, heprup.EBMUP.second},
    event_count_(0.), tot_weight_(0.), tot_weight2_(0.)
    {}

  HepMC::GenCrossSection HepMC2Interface::cross_section() const {
    HepMC::GenCrossSection xs;
    xs.set_cross_section(
      xs_scale_ * tot_weight_,
      xs_scale_ * std::sqrt(tot_weight2_)
    );
    return xs;
  }

  void HepMC2Interface::set_xs_scale(const double scale) {
    xs_scale_ = scale;
  }

  HepMC::GenEvent HepMC2Interface::init_event(Event const & event) const {

    const std::array<HepMC::GenParticle*,2> beam {
      new HepMC::GenParticle(
        HepMC::FourVector(0,0,-beam_energy_[0],beam_energy_[0]),
        beam_particle_[0], detail_HepMC::Status::beam ),
      new HepMC::GenParticle(
        HepMC::FourVector(0,0, beam_energy_[1],beam_energy_[1]),
        beam_particle_[1], detail_HepMC::Status::beam )
    };

    auto hepmc_ev{ detail_HepMC::HepMC_init_kinematics<2>(
        event, beam, HepMC::GenEvent{ HepMC::Units::GEV, HepMC::Units::MM }
    ) };
    hepmc_ev.weights().push_back( event.central().weight );
    for(auto const & var: event.variations()){
      hepmc_ev.weights().push_back( var.weight );
      // no weight name for HepMC2 since rivet3 seem to mix them up
      // (could be added via hepmc_ev.weights()[name]=weight)
    }
    return hepmc_ev;
  }

  void HepMC2Interface::set_central(
    HepMC::GenEvent & out_ev, Event const & event, int const weight_index
  ){
    EventParameters event_param;
    if(weight_index < 0)
      event_param = event.central();
    else if ( static_cast<std::size_t>(weight_index) < event.variations().size())
      event_param = event.variations(weight_index);
    else
      throw std::invalid_argument{
         "HepMC2Interface tried to access a weight outside of the variation range."
      };
    const double wt = event_param.weight;
    tot_weight_ += wt;
    tot_weight2_ += wt * wt;
    ++event_count_;

    // central always on first
    assert(out_ev.weights().size() == event.variations().size()+1);
    out_ev.weights()[0] = wt;

    out_ev.set_cross_section( cross_section() );
    out_ev.set_signal_process_id(event.type());
    out_ev.set_event_scale(event_param.mur);

    out_ev.set_event_number(event_count_);

    /// @TODO add alphaQCD (need function) and alphaQED
    /// @TODO output pdf (currently not avaiable from event alone)
  }

  HepMC::GenEvent HepMC2Interface::operator()(Event const & event,
      int const weight_index
  ){
    HepMC::GenEvent out_ev(init_event(event));
    set_central(out_ev, event, weight_index);
    return out_ev;
  }

} // namespace HEJ
#else // no HepMC2 => empty class
namespace HepMC {
  class GenEvent {};
  class GenCrossSection {};
}
namespace HEJ {
  HepMC2Interface::HepMC2Interface(LHEF::HEPRUP const & /*heprup*/){
    ignore(beam_particle_,beam_energy_,event_count_,tot_weight_,tot_weight2_);
    throw std::invalid_argument(
        "Failed to create HepMC2Interface: "
        "HEJ 2 was built without HepMC2 support"
    );
  }

  HepMC::GenEvent HepMC2Interface::operator()(
      Event const & /*event*/, int /*weight_index*/
  ){return HepMC::GenEvent();}
  HepMC::GenEvent HepMC2Interface::init_event(Event const & /*event*/) const
  {return HepMC::GenEvent();}
  void HepMC2Interface::set_central(
      HepMC::GenEvent & /*out_ev*/, Event const & /*event*/, int /*weight_index*/
  ){}
  HepMC::GenCrossSection HepMC2Interface::cross_section() const
  {return HepMC::GenCrossSection();}
}
#endif

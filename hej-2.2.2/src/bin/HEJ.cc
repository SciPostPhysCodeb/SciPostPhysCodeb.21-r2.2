/**
 *  \authors   The HEJ collaboration (see AUTHORS for details)
 *  \date      2019-2022
 *  \copyright GPLv2 or later
 */
#include <array>
#include <chrono>
#include <iostream>
#include <limits>
#include <memory>
#include <numeric>
#include <optional>


#include "yaml-cpp/yaml.h"

#include "fastjet/ClusterSequence.hh"

#include "HEJ/CombinedEventWriter.hh"
#include "HEJ/Config.hh"
#include "HEJ/CrossSectionAccumulator.hh"
#include "HEJ/Event.hh"
#include "HEJ/EventReader.hh"
#include "HEJ/BufferedEventReader.hh"
#include "HEJ/EventReweighter.hh"
#include "HEJ/filesystem.hh"
#include "HEJ/get_analysis.hh"
#include "HEJ/make_RNG.hh"
#include "HEJ/ProgressBar.hh"
#include "HEJ/stream.hh"
#include "HEJ/Unweighter.hh"
#include "HEJ/Version.hh"
#include "HEJ/YAMLreader.hh"

HEJ::Config load_config(char const * filename){
  try{
    return HEJ::load_config(filename);
  }
  catch(std::exception const & exc){
    std::cerr << "Error: " << exc.what() << '\n';
    std::exit(EXIT_FAILURE);
  }
}

std::vector<std::unique_ptr<HEJ::Analysis>> get_analyses(
    std::vector<YAML::Node> const & parameters, LHEF::HEPRUP const & heprup
){
  try{
    return HEJ::get_analyses(parameters, heprup);
  }
  catch(std::exception const & exc){
    std::cerr << "Failed to load analysis: " << exc.what() << '\n';
    std::exit(EXIT_FAILURE);
  }
}

std::string time_to_string(const time_t time){
  char s[30];
  struct tm * p = localtime(&time);
  strftime(s, 30, "%a %b %d %Y %H:%M:%S", p);
  return s;
}

void check_one_parton_per_jet(HEJ::Event const & ev) {
  const std::size_t npartons = std::count_if(
    ev.outgoing().begin(), ev.outgoing().end(),
    [](HEJ::Particle const & p) { return is_parton(p); }
  );
  if(ev.jets().size() < npartons) {
    throw std::invalid_argument{
      "Number of partons (" + std::to_string(npartons) + ") in input event"
        " does not match number of jets (" + std::to_string(ev.jets().size())+  ")"
      };
  }
}

HEJ::Event to_event(
    LHEF::HEPEUP const & hepeup,
    HEJ::JetParameters const & fixed_order_jets,
    HEJ::EWConstants const & ew_parameters,
    const double off_shell_tolerance
) {
    HEJ::Event::EventData event_data{hepeup};
    event_data.reconstruct_intermediate(ew_parameters);
    event_data.repair_momenta(off_shell_tolerance);

    HEJ::Event ev{
      std::move(event_data).cluster(
          fixed_order_jets.def, fixed_order_jets.min_pt
      )
    };
    check_one_parton_per_jet(ev);
    return ev;
}

void unweight(
  HEJ::Unweighter & unweighter,
  HEJ::WeightType weight_type,
  std::vector<HEJ::Event> & events,
  HEJ::RNG & ran
) {
    if(weight_type == HEJ::WeightType::unweighted_resum){
      unweighter.set_cut_to_maxwt(events);
    }
    events.erase(
      unweighter.unweight(begin(events), end(events), ran),
      end(events)
    );
}

// peek up to nevents events from reader
std::vector<LHEF::HEPEUP> peek_events(
    HEJ::BufferedEventReader & reader,
    const int nevents
) {
  std::vector<LHEF::HEPEUP> events;
  while(
      static_cast<int>(events.size()) < nevents
      && reader.read_event()
  ) {
    events.emplace_back(reader.hepeup());
  }
  // put everything back into the reader
  for(auto it = rbegin(events); it != rend(events); ++it) {
    reader.emplace(*it);
  }
  return events;
}

void append_resummed_events(
    std::vector<HEJ::Event> & resummation_events,
    HEJ::EventReweighter & reweighter,
    LHEF::HEPEUP const & hepeup,
    const size_t trials,
    HEJ::JetParameters const & fixed_order_jets,
    HEJ::EWConstants const & ew_parameters,
    const double off_shell_tolerance
) {
  const HEJ::Event FO_event = to_event(
    hepeup,
    fixed_order_jets,
    ew_parameters,
    off_shell_tolerance
  );
  if(reweighter.treatment(FO_event.type()) != HEJ::EventTreatment::reweight) {
    return;
  }
  const auto resummed = reweighter.reweight(FO_event, trials);
  resummation_events.insert(
      end(resummation_events),
      begin(resummed), end(resummed)
  );
}

std::optional<HEJ::ProgressBar<std::size_t>> make_progress_bar(
  std::optional<std::size_t> n_input_events,
  char const * event_file
) {
  if(n_input_events) {
    return HEJ::ProgressBar{std::cout, *n_input_events};
  }
  if(HEJ::is_regular(event_file)) {
      auto t_reader = HEJ::make_reader(event_file);
      std::size_t n_input_events = 0;
      while(t_reader->read_event()) ++n_input_events;
      return HEJ::ProgressBar{std::cout, n_input_events};
  }
  return {};
}

void train(
    HEJ::Unweighter & unweighter,
    HEJ::BufferedEventReader & reader,
    HEJ::EventReweighter & reweighter,
    const size_t total_trials,
    const double max_dev,
    double reweight_factor,
    HEJ::JetParameters const & fixed_order_jets,
    HEJ::EWConstants const & ew_parameters,
    const double off_shell_tolerance
) {
  std::cout << "Reading up to " << total_trials << " training events...\n";
  auto FO_events = peek_events(reader, total_trials);
  if(FO_events.empty()) {
    throw std::runtime_error{
      "No events generated to calibrate the unweighting weight!"
      "Please increase the number \"trials\" or deactivate the unweighting."
    };
  }
  const size_t trials = total_trials/FO_events.size();
  // adjust reweight factor so that the overall normalisation
  // is the same as in the full run
  reweight_factor *= trials;
  for(auto & hepeup: FO_events) {
    hepeup.XWGTUP *= reweight_factor;
  }
  std::cout << "Training unweighter with "
            << trials << '*' << FO_events.size() << " events\n";

  auto progress = HEJ::ProgressBar<int>{
     std::cout, static_cast<int>(FO_events.size())
  };
  std::vector<HEJ::Event> resummation_events;
  for(auto const & hepeup: FO_events) {
    append_resummed_events(
        resummation_events,
        reweighter, hepeup, trials, fixed_order_jets,
        ew_parameters,
        off_shell_tolerance
    );
    ++progress;
  }

  unweighter.set_cut_to_peakwt(resummation_events, max_dev);
  std::cout << "\nUnweighting events with weight up to "
            << unweighter.get_cut() << '\n';
}

int main(int argn, char** argv) {
  using clock = std::chrono::system_clock;

  if (argn != 3) {
    std::cerr << "\n# Usage:\n."<< argv[0] <<" config_file input_file\n\n";
    return EXIT_FAILURE;
  }

  const auto start_time = clock::now();
  {
    std::cout << "Starting " << HEJ::Version::package_name_full()
      << ", revision " << HEJ::Version::revision() << " ("
      << time_to_string(clock::to_time_t(start_time)) << ")" << std::endl;
  }
  fastjet::ClusterSequence::print_banner();

  // read configuration
  const HEJ::Config config = load_config(argv[1]);
  auto reader = HEJ::make_reader(argv[2]);
  assert(reader);

  auto heprup{ reader->heprup() };
  heprup.generators.emplace_back(LHEF::XMLTag{});
  heprup.generators.back().name = HEJ::Version::package_name();
  heprup.generators.back().version = HEJ::Version::String();

  auto analyses = get_analyses( config.analyses_parameters, heprup );
  assert(analyses.empty() || analyses.front() != nullptr);

  HEJ::CombinedEventWriter writer{config.output, std::move(heprup)};

  double global_reweight = 1.;
  auto const & max_events = config.max_events;

  // if we need the event number:
  if(std::abs(heprup.IDWTUP) == 4 || std::abs(heprup.IDWTUP) == 1 || max_events){
    // try to read from LHE head
    auto input_events{reader->number_events()};
    if(!input_events) {
      // else count manually
      auto t_reader = HEJ::make_reader(argv[2]);
      input_events = 0;
      while(t_reader->read_event()) ++(*input_events);
    }
    if(std::abs(heprup.IDWTUP) == 4 || std::abs(heprup.IDWTUP) == 1){
      // IDWTUP 4 or 1 assume average(weight)=xs, but we need sum(weights)=xs
      std::cout << "Found IDWTUP " << heprup.IDWTUP << ": "
        << "assuming \"cross section = average weight\".\n"
        << "converting to \"cross section = sum of weights\" ";
      global_reweight /= *input_events;
    }
    if(max_events && (*input_events > *max_events)){
      // maximal number of events given
      global_reweight *= *input_events/static_cast<double>(*max_events);
      std::cout << "Processing " << *max_events
                << " out of " << *input_events << " events\n";
    }
  }

  HEJ::ScaleGenerator scale_gen{
    config.scales.base,
    config.scales.factors,
    config.scales.max_ratio
  };
  std::shared_ptr<HEJ::RNG> ran{
    HEJ::make_RNG(config.rng.name, config.rng.seed)};
  assert(ran != nullptr);
  HEJ::EventReweighter hej{
    reader->heprup(),
    std::move(scale_gen),
    to_EventReweighterConfig(config),
    ran
  };

  std::optional<HEJ::Unweighter> unweighter{};
  if(config.weight_type != HEJ::WeightType::weighted) {
    unweighter = HEJ::Unweighter{};
  }
  if(config.weight_type == HEJ::WeightType::partially_unweighted) {
    HEJ::BufferedEventReader buffered_reader{std::move(reader)};
    assert(config.unweight_config);
    train(
        *unweighter,
        buffered_reader,
        hej,
        config.unweight_config->trials,
        config.unweight_config->max_dev,
        global_reweight/config.trials,
        config.fixed_order_jets,
        config.ew_parameters,
        config.off_shell_tolerance
    );
    reader = std::make_unique<HEJ::BufferedEventReader>(
        std::move(buffered_reader)
    );
  }

  // status infos & eye candy
  if (config.nlo.enabled){
    std::cout << "HEJ@NLO Truncation Enabled for NLO order: "
    << config.nlo.nj << std::endl;
  }
  size_t nevent = 0;
  std::array<int, HEJ::event_type::last_type + 1>
    nevent_type{0}, nfailed_type{0};

  auto progress = make_progress_bar(reader->number_events(), argv[2]);
  if(!progress) {
    std::cout
      << "Cannot determine number of input events. "
      "Disabling progress bar"
      << std::endl;
  }

  HEJ::CrossSectionAccumulator xs;
  std::map<HEJ::StatusCode, int> status_counter;
  size_t total_trials = 0;
  size_t total_resum = 0;

  // Loop over the events in the input file
  while(reader->read_event() && (!max_events || nevent < *max_events) ){
    ++nevent;

    // reweight events so that the total cross section is conserved
    auto hepeup = reader->hepeup();
    hepeup.XWGTUP *= global_reweight;

    const auto FO_event = to_event(
      hepeup,
      config.fixed_order_jets,
      config.ew_parameters,
      config.off_shell_tolerance
    );
    if(FO_event.central().weight == 0) {
      static const bool warned_once = [argv,nevent](){
        std::cerr
          << "WARNING: event number " << nevent
          << " in " << argv[2] << " has zero weight. "
          "Ignoring this and all further events with vanishing weight.\n";
        return true;
      }();
      (void) warned_once; // shut up compiler warnings
      continue;
    }

    auto resummed_events{ hej.reweight(FO_event, config.trials) };

    // some bookkeeping
    for(auto const & s: hej.status())
      ++status_counter[s];
    total_trials+=hej.status().size();
    ++nevent_type[FO_event.type()];

    if(resummed_events.empty()) ++nfailed_type[FO_event.type()];

    if(unweighter) {
      unweight(*unweighter, config.weight_type, resummed_events, *ran);
    }

    // analysis
    for(auto & ev: resummed_events){
      //TODO: move pass_cuts to after phase space point generation
      bool passed = analyses.empty();
      for(auto const & analysis: analyses){
        analysis->set_xs_scale(reader->scalefactor());
        if(analysis->pass_cuts(ev, FO_event)){
          passed = true;
          analysis->fill(ev, FO_event);
        };
      }
      if(passed){
        writer.set_xs_scale(reader->scalefactor());
        writer.write(ev);
      } else {
        ev.parameters()*=0; // do not use discarded events afterwards
      }
    }
    xs.fill_correlated(resummed_events);
    total_resum += resummed_events.size();
    if(progress) ++*progress;
  } // main event loop
  std::cout << '\n';
  // scale to account for num_trials of sherpa
  xs.scale(reader->scalefactor());

  for(auto const & analysis: analyses){
    analysis->finalise();
  }
  writer.finish();

  using namespace HEJ::event_type;
  std::cout<< "Events processed: " << nevent << " (" << total_resum << " resummed)"<< '\n';
  std::cout << '\t' << name(EventType::first_type) << ": "
            << nevent_type[EventType::first_type]
            << ", failed to reconstruct " << nfailed_type[EventType::first_type]
            << '\n';
  for(auto i=EventType::first_type+1; i<=EventType::last_type; i*=2){
    std::cout << '\t' << name(static_cast<EventType>(i)) << ": "
              << nevent_type[i]
              << ", failed to reconstruct " << nfailed_type[i]
              << '\n';
  }

  std::cout << '\n' << xs << '\n';

  std::cout << "Generation statistic: "
    << status_counter[HEJ::StatusCode::good] << "/" << total_trials
    << " trials successful.\n";
  for(auto && entry: status_counter){
    const double fraction = static_cast<double>(entry.second)/total_trials;
    const int percent = std::round(100*fraction);
    std::cout << std::left << std::setw(17) << (to_string(entry.first) + ":")
              << " [";
    for(int i = 0; i < percent/2; ++i) std::cout << '#';
    for(int i = percent/2; i < 50; ++i) std::cout << ' ';
    std::cout << "] " <<std::setw(2)<<std::right<< percent << "%\n";
  }

  std::chrono::duration<double> run_time = (clock::now() - start_time);
  std::cout << "\nFinished " << HEJ::Version::package_name() << " at "
    << time_to_string(clock::to_time_t(clock::now()))
    << "\n=> Runtime: " << run_time.count() << " sec ("
    << nevent/run_time.count() << " Events/sec).\n";

  return EXIT_SUCCESS;
}

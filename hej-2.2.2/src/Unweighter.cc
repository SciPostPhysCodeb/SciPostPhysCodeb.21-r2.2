/**
 *  \authors   The HEJ collaboration (see AUTHORS for details)
 *  \date      2019-2020
 *  \copyright GPLv2 or later
 */
#include "HEJ/Unweighter.hh"

namespace HEJ {

  bool Unweighter::discard(RNG & ran, Event & ev) const {
    const double awt = std::abs(ev.central().weight);
    if(awt >= get_cut() ) return false;
    if(ran.flat() > awt/get_cut()) return true;
    ev.parameters() *= get_cut()/awt;
    return false;
  }

  std::optional<Event> Unweighter::unweight(Event ev, RNG & ran) const {
    if(discard(ran, ev))
      return {};
    return ev;
  }

  std::vector<Event> Unweighter::unweight(
       std::vector<Event> events, RNG & ran
  ) const {
    events.erase(
      unweight(events.begin(), events.end(), ran),
      events.end() );
    return events;
  }
} // namespace HEJ

/**
 *  \authors   The HEJ collaboration (see AUTHORS for details)
 *  \date      2019-2020
 *  \copyright GPLv2 or later
 */
#include "HEJ/PDF.hh"

#include <cmath>
#include <cstdlib>
#include <iostream>
#include <stdexcept>
#include <string>

#include "LHAPDF/LHAPDF.h"

namespace HEJ {

  namespace {

    int to_beam(ParticleID id){
      if(std::abs(id) == pid::proton){
        return (id > 0)?1:-1;
      }
      throw std::invalid_argument(
          "unknown beam type: " + std::to_string(id)
      );
    }

  } // namespace

  PDF::PDF(int id, ParticleID beam1, ParticleID beam2):
    pdf_{LHAPDF::mkPDF(id)},
    beamtype_{{to_beam(beam1), to_beam(beam2)}}
  {}

  double PDF::pdfpt(size_t beam_idx, double x, double q, ParticleID id) const{
    if(!(inRangeQ(q) && inRangeX(x))) return 0.;

    if(id == pid::gluon){
      return pdf_->xfxQ(pid::gluon,x,q);
    }
    if(std::abs(id) <= pid::top){
      return pdf_->xfxQ(id*beamtype_[beam_idx],x,q);
    }
    std::cerr << "particle type unknown: "<< id << std::endl;
    return 0.0;

  }

  double PDF::Halphas(double q) const{
    double as = pdf_->alphasQ(q);
    if (std::isnan(as) || as > 0.5) {
      as = 0.5;
    }
    return as;
  }

  int PDF::id() const{
    return pdf_->lhapdfID();
  }

  bool PDF::inRangeQ(double q) const{
    return pdf_->inRangeQ(q);
  }

  bool PDF::inRangeX(double x) const{
    return pdf_->inRangeX(x);
  }

  // can't be in header since we forward declare LHAPDF::PDF
  PDF::~PDF() = default;
  PDF::PDF(PDF && other) noexcept = default;
  PDF & PDF::operator=(PDF && other) noexcept = default;


} // namespace HEJ

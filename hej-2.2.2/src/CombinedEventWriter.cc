/**
 *  \authors   The HEJ collaboration (see AUTHORS for details)
 *  \date      2019-2020
 *  \copyright GPLv2 or later
 */
#include "HEJ/CombinedEventWriter.hh"

#include <iostream>

#include "HEJ/exceptions.hh"
#include "HEJ/make_writer.hh"
#include "HEJ/output_formats.hh"

namespace HEJ {

  CombinedEventWriter::CombinedEventWriter(
      std::vector<OutputFile> const & outfiles,
      LHEF::HEPRUP const & heprup
  ){
    writers_.reserve(outfiles.size());
    for(OutputFile const & outfile: outfiles){
      writers_.emplace_back(
          make_format_writer(outfile.format, outfile.name, heprup)
      );
    }
  }

  void CombinedEventWriter::write(Event const & ev){
    for(auto & writer: writers_) writer->write(ev);
  }

  void CombinedEventWriter::set_xs_scale(const double scale) {
    for(auto & writer: writers_) {
      writer->set_xs_scale(scale);
    }
  }

  void CombinedEventWriter::finish(){
    EventWriter::finish();
    for(auto & writer: writers_) writer->finish();
  }

  CombinedEventWriter::~CombinedEventWriter(){
    finish_or_abort(this, "CombinedEventWriter");
  }

} // namespace HEJ

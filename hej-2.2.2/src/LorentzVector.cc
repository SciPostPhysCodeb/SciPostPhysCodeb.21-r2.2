/**
 *  \authors   The HEJ collaboration (see AUTHORS for details)
 *  \date      2020
 *  \copyright GPLv2 or later
 */
#include "HEJ/LorentzVector.hh"

#include <cmath>
#include <utility>

namespace HEJ {

  namespace {
    using COM = std::complex<double>;

    COM perp(CLHEP::HepLorentzVector const & p) {
      return COM{p.x(), p.y()};
    }

    void angle_aux(CLHEP::HepLorentzVector const & p, COM & perp_hat, double & p_plus, double & p_minus) {
      const COM p_perp = perp(p);
      if(p_perp == COM{0., 0.}){
        perp_hat = -1.;
        if(std::abs(std::abs(p.z())/p.t() - 1.) > 1e-9){
          throw std::invalid_argument("zero pT particle with |pz| != E");
        }
        if(p.z() > 0){
          p_plus  = p.plus();
          p_minus = 0.;
        }else{
          p_plus  = 0.;
          p_minus = p.minus();
        }
      }else{
        perp_hat = p_perp/std::abs(p_perp);
        p_plus  = p.plus();
        p_minus = p.minus();
      }
    }
  } // anonymous namespace

  // "angle" product angle(pi, pj) = <i j>
  // see eq. (53) (\eqref{eq:angle_product}) in developer manual
  COM angle(
    CLHEP::HepLorentzVector const & pi,
    CLHEP::HepLorentzVector const & pj
  ) {
    COM pi_perp_hat;
    COM pj_perp_hat;
    double pi_plus = NAN;
    double pi_minus = NAN;
    double pj_plus = NAN;
    double pj_minus = NAN;
    angle_aux(pi, pi_perp_hat, pi_plus, pi_minus);
    angle_aux(pj, pj_perp_hat, pj_plus, pj_minus);
    return
      + std::sqrt(pi_minus*pj_plus)*pi_perp_hat
      - std::sqrt(pi_plus*pj_minus)*pj_perp_hat;
  }

  // "square" product square(pi, pj) = [i j]
  // see eq. (54) (\eqref{eq:square_product}) in developer manual
  COM square(
    CLHEP::HepLorentzVector const & pi,
    CLHEP::HepLorentzVector const & pj
  ) {
    return -std::conj(angle(pi, pj));
  }

  // see eq:P_massive, eq:P_massive_p, eq:P_massive_q, , eq:P_massive_plus,
  // eq:P_massive_minus in the developer manual
  std::pair<CLHEP::HepLorentzVector, CLHEP::HepLorentzVector>
  split_into_lightlike(CLHEP::HepLorentzVector const & P) {
    const double pt = P.perp();
    if(P.plus() > 0){
      const double y = std::log(P.plus()/pt);
      const double E = P.m2()/(2.*P.plus());
      return std::make_pair(
        CLHEP::HepLorentzVector{P.x(), P.y(), pt*std::sinh(y), pt*std::cosh(y)},
        CLHEP::HepLorentzVector{0., 0., -E, E}
      );
    }
    const double y = std::log(pt/P.minus());
    const double E = P.m2()/(2.*P.minus());
    return std::make_pair(
      CLHEP::HepLorentzVector{P.x(), P.y(), pt*std::sinh(y), pt*std::cosh(y)},
      CLHEP::HepLorentzVector{0., 0., +E, E}
    );
  }
} // namespace HEJ

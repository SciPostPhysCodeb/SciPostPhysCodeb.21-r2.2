/**
 *  \authors   The HEJ collaboration (see AUTHORS for details)
 *  \date      2019-2020
 *  \copyright GPLv2 or later
 */
#include "HEJ/Ranlux64.hh"

#include <cstdio>

namespace HEJ {
  namespace {
    //! create Ranlux64Engine with state read from the given file
    CLHEP::Ranlux64Engine make_Ranlux64Engine(std::string const & seed_file) {
      CLHEP::Ranlux64Engine result;
      result.restoreStatus(seed_file.c_str());
      return result;
    }

    CLHEP::Ranlux64Engine make_Ranlux64Engine() {
      /*
       * some (all?) of the Ranlux64Engine constructors leave fields
       * uninitialised, invoking undefined behaviour. This can be
       * circumvented by restoring the state from a file
       */
      static const std::string state =
        "9876\n"
        "0.91280703978419097666\n"
        "0.41606065829518357191\n"
        "0.99156342622341142601\n"
        "0.030922955274050423213\n"
        "0.16206278421638486975\n"
        "0.76151768001958330956\n"
        "0.43765760066092695979\n"
        "0.42904698253748563275\n"
        "0.11476317525663759511\n"
        "0.026620053590963976831\n"
        "0.65953715764414511114\n"
        "0.30136722624439826745\n"
        "3.5527136788005009294e-15 4\n"
        "1 202\n";

      std::FILE* tmpf = std::tmpfile();
      std::fputs(state.c_str(),tmpf);
      auto result = make_Ranlux64Engine("/proc/self/fd/"+std::to_string(fileno(tmpf)));
      return result;
    }
  } // namespace

  Ranlux64::Ranlux64(): ran_{make_Ranlux64Engine()} {}

  Ranlux64::Ranlux64(std::string const & seed_file):
    ran_{make_Ranlux64Engine(seed_file)}
  {}

  double Ranlux64::flat() {
    return ran_.flat();
  }
} // namespace HEJ

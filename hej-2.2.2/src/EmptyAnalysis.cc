/**
 *  \authors   The HEJ collaboration (see AUTHORS for details)
 *  \date      2019-2020
 *  \copyright GPLv2 or later
 */
#include "HEJ/EmptyAnalysis.hh"

#include <string>
#include <vector>
#include <iostream>

#include "yaml-cpp/yaml.h"

#include "HEJ/exceptions.hh"

namespace HEJ::detail {
  namespace {
    std::vector<std::string> param_as_strings(YAML::Node const & parameters){
      using YAML::NodeType;
      switch(parameters.Type()){
      case NodeType::Null:
      case NodeType::Undefined:
        return {};
      case NodeType::Scalar:
        return {parameters.as<std::string>()};
      case NodeType::Sequence: {
        std::vector<std::string> param_strings;
        for(auto && param: parameters){
          param_strings.emplace_back(param.as<std::string>());
        }
        return param_strings;
      }
      case NodeType::Map: {
        std::vector<std::string> param_strings;
        for(auto && param: parameters){
          param_strings.emplace_back(param.first.as<std::string>());
        }
        return param_strings;
      }
      default:;
      }
      throw std::logic_error{"unreachable"};
    }
  } // namespace

  std::unique_ptr<Analysis> EmptyAnalysis::create(
      YAML::Node const & parameters, LHEF::HEPRUP const & /*unused*/
  ){
    const auto param_strings = param_as_strings(parameters);
    if(! param_strings.empty()){
      std::string error{"Unknown analysis parameter(s):"};
      for(auto && p: param_strings) error += " " + p;
      throw unknown_option{error};
    }
    return std::make_unique<EmptyAnalysis>();
  }

  void EmptyAnalysis::fill(
      Event const & /*res_event*/, Event const & /*FO_event*/
  ){
    // do nothing
  }

  bool EmptyAnalysis::pass_cuts(
      Event const & /*res_event*/, Event const & /*FO_event*/
  ){
    return true;
  }

  void EmptyAnalysis::set_xs_scale(double /*xsscale*/){
    // do nothing
  }

  void EmptyAnalysis::finalise(){
    // do nothing
  }

} // namespace HEJ::detail

namespace HEJ {
  std::unique_ptr<Analysis> EmptyAnalysis::create(
    YAML::Node const & parameters, LHEF::HEPRUP const & heprup
  ){
    return detail::EmptyAnalysis::create(parameters, heprup);
  }
}

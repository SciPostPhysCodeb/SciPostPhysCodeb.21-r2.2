#include "HEJ/Particle.hh"
#include <boost/rational.hpp>

namespace HEJ {
  boost::rational<int> charge(Particle const & p){
    return charge(p.type);
  }
}

/**
 *  \authors   The HEJ collaboration (see AUTHORS for details)
 *  \date      2019-2020
 *  \copyright GPLv2 or later
 */
#include "HEJ/RivetAnalysis.hh"

#include "HEJ/ConfigFlags.hh"
#include "HEJ/utility.hh"

#ifdef HEJ_BUILD_WITH_RIVET

#include <cstddef>
#include <ostream>
#include <utility>

#include "yaml-cpp/yaml.h"

#include "Rivet/AnalysisHandler.hh"
#include "Rivet/Config/RivetConfig.hh"

#ifdef RIVET_ENABLE_HEPMC_3

#include "HepMC3/GenEvent.h"
#include "HEJ/HepMC3Interface.hh"

#else // rivet with HepMC 2

#include "HEJ/HepMC2Interface.hh"
#include "HepMC/GenEvent.h"

#endif // RIVET_ENABLE_HEPMC_3

#include "HEJ/Event.hh"
#include "HEJ/exceptions.hh"

#endif

namespace HEJ {
  std::unique_ptr<Analysis> RivetAnalysis::create(
      YAML::Node const & config, LHEF::HEPRUP const & heprup
  ){
    return std::make_unique<RivetAnalysis>(config, heprup);
  }
}

#ifdef HEJ_BUILD_WITH_RIVET

namespace HEJ {
#ifdef RIVET_ENABLE_HEPMC_3
  using HepMCInterface=HepMC3Interface;
#else
  using HepMCInterface=HepMC2Interface;
#endif

  struct RivetAnalysis::rivet_info {
    rivet_info(std::unique_ptr<Rivet::AnalysisHandler> && h,
        std::string && s,  HepMCInterface && m):
      handler{std::move(h)}, name{std::move(s)}, hepmc{std::move(m)}
    {}
    // AnalysisHandler doesn't allow a copy constructor -> use ptr
    std::unique_ptr<Rivet::AnalysisHandler> handler;
    std::string name;
    HepMCInterface hepmc;
  };

  RivetAnalysis::RivetAnalysis(YAML::Node const & config,
                               LHEF::HEPRUP heprup
  ):
    heprup_{std::move(heprup)},
    first_event_(true)
  {

    // assert that only supported options are provided
    if(!config.IsMap()) throw invalid_type{"Rivet config has to be a map!"};
    for(auto const & entry: config){
      const auto name = entry.first.as<std::string>();
      if(name != "output" && name != "rivet")
        throw unknown_option{"Unknown option \'"+name+"\' provided to rivet."};
    }

    // get output name
    if(!config["output"])
      throw std::invalid_argument{
        "No output was provided to rivet. "
        "Either give an output or deactivate rivet."
      };
    if(!config["output"].IsScalar())
      throw invalid_type{
        "Output for rivet must be a scalar."
      };
    output_name_ = config["output"].as<std::string>();

    // read in analyses
    auto const & name_node = config["rivet"];
    switch(name_node.Type()){
    case YAML::NodeType::Scalar:
      analyses_names_.push_back(name_node.as<std::string>());
      break;
    case YAML::NodeType::Sequence:
      for(YAML::const_iterator it = name_node.begin(); it != name_node.end(); ++it){
        analyses_names_.push_back(it->as<std::string>());
      }
      break;
    default:
      throw std::invalid_argument{
        "No analysis was provided to rivet. "
        "Either give an analysis or deactivate rivet."
      };
    }
  }

  // it is a bit ugly that we can't do this directly in `initialise`
  void RivetAnalysis::init(Event const & event){
#ifdef HEJ_USE_RIVET2
    rivet_runs_.reserve(event.variations().size()+1);
#else
    ignore(event); // shut up compiler
#endif
    rivet_runs_.emplace_back( std::make_unique<Rivet::AnalysisHandler>(),
      std::string(""), HepMCInterface(heprup_)
    );
    rivet_runs_.back().handler->addAnalyses(analyses_names_);

#ifdef HEJ_USE_RIVET2
    //! scale variation in rivet 2 through multiple analyses
    if( !event.variations().empty() ){
      for(auto const & vari : event.variations()){
        rivet_runs_.emplace_back( std::make_unique<Rivet::AnalysisHandler>(),
          "."+to_simple_string(*vari.description), HepMCInterface(heprup_)
        );
        rivet_runs_.back().handler->addAnalyses(analyses_names_);
      }
    }
#endif
  }

  void RivetAnalysis::fill(Event const & event, Event const & /*unused*/){
    if(first_event_){
      first_event_=false;
      init(event);
    }
    auto hepmc_event(rivet_runs_.front().hepmc(event));
    rivet_runs_.front().handler->analyze(hepmc_event);

#ifdef HEJ_USE_RIVET2
    for(std::size_t i = 1; i < rivet_runs_.size(); ++i){
      auto & run = rivet_runs_[i];
      run.hepmc.set_central(hepmc_event, event, i-1);
      run.handler->analyze(hepmc_event);
    }
#endif
  }

  void RivetAnalysis::set_xs_scale(double scale) {
    for(auto & run: rivet_runs_) {
      run.hepmc.set_xs_scale(scale);
    }
  }

  void RivetAnalysis::finalise(){
    for(auto & run: rivet_runs_){
      run.handler->finalize();
      run.handler->writeData(output_name_+run.name+std::string(".yoda"));
    }
  }
} // namespace HEJ

#else // no rivet => create empty analysis

namespace Rivet {
  class AnalysisHandler {};
}

namespace HEJ {
  struct RivetAnalysis::rivet_info{};

  RivetAnalysis::RivetAnalysis(
    YAML::Node const & /*config*/, LHEF::HEPRUP /*heprup*/
  ){
    ignore(first_event_);
    throw std::invalid_argument(
        "Failed to create RivetAnalysis: "
        "HEJ 2 was built without rivet support"
    );
  }

  void RivetAnalysis::init(Event const & /*event*/){}
  void RivetAnalysis::fill(Event const & /*event*/, Event const & /*unused*/){}
  void RivetAnalysis::set_xs_scale(double /*xsscale*/){}

  void RivetAnalysis::finalise(){}
} // namespace HEJ

#endif

namespace HEJ {
  RivetAnalysis::~RivetAnalysis() = default;
}

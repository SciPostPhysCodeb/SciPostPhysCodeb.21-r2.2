/**
 *  \authors   The HEJ collaboration (see AUTHORS for details)
 *  \date      2019-2022
 *  \copyright GPLv2 or later
 */
#include "HEJ/make_RNG.hh"

#include <algorithm>
#include <locale>

#include "HEJ/Mixmax.hh"
#include "HEJ/Ranlux64.hh"
#include "HEJ/exceptions.hh"

namespace HEJ {
  std::unique_ptr<RNG> make_RNG(
      std::string const & name,
      std::optional<std::string> const & seed
  ) {
    std::string lname;
    std::transform(
        begin(name), end(name), std::back_inserter(lname),
        [](char c) { return std::tolower(c, std::locale()); }
    );
    if(lname == "mixmax") {
      if(seed) return std::make_unique<Mixmax>(std::stol(*seed));
      return std::make_unique<Mixmax>();
    }
    if(lname == "ranlux64") {
      if(seed) return std::make_unique<Ranlux64>(*seed);
      return std::make_unique<Ranlux64>();
    }
    throw std::invalid_argument{"Unknown random number generator: " + name};
  }
} // namespace HEJ

/**
 *  \authors   The HEJ collaboration (see AUTHORS for details)
 *  \date      2020-2022
 *  \copyright GPLv2 or later
 */
#include "HEJ/WWjets.hh"

#include <algorithm>
#include <array>
#include <cassert>
#include <cmath>
#include <iostream>

#include "HEJ/Constants.hh"
#include "HEJ/EWConstants.hh"
#include "HEJ/LorentzVector.hh"
#include "HEJ/exceptions.hh"

// generated headers
#include "HEJ/currents/jV_jV.hh"

namespace HEJ {
namespace currents {

  namespace {
    using std::conj;
    using COM = std::complex<double>;

    // W Propagator
    double WProp(const HLV & plbar, const HLV & pl,
                 ParticleProperties const & wprop
    ){
      COM propW = COM(0.,-1.)/( (pl+plbar).m2() - wprop.mass*wprop.mass
                              + COM(0.,1.)*wprop.mass*wprop.width);
      double PropFactor=(propW*conj(propW)).real();
      return PropFactor;
    }
    //! WW+Jets FKL Contributions
    /**
     * @brief WW+Jets Currents
     * @param p1out             Outgoing Particle 1 (W1 emission)
     * @param pl1bar            Outgoing anti-lepton 1 momenta
     * @param pl1               Outgoing lepton 1 momenta
     * @param p1in              Incoming Particle 1 (W1 emission)
     * @param p2out             Outgoing Particle 2 (W2 emission)
     * @param pl2bar            Outgoing anti-lepton 2 momenta
     * @param pl2               Outgoing lepton 2 momenta
     * @param p2in              Incoming Particle 2 (W2 emission)
     * @param aqlineb           Bool. Is Backwards quark line an anti-quark line?
     * @param aqlinef           Bool. Is Forwards quark line an anti-quark line?
     *
     * Calculates jW^\mu jW_\mu
     */
    template<Helicity h1, Helicity h2>
    std::vector <double> jW_jW(
        const HLV & p1out, const HLV & pl1bar, const HLV & pl1, const HLV & p1in,
        const HLV & p2out, const HLV & pl2bar, const HLV & pl2, const HLV & p2in,
        ParticleProperties const & wprop
    ){
      using helicity::minus;

      const COM amp_top = jV_jV<h1,minus,h2,minus>(p1in,p1out,p2in,p2out,pl1,pl1bar,pl2,pl2bar);
      const COM amp_bot = jV_jV<h1,minus,h2,minus>(p1in,p1out,p2in,p2out,pl2,pl2bar,pl1,pl1bar);

      const double res_top = norm(amp_top);
      const double res_bot = norm(amp_bot);
      const double res_mix = 2.*real(amp_top*conj(amp_bot));

      const double pref = WProp(pl1bar, pl1, wprop) * WProp(pl2bar, pl2, wprop);

      return {res_top*pref, res_bot*pref, res_mix*pref};
    }
  } // namespace

  std::vector <double> ME_WW_qQ(
      const HLV & p1out, const HLV& pl1bar, const HLV& pl1, const HLV & p1in,
      const HLV & p2out, const HLV& pl2bar, const HLV& pl2, const HLV & p2in,
      ParticleProperties const & wprop
  ){
    using helicity::minus;

    return jW_jW<minus, minus>(
      p1out, pl1bar, pl1, p1in,
      p2out, pl2bar, pl2, p2in,
      wprop
    );
  }

  std::vector <double> ME_WW_qbarQ(
      const HLV & p1out, const HLV& pl1bar, const HLV& pl1, const HLV & p1in,
      const HLV & p2out, const HLV& pl2bar, const HLV& pl2, const HLV & p2in,
      ParticleProperties const & wprop
  ){
    using helicity::minus;
    using helicity::plus;

    return jW_jW<plus, minus>(
      p1out, pl1bar, pl1, p1in,
      p2out, pl2bar, pl2, p2in,
      wprop
    );
  }

  std::vector <double> ME_WW_qQbar(
      const HLV & p1out, const HLV& pl1bar, const HLV& pl1, const HLV & p1in,
      const HLV & p2out, const HLV& pl2bar, const HLV& pl2, const HLV & p2in,
      ParticleProperties const & wprop
  ){
    using helicity::minus;
    using helicity::plus;

    return jW_jW<minus, plus>(
      p1out, pl1bar, pl1, p1in,
      p2out, pl2bar, pl2, p2in,
      wprop
    );
  }

  std::vector <double> ME_WW_qbarQbar(
      const HLV & p1out, const HLV& pl1bar, const HLV& pl1, const HLV & p1in,
      const HLV & p2out, const HLV& pl2bar, const HLV& pl2, const HLV & p2in,
      ParticleProperties const & wprop
  ){
    using helicity::plus;

    return jW_jW<plus, plus>(
      p1out, pl1bar, pl1, p1in,
      p2out, pl2bar, pl2, p2in,
      wprop
    );
  }

} // namespace currents
} // namespace HEJ

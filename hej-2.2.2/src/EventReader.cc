/**
 *  \authors   The HEJ collaboration (see AUTHORS for details)
 *  \date      2019-2022
 *  \copyright GPLv2 or later
 */
#include "HEJ/EventReader.hh"

#include <iostream>
#include <stdexcept>

#include "LHEF/LHEF.h"

#include "HEJ/ConfigFlags.hh"
#include "HEJ/HDF5Reader.hh"
#include "HEJ/LesHouchesReader.hh"
#include "HEJ/Version.hh"

namespace HEJ {
  std::unique_ptr<EventReader> make_reader(std::string const & filename) {
    try {
      auto reader{ std::make_unique<LesHouchesReader>(filename) };
      return reader;
    }
    catch(std::runtime_error&) {
#ifdef HEJ_BUILD_WITH_HDF5
      return std::make_unique<HDF5Reader>(filename);
#else
      throw;
#endif
    }
  }
} // namespace HEJ

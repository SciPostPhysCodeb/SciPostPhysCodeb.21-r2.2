/**
 *  \authors   The HEJ collaboration (see AUTHORS for details)
 *  \date      2019-2020
 *  \copyright GPLv2 or later
 */
#include "HEJ/get_analysis.hh"

#include <dlfcn.h>
#include <string>

#include "yaml-cpp/yaml.h"

#include "HEJ/EmptyAnalysis.hh"
#include "HEJ/RivetAnalysis.hh"

namespace HEJ {

  namespace {
    // Internal helper function to avoid get_analysis warning
    //
    // TODO: merge with `get_analysis` in HEJ 2.3
    std::unique_ptr<Analysis> get_analysis_impl(
      YAML::Node const & parameters, LHEF::HEPRUP const & heprup
    ){
      if(!parameters["plugin"]){
        if(parameters["rivet"].IsDefined())
          return RivetAnalysis::create(parameters, heprup);
        return detail::EmptyAnalysis::create(parameters, heprup);
      }

      using AnalysisMaker = std::unique_ptr<Analysis> (*)(
          YAML::Node const &, LHEF::HEPRUP const &);
      const auto plugin_name = parameters["plugin"].as<std::string>();
      void * handle = dlopen(plugin_name.c_str(), RTLD_NOW);
      char * error = dlerror();
      if(error != nullptr) throw std::runtime_error(error);

      void * sym = dlsym(handle, "make_analysis");
      error = dlerror();
      if(error != nullptr) throw std::runtime_error(error);
      auto make_analysis = reinterpret_cast<AnalysisMaker>(sym); // NOLINT

      return make_analysis(parameters, heprup);
    }
  }

  std::unique_ptr<Analysis> get_analysis(
    YAML::Node const & parameters, LHEF::HEPRUP const & heprup
  ){
    return get_analysis_impl(parameters, heprup);
  }

  std::vector<std::unique_ptr<Analysis>> get_analyses(
      std::vector<YAML::Node> const & parameters, LHEF::HEPRUP const & heprup
  ){
    std::vector<std::unique_ptr<Analysis>> anas;
    anas.reserve(parameters.size());
    for(auto const & param: parameters){
      anas.emplace_back(get_analysis_impl(param, heprup));
    }
    return anas;
  }
} // namespace HEJ

/**
 *  \authors   The HEJ collaboration (see AUTHORS for details)
 *  \date      2020
 *  \copyright GPLv2 or later
 */

#include "HEJ/EventWriter.hh"

#include <iostream>

#include "HEJ/exceptions.hh"

namespace HEJ {
  void EventWriter::finish_or_abort(
      EventWriter* writer, std::string const & name
  ) const noexcept {
    if(!finished_){
      try {
        writer->finish();
      } catch (std::exception const & e){
        std::cerr << "Failed to finish "<< name <<":\n"
          << e.what() << std::endl;
        std::terminate();
      }
    }
  }
} // namespace HEJ

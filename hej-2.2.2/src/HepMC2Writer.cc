/**
 *  \authors   The HEJ collaboration (see AUTHORS for details)
 *  \date      2019-2020
 *  \copyright GPLv2 or later
 */
#include "HEJ/HepMC2Writer.hh"

#include "LHEF/LHEF.h"

#include "HEJ/ConfigFlags.hh"

#ifdef HEJ_BUILD_WITH_HepMC2

#include <utility>

#include "HepMC/IO_GenEvent.h"

#include "HEJ/HepMC2Interface.hh"

#else

#include <cassert>

#include "HEJ/exceptions.hh"

#endif

#ifdef HEJ_BUILD_WITH_HepMC2

namespace HEJ {

  struct HepMC2Writer::HepMC2WriterImpl{
    HepMC2Interface hepmc_;

    HepMC2WriterImpl & operator=(HepMC2WriterImpl const & other) = delete;
    HepMC2WriterImpl(HepMC2WriterImpl const & other) = delete;
    HepMC2WriterImpl & operator=(HepMC2WriterImpl && other) = delete;
    HepMC2WriterImpl(HepMC2WriterImpl && other) = delete;
    ~HepMC2WriterImpl() = default;
    HepMC::IO_GenEvent writer_;

    HepMC2WriterImpl(
        std::string const & file, LHEF::HEPRUP && heprup
    ):
      hepmc_(heprup),
      writer_{file}
    {}

    void write(Event const & ev){
      auto out_ev = hepmc_(ev);
      writer_.write_event(&out_ev);
    }

    void set_xs_scale(const double scale) {
      hepmc_.set_xs_scale(scale);
    }

  };

  HepMC2Writer::HepMC2Writer(std::string const & file, LHEF::HEPRUP heprup):
    impl_{std::make_unique<HepMC2WriterImpl>(file, std::move(heprup))}
  {}

  void HepMC2Writer::set_xs_scale(const double scale) {
    impl_->set_xs_scale(scale);
  }

  void HepMC2Writer::write(Event const & ev){
    impl_->write(ev);
  }
} // namespace HEJ

#else // no HepMC2

namespace HEJ {

  struct HepMC2Writer::HepMC2WriterImpl{};

  HepMC2Writer::HepMC2Writer(
      std::string const & /*file*/, LHEF::HEPRUP /*heprup*/
  ){
      throw std::invalid_argument(
          "Failed to create HepMC writer: "
          "HEJ 2 was built without HepMC2 support"
      );
  }

  void HepMC2Writer::write(Event const & /*ev*/){
    assert(false);
  }

  void HepMC2Writer::set_xs_scale(double /* scale */) {
    assert(false);
  }

}
#endif

namespace HEJ {
  HepMC2Writer::~HepMC2Writer() = default;
}

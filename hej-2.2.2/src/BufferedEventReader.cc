/**
 *  \authors   The HEJ collaboration (see AUTHORS for details)
 *  \date      2019-2020
 *  \copyright GPLv2 or later
 */
#include "HEJ/BufferedEventReader.hh"

namespace HEJ {

  bool BufferedEventReader::read_event() {
      if(buffer_.empty()) {
        const bool read_success = reader_->read_event();
        if(read_success) cur_event_ = reader_->hepeup();
        return read_success;
      }
      cur_event_ = buffer_.top();
      buffer_.pop();
      return true;
  }
} // namespace HEJ

/**
 *  \authors   The HEJ collaboration (see AUTHORS for details)
 *  \date      2019-2022
 *  \copyright GPLv2 or later
 */
#include "HEJ/jets.hh"

#include <algorithm>
#include <cassert>
#include <cmath>

#include "HEJ/Constants.hh"

// generated headers
#include "HEJ/currents/j_j.hh"
#include "HEJ/currents/j_jqqbar.hh"
#include "HEJ/currents/j_qqbar_j.hh"
#include "HEJ/currents/juno_j.hh"

namespace {
  using COM = std::complex<double>;
} // Anonymous Namespace

namespace HEJ {
namespace currents {
  double ME_qq(
    HLV const & p1out, HLV const & p1in,
    HLV const & p2out, HLV const & p2in
  ){
    using helicity::plus;
    using helicity::minus;

    // we only have to distinguish between the two possibilities of
    // contracting same-helicity or opposite-helicity currents.
    COM const Mp = HEJ::j_j<minus, plus>(p1in, p1out, p2in, p2out);
    COM const Mm = HEJ::j_j<minus, minus>(p1in, p1out, p2in, p2out);

    double const sst = std::norm(Mm) + std::norm(Mp);

    // Multiply by factor 2 (helicities)
    return 2.*sst;
  }

  namespace {
    template<Helicity h1, Helicity hg>
    double amp_juno_j(
      HLV const & pa, HLV const & pb,
      HLV const & pg, HLV const & p1, HLV const & p2
    ) {
      // TODO: code duplication with Wjets
      const COM u1 = U1_j<h1, hg>(pa,p1,pb,p2,pg);
      const COM u2 = U2_j<h1, hg>(pa,p1,pb,p2,pg);
      const COM l  = L_j<h1, hg>(pa,p1,pb,p2,pg);

      const COM x = u1 - l;
      const COM y = u2 + l;

      return C_F*norm(x + y) - C_A*(x*conj(y)).real();
    }

  } // Anonymous Namespace

  double ME_unob_qq(
    HLV const & pg,
    HLV const & p1out, HLV const & p1in,
    HLV const & p2out, HLV const & p2in
  ){
    using helicity::minus;
    using helicity::plus;

    // only calculate half of the helicity amplitudes,
    // the remaining ones follow from CP symmetry
    const double amm = amp_juno_j<minus, minus>(p1in, p2in, pg, p1out, p2out);
    const double amp = amp_juno_j<minus,  plus>(p1in, p2in, pg, p1out, p2out);
    const double apm = amp_juno_j< plus, minus>(p1in, p2in, pg, p1out, p2out);
    const double app = amp_juno_j< plus,  plus>(p1in, p2in, pg, p1out, p2out);

    constexpr double hel_fac = 2.;

    return hel_fac * (amm + amp + apm + app);
  }

  namespace {
    // helicity amplitudes for j jqqbar contraction
    template<Helicity h1, Helicity hg>
    double amp_j_jqqbar(
      HLV const & pa, HLV const & pb,
      HLV const & p1, HLV const & p2, HLV const & p3
    ) {
      // TODO: code duplication with Wjets.cc
      // TODO: symbolic colour factors

      using std::norm;

      static constexpr double cm1m1 = 8./3.;
      static constexpr double cm2m2 = 8./3.;
      static constexpr double cm3m3 = 6.;
      static constexpr double cm1m2 =-1./3.;
      static constexpr COM cm1m3 = COM{0.,-3.};
      static constexpr COM cm2m3 = COM{0.,3.};

      const COM m1 = j_qggm1<h1,hg>(pb,p2,p3,pa,p1);
      const COM m2 = j_qggm2<h1,hg>(pb,p2,p3,pa,p1);
      const COM m3 = j_qggm3<h1,hg>(pb,p2,p3,pa,p1);

      return
        + cm1m1*norm(m1)
        + cm2m2*norm(m2)
        + cm3m3*norm(m3)
        + 2.*real(cm1m2*m1*conj(m2))
        + 2.*real(cm1m3*m1*conj(m3))
        + 2.*real(cm2m3*m2*conj(m3))
        ;
    }

  } // Anonymous Namespace

  double ME_qqbar_qg(
    HLV const & pa, HLV const & pb,
    HLV const & p1, HLV const & p2, HLV const & p3
  ){
    using helicity::minus;
    using helicity::plus;

    const double Mmmm = amp_j_jqqbar<minus, minus>(pa, pb, p1, p2, p3);
    const double Mmmp = amp_j_jqqbar<minus,  plus>(pa, pb, p1, p2, p3);
    const double Mpmm = amp_j_jqqbar< plus, minus>(pa, pb, p1, p2, p3);
    const double Mpmp = amp_j_jqqbar< plus,  plus>(pa, pb, p1, p2, p3);

    // Factor of 2 for the helicity for conjugate configurations
    return 2.*(Mmmm+Mmmp+Mpmm+Mpmp)/3./C_F;
  }

  namespace {
    // helicity amplitudes for matrix element with central qqbar
    // TODO: update to arxiv:2012.10310, around eq. (3.33)
    template<Helicity h1a, Helicity h4b>
    double amp_Cenqqbar_qq(
      HLV const & pa, HLV const &  p1,
      HLV const & pb, HLV const &  p4,
      HLV const &  pq, HLV const &  pqbar,
      HLV const &  q11, HLV const &  q12
    ){
      using std::norm;

      const COM sym = M_sym<h1a, h4b>(
        pa, p1, pb, p4, pq, pqbar, q11, q12
      );
      const COM cross = M_cross<h1a, h4b>(
        pa, p1, pb, p4, pq, pqbar, q11, q12
      );
      const COM uncross = M_qbar<h1a, h4b>(
        pa, p1, pb, p4, pq, pqbar, q11, q12
      );

      // Colour factors
      static constexpr double cmsms = 3.;
      static constexpr double cmumu = 4./3.;
      static constexpr double cmcmc = 4./3.;
      static constexpr COM cmsmu = COM{0., 3./2.};
      static constexpr COM cmsmc = COM{0.,-3./2.};
      static constexpr double cmumc = -1./6.;

      return
        +cmsms*norm(sym)
        +cmumu*norm(uncross)
        +cmcmc*norm(cross)
        +2.*real(cmsmu*sym*conj(uncross))
        +2.*real(cmsmc*sym*conj(cross))
        +2.*real(cmumc*uncross*conj(cross))
        ;
    }

  } // Anonymous Namespace

  double ME_Cenqqbar_qq(
    HLV const & pa, HLV const & pb,
    std::vector<HLV> const & partons,
    const bool qqbarmarker, const std::size_t nabove
  ){
    using helicity::plus;
    using helicity::minus;

    CLHEP::HepLorentzVector q1 = pa;
    for(std::size_t i = 0; i <= nabove; ++i){
      q1 -= partons[i];
    }
    const auto qq = split_into_lightlike(q1);
    // since q1.m2() < 0 the following assertion is always true
    // see documentation for split_into_lightlike
    assert(qq.second.e() < 0);

    HLV pq = partons[nabove+1];
    HLV pqbar = partons[nabove+2];
    if(qqbarmarker) std::swap(pq, pqbar);

    const HLV p1 = partons.front();
    const HLV pn = partons.back();

    // 8  helicity choices, but only 4 independent ones
    //(complex conjugation related).
    // In principle, the proper helicity labeling depends on
    // whether we have antiquark lines (aqlinea and aqlineb).
    // However, this only corresponds to a relabeling,
    // which doesn't change the sum over all helicities below.
    const double amp_mm = amp_Cenqqbar_qq<minus, minus>(
      pa, p1, pb, pn, pq, pqbar, qq.first, -qq.second
    );
    const double amp_mp = amp_Cenqqbar_qq<minus, plus>(
      pa, p1, pb, pn, pq, pqbar, qq.first, -qq.second
    );
    const double amp_pm = amp_Cenqqbar_qq<plus, minus>(
      pa, p1, pb, pn, pq, pqbar, qq.first, -qq.second
    );
    const double amp_pp = amp_Cenqqbar_qq<plus, plus>(
      pa, p1, pb, pn, pq, pqbar, qq.first, -qq.second
    );

    //Result (averaged, without coupling or t-channel props). Factor of
    //2 for the 4 helicity configurations I didn't work out explicitly
    const HLV q3 = q1 - pq - pqbar;

    constexpr double hel_fac = 2.;

    // TODO: explain the 1/64
    return hel_fac/64.*(amp_mm+amp_mp+amp_pm+amp_pp) / (q1.m2()*q3.m2());
  }

} // namespace currents
} // namespace HEJ

/** \file
 *  \brief Containers for Parameter variations, e.g. different Weights
 *
 *  \authors   The HEJ collaboration (see AUTHORS for details)
 *  \date      2019-2020
 *  \copyright GPLv2 or later
 */
#pragma once

#include <cstddef>
#include <memory>
#include <string>
#include <utility>
#include <vector>

#include "HEJ/exceptions.hh"

namespace HEJ {
  //! Collection of parameters, e.g. Weights, assigned to a single event
  /**
   * A number of member functions of the MatrixElement class return Parameters
   * objects containing the squares of the matrix elements for the various
   * scale choices.
   */
  template<class T>
  struct Parameters {
    T central{};
    std::vector<T> variations;

    template<class T_ext>
    Parameters<T>& operator*=(Parameters<T_ext> const & other);
    Parameters<T>& operator*=(double factor);
    template<class T_ext>
    Parameters<T>& operator/=(Parameters<T_ext> const & other);
    Parameters<T>& operator/=(double factor);
    template<class T_ext>
    Parameters<T>& operator+=(Parameters<T_ext> const & other);
    template<class T_ext>
    Parameters<T>& operator-=(Parameters<T_ext> const & other);
  };

  template<class T1, class T2> inline
  Parameters<T1> operator*(Parameters<T1> a, Parameters<T2> const & b) {
    a*=b;
    return a;
  }
  template<class T> inline
  Parameters<T> operator*(Parameters<T> a, double b) {
    a*=b;
    return a;
  }
  template<class T> inline
  Parameters<T> operator*(double b, Parameters<T> a) {
    a*=b;
    return a;
  }
  template<class T1, class T2> inline
  Parameters<T1> operator/(Parameters<T1> a, Parameters<T2> const & b) {
    a/=b;
    return a;
  }
  template<class T> inline
  Parameters<T> operator/(Parameters<T> a, double b) {
    a/=b;
    return a;
  }
  template<class T1, class T2> inline
  Parameters<T1> operator+(Parameters<T1> a, Parameters<T2> const & b) {
    a+=b;
    return a;
  }
  template<class T1, class T2> inline
  Parameters<T1> operator-(Parameters<T1> a, Parameters<T2> const & b) {
    a-=b;
    return a;
  }

  //! Alias for weight container, e.g. used by the MatrixElement
  using Weights = Parameters<double>;

  //! Description of event parameters, see also EventParameters
  struct ParameterDescription {
    //! Name of central scale choice (e.g. "H_T/2")
    std::string scale_name;
    //! Actual renormalisation scale divided by central scale
    double mur_factor{};
    //! Actual factorisation scale divided by central scale
    double muf_factor{};

    ParameterDescription() = default;
    ParameterDescription(
      std::string scale_name, double mur_factor, double muf_factor
    ):
      scale_name{std::move(scale_name)},
      mur_factor{mur_factor}, muf_factor{muf_factor}
    {}
  };

  //! generate human readable string name
  std::string to_string(ParameterDescription const & p);

  //! generate simplified string, intended for easy parsing
  //! Format: Scale_SCALENAME_MuRxx_MuFyy
  std::string to_simple_string(ParameterDescription const & p);

  //! Event parameters
  struct EventParameters{
    double mur{};              /**< Value of the Renormalisation Scale */
    double muf{};              /**< Value of the Factorisation Scale */
    double weight{};           /**< Event Weight */
    //! Optional description
    std::shared_ptr<ParameterDescription> description = nullptr;

    //! multiply weight by factor
    EventParameters& operator*=(double factor){
      weight*=factor;
      return *this;
    }
    //! divide weight by factor
    EventParameters& operator/=(double factor){
      weight/=factor;
      return *this;
    }
    //! add number to weight
    EventParameters& operator+=(double factor){
      weight+=factor;
      return *this;
    }
    //! add number to weight
    EventParameters& operator-=(double factor){
      weight-=factor;
      return *this;
    }
  };
  inline EventParameters operator*(EventParameters a, double b){
    a*=b;
    return a;
  }
  inline EventParameters operator*(double b, EventParameters a){
    a*=b;
    return a;
  }
  inline EventParameters operator/(EventParameters a, double b){
    a/=b;
    return a;
  }

  //! @{
  //! @internal Implementation of template functions
  template<class T>
  template<class T_ext>
  Parameters<T>& Parameters<T>::operator*=(Parameters<T_ext> const & other) {
    if(other.variations.size() != variations.size()) {
      throw std::invalid_argument{"Wrong number of Parameters"};
    }
    central *= other.central;
    for(std::size_t i = 0; i < variations.size(); ++i) {
      variations[i] *= other.variations[i];
    }
    return *this;
  }

  template<class T>
  Parameters<T>& Parameters<T>::operator*=(double factor) {
    central *= factor;
    for(auto & wt: variations) wt *= factor;
    return *this;
  }

  template<class T>
  template<class T_ext>
  Parameters<T>& Parameters<T>::operator/=(Parameters<T_ext> const & other) {
    if(other.variations.size() != variations.size()) {
      throw std::invalid_argument{"Wrong number of Parameters"};
    }
    central /= other.central;
    for(std::size_t i = 0; i < variations.size(); ++i) {
      variations[i] /= other.variations[i];
    }
    return *this;
  }

  template<class T>
  Parameters<T>& Parameters<T>::operator/=(double factor) {
    central /= factor;
    for(auto & wt: variations) wt /= factor;
    return *this;
  }

  template<class T>
  template<class T_ext>
  Parameters<T>& Parameters<T>::operator+=(Parameters<T_ext> const & other) {
    if(other.variations.size() != variations.size()) {
      throw std::invalid_argument{"Wrong number of Parameters"};
    }
    central += other.central;
    for(std::size_t i = 0; i < variations.size(); ++i) {
      variations[i] += other.variations[i];
    }
    return *this;
  }

  template<class T>
  template<class T_ext>
  Parameters<T>& Parameters<T>::operator-=(Parameters<T_ext> const & other) {
    if(other.variations.size() != variations.size()) {
      throw std::invalid_argument{"Wrong number of Parameters"};
    }
    central -= other.central;
    for(std::size_t i = 0; i < variations.size(); ++i) {
      variations[i] -= other.variations[i];
    }
    return *this;
  }
  //! @}
} // namespace HEJ

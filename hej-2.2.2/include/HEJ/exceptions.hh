/** \file
 *  \brief Custom exception classes
 *
 *  \authors   The HEJ collaboration (see AUTHORS for details)
 *  \date      2019-2020
 *  \copyright GPLv2 or later
 */
#pragma once

#include <stdexcept>
#include <string>

namespace HEJ {
  //! Exception indicating wrong option type
  /**
   * This exception is thrown if a configuration option has
   * the wrong type (e.g. 'trials' is not set to a number)
   */
  struct invalid_type: std::invalid_argument {
    explicit invalid_type(std::string const & what):
      std::invalid_argument{what} {}
    explicit invalid_type(char const * what):
      std::invalid_argument{what} {}
  };

  //! Exception indicating unknown option
  /**
   * This exception is thrown if an unknown configuration option
   * is set (e.g. the 'trials' setting is misspelt as 'trails')
   */
  struct unknown_option: std::invalid_argument {
    explicit unknown_option(std::string const & what):
      std::invalid_argument{what} {}
    explicit unknown_option(char const * what):
      std::invalid_argument{what} {}
  };

  //! Exception indicating missing option setting
  /**
   * This exception is thrown if a mandatory configuration option
   * (e.g. 'trials') is not set.
   */
  struct missing_option: std::logic_error {
    explicit missing_option(std::string const & what):
      std::logic_error{what} {}
    explicit missing_option(char const * what):
      std::logic_error{what} {}
  };

  //! Exception indicating functionality that has not been implemented yet
  struct not_implemented: std::logic_error {
    explicit not_implemented(std::string const & what):
      std::logic_error{what} {}
    explicit not_implemented(char const * what):
      std::logic_error{what} {}
  };

} // namespace HEJ

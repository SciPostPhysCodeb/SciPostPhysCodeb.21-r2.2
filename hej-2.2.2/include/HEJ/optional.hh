/** \file
 *  \brief Defines the optional type (deprecated, use std::optional instead)
 *
 *  \authors   The HEJ collaboration (see AUTHORS for details)
 *  \date      2019-2022
 *  \copyright GPLv2 or later
 */
#pragma once

#include <optional>

namespace HEJ {
  // Templates cannot be deprecated, so we need this workaround
  namespace detail {
    [[deprecated("`HEJ::optional` is deprecated. Use `std::optional` instead.")]]
    inline constexpr int HEJ_optional = 0;
    inline constexpr int use_std_optional_instead = HEJ_optional;
  }

  template<typename T>
  using optional = std::optional<T>;
}

/** \file
 * \brief Contains a class for the ranlux64 random number generator
 *
 *  \authors   The HEJ collaboration (see AUTHORS for details)
 *  \date      2019-2022
 *  \copyright GPLv2 or later
 */
#pragma once

#include <string>

#include "CLHEP/Random/Ranlux64Engine.h"

#include "HEJ/RNG.hh"

namespace HEJ {

  //! Ranlux64 random number generator
  /**
   *  For details on ranlux64, see \cite Luscher:1993dy, \cite James:1993np
   */
  class Ranlux64 : public RNG {
  public:
    Ranlux64();
    //! Constructor with a file as seed
    Ranlux64(std::string const & seed_file);

    //! Generate pseudorandom number between 0 and 1
    double flat() override;

  private:
    CLHEP::Ranlux64Engine ran_;
  };

} // namespace HEJ

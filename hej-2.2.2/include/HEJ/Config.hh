/** \file
 *  \brief HEJ 2 configuration parameters
 *
 *  \authors   The HEJ collaboration (see AUTHORS for details)
 *  \date      2019-2022
 *  \copyright GPLv2 or later
 */
#pragma once

#include <map>
#include <string>
#include <utility>
#include <optional>
#include <vector>

#include "fastjet/JetDefinition.hh"
#include "yaml-cpp/yaml.h"

#include "HEJ/Constants.hh"
#include "HEJ/EWConstants.hh"
#include "HEJ/Fraction.hh"
#include "HEJ/HiggsCouplingSettings.hh"
#include "HEJ/ScaleFunction.hh"
#include "HEJ/event_types.hh"
#include "HEJ/output_formats.hh"

namespace HEJ {

  //! Jet parameters
  struct JetParameters{
    fastjet::JetDefinition def;          /**< Jet Definition */
    double min_pt{};                       /**< Minimum Jet Transverse Momentum */
  };

  //! Settings for scale variation
  struct ScaleConfig{
    //! Base scale choices
    std::vector<ScaleFunction> base;
    //! Factors for multiplicative scale variation
    std::vector<double> factors;
    //! Maximum ratio between renormalisation and factorisation scale
    double max_ratio{};
  };

  //! Settings for random number generator
  struct RNGConfig {
    //! Random number generator name
    std::string name;
    //! Optional initial seed
    std::optional<std::string> seed;
  };

  //! Settings for partial unweighting
  struct PartialUnweightConfig {
    //! Number of trials for training
    size_t trials;
    //! Maximum distance in standard deviations from mean logarithmic weight
    double max_dev;
  };

  //! Settings for HEJ@NLO
  struct NLOConfig {
    //! Settings for HEJ@NLO Truncation
    bool enabled = false;
    //! NLO Born number of jets
    size_t nj = 2;
  };

  /**! Possible treatments for fixed-order input events.
   *
   *  The program will decide on how to treat an event based on
   *  the value of this enumeration.
   */
  enum class EventTreatment{
    reweight,                                  /**< Perform resummation */
    keep,                                      /**< Keep the event */
    discard,                                   /**< Discard the event */
    abort,                                     /**< Throw an exception */
  };

  //! Container to store the treatments for various event types
  using EventTreatMap = std::map<event_type::EventType, EventTreatment>;

  //! Possible setting for the event weight
  enum class WeightType{
    weighted,             //!< weighted events
    unweighted_resum,     //!< unweighted only resummation part
    partially_unweighted  //!< mixed weighted and unweighted
  };

  /**! Input parameters.
   *
   * This struct handles stores all configuration parameters
   * needed in a HEJ 2 run.
   *
   * \internal To add a new option:
   *           1. Add a member to the Config struct.
   *           2. Inside "src/YAMLreader.cc":
   *              - Add the option name to the "supported" Node in
   *                get_supported_options.
   *              - Initialise the new Config member in to_Config.
   *                The functions set_from_yaml (for mandatory options) and
   *                set_from_yaml_if_defined (non-mandatory) may be helpful.
   *           3. Add a new entry (with short description) to config.yaml
   *           4. Update the user documentation in "doc/Sphinx/"
   */
  struct Config {
    //! %Parameters for scale variation
    ScaleConfig scales;
    //! Resummation jet properties
    JetParameters resummation_jets;
    //! Fixed-order jet properties
    JetParameters fixed_order_jets;
    //! @brief Maximum transverse momentum fraction from soft radiation in any
    //!        tagging jet (e.g. extremal or qqbar jet)
    Fraction<double> soft_pt_regulator{ DEFAULT_SOFT_PT_REGULATOR };
    //! The regulator lambda for the subtraction terms
    double regulator_lambda = CLAMBDA;
    //! Number of resummation configurations to generate per fixed-order event
    size_t trials{};
    //! Maximal number of events
    std::optional<size_t> max_events;
    //! Whether to include the logarithmic correction from \f$\alpha_s\f$ running
    bool log_correction{};
    //! Event output files names and formats
    std::vector<OutputFile> output;
    //! Parameters for random number generation
    RNGConfig rng;
    //! Map to decide what to do for different event types
    EventTreatMap treat;
    //! %Parameters for custom analyses
    std::vector<YAML::Node> analyses_parameters;
    //! Settings for effective Higgs-gluon coupling
    HiggsCouplingSettings Higgs_coupling;
    //! elector weak parameters
    EWConstants ew_parameters;
    //! Type of event weight e.g. (un)weighted
    WeightType weight_type;
    //! Settings for partial unweighting
    std::optional<PartialUnweightConfig> unweight_config;
    //! HEJ@NLO settings
    NLOConfig nlo;
    //! LowPT settings
    bool lowpt = false;
    //! Tolerance towards numerical inaccuracies in input momenta
    double off_shell_tolerance = 0.;
  };

  //! Configuration options for the PhaseSpacePoint class
  struct PhaseSpacePointConfig {
    PhaseSpacePointConfig() = default;
    PhaseSpacePointConfig(
        JetParameters jet_param,
        NLOConfig nlo,
        Fraction<double> soft_pt_regulator =
          Fraction<double>{DEFAULT_SOFT_PT_REGULATOR}
    ):
    jet_param{std::move(jet_param)},
    nlo{std::move(nlo)},
    soft_pt_regulator{std::move(soft_pt_regulator)}
    {}
    //! Properties of resummation jets
    JetParameters jet_param;
    //! HEJ@NLO settings
    NLOConfig nlo;
    //! @brief Maximum transverse momentum fraction from soft radiation in any
    //!        tagging jet (e.g. extremal or qqbar jet)
    Fraction<double> soft_pt_regulator{ DEFAULT_SOFT_PT_REGULATOR };
  };

  //! Configuration options for the MatrixElement class
  struct MatrixElementConfig {
    MatrixElementConfig() = default;
    MatrixElementConfig(
        bool log_correction,
        HiggsCouplingSettings Higgs_coupling,
        EWConstants ew_parameters,
        NLOConfig nlo,
        Fraction<double> soft_pt_regulator = Fraction<double>{DEFAULT_SOFT_PT_REGULATOR},
        double regulator_lambda = CLAMBDA
    ):
    log_correction{log_correction},
    Higgs_coupling{std::move(Higgs_coupling)},
    ew_parameters{std::move(ew_parameters)},
    nlo{std::move(nlo)},
    soft_pt_regulator{soft_pt_regulator},
    regulator_lambda{regulator_lambda}
    {}

    //! Whether to include the logarithmic correction from \f$\alpha_s\f$ running
    bool log_correction{};
    //! Settings for effective Higgs-gluon coupling
    HiggsCouplingSettings Higgs_coupling;
    //! elector weak parameters
    EWConstants ew_parameters;
    //! HEJ@NLO settings
    NLOConfig nlo;
    //! @brief Maximum transverse momentum fraction from soft radiation in any
    //!        tagging jet (e.g. extremal or qqbar jet)
    Fraction<double> soft_pt_regulator{ DEFAULT_SOFT_PT_REGULATOR };
    //! The regulator lambda for the subtraction terms
    double regulator_lambda = CLAMBDA;
  };

  //! Configuration options for the EventReweighter class
  struct EventReweighterConfig {
    //! Settings for phase space point generation
    PhaseSpacePointConfig psp_config;
    //! Settings for matrix element calculation
    MatrixElementConfig ME_config;
    //! Access properties of resummation jets
    JetParameters & jet_param() {
      return psp_config.jet_param;}
    //! Access properties of resummation jets (const version)
    JetParameters const & jet_param() const {
      return psp_config.jet_param;}
    //! Treatment of the various event types
    EventTreatMap treat;
    //! Option to only keep lowpt contribution
    bool lowpt = false;
  };

  /**! Extract PhaseSpacePointConfig from Config
   *
   * \internal We do not provide a PhaseSpacePointConfig constructor from Config
   * so that PhaseSpacePointConfig remains an aggregate.
   * This faciliates writing client code (e.g. the HEJ fixed-order generator)
   * that creates a PhaseSpacePointConfig *without* a Config object.
   *
   * @see to_MatrixElementConfig, to_EventReweighterConfig
   */
  inline
  PhaseSpacePointConfig to_PhaseSpacePointConfig(Config const & conf) {
    return {
      conf.resummation_jets,
      conf.nlo,
      conf.soft_pt_regulator
    };
  }

  /**! Extract MatrixElementConfig from Config
   *
   * @see to_PhaseSpacePointConfig, to_EventReweighterConfig
   */
  inline
  MatrixElementConfig to_MatrixElementConfig(Config const & conf) {
    return {
      conf.log_correction,
      conf.Higgs_coupling,
      conf.ew_parameters,
      conf.nlo,
      conf.soft_pt_regulator,
      conf.regulator_lambda
    };
  }

  /**! Extract EventReweighterConfig from Config
   *
   * @see to_PhaseSpacePointConfig, to_MatrixElementConfig
   */
  inline
  EventReweighterConfig to_EventReweighterConfig(Config const & conf) {
    return {
      to_PhaseSpacePointConfig(conf),
      to_MatrixElementConfig(conf),
      conf.treat,
      conf.lowpt
    };
  }

} // namespace HEJ

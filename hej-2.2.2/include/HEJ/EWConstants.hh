/** \file
 *  \brief Defines the electro weak parameters
 *
 *  \authors   The HEJ collaboration (see AUTHORS for details)
 *  \date      2019-2020
 *  \copyright GPLv2 or later
 */
#pragma once

#include <cmath>
#include <memory>

#include "HEJ/PDG_codes.hh"
#include "HEJ/exceptions.hh"

namespace HEJ {
  //! collection of basic particle properties
  struct ParticleProperties {
    double mass;  //!< Mass
    double width; //!< Decay width
  };

  //! Collection of electro-weak constants
  class EWConstants {
  public:
    //! Default constructor, values have to be initialised separately
    EWConstants() = default;
    //! initialise by Vacuum expectation value & boson properties
    constexpr EWConstants(
      double vev, //!< vacuum expectation value
      ParticleProperties Wprop, //!< W boson mass & width
      ParticleProperties Zprop, //!< Z boson mass & width
      ParticleProperties Hprop  //!< Higgs boson mass & width
    ): set_{true}, vev_{vev}, Wprop_{std::move(Wprop)},
        Zprop_{std::move(Zprop)}, Hprop_{std::move(Hprop)}
    {}
    //! set constants by Vacuum expectation value & boson properties
    void set_vevWZH(
      double vev, //!< vacuum expectation value
      ParticleProperties Wprop, //!< W boson mass & width
      ParticleProperties Zprop, //!< Z boson mass & width
      ParticleProperties Hprop  //!< Higgs boson mass & width
    ){
      set_ = true; vev_= vev; Wprop_= std::move(Wprop);
      Zprop_= std::move(Zprop); Hprop_= std::move(Hprop);
    }
    //! vacuum expectation value
    double vev()     const {check_set(); return vev_;}
    //! Properties of the W boson
    ParticleProperties const & Wprop() const {check_set(); return Wprop_;}
    //! Properties of the Z boson
    ParticleProperties const & Zprop() const {check_set(); return Zprop_;}
    //! Properties of the Higgs boson
    ParticleProperties const & Hprop() const {check_set(); return Hprop_;}
    //! access Properties by boson id
    ParticleProperties const & prop(ParticleID const id) const {
      using namespace pid;
      switch(id){
      case Wp:
      case Wm:
        return Wprop();
      case Z:
        return Zprop();
      case h:
        return Hprop();
      default:
        throw std::invalid_argument("No properties available for particle "+name(id));
      }
    }
    //! cosine of Weinberg angle
    double cos_tw()   const {return Wprop().mass/Zprop().mass;}
    //! cosine square of Weinberg angle
    double cos2_tw()  const {return cos_tw()*cos_tw();}
    //! sinus Weinberg angle
    double sin_tw()   const {return std::sqrt(sin2_tw());}
    //! sinus square of Weinberg angle
    double sin2_tw()  const {return 1. - cos2_tw();}
    //! elector magnetic coupling
    double alpha_em() const {return e2()/4./M_PI;}
    //! weak coupling
    double alpha_w()  const {return gw2()/2.;}

  private:
    double gw2() const {return 4*Wprop().mass/vev()*Wprop().mass/vev();}
    double e2()  const {return gw2()*sin2_tw();}
    void check_set() const {
      if(!set_) throw std::invalid_argument("EW constants not specified");
    }
    bool set_{false};
    double vev_{};
    ParticleProperties Wprop_{};
    ParticleProperties Zprop_{};
    ParticleProperties Hprop_{};
  };
} // namespace HEJ

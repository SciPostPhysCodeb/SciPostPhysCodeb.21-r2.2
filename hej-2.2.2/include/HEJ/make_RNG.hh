/** \file
 *  \brief Declares a factory function for random number generators
 *
 *  \authors   The HEJ collaboration (see AUTHORS for details)
 *  \date      2019-2022
 *  \copyright GPLv2 or later
 */
#pragma once

#include <memory>
#include <optional>
#include <string>

#include "HEJ/RNG.hh"

namespace HEJ {
  //! Factory function for random number generators
  /**
   *  @param name      Name of the random number generator
   *  @param seed      Optional seed
   *  @returns         A pointer to an instance of a random number generator
   *
   *  At present, name should be one of "ranlux64" or "mixmax" (case insensitive).
   *  The interpretation of the seed depends on the random number generator.
   *  For ranlux64, it is the name of a seed file. For mixmax it should be a
   *  string convertible to a long integer.
   */
  std::unique_ptr<RNG> make_RNG(
      std::string const & name,
      std::optional<std::string> const & seed
  );
} // namespace HEJ

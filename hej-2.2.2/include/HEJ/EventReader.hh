/** \file
 *  \brief Header file for event reader interface
 *
 *  This header defines an abstract base class for reading events from files.
 *
 *  \authors   The HEJ collaboration (see AUTHORS for details)
 *  \date      2019-2022
 *  \copyright GPLv2 or later
 */
#pragma once

#include <cstddef>
#include <memory>
#include <optional>
#include <string>

namespace LHEF {
  class HEPEUP;
  class HEPRUP;
}

namespace HEJ {

  //! Abstract base class for reading events from files
  struct EventReader {
    //! Read an event
    virtual bool read_event() = 0;

    //! Access header text
    virtual std::string const & header() const = 0;

    //! Access run information
    virtual LHEF::HEPRUP const & heprup() const = 0;

    //! Access last read event
    virtual LHEF::HEPEUP const & hepeup() const = 0;

    //! Guess number of events from header
    virtual std::optional<std::size_t> number_events() const = 0;

    virtual double scalefactor() const {
      return 1.;
    };

    virtual ~EventReader() = default;
  };

  //! Factory function for event readers
  /**
   *  @param filename   The name of the input file
   *  @returns          A pointer to an instance of an EventReader
   *                    for the input file
   */
  std::unique_ptr<EventReader> make_reader(std::string const & filename);
} // namespace HEJ

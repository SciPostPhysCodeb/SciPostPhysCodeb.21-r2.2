/** \file
 *  \brief Header file defining all global constants used for HEJ
 *
 *  \authors   The HEJ collaboration (see AUTHORS for details)
 *  \date      2019-2020
 *  \copyright GPLv2 or later
 */
#pragma once

namespace HEJ {
//! @name QCD parameters
//! @{
  constexpr double N_C = 3.;    //!< number of Colours
  constexpr double C_A = N_C;    //!< \f$C_A\f$
  constexpr double C_F = (N_C*N_C - 1.)/(2.*N_C); //!< \f$C_F\f$
  constexpr double T_F = 0.5; //!< \f$t_f\f$
  constexpr double N_F = 5.;    //!< number light flavours
  constexpr double BETA0 = 11./3.*C_A - 4./3.*T_F*N_F;  //!< \f$\beta_0\f$
//! @}
//! @name Generation Parameters
//! @{
  //! @brief Default scale for virtual correction
  //! \f$\lambda\f$ cf. eq. (20) in \cite Andersen:2011hs
  constexpr double CLAMBDA = 0.2;
  constexpr double CMINPT = 0.2;  //!< minimal \f$p_t\f$ of all partons
  //! @brief default value for the maximal pt fraction of soft radiation in any
  //!        tagging jets
  //! This cut is needed to regulate an otherwise uncancelled divergence.
  constexpr double DEFAULT_SOFT_PT_REGULATOR = 0.1;
//! @}
//! @name Conventional Parameters
//! @{
  //! Value of first colour for colour dressing, according to LHE convention
  //! \cite Boos:2001cv
  constexpr int COLOUR_OFFSET = 501;
//! @}
} // namespace HEJ

/**
 *  \authors   The HEJ collaboration (see AUTHORS for details)
 *  \date      2019-2022
 *  \copyright GPLv2 or later
 */
/** \file
 *  \brief Functions computing the square of current contractions in WW+Jets.
 *
 *  This file contains the WW+jet specific code to compute current contractions.
 */

#pragma once

#include <vector>

#include "CLHEP/Vector/LorentzVector.h"

namespace HEJ {
  struct ParticleProperties;

namespace currents {
  using HLV = CLHEP::HepLorentzVector;

  //! Current contractions for qQ -> qQWW with leptonically decaying same-sign W
  /**
  *  @param p1out     Momentum of first final state quark
  *  @param plbar     Momentum of final state anti-lepton from first W decay
  *  @param pl        Momentum of final state lepton from first W decay
  *  @param p1in      Momentum of first initial state quark
  *  @param p2out     Momentum of second final state quark
  *  @param plbar2    Momentum of final state anti-lepton from second W decay
  *  @param pl2       Momentum of final state lepton from second W decay
  *  @param p2in      Momentum of second initial state quark
  *  @param wprop     Mass and width of the W boson
  *  @returns         Square of the current contractions for qQ -> qQWW
  *
  *  This returns the square of the current contractions in qQ->qQ WW scattering,
  *  where the two W bosons have the same charge and decay leptonically.
  */
  std::vector <double> ME_WW_qQ(
      const HLV & p1out, const HLV& pl1bar, const HLV& pl1, const HLV & p1in,
      const HLV & p2out, const HLV& pl2bar, const HLV& pl2, const HLV & p2in,
      ParticleProperties const & wprop
  );

  //! Current contractions for qbarQ -> qbarQWW with leptonically decaying same-sign W
  /**
  *  @param p1out     Momentum of final state anti-quark
  *  @param plbar     Momentum of final state anti-lepton from first W decay
  *  @param pl        Momentum of final state lepton from first W decay
  *  @param p1in      Momentum of initial state anti-quark
  *  @param p2out     Momentum of final state quark
  *  @param plbar2    Momentum of final state anti-lepton from second W decay
  *  @param pl2       Momentum of final state lepton from second W decay
  *  @param p2in      Momentum of initial state quark
  *  @param wprop     Mass and width of the W boson
  *  @returns         Square of the current contractions for qbarQ -> qQWW
  *
  *  This returns the square of the current contractions in qbarQ->qbarQ WW scattering,
  *  where the two W bosons have the same charge and decay leptonically.
  */
  std::vector <double> ME_WW_qbarQ(
      const HLV & p1out, const HLV& pl1bar, const HLV& pl1, const HLV & p1in,
      const HLV & p2out, const HLV& pl2bar, const HLV& pl2, const HLV & p2in,
      ParticleProperties const & wprop
  );

  //! Current contractions for qQbar -> qQbarWW with leptonically decaying same-sign W
  /**
  *  @param p1out     Momentum of final state quark
  *  @param plbar     Momentum of final state anti-lepton from first W decay
  *  @param pl        Momentum of final state lepton from first W decay
  *  @param p1in      Momentum of initial state quark
  *  @param p2out     Momentum of final state anti-quark
  *  @param plbar2    Momentum of final state anti-lepton from second W decay
  *  @param pl2       Momentum of final state lepton from second W decay
  *  @param p2in      Momentum of initial state anti-quark
  *  @param wprop     Mass and width of the W boson
  *  @returns         Square of the current contractions for qQbar -> qQbarWW
  *
  *  This returns the square of the current contractions in qQbar->qQbar WW scattering,
  *  where the two W bosons have the same charge and decay leptonically.
  */
  std::vector <double> ME_WW_qQbar(
      const HLV & p1out, const HLV& pl1bar, const HLV& pl1, const HLV & p1in,
      const HLV & p2out, const HLV& pl2bar, const HLV& pl2, const HLV & p2in,
      ParticleProperties const & wprop
  );

  //! Current contractions for qbarQbar -> qbarQbarWW with leptonically decaying same-sign W
  /**
  *  @param p1out     Momentum of first final state anti-quark
  *  @param plbar     Momentum of final state anti-lepton from first W decay
  *  @param pl        Momentum of final state lepton from first W decay
  *  @param p1in      Momentum of first initial state anti-quark
  *  @param p2out     Momentum of second final state anti-quark
  *  @param plbar2    Momentum of final state anti-lepton from second W decay
  *  @param pl2       Momentum of final state lepton from second W decay
  *  @param p2in      Momentum of second initial state anti-quark
  *  @param wprop     Mass and width of the W boson
  *  @returns         Square of the current contractions for qbarQbar -> qbarQbarWW
  *
  *  This returns the square of the current contractions in qbarQbar->qbarQbar WW scattering,
  *  where the two W bosons have the same charge and decay leptonically.
  */
  std::vector <double> ME_WW_qbarQbar(
      const HLV & p1out, const HLV& pl1bar, const HLV& pl1, const HLV & p1in,
      const HLV & p2out, const HLV& pl2bar, const HLV& pl2, const HLV & p2in,
      ParticleProperties const & wprop
  );

} // namespace currents
} // namespace HEJ

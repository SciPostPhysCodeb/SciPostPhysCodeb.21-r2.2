/** \file
 *  \brief Contains the get_analysis function
 *
 *  \authors   The HEJ collaboration (see AUTHORS for details)
 *  \date      2019-2020
 *  \copyright GPLv2 or later
 */
#pragma once

#include <memory>
#include <vector>

#include "HEJ/Analysis.hh"

namespace YAML {
  class Node;
}

namespace HEJ {
  //!  Load an analysis
  /**
   *  @param parameters    Analysis parameters
   *  @param heprup        General run informations
   *  @returns             A pointer to an Analysis instance
   *
   *  If parameters["plugin"] exists, an analysis (deriving from the
   *  \ref Analysis class) will be loaded from the library parameters["plugin"].
   *  Otherwise, if parameters["rivet"] exists, the corresponding RivetAnalysis
   *  will be loaded. If none of these parameters are specified, a pointer to
   *  the default EmptyAnalysis is returned.
   */
#ifndef HEJ_ALLOW_GET_ANALYSIS
  [[deprecated(
        "Instead of a pointer to an EmptyAnalysis,"
        "this function will return a null pointer from HEJ 2.3 on. "
        "Compile with -DHEJ_ALLOW_GET_ANALYSIS to disable this warning."
      )]]
#endif
  std::unique_ptr<Analysis> get_analysis(
      YAML::Node const & parameters, LHEF::HEPRUP const & heprup);

  //! Loads multiple analysis, vector version of get_analysis()
  /**
   *  @param parameters    Vector of Analysis parameters
   *  @param heprup        General run informations
   *  @returns             Vector of pointers to an Analysis instance
   */
  std::vector<std::unique_ptr<Analysis>> get_analyses(
      std::vector<YAML::Node> const & parameters, LHEF::HEPRUP const & heprup);
} // namespace HEJ

/** \file
 *  \brief Header file for the HepMC3Interface
 *
 *  \authors   The HEJ collaboration (see AUTHORS for details)
 *  \date      2019-2023
 *  \copyright GPLv2 or later
 */
#pragma once

#include <array>
#include <cstddef>
#include <memory>

#include "HEJ/PDG_codes.hh"

namespace HepMC3 {
  class GenCrossSection;
  class GenEvent;
  class GenRunInfo;
}

namespace LHEF {
  class HEPRUP;
}

namespace HEJ {
  class Event;
  //! This class converts the Events into HepMC3::GenEvents
  /**
   *   \details The output is depended on the HepMC3 version HEJ is compiled with,
   *   both HepMC3 2 and HepMC3 3 are supported. If HEJ 2 is compiled
   *   without HepMC3 calling this interface will throw an error.
   *
   *   This interface will also keep track of the cross section of all the events that
   *   being fed into it.
   */

  class HepMC3Interface{
  public:
    HepMC3Interface(LHEF::HEPRUP heprup);
    ~HepMC3Interface();
    HepMC3Interface & operator=(HepMC3Interface const & other) = default;
    HepMC3Interface(HepMC3Interface const & other) = default;
    HepMC3Interface & operator=(HepMC3Interface && other) = default;
    HepMC3Interface(HepMC3Interface && other) = default;
    /**
     * \brief main function to convert an event into HepMC3::GenEvent
     *
     * \param event          Event to convert
     * \param weight_index   optional selection of specific weight
     *                       (negative value gives central weight)
     */
    HepMC3::GenEvent operator()(Event const & event, int weight_index = -1);
    /**
     * \brief Sets the central value from \p event to \p out_ev
     *
     * \param out_ev         HepMC3::GenEvent to write to
     * \param event          Event to convert
     * \param weight_index   optional selection of specific weight
     *                       (negative value gives "central")
     *
     * This overwrites the central value of \p out_ev.
     */
    void set_central(HepMC3::GenEvent & out_ev, Event const & event,
      int weight_index = -1);

    //! Pointer to generic run informations
    std::shared_ptr<HepMC3::GenRunInfo> run_info(){
      return run_info_;
    }

    //! Set the ratio (cross section) / (sum of event weights)
    void set_xs_scale(double scale);

  protected:
    /**
     * \brief initialise generic event infomrations (not central weights)
     *
     * \param event          Event to convert
     */
    HepMC3::GenEvent init_event(Event const & event) const;

  private:
    std::array<ParticleID,2> beam_particle_{};
    std::array<double,2> beam_energy_{};
    std::shared_ptr<HepMC3::GenRunInfo> run_info_;
    std::size_t event_count_;
    double tot_weight_;
    double tot_weight2_;
    double xs_scale_ = 1.;
    std::shared_ptr<HepMC3::GenCrossSection> xs_;
  };
} // namespace HEJ

/** \file
 *
 * \brief Contains all the necessary classes and functions for interaction with PDFs.
 *
 *  \authors   The HEJ collaboration (see AUTHORS for details)
 *  \date      2019-2020
 *  \copyright GPLv2 or later
 */
#pragma once

#include <array>
#include <cstddef>
#include <memory>

#include "HEJ/PDG_codes.hh"

namespace LHAPDF {
  class PDF;
}

namespace HEJ {
  //! Class for interaction with a PDF set
  class PDF {
  public:
    /**
     * \brief PDF Constructor
     * @param id         Particle ID according to PDG
     * @param beam1      Particle ID of particle in beam 1
     * @param beam2      Particle ID of particle in beam 2
     */
    PDF(int id, ParticleID beam1, ParticleID beam2);

    /**
     * \brief Calculate the pdf value x*f(x, q)
     * @param beam_idx  Beam number (0 or 1)
     * @param x         Momentum fraction
     * @param q         Energy scale
     * @param id        PDG particle id
     * @returns         x*f(x, q)
     *
     * Returns 0 if x or q are outside the range covered by the PDF set
     */
    double pdfpt(std::size_t beam_idx, double x, double q, ParticleID id) const;

    /**
     * \brief Value of the strong coupling \f$\alpha_s(q)\f$ at the given scale
     * @param q         Renormalisation scale
     * @returns         Value of the strong coupling constant
     */
    double Halphas(double q) const;

    //! Check if the energy scale is within the range covered by the PDF set
    /**
     * @param q        Energy Scale
     * @returns        true if q is within the covered range, false otherwise
     */
    bool inRangeQ(double q) const;

    //! Check if the momentum fraction is within the range covered by the PDF set
    /**
     * @param x        Momentum Fraction
     * @returns        true if x is within the covered range, false otherwise
     */
    bool inRangeX(double x) const;

    //! PDF id of the current set
    int id() const;

    PDF(PDF const & other) = delete; //!< Copy not possible
    PDF & operator=(PDF const & other) = delete; //!< Copy not possible
    PDF(PDF && other) noexcept; //!< Moving allowed
    PDF & operator=(PDF && other) noexcept; //!< Moving allowed
    ~PDF();

  private:
    //! @internal unique_ptr does not allow copy
    std::unique_ptr<LHAPDF::PDF> pdf_;

    std::array<int, 2> beamtype_{};
  };
} // namespace HEJ

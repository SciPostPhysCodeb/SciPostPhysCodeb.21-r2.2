/** \file
 *  \brief HEJ 2 interface to rivet analyses
 *
 *  \authors   The HEJ collaboration (see AUTHORS for details)
 *  \date      2019-2023
 *  \copyright GPLv2 or later
 */
#pragma once

#include <memory>
#include <string>
#include <vector>

#include "LHEF/LHEF.h"

#include "HEJ/Analysis.hh"

namespace YAML {
  class Node;
}

namespace HEJ {
  /**
    * @brief Class representing a Rivet analysis
    *
    * This class inherits from Analysis and can therefore be used
    * like any other HEJ 2 analysis.
    */
  class RivetAnalysis: public Analysis {
  public:
    //! Create RivetAnalysis
    static std::unique_ptr<Analysis> create(
        YAML::Node const & config, LHEF::HEPRUP const & heprup);

    //! Constructor
    /**
     *  @param config    Configuration parameters
     *  @param heprup    General run informations
     *
     * config["rivet"] should be the name of a single Rivet analysis or
     * a list of Rivet analyses. config["output"] is the prefix for
     * the .yoda output files.
     */
    RivetAnalysis(YAML::Node const & config, LHEF::HEPRUP heprup);
    ~RivetAnalysis() override;
    //! Pass an event to the underlying Rivet analysis
    void fill(Event const & event, Event const & /*unused*/) override;
    //! no additional cuts are applied
    bool pass_cuts(Event const & /*unused*/, Event const & /*unused*/
    ) override { return true; }
    void set_xs_scale(double scale) override;
    void finalise() override;

  private:
    std::vector<std::string> analyses_names_;
    std::string output_name_;
    LHEF::HEPRUP heprup_;

    /// struct to organise the infos per rivet run/scale setting
    struct rivet_info;
    std::vector<rivet_info> rivet_runs_;

    /**
     *  \internal
     * @brief Calculates the scale variation from the first event for the output
     *        file
     */
    void init(Event const & event);
    bool first_event_;
  };
} // namespace HEJ

/** \file
 *  \brief Header file for status codes of event generation
 *
 *  \authors   The HEJ collaboration (see AUTHORS for details)
 *  \date      2019-2020
 *  \copyright GPLv2 or later
 */
#pragma once

#include <string>

#include "HEJ/exceptions.hh"

namespace HEJ {
  //! Possible status codes from the event generation
  enum StatusCode{
    good,
    discard,
    empty_jets,
    failed_reshuffle,
    failed_resummation_cuts,
    failed_split,
    too_much_energy,
    gluon_in_qqbar,
    wrong_jets,
    unspecified // should never appear
  };

  //! Get name of StatusCode
  //! @TODO better names
  inline std::string to_string(StatusCode s){
    switch(s){
      case good:                    return "good";
      case discard:                 return "discard";
      case empty_jets:              return "empty jets";
      case failed_reshuffle:        return "failed reshuffle";
      case failed_resummation_cuts: return "below cuts";
      case failed_split:            return "failed split";
      case too_much_energy:         return "too much energy";
      case gluon_in_qqbar:          return "gluon inside qqbar";
      case wrong_jets:              return "wrong jets";
      case unspecified:             return "unspecified";
      default:{}
    }
    throw std::logic_error{"unreachable"};
  }
} // namespace HEJ

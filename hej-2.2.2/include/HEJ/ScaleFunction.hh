/** \file
 *  \brief Functions to calculate the (renormalisation and factorisation) scales for an event
 *
 *  \authors   The HEJ collaboration (see AUTHORS for details)
 *  \date      2019-2020
 *  \copyright GPLv2 or later
 */
#pragma once

#include <functional>
#include <memory>
#include <string>
#include <utility>
#include <vector>

namespace HEJ {
  class Event;

  //! Class to calculate the scale associated with an event
  class ScaleFunction {
  public:
    //! Constructor
    /**
     *  @param name     Name of the scale choice (e.g. H_T)
     *  @param fun      Function used to calculate the scale
     */
    ScaleFunction(std::string name, std::function<double(Event const &)> fun):
      name_{std::move(name)},
      fun_{std::move(fun)}
    {}

    //! Name of the scale choice
    std::string const & name() const {
      return name_;
    }

    //! Calculate the scale associated with an event
    double operator()(Event const & ev) const {
      return fun_(ev);
    }

  private:
    friend ScaleFunction operator*(double factor, ScaleFunction base_scale);
    friend ScaleFunction operator/(ScaleFunction base_scale, double denom);
    friend ScaleFunction operator*(ScaleFunction factor, ScaleFunction base_scale);
    friend ScaleFunction operator/(ScaleFunction base_scale, ScaleFunction denom);

    std::string name_;
    std::function<double(Event const &)> fun_;
  };

  //! Multiply a scale choice by a constant factor
  /**
   * For example, multiplying 0.5 and a scale function for H_T
   * will result in a scale function for H_T/2.
   */
  ScaleFunction operator*(double factor, ScaleFunction base_scale);
  //! Multiply a scale choice by a second one
  /**
   * For example, multiplying H_T and m_j1j2
   * will result in a scale function for H_T*m_j1j2.
   */
  ScaleFunction operator*(ScaleFunction factor, ScaleFunction base_scale);
  //! Divide a scale choice by a constant factor
  /**
   * For example, dividing a scale function for H_T by 2
   * will result in a scale function for H_T/2.
   */
  ScaleFunction operator/(ScaleFunction base_scale, double denom);
  //! Divide a scale choice by a second one
  /**
   * For example, dividing a scale function for H_T by m_j1j2
   * will result in a scale function for H_T/m_j1j2.
   */
  ScaleFunction operator/(ScaleFunction base_scale, ScaleFunction denom);

  //! Calculate \f$H_T\f$ for the input event
  /**
   * \f$H_T\f$ is the sum of the (scalar) transverse momenta of all
   * final-state particles
   */
  double H_T(Event const & /*ev*/);
  //! The maximum of all (scalar) jet transverse momentum
  double max_jet_pt(Event const & /*ev*/);
  //! The invariant mass of the sum of all jet momenta
  double jet_invariant_mass(Event const & /*ev*/);
  //! Invariant mass of the two hardest jets
  double m_j1j2(Event const & /*ev*/);

  //! Functor that returns a fixed scale regardless of the input event
  class FixedScale {
  public:
    explicit FixedScale(double mu): mu_{mu} {}
    double operator()(Event const & /*unused*/) const {
      return mu_;
    }
  private:
    double mu_;
  };

  struct ParameterDescription;

  //! Generate combinations of renormalisation and factorisation scales
  class ScaleGenerator{
  public:
    ScaleGenerator() = default;

    /** \brief Constructor
     * @param scale_functions_begin   Iterator to first base scale
     * @param scale_functions_end     Iterator past last base scale
     * @param scale_factors_begin     Iterator to first scale factor
     * @param scale_factors_end       Iterator past last scale factor
     * @param max_scale_ratio         Maximum ratio between renormalisation
     *                                and factorisation scale
     */
    template<class ScaleFunIterator, class FactorIterator>
    ScaleGenerator(
        ScaleFunIterator scale_functions_begin,
        ScaleFunIterator scale_functions_end,
        FactorIterator scale_factors_begin,
        FactorIterator scale_factors_end,
        double max_scale_ratio
    ):
      scales_(scale_functions_begin, scale_functions_end),
      scale_factors_(scale_factors_begin, scale_factors_end),
      max_scale_ratio_{max_scale_ratio}
    {
      gen_descriptions();
    }

    /** \brief Constructor
     * @param scales            Base scales
     * @param scale_factors     Factors to multiply the base scales
     * @param max_scale_ratio   Maximum ratio between renormalisation
     *                          and factorisation scale
     */
    ScaleGenerator(
        std::vector<ScaleFunction> scales,
        std::vector<double> scale_factors,
        double max_scale_ratio
    ):
      scales_(std::move(scales)),
      scale_factors_(std::move(scale_factors)),
      max_scale_ratio_{max_scale_ratio}
    {
      gen_descriptions();
    }

    /** \brief Adjust event parameters, adding scale variation
     *
     * The central renormalisation and factorisation scale of the returned
     * event is given be the first base scale passed to the constructor.
     * The scale variation (stored in event.variation()) is constructed as
     * follows: For each base scale according to the arguments of the
     * constructor we add one variation where both renormalisation and
     * factorisation scale are set according to the current base scale.
     * Then, all combinations where the base renormalisation and factorisation
     * scales are multiplied by one of the scale factors are added.
     * The case were both scales are multiplied by one is skipped.
     * Scale combinations where the ratio is larger than the maximum scale ratio
     * set in the constructor are likewise discarded.
     */
    Event operator()(Event event) const;

  private:
    void gen_descriptions();

    std::vector<ScaleFunction> scales_;
    std::vector<double> scale_factors_;
    std::vector<std::shared_ptr<ParameterDescription>> descriptions_;
    double max_scale_ratio_{};
  };

} // namespace HEJ

/** \file
 *  \brief Declares a factory function for event writers
 *
 *  \authors   The HEJ collaboration (see AUTHORS for details)
 *  \date      2019-2020
 *  \copyright GPLv2 or later
 */
#pragma once

#include <memory>
#include <string>

#include "HEJ/EventWriter.hh"
#include "HEJ/output_formats.hh"

namespace LHEF{
  class HEPRUP;
}

namespace HEJ {

  //! Factory function for event writers
  /**
   *  @param format    The format of the output file
   *  @param outfile   The name of the output file
   *  @param heprup    General process information
   *  @returns         A pointer to an instance of an EventWriter
   *                   for the desired format
   */
  std::unique_ptr<EventWriter> make_format_writer(
      FileFormat format,
      std::string const &  outfile,
      LHEF::HEPRUP const & heprup
  );

} // namespace HEJ

/**
 *  \authors   The HEJ collaboration (see AUTHORS for details)
 *  \date      2020
 *  \copyright GPLv2 or later
 */
/** \file
 *  \brief Functions computing the square of current contractions in Z+Jets.
 */

#pragma once

#include <vector>

#include "CLHEP/Vector/LorentzVector.h"
#include "HEJ/PDG_codes.hh"

namespace HEJ {
  struct ParticleProperties;

namespace currents {
  using HLV = CLHEP::HepLorentzVector;

  //! Square of qQ->qQe+e- Z+Jets Scattering Current
  /**
   *  @param pa        Momentum of initial state quark
   *  @param pb        Momentum of initial state quark
   *  @param p1        Momentum of final state quark
   *  @param p2        Momentum of final state quark
   *  @param pep       Momentum of final state positron
   *  @param pem       Momentum of final state electron
   *  @param aptype    Initial particle 1 type
   *  @param bptype    Initial particle 2 type
   *  @param zprop     Mass and width of the Z boson
   *  @param stw2      Value of sin(theta_w)^2
   *  @param ctw       Value of cos(theta_w)
   *  @returns         Square of the current contractions for qQ->qQe+e- Scattering
   *
   *  This returns the square of the current contractions in qQ->qQ scattering
   *  with an emission of a Z Boson.
   */
  std::vector <double> ME_Z_qQ(const HLV & pa, const HLV & pb,
                              const HLV & p1, const HLV & p2,
                              const HLV & pep, const HLV & pem,
                              ParticleID aptype, ParticleID bptype,
                              ParticleProperties const & zprop,
                              double stw2, double ctw);

  //! Square of qg->qge+e- Z+Jets Scattering Current
  /**
   *  @param pa        Momentum of initial state quark
   *  @param pb        Momentum of initial state gluon
   *  @param p1        Momentum of final state quark
   *  @param p2        Momentum of final state gluon
   *  @param pep       Momentum of final state positron
   *  @param pem       Momentum of final state electron
   *  @param aptype    Initial particle 1 type
   *  @param bptype    Initial particle 2 type
   *  @param zprop     Mass and width of the Z boson
   *  @param stw2      Value of sin(theta_w)^2
   *  @param ctw       Value of cos(theta_w)
   *  @returns         Square of the current contractions for qg->qge+e- Scattering
   *
   *  This returns the square of the current contractions in qg->qg scattering
   *  with an emission of a Z Boson.
   */
  double ME_Z_qg(const HLV & pa, const HLV & pb,
                const HLV & p1, const HLV & p2,
                const HLV & pep, const HLV & pem,
                ParticleID aptype, ParticleID bptype,
                ParticleProperties const & zprop,
                double stw2, double ctw);

  //! Square of qQ->gqQe+e- Z+Jets Unordered Current
  /**
   *  @param pa        Momentum of initial state quark a
   *  @param pb        Momentum of initial state quark b
   *  @param pg        Momentum of final state unordered gluon
   *  @param p1        Momentum of final state quark a
   *  @param p2        Momentum of final state quark b
   *  @param pep       Momentum of final state positron
   *  @param pem       Momentum of final state electron
   *  @param aptype    Initial particle 1 type
   *  @param bptype    Initial particle 2 type
   *  @param zprop     Mass and width of the Z boson
   *  @param stw2      Value of sin(theta_w)^2
   *  @param ctw       Value of cos(theta_w)
   *  @returns         Square of the current contractions for qQ->gqQe+e- Scattering
   *
   *  This returns the square of the current contractions in qQ->gqQ scattering
   *  with an emission of a Z Boson.
   */
  std::vector <double> ME_Zuno_qQ(const HLV & pa, const HLV & pb,
                                  const HLV & pg, const HLV & p1, const HLV & p2,
                                  const HLV & pep, const HLV & pem,
                                  ParticleID aptype, ParticleID bptype,
                                  ParticleProperties const & zprop,
                                  double stw2, double ctw);

  //! Square of qg->gqge+e- Z+Jets Unordered Current
  /**
   *  @param pa        Momentum of initial state quark
   *  @param pb        Momentum of initial state gluon
   *  @param pg        Momentum of final state unordered gluon
   *  @param p1        Momentum of final state quark
   *  @param p2        Momentum of final state gluon
   *  @param pep       Momentum of final state positron
   *  @param pem       Momentum of final state electron
   *  @param aptype    Initial particle 1 type
   *  @param bptype    Initial particle 2 type
   *  @param zprop     Mass and width of the Z boson
   *  @param stw2      Value of sin(theta_w)^2
   *  @param ctw       Value of cos(theta_w)
   *  @returns         Square of the current contractions for qg->gqge+e- Scattering
   *
   *  This returns the square of the current contractions in qg->gqg scattering
   *  with an emission of a Z Boson.
   */
  double ME_Zuno_qg(const HLV & pa, const HLV & pb,
                    const HLV & pg, const HLV & p1, const HLV & p2,
                    const HLV & pep, const HLV & pem,
                    ParticleID aptype, ParticleID bptype,
                    ParticleProperties const & zprop,
                    double stw2, double ctw);


} // namespace currents
} // namespace HEJ

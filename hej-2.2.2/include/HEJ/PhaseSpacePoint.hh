/** \file
 *  \brief Contains the PhaseSpacePoint Class
 *
 *  \authors   The HEJ collaboration (see AUTHORS for details)
 *  \date      2019-2020
 *  \copyright GPLv2 or later
 */
#pragma once

#include <array>
#include <cstddef>
#include <unordered_map>
#include <utility>
#include <vector>

#include "fastjet/PseudoJet.hh"

#include "HEJ/Config.hh"
#include "HEJ/Event.hh"
#include "HEJ/Particle.hh"
#include "HEJ/StatusCode.hh"

namespace HEJ {
  struct RNG;

  //! Generated point in resummation phase space
  class PhaseSpacePoint{
  public:
    //! No default PhaseSpacePoint Constructor
    PhaseSpacePoint() = delete;

    //! PhaseSpacePoint Constructor
    /**
     * @param ev               Clustered Jet Event
     * @param conf             Configuration parameters
     * @param ran              Random number generator
     */
    PhaseSpacePoint(
        Event const & ev,
        PhaseSpacePointConfig conf,
        RNG & ran
    );

    //! Get phase space point weight
    double weight() const{
      return weight_;
    }

    //! Access incoming particles
    std::array<Particle, 2> const & incoming() const{
      return incoming_;
    }

    //! Access outgoing particles
    std::vector<Particle> const & outgoing() const{
      return outgoing_;
    }

    //! Particle decays
    /**
     *  The key in the returned map corresponds to the index in the
     *  vector returned by outgoing()
     */
    std::unordered_map<std::size_t, std::vector<Particle>> const &  decays()
    const{
      return decays_;
    }

    //! Status code of generation
    StatusCode status() const{
        return status_;
    }

  private:
    friend Event::EventData to_EventData(PhaseSpacePoint psp);
    //! /internal returns the clustered jets sorted in rapidity
    std::vector<fastjet::PseudoJet> cluster_jets(
        std::vector<fastjet::PseudoJet> const & partons
    ) const;
    bool pass_resummation_cuts(
        std::vector<fastjet::PseudoJet> const & jets
    ) const;
    bool pass_extremal_cuts(
        fastjet::PseudoJet const & ext_parton,
        fastjet::PseudoJet const & jet
    ) const;
    double estimate_emission_rapidity_range(Event const & event) const;
    double estimate_ng_mean(Event const & event) const;
    int sample_ng(Event const & event, RNG & ran);
    int sample_ng_jets(Event const & event, int ng, RNG & ran);
    double probability_in_jet(Event const & event) const;
    std::vector<fastjet::PseudoJet> gen_non_jet(
        int ng_non_jet, double ptmin, double ptmax, RNG & ran
    );
    void rescale_qqbar_rapidities(
      std::vector<fastjet::PseudoJet> & out_partons,
      std::vector<fastjet::PseudoJet> const & jets,
      double ymin1, double ymax2,
      int qqbar_backjet
    );
    void rescale_rapidities(
      std::vector<fastjet::PseudoJet> & partons,
      double ymin, double ymax
    );
    //! @return jets & Bosons
    std::pair< std::vector<fastjet::PseudoJet>,
               std::vector<fastjet::PseudoJet> >
    reshuffle(
        Event const & ev,
        fastjet::PseudoJet const & q
    );
    /** \interal final jet test
     *   - number of jets must match Born kinematics
     *   - no partons designated as nonjet may end up inside jets
     *   - all other outgoing partons *must* end up inside jets
     *   - the extremal (in rapidity) partons must be inside the extremal jets
     *   - rapidities must be the same (by construction)
     */
    bool jets_ok(
      Event const & Born_event,
      std::vector<fastjet::PseudoJet> const & partons
    ) const;
    void reconstruct_incoming(std::array<Particle, 2> const & Born_incoming);
    /** \interal Distribute gluon in jet
     * @param jets          jets to distribute gluon in
     * @param ng_jets       number of gluons
     * @param qqbar_backjet position of first (backwards) qqbar jet
     *
     * relies on JetSplitter
     */
    std::vector<fastjet::PseudoJet> split(
      Event const & Born_event,
      std::vector<fastjet::PseudoJet> const & jets,
      int ng_jets,
      RNG & ran
    );
    std::vector<int> distribute_jet_partons(
        int ng_jets, std::vector<fastjet::PseudoJet> const & jets, RNG & ran
    );
    std::vector<fastjet::PseudoJet> split(
      Event const & Born_event,
      std::vector<fastjet::PseudoJet> const & jets,
      std::vector<int> const & np_in_jet,
      RNG & ran
    );
    bool split_preserved_jets(
        std::vector<fastjet::PseudoJet> const & jets,
        std::vector<fastjet::PseudoJet> const & jet_partons
    ) const;
    template<class Particle>
    Particle const & most_backward_FKL(
        std::vector<Particle> const & partons
    ) const;
    template<class Particle>
    Particle const & most_forward_FKL(
        std::vector<Particle> const & partons
    ) const;
    template<class Particle>
    Particle & most_backward_FKL(std::vector<Particle> & partons) const;
    template<class Particle>
    Particle & most_forward_FKL(std::vector<Particle> & partons) const;
    bool extremal_ok(
      Event const & Born_event,
      std::vector<fastjet::PseudoJet> const & partons
    ) const;
    /** \internal
     * Assigns PDG IDs to outgoing partons, i.e. labels them as quarks
     *
     * \note This function assumes outgoing_ to be pure partonic when called,
     *       i.e.  A/W/Z/h bosons should _not be set_ at this stage
     */
    void label_quarks(Event const & event);
    /** \internal
     * This function will label the qqbar pair in a qqbar event back to their
     * original types from the input event.
     */
    void label_qqbar(Event const & event);
    void boost_AWZH_bosons_from(
        std::vector<fastjet::PseudoJet> const & boosted_bosons,
        Event const & event
    );

    bool momentum_conserved() const;

    bool contains_idx(
        fastjet::PseudoJet const & jet, fastjet::PseudoJet const & parton
    ) const;

    //! @TODO replace this with an type
    bool unob_, unof_, qqbarb_, qqbarf_, qqbar_mid_;

    double weight_;

    PhaseSpacePointConfig param_;

    std::array<Particle, 2> incoming_;
    std::vector<Particle> outgoing_;
    //! \internal Particle decays in the format {outgoing index, decay products}
    std::unordered_map<std::size_t, std::vector<Particle>> decays_;

    StatusCode status_;
  };

  //! Extract Event::EventData from PhaseSpacePoint
  Event::EventData to_EventData(PhaseSpacePoint psp);

} // namespace HEJ

/** \file
 *  \brief Defines the settings for Higgs boson coupling to gluons
 *
 *  \authors   The HEJ collaboration (see AUTHORS for details)
 *  \date      2019-2020
 *  \copyright GPLv2 or later
 */
#pragma once

#include <limits>

namespace HEJ {
  //! Settings for Higgs boson coupling to gluons
  struct HiggsCouplingSettings{
    //! Top quark mass
    double mt = std::numeric_limits<double>::infinity();
    //! Bottom quark mass
    double mb = 4.7;
    //! Whether to use impact factors
    bool use_impact_factors = true;
    //! Whether to include bottom quark effects
    bool include_bottom = false;
  };
} // namespace HEJ

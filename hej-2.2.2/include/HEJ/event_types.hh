/** \file
 *  \brief Define different types of events.
 *
 *  \authors   The HEJ collaboration (see AUTHORS for details)
 *  \date      2019-2020
 *  \copyright GPLv2 or later
 */
#pragma once

#include <string>

#include "HEJ/exceptions.hh"

namespace HEJ {

  //! Namespace for event types
  namespace event_type {
    //! Possible event types
    enum EventType: std::size_t {
      non_resummable = 0,     //!< event configuration not covered by All Order resummation
      invalid = 1,            //!< unphysical event, e.g. violating conservation laws
      unknown = 2,            //!< configuration not considered by HEJ, e.g di-Higgs
      FKL = 4,                //!< FKL-type event
      unordered_backward = 8, //!< event with unordered backward emission
      unordered_forward = 16, //!< event with unordered forward emission
      extremal_qqbar_backward = 32,        //!< event with a backward extremal qqbar
      extremal_qqbar_forward = 64,         //!< event with a forward extremal qqbar
      central_qqbar = 128,                 //!< event with a central qqbar
      unob = unordered_backward,           //!< alias for unordered_backward
      unof = unordered_forward,            //!< alias for unordered_forward
      qqbar_exb = extremal_qqbar_backward, //!< alias for extremal_qqbar_backward
      qqbar_exf = extremal_qqbar_forward,  //!< alias for extremal_qqbar_forward
      qqbar_mid = central_qqbar,           //!< alias for central_qqbar
      first_type = non_resummable,         //!< alias for numerically smallest enumerator
      last_type = central_qqbar            //!< alias for numerically largest enumerator
    };

    constexpr std::size_t UNO =  unordered_backward | unordered_forward;

    constexpr std::size_t EXTREMAL_QQBAR =
      extremal_qqbar_backward | extremal_qqbar_forward;

    constexpr std::size_t QQBAR =
      EXTREMAL_QQBAR | central_qqbar;

    constexpr auto NLL = UNO | QQBAR;

    constexpr auto RESUMMABLE = FKL | NLL;

    constexpr auto VALID = RESUMMABLE | non_resummable;

    //! Event type names
    /**
     * For example, name(FKL) is the string "FKL"
     */
    inline
    std::string name(EventType type) {
      switch(type) {
      case FKL:
        return "FKL";
      case unordered_backward:
        return "unordered backward";
      case unordered_forward:
        return "unordered forward";
      case extremal_qqbar_backward:
        return "extremal qqbar backward";
      case extremal_qqbar_forward:
        return "extremal qqbar forward";
      case central_qqbar:
        return "central qqbar";
      case non_resummable:
        return "non-resummable";
      case invalid:
        return "invalid";
      case unknown:
        return "unknown";
      default:
        throw std::logic_error{"Unreachable"};
      }
    }

    //! Returns True for a HEJ \ref event_type::EventType "EventType"
    inline
    constexpr bool is_resummable(EventType type) {
      return type & RESUMMABLE;
    }

    //! Returns True for an unordered \ref event_type::EventType "EventType"
    inline
    constexpr bool is_uno(EventType type) {
      return type & UNO;
    }

    //! Returns True for an extremal_qqbar \ref event_type::EventType "EventType"
    inline
    constexpr bool is_ex_qqbar(EventType type) {
      return type & EXTREMAL_QQBAR;
    }

    //! Returns True for an central_qqbar \ref event_type::EventType "EventType"
    inline
    constexpr bool is_mid_qqbar(EventType type) {
      return type == central_qqbar;
    }

    //! Returns True for any qqbar event \ref event_type::EventType "EventType"
    inline
    constexpr bool is_qqbar(EventType type) {
      return type & QQBAR;
    }

  } // namespace event_type
} // namespace HEJ

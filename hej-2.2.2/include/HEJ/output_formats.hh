/** \file
 *  \brief Defines formats for output to event files
 *
 *  \authors   The HEJ collaboration (see AUTHORS for details)
 *  \date      2019
 *  \copyright GPLv2 or later
 */
#pragma once

#include <stdexcept>
#include <string>

namespace HEJ {

  //! Supported event file formats
  enum class FileFormat{
    Les_Houches, /*!< Les Houches Output */
    HepMC3,      /*!< HepMC3 Output */
    HepMC2,      /*!< HepMC2 Output */
    HDF5,        /*!< HDF5 Output */
    HepMC=HepMC3 /*!< HepMC3 Output */
  };

  //! Convert a file format to a string
  inline std::string to_string(FileFormat f){
    switch(f){
    case FileFormat::Les_Houches: return "Les Houches";
    case FileFormat::HepMC2: return "HepMC2";
    case FileFormat::HepMC3: return "HepMC3";
    case FileFormat::HDF5: return "HDF5";
    default:
      throw std::logic_error("unhandled file format");
    }
  }

  //! Output file specification
  struct OutputFile{
    std::string name;     /**< Output File Name */
    FileFormat format;    /**< Output File Format */
  };

} // namespace HEJ

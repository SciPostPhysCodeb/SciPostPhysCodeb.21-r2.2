/** \file
 *  \brief Declares the CombinedEventWriter class
 *
 *  \authors   The HEJ collaboration (see AUTHORS for details)
 *  \date      2019-2023
 *  \copyright GPLv2 or later
 */
#pragma once

#include <memory>
#include <vector>

#include "HEJ/EventWriter.hh"

namespace LHEF {
  class HEPRUP;
}

namespace HEJ {
  class Event;
  struct OutputFile;

  //! Write event output to zero or more output files.
  class CombinedEventWriter: public EventWriter {
  public:
    //! Constructor
    /**
     * @param outfiles     Specifies files output should be written to.
     *                     Each entry in the vector contains a file name
     *                     and output format.
     * @param heprup       General process information
     */
    CombinedEventWriter(
        std::vector<OutputFile> const & outfiles,
        LHEF::HEPRUP const & heprup
    );
    ~CombinedEventWriter() override;

    //! Write one event to all output files
    void write(Event const & /*ev*/) override;

    //! Set the ratio (cross section) / (sum of event weights)
    void set_xs_scale(double scale) override;

    //! Finish writing, e.g. write out general informations at end of run
    //! @warning using the writer after finishing is not guaranteed to work
    void finish() override;

  private:
    std::vector<std::unique_ptr<EventWriter>> writers_;
  };

} // namespace HEJ

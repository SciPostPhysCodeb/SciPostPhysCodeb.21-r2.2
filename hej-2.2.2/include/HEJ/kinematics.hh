/** \file
 *  \brief Contains function to compute the incoming momentum from outgoing.
 *
 *  \authors   The HEJ collaboration (see AUTHORS for details)
 *  \date      2019-2020
 *  \copyright GPLv2 or later
 */
#pragma once

#include <tuple>
#include <vector>

namespace fastjet {
  class PseudoJet;
}

namespace HEJ {
  struct Particle;

  /** \brief Compute the incoming momentum from momentum conservation.
   */
  std::tuple<fastjet::PseudoJet, fastjet::PseudoJet> incoming_momenta(
            std::vector<Particle> const & outgoing    /**< Outgoing particles */
  );
}

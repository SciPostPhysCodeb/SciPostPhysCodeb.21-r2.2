/** \file
 *  \brief Functions to calculate the kinematics of resummation jets,
 *         i.e. resuffling the jet momenta
 *
 *  \authors   The HEJ collaboration (see AUTHORS for details)
 *  \date      2019-2020
 *  \copyright GPLv2 or later
 */
#pragma once

#include <vector>

namespace fastjet {
  class PseudoJet;
}

namespace HEJ {
  /**
   * \brief Calculate the resummation jet momenta
   * @param p_born              born Jet Momenta
   * @param qperp               Sum of non-jet Parton Transverse Momenta
   * @returns                   Resummation Jet Momenta
   */
  std::vector<fastjet::PseudoJet> resummation_jet_momenta(
      std::vector<fastjet::PseudoJet const *> const & p_born,
      fastjet::PseudoJet const & qperp
  );

  /**
   * \brief Calculate additional weight from changing the jet momenta
   * @param p_born              born Jet Momenta
   * @param qperp               Sum of non-jet Parton Transverse Momenta
   *
   *  Computes the Jacobian for changing the original delta functions
   *  expressed in terms of jet momenta to delta functions of the
   *  parton momenta in the resummation phase space
   */
  double resummation_jet_weight(
      std::vector<fastjet::PseudoJet const *> const & p_born,
      fastjet::PseudoJet const & qperp
  );

} // namespace HEJ

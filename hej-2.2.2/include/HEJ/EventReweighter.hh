/** \file
 *  \brief Declares the EventReweighter class
 *
 *  EventReweighter is the main class used within HEJ 2. It reweights the
 *  resummation events.
 *
 *  \authors   The HEJ collaboration (see AUTHORS for details)
 *  \date      2019-2020
 *  \copyright GPLv2 or later
 */
#pragma once

#include <array>
#include <cstddef>
#include <memory>
#include <utility>
#include <vector>

#include "HEJ/Config.hh"
#include "HEJ/MatrixElement.hh"
#include "HEJ/PDF.hh"
#include "HEJ/PDG_codes.hh"
#include "HEJ/Parameters.hh"
#include "HEJ/ScaleFunction.hh"
#include "HEJ/StatusCode.hh"
#include "HEJ/event_types.hh"

namespace LHEF {
  class HEPRUP;
}

namespace HEJ {
  class Event;
  struct RNG;

  //! Beam parameters
  /**
   *  Currently, only symmetric beams are supported,
   *  so there is a single beam energy.
   */
  struct Beam{
    double E;                                /**< Beam energy */
    std::array<ParticleID, 2> type;          /**< Beam particles */
  };

  //! Main class for reweighting events in HEJ.
  class EventReweighter{
    using EventType = event_type::EventType;

  public:
    EventReweighter(
        Beam const & beam,                    /**< Beam Energy */
        int pdf_id,                           /**< PDF ID */
        ScaleGenerator scale_gen,             /**< Scale settings */
        EventReweighterConfig conf,           /**< Configuration parameters */
        std::shared_ptr<RNG> ran              /**< Random number generator */
    );

    EventReweighter(
        LHEF::HEPRUP const & heprup,          /**< LHEF event header */
        ScaleGenerator scale_gen,             /**< Scale settings */
        EventReweighterConfig conf,           /**< Configuration parameters */
        std::shared_ptr<RNG> ran              /**< Random number generator */
    );

    //! Get the used pdf
    PDF const & pdf() const;

    //! Check the lowpt only restriction passes for lowpt runs
    bool pass_low_pt(
        HEJ::Event const & input_ev
    );

    //! Get event treatment
    EventTreatment treatment(EventType type) const;

    //! Generate resummation events for a given fixed-order event
    /**
     *  @param ev             Fixed-order event corresponding
     *                        to the resummation events
     *  @param num_events     Number of trial resummation configurations.
     *  @returns              A vector of resummation events.
     *
     *  The result vector depends on the type of the input event and the
     *  \ref EventTreatment of different types as specified in the constructor:
     *
     *  - EventTreatment::reweight: The result vector contains between 0 and
     *                              num_events resummation events.
     *  - EventTreatment::keep:     If the input event passes the resummation
     *                              jet cuts the result vector contains one
     *                              event. Otherwise it is empty.
     *  - EventTreatment::discard:  The result vector is empty
     */
    std::vector<Event> reweight(
        Event const & ev,
        std::size_t num_events
    );

    //! Gives all StatusCodes of the last reweight()
    /**
     * Each StatusCode corresponds to one tried generation. Only good
     * StatusCodes generated an event.
     */
    std::vector<StatusCode> const & status() const {
        return status_;
    }

  private:

    /** \internal
     * \brief main generation/reweighting function:
     * generate phase space points and divide out Born factors
     */
    std::vector<Event> gen_res_events(
        Event const & ev, std::size_t phase_space_points
    );
    std::vector<Event> rescale(
        Event const & Born_ev, std::vector<Event> events
    ) const;

    /** \internal
     * \brief Do the Jets pass the resummation Cuts?
     *
     * @param ev               Event in Question
     * @returns                0 or 1 depending on if ev passes Jet Cuts
     */
    bool jets_pass_resummation_cuts(Event const & ev) const;

    /** \internal
     * \brief pdf_factors Function
     *
     * @param ev         Event in Question
     * @returns          EventFactor due to PDFs
     *
     * Calculates the Central value and the variation due
     * to the PDF choice made.
     */
    Weights pdf_factors(Event const & ev) const;

    /** \internal
     * \brief matrix_elements Function
     *
     * @param ev         Event in question
     * @returns          EventFactor due to MatrixElements
     *
     * Calculates the Central value and the variation due
     * to the Matrix Element.
     */
    Weights matrix_elements(Event const & ev) const;

    /** \internal
     * \brief Scale-dependent part of fixed-order matrix element
     *
     * @param ev         Event in question
     * @returns          EventFactor scale variation due to FO-ME.
     *
     * This is only called to compute the scale variation for events where
     * we don't do resummation (e.g. non-FKL).
     * Since at tree level the scale dependence is just due to alpha_s,
     * it is enough to return the alpha_s(mur) factors in the matrix element.
     * The rest drops out in the ratio of (output event ME)/(input event ME),
     * so we never have to compute it.
     */
    Weights fixed_order_scale_ME(Event const & ev) const;

    /** \internal
     * \brief Computes the tree level matrix element
     *
     * @param ev                Event in Question
     * @returns                 HEJ approximation to Tree level Matrix Element
     *
     * This computes the HEJ approximation to the tree level FO
     * Matrix element which is used within the LO weighting process.
     */
    double tree_matrix_element(Event const & ev) const;

    //! \internal General parameters
    EventReweighterConfig param_;

    //! \internal Beam energy
    double E_beam_;

    //! \internal PDF
    PDF pdf_;

    //! \internal Object to calculate the square of the matrix element
    MatrixElement MEt2_;
    //! \internal Object to calculate event renormalisation and factorisation scales
    ScaleGenerator scale_gen_;
    //! \internal random number generator
    std::shared_ptr<RNG> ran_;
    //! \internal StatusCode of each attempt
    std::vector<StatusCode> status_;
  };

  //! Exception indicating that an event is not accepted by the reweighter
  /**
   *  When constructing an EventReweighter one can specify the \ref abort treatment
   *  for a certain EventType. If such an Event is passed to EventReweighter::reweight(),
   *  this exception is thrown.
   */
  struct abort_event: std::invalid_argument {
    explicit abort_event(Event const & ev);
  };

} // namespace HEJ

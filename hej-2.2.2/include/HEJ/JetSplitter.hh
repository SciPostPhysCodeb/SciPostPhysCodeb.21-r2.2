/**
 * \file
 * \brief Declaration of the JetSplitter class
 *
 *  \authors   The HEJ collaboration (see AUTHORS for details)
 *  \date      2019-2020
 *  \copyright GPLv2 or later
 */
#pragma once

#include <vector>

#include "fastjet/JetDefinition.hh"
#include "fastjet/PseudoJet.hh"

namespace HEJ {
  struct RNG;

  //! Class to split jets into their constituents
  class JetSplitter {
  public:
    //! Wrapper for return values
    struct SplitResult {
      std::vector<fastjet::PseudoJet> constituents;
      double weight;
    };

    //! Constructor
    /**
     * @param jet_def   Jet definition
     * @param min_pt    Minimum jet transverse momentum
     */
    JetSplitter(fastjet::JetDefinition jet_def, double min_pt);

    //! Split a get into constituents
    /**
     * @param j2split   Jet to be split
     * @param ncons     Number of constituents
     * @param ran       Random number generator
     * @returns         The constituent momenta together with the associated
     *                  weight
     */
    SplitResult split(
        fastjet::PseudoJet const & j2split, int ncons, RNG & ran
    ) const;

    //! Maximum distance of constituents to jet axis
    static constexpr double R_FACTOR = 5./3.;

  private:
    //! \internal split jet into two partons
    SplitResult Split2(fastjet::PseudoJet const & j2split, RNG & ran) const;

    double min_jet_pt_;
    fastjet::JetDefinition jet_def_;
  };
} // namespace HEJ

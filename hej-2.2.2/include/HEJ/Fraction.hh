/** \file
 *  \brief Header file for fractions, i.e. floating point numbers between 0 and 1.
 *
 *  \authors   The HEJ collaboration (see AUTHORS for details)
 *  \date      2020
 *  \copyright GPLv2 or later
 */
#pragma once

#include <stdexcept>
#include <string>
#include <type_traits>

namespace HEJ {

  //! Class for floating point numbers between 0 and 1
  template<
    class Real,
    typename = std::enable_if_t<std::is_floating_point<Real>::value>
    >
  class Fraction {
  public:
    Fraction() = default;

    explicit Fraction(Real r): f_{r} {
      if(f_ < 0 || f_ > 1) {
        throw std::invalid_argument{
          std::to_string(f_) + " is not between 0 and 1"
        };
      }
    }

    Fraction & operator=(Real r) {
      f_ = Fraction{r};
      return *this;
    }

    operator Real() const {
      return f_;
    }

  private:
    Real f_;
  };
} // namespace HEJ

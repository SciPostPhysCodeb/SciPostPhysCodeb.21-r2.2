/**
 *  \authors   The HEJ collaboration (see AUTHORS for details)
 *  \date      2019-2022
 *  \copyright GPLv2 or later
 */
/** \file
 *  \brief Functions computing the square of current contractions in W+Jets.
 *
 *  This file contains all the W+Jet specific components to compute
 *  the current contractions for valid HEJ processes.
 */

#pragma once

#include <vector>

#include "CLHEP/Vector/LorentzVector.h"

namespace HEJ {
  struct ParticleProperties;

namespace currents {
  using HLV = CLHEP::HepLorentzVector;


  //! Square of qQ->qenuQ W+Jets Scattering Current
  /**
  *  @param p1out     Momentum of final state quark
  *  @param plbar     Momentum of final state anti-lepton
  *  @param pl        Momentum of final state lepton
  *  @param p1in      Momentum of initial state quark
  *  @param p2out     Momentum of final state quark
  *  @param p2in      Momentum of intial state quark
  *  @param wprop     Mass and width of the W boson
  *  @returns         Square of the current contractions for qQ->qenuQ
  *
  *  This returns the square of the current contractions in qQ->qenuQ scattering
  *  with an emission of a W Boson off the quark q.
  */
  double ME_W_qQ(HLV const & p1out,
                 HLV const & plbar, HLV const & pl,
                 HLV const & p1in,
                 HLV const & p2out, HLV const & p2in,
                 ParticleProperties const & wprop);

  //! W+uno same leg
  /**
  *  @param p1out     Momentum of final state quark a
  *  @param p1in      Momentum of initial state quark a
  *  @param p2out     Momentum of final state quark b
  *  @param p2in      Momentum of intial state quark b
  *  @param pg        Momentum of final state unordered gluon
  *  @param plbar     Momentum of final state anti-lepton
  *  @param pl        Momentum of final state lepton
  *  @param wprop     Mass and width of the W boson
  *  @returns         Square of the current contractions for qQ->qQg
  *
  *  This returns the square of the current contractions in gqQ->gqQ scattering
  *  with an emission of a W Boson.
  */
  double ME_Wuno_qQ(HLV const & p1out, HLV const & p1in,
                    HLV const & p2out, HLV const & p2in, HLV const & pg,
                    HLV const & plbar, HLV const & pl,
                    ParticleProperties const & wprop);

  //! qQg Wjets Unordered backwards opposite leg to W
  /**
  *  @param p1out     Momentum of final state quark a
  *  @param p1in      Momentum of initial state quark a
  *  @param p2out     Momentum of final state quark b
  *  @param p2in      Momentum of intial state quark b
  *  @param pg        Momentum of final state unordered gluon
  *  @param plbar     Momentum of final state anti-lepton
  *  @param pl        Momentum of final state lepton
  *  @param wprop     Mass and width of the W boson
  *  @returns         Square of the current contractions for qQ->qQg Scattering
  *
  *  This returns the square of the current contractions in qQg->qQg scattering
  *  with an emission of a W Boson.
  */
  double ME_W_unob_qQ(HLV const & p1out, HLV const & p1in,
                      HLV const & p2out, HLV const & p2in, HLV const & pg,
                      HLV const & plbar, HLV const & pl,
                      ParticleProperties const & wprop);

  //! W+Extremal qqbar. qqbar+Q
  /**
  *  @param pgin      Momentum of initial state gluon
  *  @param pqbarout  Momentum of final state anti-quark a
  *  @param plbar     Momentum of final state anti-lepton
  *  @param pl        Momentum of final state lepton
  *  @param pqout     Momentum of final state quark a
  *  @param p2out     Momentum of initial state anti-quark b
  *  @param p2in      Momentum of final state gluon b
  *  @param wprop     Mass and width of the W boson
  *  @returns         Square of the current contractions for ->qqbarQ
  *
  *  Calculates the square of the current contractions with extremal qqbar pair
  *  production. This is calculated through the use of crossing symmetry.
  */
  double ME_WExqqbar_qqbarQ(HLV const & pgin, HLV const & pqbarout,
                            HLV const & plbar, HLV const & pl,
                            HLV const & pqout, HLV const & p2out,
                            HLV const & p2in,
                            ParticleProperties const & wprop);

  //! W+Extremal qqbar. gg->qqbarg. qqbar on forwards leg, W emission backwards leg.
  /**
  *  @param pa        Momentum of initial state (anti-)quark
  *  @param pb        Momentum of initial state gluon
  *  @param p1        Momentum of final state (anti-)quark (after W emission)
  *  @param pq        Momentum of final state quark
  *  @param pqbar     Momentum of final state anti-quark
  *  @param plbar     Momentum of final state anti-lepton
  *  @param pl        Momentum of final state lepton
  *  @param aqlinepa  Is opposite extremal leg to qqbar a quark or antiquark line
  *  @param wprop     Mass and width of the W boson
  *  @returns         Square of the current contractions for gq->qqbarqW
  *
  *  Calculates the square of the current contractions with extremal qqbar pair
  *  production. This is calculated via current contraction of existing
  *  currents. Assumes qqbar split from forwards leg, W emission from backwards
  *  leg. Switch input (pa<->pb, p1<->pn) if calculating forwards qqbar.
  */
  double ME_W_Exqqbar_QQq(HLV const & pa, HLV const & pb, HLV const & p1,
                          HLV const & p2, HLV const & p3,
                          HLV const & plbar, HLV const & pl, bool aqlinepa,
                          ParticleProperties const & wprop);

  //! W+Jets qqbarCentral. qqbar W emission.
  /**
  *  @param pa                Momentum of initial state particle a
  *  @param pb                Momentum of initial state particle b
  *  @param pl                Momentum of final state lepton
  *  @param plbar             Momentum of final state anti-lepton
  *  @param partons           Vector of outgoing parton momenta
  *  @param aqlinepa          True= pa is anti-quark
  *  @param aqlinepb          True= pb is anti-quark
  *  @param qqbar_marker      Ordering of the qqbar pair produced (q-qbar vs qbar-q)
  *  @param nabove            Number of lipatov vertices "above" qqbar pair
  *  @param wprop             Mass and width of the W boson
  *  @returns                 Square of the current contractions for qq>qQQbarWq
  *
  *  Calculates the square of the current contractions with extremal qqbar pair
  *  production. This is calculated through the use of crossing symmetry.
  */
  double ME_WCenqqbar_qq(HLV const & pa, HLV const & pb,
                         HLV const & pl, HLV const & plbar,
                         std::vector<HLV> const & partons,
                         bool aqlinepa, bool aqlinepb, bool qqbar_marker,
                         int nabove,
                         ParticleProperties const & wprop);

  //! W+Jets qqbarCentral. W emission from backwards leg.
  /**
  *  @param pa                Momentum of initial state particle a
  *  @param pb                Momentum of initial state particle b
  *  @param pl                Momentum of final state lepton
  *  @param plbar             Momentum of final state anti-lepton
  *  @param partons           outgoing parton momenta
  *  @param aqlinepa          True= pa is anti-quark
  *  @param aqlinepb          True= pb is anti-quark
  *  @param qqbar_marker      Ordering of the qqbar pair produced (q-qbar vs qbar-q)
  *  @param nabove            Number of lipatov vertices "above" qqbar pair
  *  @param nbelow            Number of lipatov vertices "below" qqbar pair
  *  @param forwards          Swap to emission off front leg
  *                           TODO: remove so args can be const
  *  @param wprop             Mass and width of the W boson
  *  @returns                 Square of the current contractions for qq>qQQbarWq
  *
  *  Calculates the square of the current contractions with extremal qqbar pair
  *  production. This is calculated through the use of crossing symmetry.
  */
  double ME_W_Cenqqbar_qq(HLV pa, HLV pb,
                          HLV pl, HLV plbar,
                          std::vector<HLV> partons,
                          bool aqlinepa, bool aqlinepb, bool qqbar_marker,
                          int nabove, int nbelow, bool forwards,
                          ParticleProperties const & wprop);

} // namespace currents
} // namespace HEJ

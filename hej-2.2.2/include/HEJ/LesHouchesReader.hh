/** \file
 *  \brief Header file for reading events in the Les Houches Event File format.
 *
 *  \authors   The HEJ collaboration (see AUTHORS for details)
 *  \date      2019-2022
 *  \copyright GPLv2 or later
 */
#pragma once

#include <string>
#include <numeric>

#include "LHEF/LHEF.h"

#include "HEJ/EventReader.hh"
#include "HEJ/stream.hh"

namespace HEJ {

  //! Class for reading events from a file in the Les Houches Event File format
  class LesHouchesReader : public EventReader{
  public:
    //! Contruct object reading from the given file
    explicit LesHouchesReader(std::string const & filename);

    //! Read an event
    bool read_event() override;

    //! Access header text
    std::string const & header() const override {
      return reader_.headerBlock;
    }

    //! Access run information
    LHEF::HEPRUP const & heprup() const override {
      return reader_.heprup;
    }

    //! Access last read event
    LHEF::HEPEUP const & hepeup() const override {
      return reader_.hepeup;
    }

    std::optional<size_t> number_events() const override {
      std::size_t start = header().rfind("Number of Events");
      start = header().find_first_of("123456789", start);
      if(start == std::string::npos) {
        return {};
      }
      const std::size_t end = header().find_first_not_of("0123456789", start);
      return std::stoi(header().substr(start, end - start));
    }

    double scalefactor() const override {
      if (generator_==Generator::Sherpa) {
        const std::size_t num_trials = std::accumulate(
          num_trials_.begin(), num_trials_.end(), 0
        );
        return 1./static_cast<double>(num_trials);
      }
      else
        return 1.;
    }

  private:
    enum class Generator{
      HEJ,
      HEJFOG,
      Sherpa,
      MG,
      unknown
    };

    bool calculate_XSECUP() {
      return calculate_XSECUP_;
    }
    Generator get_generator(
      LHEF::HEPRUP const & heprup, std::string const & header
    );

    istream stream_;
    LHEF::Reader reader_;
    std::vector<size_t> num_trials_;
    Generator generator_;
    bool calculate_XSECUP_;
  };

} // namespace HEJ

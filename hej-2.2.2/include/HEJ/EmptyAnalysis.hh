/** \file
 *  \brief Declaration of the trivial (empty) analysis
 *
 *  \authors   The HEJ collaboration (see AUTHORS for details)
 *  \date      2019-2022
 *  \copyright GPLv2 or later
 */
#pragma once

#include "HEJ/detail/EmptyAnalysis.hh"

namespace HEJ {
  /** An analysis that does nothing
   *
   *  This analysis is used by default if no user analysis is specified.
   *  The member functions don't do anything and events passed to the
   *  analysis are simply ignored.
   *
   *  \internal The actual logic is in detail::EmptyAnalysis.
   *            The reason for this split is that until HEJ 2.3
   *            `get_analysis` should return an `EmptyAnalysis` as a fallback,
   *            to not destroy backward compatibility without warning.
   *            Returning a detail::EmptyAnalysis instead avoids an internal
   *            deprecation warning during compilation.
   */
  struct [[deprecated("to be removed in HEJ 2.3")]]
  EmptyAnalysis: detail::EmptyAnalysis {
    static std::unique_ptr<Analysis> create(
        YAML::Node const & parameters,
        LHEF::HEPRUP const & /*unused*/
    );

    ~EmptyAnalysis() override = default;
  };
} // namespace HEJ

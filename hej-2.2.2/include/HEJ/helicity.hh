/** \file
 *  \brief Definition of Helicity
 *
 *  \authors   The HEJ collaboration (see AUTHORS for details)
 *  \date      2019-2020
 *  \copyright GPLv2 or later
 */
#pragma once

namespace HEJ {
  namespace helicity {
    enum Helicity {
      minus, plus
    };
  }
  using helicity::Helicity;

  /**
   * \brief Flip helicity
   * @param h   Helicity to flip
   * @returns   plus iff the input helicity was minus and vice versa
   */
  inline
  constexpr Helicity flip(Helicity h) {
    return (h == helicity::plus)?helicity::minus:helicity::plus;
  }
} // namespace HEJ

/** \file
 *  \brief Declares input streams
 *
 *  \authors   The HEJ collaboration (see AUTHORS for details)
 *  \date      2019-2020
 *  \copyright GPLv2 or later
 */
#pragma once

#include <fstream>
#include <memory>
#include <string>

#include "boost/iostreams/filtering_stream.hpp"

namespace HEJ {

  //! Small wrapper around boost's filtering_istream
  class istream {
    using boost_istream = boost::iostreams::filtering_istream;
  public:
    //! Constructor
    /**
     *  @param filename   Name of input file
     */
    explicit istream(std::string const & filename);

    //! Conversion to boost_istream
    operator boost_istream& () const noexcept {
      return *stream_;
    }

  private:
    std::ifstream file_;
    std::unique_ptr<boost_istream> stream_;
  };

} // namespace HEJ

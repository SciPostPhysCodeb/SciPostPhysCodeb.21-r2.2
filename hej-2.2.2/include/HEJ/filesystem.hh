/**
 * \file
 * \brief Utilities for interacting with the file system
 *
 *  \authors   The HEJ collaboration (see AUTHORS for details)
 *  \date      2023
 *  \copyright GPLv2 or later
 *
 * `std::filesystem` requires special library-dependent linker flags with older
 * versions of libstc++ and libc++. To avoid this we reimplement the limited
 * functionality we need using only POSIX.
 *
 */
#pragma once

namespace HEJ {
  //! Check if the file is regular
  bool is_regular(char const * filename);
}

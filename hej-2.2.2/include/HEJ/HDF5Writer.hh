/** \file
 *  \brief Contains the EventWriter for HDF5 Output.
 *
 *  The output format is specified in arXiv:1905.05120.
 *
 *  \authors   The HEJ collaboration (see AUTHORS for details)
 *  \date      2019-2023
 *  \copyright GPLv2 or later
 */
#pragma once

#include <memory>
#include <string>

#include "HEJ/EventWriter.hh"

namespace LHEF {
  class HEPRUP;
}

namespace HEJ {
  class Event;

  //! This is an event writer specifically for HDF5 output.
  /**
   * \internal Implementation note: This uses the pimpl ("pointer to
   * implementation") idiom. HDF5 support is optional. Without pimpl,
   * we would have to specify whether HDF5 is available via the
   * preprocessor whenever this header is included. We don't want to
   * burden users of the HEJ library (for example the HEJ fixed-order
   * generator) with those details
   */
  class HDF5Writer: public EventWriter{
  public:
    //! Constructor
    /**
     * @param file      name of the output file
     * @param heprup    general process information
     */
    HDF5Writer(std::string const & file, LHEF::HEPRUP heprup);
    HDF5Writer() = delete;

    //! Write an event to the output file
    void write(Event const & ev) override;

    //! Set the ratio (cross section) / (sum of event weights)
    void set_xs_scale(double scale) override;

    //! Finish writing
    void finish() override;

    ~HDF5Writer() override;

  private:
    struct HDF5WriterImpl;

    std::unique_ptr<HDF5WriterImpl> impl_;
  };

} // namespace HEJ

/** \file
 *  \brief Template functions shared between HepMC2 and HepMC3
 *
 *  \authors The HEJ collaboration (see AUTHORS for details)
 *  \date 2019-2020
 *  \copyright GPLv2 or later
 */
#pragma once

#include "HEJ/Event.hh"
#include "HEJ/Particle.hh"

namespace HEJ {
  namespace detail_HepMC {

    //! Convert Particle to HepMC 4-vector (x,y,z,E)
    template<class FourVector>
    FourVector to_FourVector(Particle const & sp){
      return {sp.px(), sp.py(), sp.pz(), sp.E()};
    }
    namespace status_codes {
      //! Conventional status codes in HepMC
      enum Status: unsigned {
        out = 1,    //!< Final outgoing
        decay = 2,  //!< Decaying
        beam = 4,   //!< Beam
        in = 11,    //!< Incoming
      };
    }
    using Status = status_codes::Status;

    //! @name Helper classes/functions
    //! @brief Have to be implemented for each HepMC version V
    //!@{
    template<int V> struct HepMCVersion;
    template<int V>
    using GenEvent = typename HepMCVersion<V>::GenEvent;
    template<int V>
    using Beam = typename HepMCVersion<V>::Beam;

    //! Make HepMC particle
    template<int V>
    auto make_particle_ptr(
        Particle const & sp, int status
    );

    //! Make HepMC vetex
    template<int V>
    auto make_vx_ptr();
    //!@}

    /**
     * @brief Template class to initialise the kinematics for HepMC
     *
     * @tparam V        HepMC major version
     * @param event     HEJ event
     * @param beam      beam particles
     * @param out_ev    new HepMC event
     */
    template<int V>
    GenEvent<V> HepMC_init_kinematics(
        Event const & event, Beam<V> const & beam, GenEvent<V> && out_ev
    ){
      out_ev.set_beam_particles(beam[0], beam[1]);

      auto vx = make_vx_ptr<V>();
      for(size_t i=0; i<event.incoming().size(); ++i){
        auto particle = make_particle_ptr<V>(event.incoming()[i], Status::in);
        auto vx_beam = make_vx_ptr<V>();
        vx_beam->add_particle_in(beam[i]);
        vx_beam->add_particle_out(particle);
        out_ev.add_vertex(vx_beam);
        vx->add_particle_in(particle);
      }
      for(size_t i=0; i < event.outgoing().size(); ++i){
        auto const & out = event.outgoing()[i];
        auto particle = make_particle_ptr<V>(out, Status::out);
        const int status = event.decays().count(i)?Status::decay:Status::out;
        particle->set_status(status);
        if( status == Status::decay ){
          auto vx_decay = make_vx_ptr<V>();
          vx_decay->add_particle_in(particle);
          for( auto const & decay: event.decays().at(i) ){
            vx_decay->add_particle_out(
                make_particle_ptr<V>(decay, Status::out)
            );
          }
          out_ev.add_vertex(vx_decay);
        }
        vx->add_particle_out(particle);
      }
      out_ev.add_vertex(vx);

      return out_ev;
    }
  } // namespace detail_HepMC
} // namespace HEJ

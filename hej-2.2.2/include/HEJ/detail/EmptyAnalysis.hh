/** \file
 *  \brief Internal header for empty analysis, to be removed in HEJ 2.3
 *
 *  \authors   The HEJ collaboration (see AUTHORS for details)
 *  \date      2022-2023
 *  \copyright GPLv2 or later
 */

#pragma once

#include <memory>

#include "HEJ/Analysis.hh"

namespace YAML {
  class Node;
}
namespace LHEF {
  class HEPRUP;
}

namespace HEJ {
  class Event;

  namespace detail {
    struct EmptyAnalysis: Analysis{
      //! Create EmptyAnalysis
      static std::unique_ptr<Analysis> create(
        YAML::Node const & parameters, LHEF::HEPRUP const & /*unused*/);

      //! Fill event into analysis (e.g. to histograms)
      /**
       *  This function does nothing
       */
      void fill(Event const & /*res_event*/, Event const & /*FO_event*/) override;
      //! Whether a resummation event passes all cuts
      /**
       *  There are no cuts, so all events pass
       */
      bool pass_cuts(Event const & /*res_event*/, Event const & /*FO_event*/) override;
      //! Set the ratio (cross section) / (sum of event weights)
      void set_xs_scale(double scale) override;
      //! Finalise analysis
      /**
       * This function does nothing
       */
      void finalise() override;

      ~EmptyAnalysis() override = default;
    };
  }
} // namespace HEJ

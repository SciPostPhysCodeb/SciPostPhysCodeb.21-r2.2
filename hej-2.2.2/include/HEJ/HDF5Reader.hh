/** \file
 *  \brief Header file for reading events in the HDF5 event format.
 *
 *  \authors   The HEJ collaboration (see AUTHORS for details)
 *  \date      2019-2020
 *  \copyright GPLv2 or later
 */
#pragma once

#include <memory>
#include <string>

#include "HEJ/EventReader.hh"

namespace HEJ {

  //! Class for reading events from a file in the HDF5 file format
  /**
   * @details This format is specified in \cite Hoeche:2019rti.
   */
  class HDF5Reader : public EventReader{
  public:
    HDF5Reader() = delete;

    //! Contruct object reading from the given file
    explicit HDF5Reader(std::string const & filename);

    //! Read an event
    bool read_event() override;

    //! Access header text
    std::string const & header() const override;

    //! Access run information
    LHEF::HEPRUP const & heprup() const override;

    //! Access last read event
    LHEF::HEPEUP const & hepeup() const override;

    //! Get number of events
    std::optional<size_t> number_events() const override;

    ~HDF5Reader() override;

  private:
    struct HDF5ReaderImpl;

    std::unique_ptr<HDF5ReaderImpl> impl_;
  };

} // namespace HEJ

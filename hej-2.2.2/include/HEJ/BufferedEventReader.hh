/** \file
 *  \brief Event reader with internal buffer
 *
 *  \authors   The HEJ collaboration (see AUTHORS for details)
 *  \date      2019-2022
 *  \copyright GPLv2 or later
 */
#pragma once

#include <cstddef>
#include <memory>
#include <optional>
#include <stack>
#include <string>
#include <utility>
#include <vector>

#include "LHEF/LHEF.h"

#include "HEJ/EventReader.hh"

namespace HEJ {

  //! Event reader with internal buffer
  /**
   * Having a buffer makes it possible to put events back into the reader
   * so that they are read again
   */
  class BufferedEventReader: public EventReader {
  public:
    explicit BufferedEventReader(std::unique_ptr<EventReader> reader):
      reader_{std::move(reader)}
    {}

    //! Read an event
    bool read_event() override;

    //! Access header text
    std::string const & header() const override {
      return reader_->header();
    }

    //! Access run information
    LHEF::HEPRUP const & heprup() const override {
      return reader_->heprup();
    }

    //! Access last read event
    LHEF::HEPEUP const & hepeup() const override {
      return cur_event_;
    }

    //! Guess number of events from header
    std::optional<std::size_t> number_events() const override {
      return reader_->number_events();
    }

    //! Push event back into reader
    template< class... T>
    void emplace(T&&... args) {
      buffer_.emplace(std::forward<T>(args)...);
    }

  private:
    std::stack<LHEF::HEPEUP, std::vector<LHEF::HEPEUP>> buffer_;
    std::unique_ptr<EventReader> reader_;
    LHEF::HEPEUP cur_event_;
  };
} // namespace HEJ

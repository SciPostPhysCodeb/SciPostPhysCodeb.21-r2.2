/** \file
 *  \brief Contains the writer for LesHouches output
 *
 *  \authors   The HEJ collaboration (see AUTHORS for details)
 *  \date      2019-2023
 *  \copyright GPLv2 or later
 */
#pragma once

#include <fstream>
#include <memory>
#include <string>

#include "LHEF/LHEF.h"

#include "HEJ/EventWriter.hh"

namespace HEJ {
  class Event;

  //! Class for writing events to a file in the Les Houches Event File format
  class LesHouchesWriter : public EventWriter{
  public:
    //! Constructor
    /**
     * @param file    Name of output file
     * @param heprup  General process information
     */
    LesHouchesWriter(std::string const & file, LHEF::HEPRUP heprup);
    LesHouchesWriter(LesHouchesWriter const & other) = delete;
    LesHouchesWriter & operator=(LesHouchesWriter const & other) = delete;
    /** @TODO in principle, this class should be movable
     *       but that somehow(?) breaks the write member function
     */
    LesHouchesWriter(LesHouchesWriter && other) = delete;
    LesHouchesWriter & operator=(LesHouchesWriter && other) = delete;
    ~LesHouchesWriter() override;

    //! Write an event to the file specified in the constructor
    void write(Event const & ev) override;

    //! Set the ratio (cross section) / (sum of event weights)
    void set_xs_scale(double scale) override;

    //! Dump general information & finish writing
    void finish() override;

  private:
    void write_init(){
      writer_->init();
    }

    void rewrite_init_and_close();

    LHEF::HEPRUP & heprup(){
      return writer_->heprup;
    }

    LHEF::HEPEUP & hepeup(){
      return writer_->hepeup;
    }

    std::fstream out_;
    std::unique_ptr<LHEF::Writer> writer_;
    double xs_scale_ = 1.;
  };
} // namespace HEJ

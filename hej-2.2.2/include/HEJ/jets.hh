/**
 *  \authors   The HEJ collaboration (see AUTHORS for details)
 *  \date      2019-2022
 *  \copyright GPLv2 or later
 */
/** \file
 *  \brief Functions computing the square of current contractions in pure jets.
 *
 *  This file contains all the necessary functions to compute the
 *  current contractions for all valid pure jet HEJ processes, which
 *  so far is FKL and unordered processes. It will also contain some
 *  pure jet ME components used in other process ME calculations
 *
 */
#pragma once

#include <array>
#include <cmath>
#include <complex>
#include <ostream>
#include <vector>

#include "CLHEP/Vector/LorentzVector.h"

namespace HEJ {
namespace currents {
  using HLV = CLHEP::HepLorentzVector;

  //! Helicity-summed current contraction
  /**
  *  @param p1out     Final-state momentum of first current
  *  @param p1in      Initial-state momentum of first current
  *  @param p2out     Final-state momentum of second current
  *  @param p2in      Initial-state momentum of second current
  *  @returns         Current contractions
  *
  * \internal
  * See eq:S in the developer manual
  */
  double ME_qq(
    HLV const & p1out, HLV const & p1in,
    HLV const & p2out, HLV const & p2in
  );

  //! Helicity-summed current contraction with unordered gluon emission
  /**
  *  @param pg        Momentum of gluon emitted off first current
  *  @param p1out     Final-state momentum of first current
  *  @param p1in      Initial-state momentum of first current
  *  @param p2out     Final-state momentum of second current
  *  @param p2in      Initial-state momentum of second current
  *  @returns         Current contraction
  *
  * \internal
  * See eq:S_uno in the developer manual
  */
  double ME_unob_qq(
    HLV const & pg,
    HLV const & p1out, HLV const & p1in,
    HLV const & p2out, HLV const & p2in
  );

  //! Helicity-summed current contraction with extremal quark/anti-quark emission
  /**
  *  @param pa     Initial-state momentum of first current
  *  @param pb     Initial-state momentum of gluon splitting into qqbar
  *  @param p1     Final-state momentum of first current
  *  @param p2     Less extremal (anti-)quark from splitting momentum
  *  @param p3     More extremal (anti-)quark from splitting momentum
  *  @returns      Current contraction
  */
  double ME_qqbar_qg(
    HLV const & pa, HLV const & pb,
    HLV const & p1, HLV const & p2, HLV const & p3
  );

  //! Square of qq->qQQbarq Pure Jets Central qqbar Scattering Current
  /**
  *  @param pa        Momentum of incoming leg a
  *  @param pb        Momentum of incoming leg b
  *  @param partons   vector of outgoing partons
  *  @param qqbarmarker Is anti-quark further back in rapidity than quark
  *                     (qqbar pair)
  *  @param nabove    Number of gluons emitted above qqbar pair (back in rap)
  *  @returns         Square of the current contractions for qq->q+QQbar+q
  */
  double ME_Cenqqbar_qq(
    HLV const & pa, HLV const & pb,
    std::vector<HLV> const & partons,
    bool qqbarmarker, std::size_t nabove
  );
} // namespace currents
} // namespace HEJ

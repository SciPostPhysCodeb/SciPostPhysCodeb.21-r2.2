/** \file
 *  \brief Header file for the EventWriter interface.
 *
 *  \authors   The HEJ collaboration (see AUTHORS for details)
 *  \date      2019-2023
 *  \copyright GPLv2 or later
 */
#pragma once

#include <string>

namespace HEJ {
  class Event;

  //! Pure abstract base class for event writers
  class EventWriter {
  public:
    //! Write an event
    virtual void write(Event const &) = 0;

    //! Set the ratio (cross section) / (sum of event weights)
    virtual void set_xs_scale(double scale) = 0;

    //! Finish writing, e.g. write out general informations at end of run
    //! @warning using the writer after finishing is not guaranteed to work
    virtual void finish(){finished_=true;};

    virtual ~EventWriter() = default;
  protected:
    //! @brief If writer is not finished run finish() and abort on error
    //!
    //! @param writer   writer that is expected to finish, i.e. `this`
    //! @param name     name for error message
    //!
    //! Used in the destructor of inherited EventWriter to force finish()
    //! @warning When finish() failed this will abort the whole program!
    void finish_or_abort(
        EventWriter* writer, std::string const & name
    ) const noexcept;
    bool finished() const {return finished_;};
  private:
    bool finished_ = false;
  };

} // namespace HEJ

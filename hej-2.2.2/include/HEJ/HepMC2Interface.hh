/** \file
 *  \brief Header file for the HepMC2Interface
 *
 *  \authors   The HEJ collaboration (see AUTHORS for details)
 *  \date      2019-2023
 *  \copyright GPLv2 or later
 */
#pragma once

#include <array>
#include <cstddef>

#include "HEJ/PDG_codes.hh"

namespace HepMC {
  class GenCrossSection;
  class GenEvent;
}

namespace LHEF {
  class HEPRUP;
}

namespace HEJ {
  class Event;
  //! This class converts the Events into HepMC::GenEvents
  /**
   *   \details The output is depended on the HepMC version HEJ is compiled with,
   *   both HepMC 2 and HepMC 3 are supported. If HEJ 2 is compiled
   *   without HepMC calling this interface will throw an error.
   *
   *   This interface will also keep track of the cross section of all the events that
   *   being fed into it.
   */

  class HepMC2Interface{
  public:
    HepMC2Interface(LHEF::HEPRUP const & /*heprup*/);
    /**
     * \brief main function to convert an event into HepMC::GenEvent
     *
     * \param event          Event to convert
     * \param weight_index   optional selection of specific weight
     *                       (negative value gives central weight)
     */
    HepMC::GenEvent operator()(Event const & event,
                               int weight_index = -1);
    /**
     * \brief Sets the central value from \p event to \p out_ev
     *
     * \param out_ev         HepMC::GenEvent to write to
     * \param event          Event to convert
     * \param weight_index   optional selection of specific weight
     *                       (negative value gives "central")
     *
     * This overwrites the central value of \p out_ev.
     */
    void set_central(HepMC::GenEvent & out_ev, Event const & event,
                     int weight_index = -1);

    //! Set the ratio (cross section) / (sum of event weights)
    void set_xs_scale(double scale);

  protected:
    /**
     * \brief initialise generic event infomrations (not central weights)
     *
     * \param event          Event to convert
     */
    HepMC::GenEvent init_event(Event const & event) const;

  private:
    std::array<ParticleID,2> beam_particle_{};
    std::array<double,2> beam_energy_{};
    std::size_t event_count_;
    double tot_weight_;
    double tot_weight2_;
    double xs_scale_ = 1.;
    HepMC::GenCrossSection cross_section() const;
  };
} // namespace HEJ

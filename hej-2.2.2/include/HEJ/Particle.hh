/**
 * \file Particle.hh
 * \brief Contains the particle struct
 *
 *  \authors   The HEJ collaboration (see AUTHORS for details)
 *  \date      2019-2022
 *  \copyright GPLv2 or later
 */
#pragma once

#include <optional>
#include <utility>
#include <vector>

#include "boost/rational.hpp"
#include "fastjet/PseudoJet.hh"

#include "HEJ/PDG_codes.hh"

namespace HEJ {

  using Colour = std::pair<int,int>;

  //! Class representing a particle
  struct Particle {
    //! particle type
    ParticleID type = pid::unspecified;
    //! particle momentum
    fastjet::PseudoJet p;
    //! (optional) colour & anti-colour
    std::optional<Colour> colour;

    //! get rapidity
    double rapidity() const{
      return p.rapidity();
    }
    //! get transverse momentum
    double perp() const{
      return p.perp();
    }
    //! get transverse momentum
    double pt() const{
      return perp();
    }
    //! get momentum in x direction
    double px() const{
      return p.px();
    }
    //! get momentum in y direction
    double py() const{
      return p.py();
    }
    //! get momentum in z direction
    double pz() const{
      return p.pz();
    }
    //! get energy
    double E() const{
      return p.E();
    }
    //! get mass
    double m() const{
      return p.m();
    }
  };

  //! Functor to compare rapidities
  /**
   *  This can be used whenever a rapidity comparison function is needed,
   *  for example in many standard library functions.
   *
   *  @see pz_less
   */
  struct rapidity_less{
    template<class FourVector>
    bool operator()(FourVector const & p1, FourVector const & p2){
      return p1.rapidity() < p2.rapidity();
    }
  };

  //! Functor to compare momenta in z direction
  /**
   *  This can be used whenever a pz comparison function is needed,
   *  for example in many standard library functions.
   *
   *  @see rapidity_less
   */
  struct pz_less{
    template<class FourVector>
    bool operator()(FourVector const & p1, FourVector const & p2){
      return p1.pz() < p2.pz();
    }
  };

  //! Convert a vector of Particles to a vector of particle momenta
  inline
  std::vector<fastjet::PseudoJet> to_PseudoJet(
      std::vector<Particle> const & v
  ){
    std::vector<fastjet::PseudoJet> result;
    result.reserve(v.size());
    for(auto && sp: v) result.emplace_back(sp.p);
    return result;
  }

  //! Functor to compare particle type (PDG)
  /**
   *  This can be used whenever a particle-type comparison is needed
   *  for example in many standard library functions.
   */
  struct type_less{
    template<class Particle>
    bool operator()(Particle const & p1, Particle const & p2){
      return p1.type < p2.type;
    }
  };

  //! Check if the argument is an antiparticle
  inline
  constexpr bool is_antiparticle(Particle const & p){
    return is_antiparticle(p.type);
  }

  //! Check if a particle is a parton, i.e. quark, antiquark, or gluon
  inline
  constexpr bool is_parton(Particle const & p){
    return is_parton(p.type);
  }

  //! Check if a particle is a quark
  inline
  constexpr bool is_quark(Particle const & p){
    return is_quark(p.type);
  }

  //! Check if a particle is an anti-quark
  inline
  constexpr bool is_antiquark(Particle const & p){
    return is_antiquark(p.type);
  }

  //! Check if a particle is a quark or anit-quark
  inline
  constexpr bool is_anyquark(Particle const & p){
    return is_anyquark(p.type);
  }

  /**
   * \brief Function to determine if particle is a lepton
   * @param p         the particle
   * @returns         true if the particle is a lepton, false otherwise
   */
  inline
  constexpr bool is_lepton(Particle const & p){
    return is_lepton(p.type);
  }

  /**
   * \brief Function to determine if particle is an antilepton
   * @param p         the particle
   * @returns         true if the particle is an antilepton, false otherwise
   */
  inline
  constexpr bool is_antilepton(Particle const & p){
    return is_antilepton(p.type);
  }

  /**
   * \brief Function to determine if particle is an (anti-)lepton
   * @param p         the particle
   * @returns         true if the particle is a lepton or antilepton, false otherwise
   */
  inline
  constexpr bool is_anylepton(Particle const & p){
    return is_anylepton(p.type);
  }

  /**
   * \brief Function to determine if particle is a neutrino
   * @param p         the particle
   * @returns         true if the particle is a neutrino, false otherwise
   */
  inline
  constexpr bool is_neutrino(Particle const & p){
    return is_neutrino(p.type);
  }

  /**
   * \brief Function to determine if particle is an antineutrino
   * @param p         the particle
   * @returns         true if the particle is an antineutrino, false otherwise
   */
  inline
  constexpr bool is_antineutrino(Particle const & p){
    return is_antineutrino(p.type);
  }

  /**
   * \brief Function to determine if particle is an (anti-)neutrino
   * @param p         the particle
   * @returns         true if the particle is a neutrino or antineutrino, false otherwise
   */
  inline
  constexpr bool is_anyneutrino(Particle const & p){
    return is_anyneutrino(p.type);
  }

  //! Check if a particle is massless
  inline
  constexpr bool is_massless(Particle const & p){
    return is_massless(p.type);
  }

  //! Check if a particle is massive
  inline
  constexpr bool is_massive(Particle const & p){
    return is_massive(p.type);
  }

  /**
   * \brief Function to determine if particle is a charged lepton
   * @param p    the particle
   * @returns    true if the particle is a charged lepton, false otherwise
   */
  inline
  constexpr bool is_charged_lepton(Particle const & p){
    return is_charged_lepton(p.type);
  }

  /**
   * \brief Function to determine if particle is a charged lepton
   * @param p    the particle
   * @returns    true if the particle is a charged lepton, false otherwise
   */
  inline
  constexpr bool is_charged_antilepton(Particle const & p){
    return is_charged_antilepton(p.type);
  }

  /**
   * \brief Function to determine if particle is a charged lepton or charged antilepton
   * @param p    the particle
   * @returns    true if the particle is a charged lepton or charged antilepton, false otherwise
   */
  inline
  constexpr bool is_charged_anylepton(Particle const & p){
    return is_charged_anylepton(p.type);
  }

  //! Check if a particle is a photon, W or Z boson
  inline
  constexpr bool is_AWZ_boson(Particle const & particle){
    return is_AWZ_boson(particle.type);
  }

  //! Check if a particle is a photon, W, Z, or Higgs boson
  inline
  constexpr bool is_AWZH_boson(Particle const & particle){
    return is_AWZH_boson(particle.type);
  }

  //! Extract all partons from a vector of particles
  inline
  std::vector<Particle> filter_partons(
      std::vector<Particle> const & v
  ){
    std::vector<Particle> result;
    result.reserve(v.size());
    std::copy_if(
        begin(v), end(v), std::back_inserter(result),
        [](Particle const & p){ return is_parton(p); }
    );
    return result;
  }

  //! Extract all AWZH bosons from a vector of particles
  inline
  std::vector<Particle> filter_AWZH_bosons(
      std::vector<Particle> const & v
  ){
    std::vector<Particle> result;
    std::copy_if(
        begin(v), end(v), std::back_inserter(result),
        [](Particle const & p){ return is_AWZH_boson(p); }
    );
    return result;
  }

  //! Particle electric charge
  boost::rational<int> charge(Particle const & p);

} // namespace HEJ

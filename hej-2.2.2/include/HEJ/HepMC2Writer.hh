/** \file
 *  \brief Contains the EventWriter for HepMC Output.
 *
 *  \authors   The HEJ collaboration (see AUTHORS for details)
 *  \date      2019-2023
 *  \copyright GPLv2 or later
 */
#pragma once

#include <memory>
#include <string>

#include "HEJ/EventWriter.hh"

namespace LHEF {
  class HEPRUP;
}

namespace HEJ {
  class Event;

  //! This is an event writer specifically for HepMC output.
  /**
   * \internal Implementation note:
   * This uses the pimpl ("pointer to implementation") idiom.
   * HepMC support is optional and the implementation depends on the
   * HepMC version. Without pimpl, we would have to specify the HepMC version
   * via the preprocessor whenever this header is included. We don't want to
   * burden users of the HEJ library (for example the HEJ fixed-order generator)
   * with those details
   */
  class HepMC2Writer: public EventWriter{
  public:
    //! Constructor
    /**
     * @param file      name of the output file
     * @param heprup    general process information
     */
    HepMC2Writer(std::string const & file, LHEF::HEPRUP heprup);
    HepMC2Writer() = delete;

    //! Write an event to the output file
    void write(Event const & ev) override;

    //! Set the ratio (cross section) / (sum of event weights)
    void set_xs_scale(double scale) override;

    ~HepMC2Writer() override;

  private:
    struct HepMC2WriterImpl;

    std::unique_ptr<HepMC2WriterImpl> impl_;
  };

} // namespace HEJ

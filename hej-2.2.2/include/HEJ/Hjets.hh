/**
 *  \authors   The HEJ collaboration (see AUTHORS for details)
 *  \date      2019-2022
 *  \copyright GPLv2 or later
 */
/** \file
 *  \brief Functions computing the square of current contractions in H+Jets.
 *
 *  This file contains all the H+Jet specific components to compute
 *  the current contractions for valid HEJ processes.
 */
#pragma once

#include "CLHEP/Vector/LorentzVector.h"

namespace HEJ {
namespace currents {
  using HLV = CLHEP::HepLorentzVector;

  /**
   * @brief Square of gq->Hq current contraction
   * @param ph                Outgoing Higgs boson momentum
   * @param pg                Incoming gluon momentum
   * @param pn                Momentum of outgoing particle in FKL current
   * @param pb                Momentum of incoming particle in FKL current
   * @param mt                top mass (inf or value)
   * @param inc_bottom        whether to include bottom mass effects (true) or not (false)
   * @param mb                bottom mass (value)
   * @param vev               Higgs vacuum expectation value
   *
   * Calculates helicity-averaged || \epsilon_\mu V_H^{\mu\nu} j_\nu ||^2.
   * See eq:S_gf_Hf in developer manual
   */
  double ME_H_gq(
    HLV const & ph, HLV const & pa,
    HLV const & pn, HLV const &  pb,
    double mt, bool inc_bottom, double mb, double vev
  );

  /**
   * @brief Square of qg -> gqH current contraction
   * @param pg                Outgoing (unordered) gluon momentum
   * @param ph                Outgoing quark momentum
   * @param pa                Incoming quark momentum
   * @param ph                Outgoing Higgs boson momentum
   * @param pb                Incoming gluon momentum
   * @param mt                top mass (inf or value)
   * @param inc_bottom        whether to include bottom mass effects (true) or not (false)
   * @param mb                bottom mass (value)
   * @param vev               Higgs vacuum expectation value
   *
   * Calculates helicity-averaged || j_{uno \mu} V_H^{\mu\nu} \epsilon_\nu ||^2.
   * See eq:S_gf_Hf in developer manual
   */
  double ME_juno_jgH(
    HLV const & pg,
    HLV const & p1, HLV const & pa,
    HLV const & ph, HLV const & pb,
    double mt, bool inc_bottom, double mb, double vev
  );

  //! Square of qQ->qHQ Higgs+Jets Scattering Current
  /**
  *  @param p1out            Momentum of final state quark
  *  @param p1in             Momentum of initial state quark
  *  @param p2out            Momentum of final state quark
  *  @param p2in             Momentum of intial state quark
  *  @param qH1              Momentum of t-channel propagator before Higgs
  *  @param qH2              Momentum of t-channel propagator after Higgs
  *  @param mt               Top quark mass
  *  @param include_bottom   Specifies whether bottom corrections are included
  *  @param mb               Bottom quark mass
  *  @param vev              Vacuum expectation value
  *  @returns                Square of the current contractions for qQ->qQ
  *
  *  q~p1 Q~p2 (i.e. ALWAYS p1 for quark, p2 for quark)
  *  should be called with qH1 meant to be contracted with p2 in first part of
  *  vertex (i.e. if Q is backward, qH1 is forward)
  */
  double ME_H_qQ(HLV const & p1out, HLV const & p1in,
                 HLV const & p2out, HLV const & p2in,
                 HLV const & qH1, HLV const & qH2,
                 double mt,
                 bool include_bottom, double mb, double vev);

  //! Square of qQ->qQg Higgs+Jets Unordered b Scattering Current
  /**
  *  @param pg               Momentum of unordered b gluon
  *  @param p1out            Momentum of final state quark
  *  @param p1in             Momentum of initial state quark
  *  @param p2out            Momentum of final state quark
  *  @param p2in             Momentum of intial state quark
  *  @param qH1              Momentum of t-channel propagator before Higgs
  *  @param qH2              Momentum of t-channel propagator after Higgs
  *  @param mt               Top quark mass
  *  @param include_bottom   Specifies whether bottom corrections are included
  *  @param mb               Bottom quark mass
  *  @param vev              Vacuum expectation value
  *  @returns                Square of the current contractions for qQ->qQg
  *
  *  This construction is taking rapidity order: p1out >> p2out > pg
  */
  double ME_H_unob_qQ(HLV const & pg, HLV const & p1out,
                      HLV const & p1in, HLV const & p2out,
                      HLV const & p2in, HLV const & qH1,
                      HLV const & qH2,
                      double mt,
                      bool include_bottom, double mb, double vev);
  //! @}
} // namespace currents
} // namespace HEJ

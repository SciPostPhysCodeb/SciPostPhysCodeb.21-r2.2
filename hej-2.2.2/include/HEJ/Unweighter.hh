/**
 *  \authors   The HEJ collaboration (see AUTHORS for details)
 *  \date      2019-2022
 *  \copyright GPLv2 or later
 */
#pragma once

#include <functional>
#include <optional>
#include <vector>

namespace HEJ {
  class Event;
  struct RNG;

  /**
   * @brief Unweight events
   * @details Throws away events below with abs(weight)<cut with probability
   *          wt/cut
   */
  class Unweighter {
  public:
    //! Constructor
    //! @param cut Initial value of cut, negative values for no cut
    Unweighter(double cut):
      cut_(cut){}
    Unweighter() = default;

    //! Explicitly set cut
    void set_cut(double max_weight){
      cut_ = max_weight;
    }
    //! Set cut as max(weight) of events
    void set_cut_to_maxwt(std::vector<Event> const & events){
      set_cut_to_maxwt(events.cbegin(), events.cend());
    }
    //! Iterator version of set_max()
    template<class ConstIt>
    void set_cut_to_maxwt(ConstIt begin, ConstIt end);

    //! Estimate some reasonable cut for partial unweighting
    /**
     * @param events            Events used for the estimation
     * @param max_dev           Standard derivation to include above mean weight
     */
    void set_cut_to_peakwt(std::vector<Event> const & events, double max_dev){
      set_cut_to_peakwt(events.cbegin(), events.cend(), max_dev);
    }
    //! Iterator version of set_cut_to_peakwt()
    template<class ConstIt>
    void set_cut_to_peakwt(ConstIt begin, ConstIt end, double max_dev);

    //! Returns current value of the cut
    double get_cut() const {
      return cut_;
    }

    //! Unweight one event, returns original event if weight > get_cut()
    std::optional<Event> unweight(Event ev, RNG & ran) const;
    //! Unweight for multiple events at once
    std::vector<Event> unweight(
      std::vector<Event> events, RNG & ran
    ) const;
    //! @brief Iterator implementation of unweight(),
    /**
     * Usage similar to std::remove(), i.e. use with erase()
     *
     * @return Beginning of "discarded" range
     */
    template<class Iterator>
    Iterator unweight(
      Iterator begin, Iterator end, RNG & ran
    ) const;

  private:
    double cut_{-1};
    //! Returns true if element can be removed/gets discarded
    //! directly corrects weight if is accepted (not removed)
    bool discard(RNG & ran, Event & ev) const;
  };
} // namespace HEJ

// implementation of template functions
#include "HEJ/detail/Unweighter_impl.hh"

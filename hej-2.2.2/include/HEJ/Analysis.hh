/** \file
 *  \brief Header file for the Analysis interface
 *
 * This header contains declarations that facilitate creating custom analyses
 * to be used with HEJ 2.
 * \todo link to user documentation
 *
 *  \authors   The HEJ collaboration (see AUTHORS for details)
 *  \date      2022-2023
 *  \copyright GPLv2 or later
 */
#pragma once

namespace LHEF {
  class HEPRUP;
}

//! Main HEJ 2 Namespace
namespace HEJ {
  class Event;

  //! Analysis base class
  /**
   *  This is the interface that all analyses should implement,
   *  i.e. all custom analyses have to be derived from this struct.
   */
  struct Analysis {
    /**
     * @param res_event     The event in resummation phase space
     * @param FO_event      The original fixed-order event
     */
    virtual void fill(Event const & res_event, Event const & FO_event) = 0;
    //! Decide whether an event passes the cuts
    /**
     * @param res_event     The event in resummation phase space
     * @param FO_event      The original fixed-order event
     * @returns             Whether the event passes all cuts
     */
    virtual bool pass_cuts(Event const & res_event, Event const & FO_event) = 0;
    //! Finalise analysis
    /**
     * This function is called after all events have been processed and
     * can be used for example to print out or save the results.
     */
    virtual void finalise() = 0;

    //! Set the ratio (cross section) / (sum of event weights)
    virtual void set_xs_scale(double scale) = 0;

    virtual ~Analysis() = default;
  };

} // namespace HEJ

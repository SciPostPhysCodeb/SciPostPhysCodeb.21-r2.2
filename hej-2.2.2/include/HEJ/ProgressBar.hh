/** \file
 *  \brief Contains the ProgressBar class
 *
 *  \authors   The HEJ collaboration (see AUTHORS for details)
 *  \date      2019
 *  \copyright GPLv2 or later
 */
#pragma once

#include <algorithm>
#include <functional>
#include <ostream>
#include <stdexcept>

namespace HEJ {

  //! Class representing (and printing) a progress bar
  template<typename T>
  class ProgressBar {
  public:
    //! Constructor
    /**
     * @param out    Output stream
     * @param max    Maximum value of the progress parameter
     *
     * This will print a fixed-width progress bar, which is initially at 0%.
     */
    ProgressBar(std::ostream & out, T max) :
      out_{out}, max_{max}
    {
      if(max < 0) {
        throw std::invalid_argument{
          "Maximum in progress bar has to be positive"
        };
      }
      if (max != 0) {
        print_bar();
      }
    }

    //! Increment progress
    /**
     *  @param count    Value to add to the current progress parameter
     *
     *  After updating the progess parameter, the progress bar is updated
     *  to a percentage that corresponds to the ratio of the current and
     *  maximum progress parameters.
     */
    ProgressBar & increment(T count) {
      counter_ += count;
      update_progress();
      return *this;
    }

    //! Increase progress by one unit
    /**
     *  After updating the progess parameter, the progress bar is updated
     *  to a percentage that corresponds to the ratio of the current and
     *  maximum progress parameters.
     */
    ProgressBar & operator++() {
      ++counter_;
      update_progress();
      return *this;
    }

  private:
    void update_progress() {
      counter_ = std::min(counter_, max_);
      const int ndots = (100*counter_)/max_;
      const int new_dots = ndots - ndots_;
      if(new_dots > 0) {
        for(int dot = 0; dot < new_dots; ++dot) out_.get() << '.';
        out_.get().flush();
        ndots_ = ndots;
      }
    }

    void print_bar() const {
      out_.get() << "0% ";
      for(int i = 10; i <= 100; i+= 10){
        out_.get() << "       " + std::to_string(i) + "%";
      }
      out_.get() << "\n|";
      for(int i = 10; i <= 100; i+= 10){
        out_.get() << "---------|";
      }
      out_.get() << '\n';
    }

    std::reference_wrapper<std::ostream> out_;
    T counter_ = 0;
    T ndots_ = 0;
    T max_;
  };
} // namespace HEJ

/** \file PDG_codes.hh
 *  \brief Contains the Particle IDs of all relevant SM particles.
 *
 *  Large enumeration included which has multiple entries for potential
 *  alternative names of different particles. There are also functions
 *  which can be used to determine if a particle is a parton or if
 *  it is a non-gluon boson.
 *
 *  \authors   The HEJ collaboration (see AUTHORS for details)
 *  \date      2019-2020
 *  \copyright GPLv2 or later
 */
#pragma once

#include <cstdlib>
#include <string>

#include "boost/rational.hpp"

namespace HEJ {

  //! particle ids according to PDG
  namespace pid {
    //! The possible particle identities. We use PDG IDs as standard.
    enum ParticleID: int{
      //! Unspecified type, should never be used!, debug only
      unspecified = 0,
      d = 1,                           /*!< Down Quark */
      down = d,                        /*!< Down Quark */
      u = 2,                           /*!< Up Quark */
      up = u,                          /*!< Up Quark */
      s = 3,                           /*!< Strange Quark */
      strange = s,                     /*!< Strange Quark */
      c = 4,                           /*!< Charm Quark */
      charm = c,                       /*!< Charm Quark */
      b = 5,                           /*!< Bottom Quark */
      bottom = b,                      /*!< Bottom Quark */
      t = 6,                           /*!< Top Quark */
      top = t,                         /*!< Top Quark */
      e = 11,                          /*!< Electron */
      electron = e,                    /*!< Electron */
      nu_e = 12,                       /*!< Electron Neutrino */
      electron_neutrino = nu_e,        /*!< Electron neutrino */
      mu = 13,                         /*!< Muon */
      muon = mu,                       /*!< Muon */
      nu_mu = 14,                      /*!< Muon Neutrino */
      muon_neutrino = nu_mu,           /*!< Muon Neutrino */
      tau = 15,                        /*!< Tau */
      nu_tau = 16,                     /*!< Tau Neutrino */
      tau_neutrino = nu_tau,           /*!< Tau Neutrino */
      d_bar = -d,                      /*!< Anti-Down Quark */
      antidown = d_bar,                /*!< Anti-Down Quark */
      u_bar = -u,                      /*!< Anti-Up quark */
      antiup = -u,                     /*!< Anti-Up quark */
      s_bar = -s,                      /*!< Anti-Strange Quark */
      antistrange = -s,                /*!< Anti-Strange Quark */
      c_bar = -c,                      /*!< Anti-Charm Quark */
      anticharm = -c,                  /*!< Anti-Charm Quark */
      b_bar = -b,                      /*!< Anti-Bottom Quark */
      antibottom = -b,                 /*!< Anti-Bottom Quark */
      t_bar = -t,                      /*!< Anti-Top Quark */
      antitop = -t,                    /*!< Anti-Top Quark */
      e_bar = -e,                      /*!< Positron */
      positron = e_bar,                /*!< Positron */
      antielectron = positron,         /*!< Positron */
      nu_e_bar = -nu_e,                /*!< Electron Anti-Neutrino */
      electron_antineutrino = nu_e_bar,/*!< Electron Anti-Neutrino */
      mu_bar = -mu,                    /*!< Anti-Muon */
      antimuon = -mu,                  /*!< Anti-Muon */
      nu_mu_bar = -nu_mu,              /*!< Muon Anti-Neutrino */
      muon_antineutrino = nu_mu_bar,   /*!< Muon Anti-Neutrino */
      tau_bar = -tau,                  /*!< Anti-Tau */
      antitau = tau_bar,               /*!< Anti-Tau */
      nu_tau_bar = -nu_tau,            /*!< Tau Anti-Neutrino */
      tau_antineutrino = nu_tau_bar,   /*!< Tau Anti-Neutrino */
      gluon = 21,                      /*!< Gluon */
      g = gluon,                       /*!< Gluon */
      photon = 22,                     /*!< Photon */
      gamma = photon,                  /*!< Photon */
      Z = 23,                          /*!< Z Boson */
      Z_photon_mix = 81,               /*!< Z/photon superposition */
      Z_gamma_mix = Z_photon_mix,      /*!< Z/photon superposition */
      Wp = 24,                         /*!< W- Boson */
      Wm = -Wp,                        /*!< W+ Boson */
      h = 25,                          /*!< Higgs Boson */
      Higgs = h,                       /*!< Higgs Boson */
      higgs = h,                       /*!< Higgs Boson */
      p = 2212,                        /*!< Proton */
      proton = p,                      /*!< Proton */
      p_bar = -p,                      /*!< Anti-Proton */
      antiproton = p_bar,              /*!< Anti-Proton */
    };

    //! Get the of the particle with the given PDG ID
    std::string name(ParticleID id);

    //! return the negative flavour of the given PDG ID
    ParticleID anti(ParticleID id);

  } // namespace pid

  using ParticleID = pid::ParticleID;

  //! Convert a particle name to the corresponding PDG particle ID
  ParticleID to_ParticleID(std::string const & name);

  /**
   * \brief Determine if the PDG ID denotes an antiparticle
   * @param id    PDG ID of particle
   * @returns     true if the PDG ID denotes an antiparticle, false otherwise
   */
  inline
  constexpr bool is_antiparticle(ParticleID id) {
    return id < 0;
  }

  /**
   * \brief Function to determine if particle is a quark
   * @param id    PDG ID of particle
   * @returns     true if the particle is a quark, false otherwise
   */
  inline
  constexpr bool is_quark(ParticleID id){
    return (id >= pid::down && id <= pid::top);
  }

  /**
   * \brief Function to determine if particle is an antiquark
   * @param id    PDG ID of particle
   * @returns     true if the particle is an antiquark, false otherwise
   */
  inline
  constexpr bool is_antiquark(ParticleID id){
    return (id <= pid::d_bar && id >= pid::t_bar);
  }

  /**
   * \brief Function to determine if particle is an (anti-)quark
   * @param id    PDG ID of particle
   * @returns     true if the particle is a quark or antiquark, false otherwise
   */
  inline
  constexpr bool is_anyquark(ParticleID id){
    return is_quark(id) || is_antiquark(id);
  }

  /**
   * \brief Function to determine if particle is a gluon
   * @param id    PDG ID of particle
   * @returns     true if the particle is a gluon, false otherwise
   */
  inline
  constexpr bool is_gluon(ParticleID id){
    return id == pid::gluon;
  }
  /**
   * \brief Function to determine if particle is a parton
   * @param id    PDG ID of particle
   * @returns     true if the particle is a parton, false otherwise
   */
  inline
  constexpr bool is_parton(ParticleID id){
    return is_gluon(id)
      || (is_anyquark(id) && std::abs(id) != pid::top);
  }

  /**
   * \brief function to determine if the particle is a photon, W or Z
   * @param id    PDG ID of particle
   * @returns     true if the partice is an A,W,Z, or H, false otherwise
   */
  inline
  constexpr bool is_AWZ_boson(ParticleID id){
    return id == pid::Wm || (id >= pid::photon && id <= pid::Wp)
            || id == pid::Z_photon_mix;
  }

  /**
   * \brief function to determine if the particle is a photon, W, Z, or Higgs
   *        boson
   * @param id    PDG ID of particle
   * @returns     true if the partice is an A,W,Z, or H, false otherwise
   */
  inline
  constexpr bool is_AWZH_boson(ParticleID id){
    return is_AWZ_boson(id) || (id == pid::Higgs);
  }

  /**
   * \brief Function to determine if particle is a lepton
   * @param id    PDG ID of particle
   * @returns     true if the particle is a lepton, false otherwise
   */
  inline
  constexpr bool is_lepton(ParticleID id){
    return (id >= pid::electron && id <= pid::tau_neutrino);
  }

  /**
   * \brief Function to determine if particle is an antilepton
   * @param id    PDG ID of particle
   * @returns     true if the particle is an antilepton, false otherwise
   */
  inline
  constexpr bool is_antilepton(ParticleID id){
    return (id <= pid::positron && id >= pid::nu_tau_bar);
  }

  /**
   * \brief Function to determine if particle is an (anti-)lepton
   * @param id    PDG ID of particle
   * @returns     true if the particle is a lepton or antilepton,
   *                  false otherwise
   */
  inline
  constexpr bool is_anylepton(ParticleID id){
    return ( is_lepton(id) || is_antilepton(id));
  }

  /**
   * \brief Function to determine if particle is a neutrino
   * @param id    PDG ID of particle
   * @returns     true if the particle is a neutrino, false otherwise
   */
  inline
  constexpr bool is_neutrino(ParticleID id){
    return (id == pid::nu_e || id == pid::tau_neutrino
            || id == pid::muon_neutrino);
  }

  /**
   * \brief Function to determine if particle is an antineutrino
   * @param id    PDG ID of particle
   * @returns     true if the particle is an antineutrino, false otherwise
   */
  inline
  constexpr bool is_antineutrino(ParticleID id){
    return (id == pid::nu_e_bar || id == pid::nu_tau_bar
            || id == pid::nu_mu_bar);
  }

  /**
   * \brief Function to determine if particle is an (anti-)neutrino
   * @param id    PDG ID of particle
   * @returns     true if the particle is a neutrino or antineutrino,
   *              false otherwise
   */
  inline
  constexpr bool is_anyneutrino(ParticleID id){
    return ( is_neutrino(id) || is_antineutrino(id));
  }

  //! Check if a particle is massless
  inline
  constexpr bool is_massless(ParticleID id){
    // cannot use `std::abs` because it's not `constexpr`
    const int abs_id = (id >= 0)?id:-id;
    switch(abs_id){
    case pid::bottom:
    case pid::top:
    case pid::tau:
    case pid::Z:
    case pid::Z_photon_mix:
    case pid::Wp:
    case pid::Higgs:
    case pid::proton:
      return false;
    default:
      return true;
    }
  }

  //! Check if a particle is massive
  inline
  constexpr bool is_massive(ParticleID id){
    return !is_massless(id);
  }

  /**
   * \brief Function to determine if particle is a charged lepton
   * @param id    PDG ID of particle
   * @returns     true if the particle is a charged lepton, false otherwise
   */
  inline
  constexpr bool is_charged_lepton(ParticleID id){
    return is_lepton(id) && !is_neutrino(id);
  }

  /**
   * \brief Function to determine if particle is a charged lepton
   * @param id    PDG ID of particle
   * @returns     true if the particle is a charged lepton, false otherwise
   */
  inline
  constexpr bool is_charged_antilepton(ParticleID id){
    return is_antilepton(id) && !is_antineutrino(id);
  }

  /**
   * \brief Function to determine if particle is a charged lepton or charged antilepton
   * @param id    PDG ID of particle
   * @returns     true if the particle is a charged lepton or charged antilepton, false otherwise
   */
  inline
  constexpr bool is_charged_anylepton(ParticleID id){
    return is_anylepton(id) && !is_anyneutrino(id);
  }

  /**
   * \brief Electric charge of the given particle
   * @param id    PDG ID of particle
   * @returns     The electric charge in units of the elementary charge
   */
  boost::rational<int> charge(ParticleID id);

} // namespace HEJ

/** \file
 *  \brief Interface for pseudorandom number generators
 *
 *  We select our random number generator at runtime according to the
 *  configuration file. This interface guarantees that we can use all
 *  generators in the same way.
 *
 *  \authors   The HEJ collaboration (see AUTHORS for details)
 *  \date      2019-2022
 *  \copyright GPLv2 or later
 */
#pragma once

#include <limits>

namespace HEJ {

  //! Interface for random number generator
  //
  // See https://en.cppreference.com/w/cpp/named_req/UniformRandomBitGenerator
  // for requirements.
  struct RNG {
    //! Random number type, see std::RandomNumberDistribution
    using result_type = unsigned;

    //! Generate random number in [0,1)
    virtual double flat() = 0;

    //! Minimum number that can be generated
    constexpr static result_type min() {
      return 0u;
    }

    //! Maximum number that can be generated
    constexpr static result_type max() {
      return std::numeric_limits<result_type>::max() - 1;
    }

    //! Generate random number in [min(), max()]
    result_type operator()() {
      return flat()*std::numeric_limits<result_type>::max();
    }

    virtual ~RNG() = default;
  };
} // namespace HEJ

/** \file
 * \brief The Mixmax random number generator
 *
 *  \authors   The HEJ collaboration (see AUTHORS for details)
 *  \date      2019-2022
 *  \copyright GPLv2 or later
 */
#pragma once

#include "CLHEP/Random/MixMaxRng.h"
#include "CLHEP/Random/Randomize.h"

#include "HEJ/RNG.hh"

namespace HEJ {

  //! MIXMAX random number generator
  /**
   *  For details on MIXMAX, see \cite Savvidy:2014ana
   */
  class Mixmax : public RNG {
  public:
    Mixmax() = default;
    //! Constructor with explicit seed
    Mixmax(long seed): ran_{seed} {}

    //! Generate pseudorandom number between 0 and 1
    double flat() override {
      return ran_.flat();
    }

  private:
    CLHEP::MixMaxRng ran_;
  };

} // namespace HEJ

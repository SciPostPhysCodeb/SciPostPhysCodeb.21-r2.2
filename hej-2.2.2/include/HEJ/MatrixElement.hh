/** \file
 *  \brief Contains the MatrixElement Class
 *
 *  \authors   The HEJ collaboration (see AUTHORS for details)
 *  \date      2019-2020
 *  \copyright GPLv2 or later
 */
#pragma once

#include <functional>
#include <vector>

#include "fastjet/PseudoJet.hh"

#include "HEJ/Config.hh"
#include "HEJ/PDG_codes.hh"
#include "HEJ/Parameters.hh"

namespace CLHEP {
  class HepLorentzVector;
}

namespace HEJ {
  class Event;
  struct Particle;

  //! Class to calculate the squares of matrix elements
  class MatrixElement{
  public:
    /** \brief MatrixElement Constructor
     * @param alpha_s        Function taking the renormalisation scale
     *                       and returning the strong coupling constant
     * @param conf           General matrix element settings
     */
    MatrixElement(
        std::function<double (double)> alpha_s,
        MatrixElementConfig conf
    );

    /**
     * \brief squares of regulated HEJ matrix elements
     * @param event          The event for which to calculate matrix elements
     * @returns              The squares of HEJ matrix elements including virtual corrections
     *
     * This function returns one value for the central parameter choice
     * and one additional value for each entry in \ref Event.variations().
     * See eq. (22) in \cite Andersen:2011hs for the definition of the squared
     * matrix element.
     *
     * Incoming momenta must be lightlike and have vanishing transverse momentum.
     *
     * \internal Relation to standard HEJ Met2: MatrixElement = Met2*shat^2/(pdfta*pdftb)
     */
    Weights operator()(Event const & event) const;

    //! Squares of HEJ tree-level matrix elements
    /**
     * @param event          The event for which to calculate matrix elements
     * @returns              The squares of HEJ matrix elements without virtual corrections
     *
     * cf. eq. (22) in \cite Andersen:2011hs
     */
    Weights tree(Event const & event) const;

    /**
     * \brief Virtual corrections to matrix element squares
     * @param event         The event for which to calculate matrix elements
     * @returns             The virtual corrections to the squares of the matrix elements
     *
     * The all order virtual corrections to LL in the MRK limit is
     * given by replacing 1/t in the scattering amplitude according to the
     * lipatov ansatz.
     *
     * If there is more than one entry in the returned vector, each entry
     * corresponds to the contribution from the interference of two
     * channels. The order of these entries matches the one returned by
     * the tree_kin member function, but is otherwise unspecified.
     *
     * cf. second-to-last line of eq. (22) in \cite Andersen:2011hs
     * note that indices are off by one, i.e. out[0].p corresponds to p_1
     */
    std::vector<Weights> virtual_corrections(Event const & event) const;

    /**
     * \brief Scale-dependent part of tree-level matrix element squares
     * @param event         The event for which to calculate matrix elements
     * @returns             The scale-dependent part of the squares of the
     *                      tree-level matrix elements
     *
     * The tree-level matrix elements factorises into a renormalisation-scale
     * dependent part, given by the strong coupling to some power, and a
     * scale-independent remainder. This function only returns the former parts
     * for the central scale choice and all \ref Event.variations().
     *
     * @see tree, tree_kin
     */
    Weights tree_param(Event const & event) const;

    /**
     * \brief Kinematic part of tree-level matrix element squares
     * @param event         The event for which to calculate matrix elements
     * @returns             The kinematic part of the squares of the
     *                      tree-level matrix elements
     *
     * The tree-level matrix elements factorises into a renormalisation-scale
     * dependent part, given by the strong coupling to some power, and a
     * scale-independent remainder. This function only returns the latter part.
     *
     * If there is more than one entry in the returned vector, each entry
     * corresponds to the contribution from the interference of two
     * channels. The order of these entries matches the one returned by
     * the virtual_corrections member function, but is otherwise unspecified.
     *
     * Incoming momenta must be lightlike and have vanishing transverse momentum.
     *
     * @see tree, tree_param
     */
    std::vector<double> tree_kin(Event const & event) const;

  private:
    double tree_param(
        Event const & event,
        double mur
    ) const;

    double virtual_corrections_W(
        Event const & event,
        double mur,
        Particle const & WBoson
    ) const;
    std::vector <double> virtual_corrections_WW(
        Event const & event,
        double mur
    ) const;
    std::vector <double> virtual_corrections_Z_qq(
        Event const & event,
        double mur,
        Particle const & ZBoson
    ) const;
    double virtual_corrections_Z_qg(
        Event const & event,
        double mur,
        Particle const & ZBoson,
        bool is_gq_event
    ) const;

    template<class InputIterator>
    std::vector <double> virtual_corrections_interference(
      InputIterator begin_parton, InputIterator end_parton,
      fastjet::PseudoJet const & q0_t,
      fastjet::PseudoJet const & q0_b,
      const double mur
    ) const;

    std::vector<double> virtual_corrections(
        Event const & event,
        double mur
    ) const;

    //! \internal cf. last line of eq. (22) in \cite Andersen:2011hs
    double omega0(
        double alpha_s, double mur,
        fastjet::PseudoJet const & q_j
    ) const;

    double tree_kin_jets(
        Event const & ev
    ) const;
    double tree_kin_W(
        Event const & ev
    ) const;
    std::vector <double> tree_kin_WW(
        Event const & ev
    ) const;
    std::vector <double> tree_kin_Z(
        Event const & ev
    ) const;
    double tree_kin_Higgs(
        Event const & ev
    ) const;
    double tree_kin_Higgs_first(
        Event const & ev
    ) const;
    double tree_kin_Higgs_last(
        Event const & ev
    ) const;

    double tree_kin_Higgs_between(
        Event const & ev
    ) const;

    double tree_param_partons(
        double alpha_s, double mur,
        std::vector<Particle> const & partons
    ) const;

    std::vector<int> in_extremal_jet_indices(
        std::vector<fastjet::PseudoJet> const & partons
    ) const;

    double MH2_backwardH(
      ParticleID type_forward,
      CLHEP::HepLorentzVector const & pa,
      CLHEP::HepLorentzVector const & pb,
      CLHEP::HepLorentzVector const & pH,
      CLHEP::HepLorentzVector const & pn
    ) const;

    double MH2_unob_forwardH(
      CLHEP::HepLorentzVector const & pa,
      CLHEP::HepLorentzVector const & pb,
      CLHEP::HepLorentzVector const & pg,
      CLHEP::HepLorentzVector const & p1,
      CLHEP::HepLorentzVector const & pH
    ) const;

    std::function<double (double)> alpha_s_;

    MatrixElementConfig param_;
  };

} // namespace HEJ

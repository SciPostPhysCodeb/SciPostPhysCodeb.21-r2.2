/** \file
 *  \brief Auxiliary functions for Lorentz vectors
 *
 *  \authors   The HEJ collaboration (see AUTHORS for details)
 *  \date      2019-2020
 *  \copyright GPLv2 or later
 */
#pragma once

#include <complex>
#include <utility>

#include "CLHEP/Vector/LorentzVector.h"

#include "HEJ/Particle.hh"

namespace HEJ {
  //! "dot" product
  inline
  auto dot(
    CLHEP::HepLorentzVector const & pi,
    CLHEP::HepLorentzVector const & pj
  ) {
    return pi.dot(pj);
  }

  //! "angle" product angle(pi, pj) = \<i j\>
  std::complex<double> angle(
    CLHEP::HepLorentzVector const & pi,
    CLHEP::HepLorentzVector const & pj
  );

  //! "square" product square(pi, pj) = [i j]
  std::complex<double> square(
    CLHEP::HepLorentzVector const & pi,
    CLHEP::HepLorentzVector const & pj
  );

  //! Invariant mass
  inline
  auto m2(CLHEP::HepLorentzVector const & h1) {
    return h1.m2();
  }

  //! Plus component
  inline
  auto plus(CLHEP::HepLorentzVector const & h1) {
    return h1.plus();
  }

  //! Minus component
  inline
  auto minus(CLHEP::HepLorentzVector const & h1) {
    return h1.minus();
  }

  inline
  auto perphat(CLHEP::HepLorentzVector const & h1) {
    const auto perp = std::complex<double>{h1.x(), h1.y()};
    return perp/std::abs(perp);
  }

  //! Split a single Lorentz vector into two lightlike Lorentz vectors
  /**
   *  @param P   Lorentz vector to be split
   *  @returns   A pair (p, q) of Lorentz vectors with P = p + q and p^2 = q^2 = 0
   *
   *  P.perp() has to be positive.
   *
   *  p.e() is guaranteed to be positive.
   *  In addition, if either of P.plus() or P.minus() is positive,
   *  q.e() has the same sign as P.m2()
   */
  std::pair<CLHEP::HepLorentzVector, CLHEP::HepLorentzVector>
  split_into_lightlike(CLHEP::HepLorentzVector const & P);

  inline
  CLHEP::HepLorentzVector to_HepLorentzVector(fastjet::PseudoJet const & mom){
    return {mom.px(), mom.py(), mom.pz(), mom.e()};
  }

  inline
  CLHEP::HepLorentzVector to_HepLorentzVector(Particle const & particle){
    return to_HepLorentzVector(particle.p);
  }

  inline
  fastjet::PseudoJet to_PseudoJet(CLHEP::HepLorentzVector const & mom){
    return {mom.px(), mom.py(), mom.pz(), mom.e()};
  }
} // namespace HEJ

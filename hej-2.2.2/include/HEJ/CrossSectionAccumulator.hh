/**
 *  \authors   The HEJ collaboration (see AUTHORS for details)
 *  \date      2019-2020
 *  \copyright GPLv2 or later
 */
#pragma once

#include <iterator>
#include <map>
#include <ostream>
#include <vector>

#include "HEJ/event_types.hh"

namespace HEJ {
  class Event;

  //! Collection of Cross Section with its uncertainty
  template<typename T>
  struct XSWithError {
    T value = T{}; //!< Cross Section
    T error = T{}; //!< Squared error (Variance)
  };

  /**
   * @brief Sum of Cross Section for different subproccess
   */
  class CrossSectionAccumulator {
  public:
    //! Fill with single event
    //! @note for multiple resummation events use fill_correlated() instead
    void fill(Event const & ev);
    //! Fill by weight and type
    //! @note for multiple resummation events use fill_correlated() instead
    void fill(double wt, event_type::EventType type);
    //! Fill by weight, error and type
    //! @note The error will be _added_ to the current error
    void fill(double wt, double err, event_type::EventType type);

    /**
     * @brief   Fill with multiple correlated weights
     * @details This should be used to fill multiple reweighted events,
     *          coming from the same fixed order point.
     *          Total error for \f$N\f$ fixed order points each giving \f$M_i\f$
     *          resummation events is:
     *          \f[
     *          \delta^2=\sum_i \left(\sum_j w_{i,j}\right)^2
     *                   +\sum_{i,j} \left(w_{i,j}\right)^2,
     *          \f]
     * @note    This is equivalent to fill() for only one reweighted event
     *          coming from each fixed order point (\f$M_i=1\f$)
     */
    void fill_correlated(std::vector<Event> const & evts);
    //! iterator implementation of fill_correlated()
    template<class ConstIt>
    void fill_correlated(ConstIt begin, ConstIt end);
    //! explicit version of fill_correlated() by giving sum(wt) and sum(wt^2)
    void fill_correlated(double sum, double sum2, event_type::EventType type);

    //! begin of Cross Section and error for subprocesses
    auto begin() const {
      return std::begin(xs_);
    }
    //! end of Cross Section and error for subprocesses
    auto end() const {
      return std::end(xs_);
    }
    //! total Cross Section and error
    XSWithError<double> const & total() const {
      return total_;
    }
    //! Cross Section and error of specific type
    XSWithError<double> const & at(event_type::EventType type) const {
      return xs_.at(type);
    }
    //! Cross Section and error of specific type
    XSWithError<double> const & operator[](event_type::EventType type) const {
      return at(type);
    }

    // method to scale total_ and xs_ after they have been filled
    void scale(double xsscale);

  private:
    std::map<event_type::EventType, XSWithError<double>> xs_;
    XSWithError<double> total_;
  };

  //! Print CrossSectionAccumulator to stream
  std::ostream& operator<<(std::ostream& os, const CrossSectionAccumulator& xs);

// ------------ Implementation ------------

  template<class ConstIt>
  void CrossSectionAccumulator::fill_correlated(ConstIt begin, ConstIt end){
    if(std::distance(begin, end) < 2){ // only one event
      fill(*begin);
      return;
    }
    double sum = 0.;
    double sum2 = 0.;
    const auto type = begin->type();
    for(; begin != end; ++begin){
      double const wt = begin->central().weight;
      sum += wt;
      sum2 += wt*wt;
    }
    fill_correlated(sum, sum2, type);
  }
} // namespace HEJ

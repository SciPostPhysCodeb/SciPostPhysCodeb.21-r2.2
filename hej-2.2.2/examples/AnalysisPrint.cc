//! HEJ analysis to output the cross section to a file

#include <cmath>
#include <fstream>
#include <iostream>
#include <memory>
#include <string>

#include "HEJ/Analysis.hh"
#include "HEJ/Event.hh"

#include "yaml-cpp/yaml.h"

#include "LHEF/LHEF.h"

namespace {
  class AnalysisPrint: public HEJ::Analysis {
  public:
    AnalysisPrint(YAML::Node const & config, LHEF::HEPRUP const & heprup):
      wt_sum_{0.}, wt2_sum_{0.},
      outfile_{config["output"].as<std::string>()},
      generators_{heprup.generators}
    {}

    void fill(
      HEJ::Event const & event,
      HEJ::Event const & /* FO_event */
    ) override {
      const double wt = event.central().weight;
      wt_sum_ += wt;
      wt2_sum_ += wt*wt; // this error estimate is too small
    }

    bool pass_cuts(
      HEJ::Event const & /* event */,
      HEJ::Event const & /* FO_event */
    ) override {
      return true;
    }

    void set_xs_scale(double xsscale) override {
      xs_scale_ = xsscale;
    }

    void finalise() override {
      // compute cross section from sum of weights and scaling factor
      const double xsection = wt_sum_ * xs_scale_;
      const double xsection_error = std::sqrt(wt2_sum_) * xs_scale_;

      // print to screen
      std::cout << "Generated with:\n";
      for(auto const & generator: generators_)
        std::cout << generator.name << " " << generator.version << "\n";
      std::cout << "cross section: " << xsection << " +- "
        << xsection_error << std::endl;

      // print to file
      std::ofstream fout{outfile_};
      fout << "Generated with\n";
      for(auto const & generator: generators_)
        fout << generator.name << " " << generator.version << "\n";
      fout << "cross section: " << xsection << " +- "
        << xsection_error << std::endl;
    }

    private:
    double wt_sum_, wt2_sum_;
    double xs_scale_{1.0};
    std::string outfile_;
    std::vector<LHEF::Generator> generators_;
  };
}

extern "C"
__attribute__((visibility("default")))
std::unique_ptr<HEJ::Analysis> make_analysis(
    YAML::Node const & config, LHEF::HEPRUP const & heprup
){
  return std::make_unique<AnalysisPrint>(config, heprup);
}

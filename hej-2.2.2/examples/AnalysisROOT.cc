//! Simple HEJ analysis to plot Delta y_{fb} with ROOT

#include <memory>     // for std::unique_ptr
#include <iostream>   // for std::cout
#include <stdexcept>

#include "TFile.h"
#include "TKey.h"
#include "TH1.h"

#include "HEJ/Analysis.hh"
#include "HEJ/Event.hh"
#include "HEJ/Version.hh"

namespace YAML {
  class Node;
}

namespace LHEF {
  class HEPRUP;
}
namespace {
  void Fill2DHistogram(TH1D & h,double x,double wt)
  {
    const int binnumber=h.GetXaxis()->FindBin(x);
    const double binwidth=h.GetXaxis()->GetBinWidth(binnumber);
    h.Fill(x,wt/binwidth);
  }

  class AnalysisROOT: public HEJ::Analysis {
  private:
    double xs_scale_{1.0};
    TFile hfile_;
    TH1D htotal_,hydif_,hyf_,hyb_;
  public:
    AnalysisROOT(
      YAML::Node const & /* config */, LHEF::HEPRUP const & /* heprup */
    ):hfile_{"HEJ.root","RECREATE",
             ("Output from "+HEJ::Version::package_name_full()).c_str()},
      htotal_{"total","total",1,-0.5,.5},
      hydif_{"ydif","ydif",40,0.,10.},
      hyf_{"yf","yf",40,-5.,5.},
      hyb_{"yb","yb",40,-5.,5.}
    {
      // Setup histograms
      htotal_.Sumw2();
      hydif_.Sumw2();
      hyf_.Sumw2();
      hyb_.Sumw2();
    }

    void fill(
      HEJ::Event const & event,
      HEJ::Event const & /* FO_event */
    ) override {
      const double weight=event.central().weight;
      htotal_.Fill(0.0,weight);
      auto const & jets=event.jets(); // jets are sorted in rapidity
      Fill2DHistogram(hydif_,
          fabs(jets.back().rapidity()-jets.front().rapidity()), weight);
      Fill2DHistogram(hyf_, jets.front().rapidity(), weight);
      Fill2DHistogram(hyb_, jets.back().rapidity(), weight);
    }

    bool pass_cuts(
      HEJ::Event const & event,
      HEJ::Event const & /* FO_event */
    ) override {
      if (event.jets().size()<2)
        return false;
      return true;
    }

    void set_xs_scale(double xsscale) override {
      xs_scale_ = xsscale;
    }

    void finalise() override {
      scale();
      hfile_.Write();
      hfile_.Close();
      std::cout << "AnalysisROOT bids you farewell\n"; // be polite
    }

  private:
    void scale() {
      std::cout << "AnalysisROOT scales the histograms\n";

      TList* list = hfile_.GetListOfKeys() ;
      if(!list) throw std::logic_error{"<E> No keys defined\n"};
      TIter next(list) ;
      TKey* key ;
      TObject* obj ;

      while ( (key = (TKey*)next()) ) {
        obj = key->ReadObj() ;
        auto hist = dynamic_cast<TH1*>(obj);
        if(hist) {
          std::cerr << "Histo name: " << hist->GetName()
                    << " title: " << hist->GetTitle()
                    << "will be scaled\n";
          hist->Scale(xs_scale_);
        } else {
          std::cerr << "<W> Object " << obj->GetName()
                    << " is not 1D or 2D histogram : will not be scaled\n";
        }
      }
    }
  };
}

extern "C"
__attribute__((visibility("default")))
std::unique_ptr<HEJ::Analysis> make_analysis(
    YAML::Node const & config, LHEF::HEPRUP const & heprup
){
  return std::make_unique<AnalysisROOT>(config, heprup);
}

# Example Analyses and Custom Scales for HEJ

This directory contains a number of examples for HEJ analyses and
custom scale settings. Their usage is explained on
https://hej.hepforge.org/doc/current/user/analyses.html and
https://hej.hepforge.org/doc/current/user/scales.html
See the individual files for details.

## Compilation

The examples are compiled automatically together with the rest of HEJ,
see https://hej.hepforge.org/doc/current/user/installation.html for
instructions. The compiled libraries are saved in the `examples`
subdirectory of the build directory.

To include examples requiring ROOT, you have to add
`-DEXCLUDE_ROOT=False` to the `cmake` command used to compile HEJ. You
can specify the ROOT installation directory with
`-DROOT_DIR=/directory/containing/ROOTConfig.cmake/`.

It may also be necessary to preload the ROOT core library when running
HEJ or HEJFOG with a ROOT-based analysis:

```sh
LD_PRELOAD=/path/to/libCore.so HEJ config.yml eventfile
```

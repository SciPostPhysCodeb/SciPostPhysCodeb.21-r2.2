//! simple HEJ analysis that doesn't do anything

#include <memory>     // for std::unique_ptr
#include <iostream>

#include "HEJ/Analysis.hh"

namespace YAML {
  class Node;
}

namespace LHEF {
  class HEPRUP;
}

namespace {
  class AnalysisTemplate: public HEJ::Analysis {
    public:
      AnalysisTemplate(
        YAML::Node const & /* config */, LHEF::HEPRUP const & /* heprup */
      ) {}

      void fill(
        HEJ::Event const & /* event */,
        HEJ::Event const & /* FO_event */
      ) override {
      }

      bool pass_cuts(
        HEJ::Event const & /* event */,
        HEJ::Event const & /* FO_event */
      ) override {
        return true;
      }

      void set_xs_scale(double /* xsscale */) override {
        // typically store this information for later
      }

      void finalise() override {
        std::cout << "bye" << std::endl; // be polite
      }
    };
}

extern "C"
__attribute__((visibility("default")))
std::unique_ptr<HEJ::Analysis> make_analysis(
    YAML::Node const & config, LHEF::HEPRUP const & heprup
){
  return std::make_unique<AnalysisTemplate>(config, heprup);
}

#include "HEJ/Event.hh"

extern "C"
__attribute__((visibility("default")))
double softest_jet_pt(HEJ::Event const & ev){
  const auto softest_jet = sorted_by_pt(ev.jets()).back();
  return softest_jet.perp();
}

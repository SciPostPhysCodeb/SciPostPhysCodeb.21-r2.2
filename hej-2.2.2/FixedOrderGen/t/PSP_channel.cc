/**
 *  check that the sum of all possible quarks is the same as a proton
 *
 *  \authors   The HEJ collaboration (see AUTHORS for details)
 *  \date      2020
 *  \copyright GPLv2 or later
 */
#include <array>
#include <cmath>
#include <iostream>
#include <memory>

#include "HEJ/Mixmax.hh"
#include "HEJ/Event.hh"

#include "config.hh"
#include "EventGenerator.hh"

namespace {
  constexpr double max_sigma = 3.6;
}

//! throw error if condition not fulfilled
#define ASSERT(x) if(!(x)) { \
    throw std::logic_error("Assertion '" #x "' failed."); \
  }

//! throw error if condition not fulfilled
#define ASSERT_THROW(x, exception) try { \
      x; \
      std::cerr << "'" #x "' did not throw an exception.\n"; \
      throw; \
    } catch(exception const & ex){ \
      std::cout << "throw triggered: " << ex.what() << std::endl; \
    } \
    catch (...) { \
      std::cerr << "Unexpected exception thrown for '" #x "'.\n"; \
      throw; \
    }

namespace {
  const static std::array<HEJ::ParticleID, 11> PARTONS{
    HEJ::ParticleID::g,
    HEJ::ParticleID::d, HEJ::ParticleID::u,
    HEJ::ParticleID::s, HEJ::ParticleID::c,
    HEJ::ParticleID::b,
    HEJ::ParticleID::d_bar, HEJ::ParticleID::u_bar,
    HEJ::ParticleID::s_bar, HEJ::ParticleID::c_bar,
    HEJ::ParticleID::b_bar
  };

  bool only_channel(
    HEJFOG::Subleading channels,
    HEJFOG::subleading::Channels selected
  ){
    return channels[selected] && channels.reset(selected).none();
  }

  bool can_couple_to_W(HEJ::ParticleID const id, HEJ::ParticleID const boson){
    using namespace HEJ;
    if(!is_anyquark(id)){
      return false;
    }
    if(std::abs(id)==HEJ::ParticleID::b || std::abs(id)==HEJ::ParticleID::top)
      return false;
    // Wp:
    if(boson>0){
      // anti-down
      if(id%2==0){
        return is_quark(id);
      }
      // or up-type quark
      return is_antiquark(id);
    }
    // Wm:
    // down
    if(id%2==0){
      return is_antiquark(id);
    }
    // or anti-up-type quark
    return is_quark(id);
  }

  bool can_couple_to_Z(HEJ::ParticleID const id){
    using namespace HEJ;
    return is_anyquark(id);
  }
}

int main(int argc, char const *argv[])
{
  if(argc != 2 && argc != 3){
    std::cerr << "Usage: " << argv[0] << " config.yaml\n";
    return EXIT_FAILURE;
  }
  const bool short_only = argc==3;
  std::cout <<std::scientific;

  auto config = HEJFOG::load_config(argv[1]);
  config.events/=2.;

  if(short_only)
    config.events/=20.;

  double tot_weight = 0.;
  double tot_err = 0.;
  config.process.incoming[0] = config.process.incoming[1] = HEJ::ParticleID::proton;
  {
    HEJFOG::EventGenerator gen{config};
    for(std::size_t i=0; i<config.events; ++i){
      auto const ev = gen.gen_event();
      if(ev){
        if(config.process.boson && *config.process.boson == HEJ::ParticleID::Z_photon_mix){
          const auto boson = std::find_if(
              begin(ev->outgoing()), end(ev->outgoing()),
              [&](HEJ::Particle const & p){ return p.type == *config.process.boson; }
          );
          if(boson->m() < 81. || boson->m() > 101.){
            continue;
          }
        }
        double const wgt = ev->central().weight/config.events;
        tot_weight += wgt;
        tot_err += wgt*wgt;
      }
    }
    ASSERT(tot_weight!=0.);
  }
  tot_err = std::sqrt(tot_err);


  config.events /= PARTONS.size();
  double tot_channels = 0.;
  double err_channels = 0.;
  for(auto b1: PARTONS){
    for(auto b2: PARTONS){
      double wgt_channel = 0.;
      double err_channel = 0.;
      config.process.incoming[0] = b1;
      config.process.incoming[1] = b2;
      std::cout << name(b1) << "+" << name(b2) << " " <<std::flush;
      // for pure subleading configurations some setups states should throw
      if(config.subleading_fraction == 1.){
        if( only_channel(config.subleading_channels, HEJFOG::subleading::uno) ){
          if(HEJ::is_gluon(b1) && HEJ::is_gluon(b2)){
            std::cout << "uno" << std::endl;
            ASSERT_THROW(HEJFOG::EventGenerator{config}, std::invalid_argument);
            continue;
          }
        }
        if( only_channel(config.subleading_channels, HEJFOG::subleading::eqqbar) ){
          if(HEJ::is_anyquark(b1) && HEJ::is_anyquark(b2)){
            std::cout << "eqqbar" << std::endl;
            ASSERT_THROW(HEJFOG::EventGenerator{config}, std::invalid_argument);
            continue;
          }
        }
      }
      // for W+jets we need the correct quarks
      if(config.process.boson
          && std::abs(*config.process.boson) == HEJ::ParticleID::Wp
      ){
        if(config.process.njets>3
            && config.subleading_fraction>0.
            && config.subleading_channels[HEJFOG::subleading::cqqbar]){
          // this will force central qqbar
        } else if(config.process.njets>2
            && config.subleading_fraction>0.
            && config.subleading_channels[HEJFOG::subleading::eqqbar]){
          // this will force extremal qqbar
        } else {
          auto const boson = *config.process.boson;
          if(!can_couple_to_W(b1, boson)
              && !can_couple_to_W(b2, boson)
          ){
            std::cout << "bad " << name(boson) << std::endl;
            ASSERT_THROW(HEJFOG::EventGenerator{config}, std::invalid_argument);
            continue;
          }
        }
      }
      // for Z+jets we need quarks
      if(config.process.boson && *config.process.boson == HEJ::ParticleID::Z_photon_mix){
        if(config.process.njets>3
            && config.subleading_fraction>0.
            && config.subleading_channels[HEJFOG::subleading::cqqbar]){
          // this will force central qqbar
        } else if(config.process.njets>2
            && config.subleading_fraction>0.
            && config.subleading_channels[HEJFOG::subleading::eqqbar]){
          // this will force extremal qqbar
        } else {
          auto const boson = *config.process.boson;
          if(!can_couple_to_Z(b1) && !can_couple_to_Z(b2)
          ){
            std::cout << "bad " << name(boson) << std::endl;
            ASSERT_THROW(HEJFOG::EventGenerator{config}, std::invalid_argument);
            continue;
          }
        }
      }
      // everything else should work
      HEJFOG::EventGenerator gen{config};
      for(std::size_t i=0; i<config.events; ++i){
        auto const ev = gen.gen_event();
        if(ev){
          if(config.process.boson && *config.process.boson == HEJ::ParticleID::Z_photon_mix){
            const auto boson = std::find_if(
                begin(ev->outgoing()), end(ev->outgoing()),
                [&](HEJ::Particle const & p){ return p.type == *config.process.boson; }
            );
            if(boson->m() < 81. || boson->m() > 101.){
              continue;
            }
          }
          double const wgt = ev->central().weight/config.events;
          wgt_channel += wgt;
          err_channel += wgt*wgt;
        }
      }
      ASSERT(wgt_channel!=0.);
      tot_channels += wgt_channel;
      err_channels += err_channel;
      err_channel = std::sqrt(err_channel);
      std::cout << "=> " << wgt_channel << " +/- " << err_channel << std::endl;
    }
  }
  err_channels = std::sqrt(err_channels);
  std::cout << tot_weight << " +/- " << tot_err
    << " vs. " << tot_channels << " +/- " << err_channels << std::endl;

  const double deviation = std::abs(tot_weight - tot_channels) /
    sqrt(err_channels*err_channels+tot_err*tot_err);
  std::cout << "(" << deviation << " sigma)" << std::endl;
  ASSERT(deviation < max_sigma);

  return EXIT_SUCCESS;
}

/**
 *  check that adding uno emissions doesn't change the FKL cross section
 *
 *  \authors   The HEJ collaboration (see AUTHORS for details)
 *  \date      2019-2020
 *  \copyright GPLv2 or later
 */

#include <algorithm>
#include <cmath>
#include <cstdlib>
#include <iostream>
#include <memory>

#include "HEJ/Event.hh"
#include "HEJ/event_types.hh"
#include "HEJ/PDG_codes.hh"
#include "HEJ/Ranlux64.hh"
#include "HEJ/ScaleFunction.hh"

#include "config.hh"
#include "EventGenerator.hh"
#include "Subleading.hh"
#include "Status.hh"

//! throw error if condition not fulfilled
#define ASSERT(x) if(!(x)) { \
    throw std::logic_error("Assertion '" #x "' failed."); \
  }

namespace {
  constexpr double invGeV2_to_pb = 389379292.;
}

int main(int argn, char const *argv[]){
  using namespace HEJFOG;
  if(argn != 4){
    std::cerr << "Usage: " << argv[0] << " config.yml xs xs_err";
    return EXIT_FAILURE;
  }

  const double xs_ref = std::stod(argv[2]);
  const double err_ref = std::stod(argv[3]);
  (void) err_ref;

  auto config = load_config(argv[1]);
  config.subleading_fraction = 0.37;

  HEJFOG::EventGenerator generator{config};

  double xs = 0., xs_err = 0.;
  std::size_t uno_found = 0;
  for (std::size_t trials = 0; trials < config.events; ++trials){
    auto ev = generator.gen_event();
    if(generator.status() != Status::good) continue;
    ASSERT(ev);
    if(ev->type() != HEJ::event_type::FKL){
      ++uno_found;
      continue;
    }
    xs += ev->central().weight;
    xs_err += ev->central().weight*ev->central().weight;
  }
  xs *= invGeV2_to_pb/config.events;
  xs_err = std::sqrt(xs_err);
  xs_err *= invGeV2_to_pb/config.events;
  std::cout << xs_ref << " ~ " << xs << " +- " << xs_err << '\n';
  std::cout << uno_found << " events with unordered emission" << std::endl;
  ASSERT(uno_found > 0);
  ASSERT(std::abs(xs - xs_ref) < 3*xs_err);
  ASSERT(xs_err < 0.05*xs);
  return EXIT_SUCCESS;
}

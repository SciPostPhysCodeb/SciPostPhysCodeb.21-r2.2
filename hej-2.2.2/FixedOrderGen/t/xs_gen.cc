/**
 *  \authors   The HEJ collaboration (see AUTHORS for details)
 *  \date      2020
 *  \copyright GPLv2 or later
 */
#include <algorithm>
#include <cmath>
#include <cstdlib>
#include <iostream>
#include <memory>

#include "HEJ/Event.hh"
#include "HEJ/PDG_codes.hh"
#include "HEJ/ScaleFunction.hh"
#include "HEJ/utility.hh"

#include "config.hh"
#include "EventGenerator.hh"
#include "Status.hh"

//! throw error if condition not fulfilled
#define ASSERT(x) if(!(x)) { \
    throw std::logic_error("Assertion '" #x "' failed."); \
  }

namespace {
  constexpr double invGeV2_to_pb = 389379292.;

  constexpr double long_err_tolerance = 37.;
  constexpr double max_sigma = 5.;
}

int main(int argn, char const *argv[]){
  using namespace HEJFOG;
  if(argn != 4 && argn != 5){
    std::cerr << "Usage: " << argv[0] << " config.yml xs xs_err";
    return EXIT_FAILURE;
  }

  const bool short_only = argn==5;

  const double xs_ref = std::stod(argv[2]);
  const double err_ref = std::stod(argv[3]);

  auto config = load_config(argv[1]);

  HEJFOG::EventGenerator generator{config};
  if(short_only)
    config.events/=10.;

  double xs = 0., xs_err = 0.;
  for (std::size_t trials = 0; trials < config.events; ++trials){
    auto ev = generator.gen_event();
    if(generator.status() != Status::good) continue;
    ASSERT(ev);

    if(config.process.boson){
      const auto boson = std::find_if(
          begin(ev->outgoing()), end(ev->outgoing()),
          [&](HEJ::Particle const & p){ return p.type == *config.process.boson; }
      );
      ASSERT(boson != end(ev->outgoing()));

      if(!config.process.boson_decay.empty()){
        ASSERT(ev->decays().size() == 1);
        const auto decay = ev->decays().begin();
        ASSERT(ev->outgoing().size() > decay->first);
        ASSERT(decay->first == static_cast<std::size_t>(
                std::distance(ev->outgoing().begin(), boson)));
        ASSERT(decay->second.size() == 2);
        auto const & decay_part = decay->second;
        ASSERT(decay_part[0].type == config.process.boson_decay[0]);
        ASSERT(decay_part[1].type == config.process.boson_decay[1]);
        ASSERT(HEJ::nearby_ep(decay_part[0].p + decay_part[1].p, boson->p, 1e-6));

        if(boson->type == HEJ::ParticleID::Z_photon_mix
           && (boson->m() < 81. || boson->m() > 101.)){
          continue;
        }
      }
    }

    xs += ev->central().weight;
    xs_err += ev->central().weight*ev->central().weight;
  }
  xs_err = std::sqrt(xs_err);
  xs *= invGeV2_to_pb/config.events;
  xs_err *= invGeV2_to_pb/config.events;
  std::cout <<std::scientific
    << xs_ref << " +- " << err_ref
    <<std::fixed<< " (" << err_ref/xs_ref*100. << "%)"
    <<std::scientific<< " ~ " << xs << " +- " << xs_err
    <<std::fixed<< " (" << xs_err/xs*100. << "%)" << std::endl;
  const double deviation = std::abs(xs - xs_ref)/sqrt(xs_err*xs_err+err_ref*err_ref);

  std::cout << "=> " << deviation << " sigma" << std::endl;

  ASSERT(deviation < max_sigma);
  if(!short_only){
    ASSERT(std::abs(err_ref - xs_err) < long_err_tolerance*err_ref);
  }

  return EXIT_SUCCESS;
}

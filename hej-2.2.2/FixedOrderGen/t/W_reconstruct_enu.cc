/**
 *  \brief     that the reconstruction of the W works
 *
 *  \authors   The HEJ collaboration (see AUTHORS for details)
 *  \date      2019-2020
 *  \copyright GPLv2 or later
 */
#include <cstdlib>
#include <iostream>
#include <memory>
#include <string>

#include "HEJ/Event.hh"
#include "HEJ/Mixmax.hh"
#include "HEJ/ScaleFunction.hh"

#include "config.hh"
#include "EventGenerator.hh"
#include "Status.hh"

//! throw error if condition not fulfilled
#define ASSERT(x) if(!(x)) { \
    throw std::logic_error("Assertion '" #x "' failed."); \
  }

namespace {
  using namespace HEJFOG;
  using namespace HEJ;

  constexpr std::size_t num_events = 1000;
  constexpr double invGeV2_to_pb = 389379292.;
}

double get_xs(std::string config_name){
  auto config { load_config(config_name) };
  config.events = num_events;
  config.rng.seed = "11";
  HEJFOG::EventGenerator generator{config};

  double xs = 0.;
  for (std::size_t trials = 0; trials < config.events; ++trials){
    auto ev = generator.gen_event();
    if(generator.status() != Status::good) continue;
    ASSERT(ev);
    ev->central().weight *= invGeV2_to_pb;
    ev->central().weight /= config.events;
    xs += ev->central().weight;
  }
  return xs;
}

int main(){

  double xs_W{   get_xs("Wp_2j.yml")};
  double xs_enu{ get_xs("Wp_2j_decay.yml")};
  if(std::abs(xs_W/xs_enu-1.)>1e-6){
    std::cerr << "Reconstructing the W in the runcard gave a different results ("
      << xs_W << " vs. "<< xs_enu << " -> " << std::abs(xs_W/xs_enu-1.)*100 << "%)\n";
    return EXIT_FAILURE;
  }

  return EXIT_SUCCESS;
}

/**
 * \brief      check that the PSP generates only "valid" Z + 2 jets events
 *
 *  \authors   The HEJ collaboration (see AUTHORS for details)
 *  \date      2019-2020
 *  \copyright GPLv2 or later
 */

#include <cstdlib>
#include <iostream>
#include <string>
#include <vector>

#include "fastjet/JetDefinition.hh"

#include "HEJ/EWConstants.hh"
#include "HEJ/exceptions.hh"
#include "HEJ/Mixmax.hh"
#include "HEJ/Particle.hh"
#include "HEJ/PDF.hh"
#include "HEJ/PDG_codes.hh"

#include "Decay.hh"
#include "JetParameters.hh"
#include "PhaseSpacePoint.hh"
#include "Process.hh"
#include "Status.hh"
#include "Subleading.hh"

namespace {
  using namespace HEJFOG;
  using namespace HEJ;

  void print_psp(PhaseSpacePoint const & psp){
    std::cerr << "Process:\n"
      << psp.incoming()[0].type << " + "<< psp.incoming()[1].type << " -> ";
    for(auto const & out: psp.outgoing()){
      std::cerr << out.type << " ";
    }
    std::cerr << "\n";
  }

  void bail_out(PhaseSpacePoint const & psp, std::string msg){
    print_psp(psp);
    throw std::logic_error{msg};
  }

  bool check_Z2j(PhaseSpacePoint const & psp){
    bool found_quark = false;
    std::vector<Particle> out_partons;
    std::vector<Particle> Z;
    for(auto const & p: psp.outgoing()){
      if(p.type == pid::Z_photon_mix) Z.push_back(p);
      else if(is_parton(p)) out_partons.push_back(p);
      else bail_out(psp, "Found particle which is not Z or parton");
    }
    if(Z.size() != 1 || out_partons.size() != 2){
      bail_out(psp, "Found wrong number of outgoing partons");
    }
    for(std::size_t j=0; j<2; ++j){
      auto const & in = psp.incoming()[j];
      auto const & out = out_partons[j];
      if(is_quark(in) || is_antiquark(in)) {
        found_quark = true;
        if(in.type != out.type) {
          bail_out(psp, "Switch in quark type");
        }
      }
    }
    if(!found_quark) {
      bail_out(psp, "Found no initial quark");
    }
    return true;
  }
}

int main(){
  constexpr std::size_t n_psp_base = 1337;
  const JetParameters jet_para{
    fastjet::JetDefinition(fastjet::JetAlgorithm::antikt_algorithm, 0.4), 30, 5, 30};
  PDF pdf(11000, pid::proton, pid::proton);
  constexpr double E_cms = 13000.;
  constexpr double subl_chance = 0.0;
  const Subleading subl_channels = subleading::NONE;
  const ParticlesDecayMap boson_decays{
      {pid::Wp, {Decay{ {pid::e_bar, pid::nu_e}, 1.} }},
      {pid::Wm, {Decay{ {pid::e, pid::nu_e_bar}, 1.} }}
    };
  constexpr EWConstants ew_constants{246.2196508, {80.385, 2.085},
                                                  {91.187, 2.495},
                                                  {125, 0.004165} };
  HEJ::Mixmax ran{};

  // Z2j
  Process proc {{pid::proton,pid::proton}, 2, pid::Z_photon_mix, {pid::e_bar,pid::e}};
  std::size_t n_psp = n_psp_base;
  for( std::size_t i = 0; i<n_psp; ++i){
    const PhaseSpacePoint psp{proc,jet_para,pdf,E_cms, subl_chance,subl_channels,
      boson_decays, ew_constants, ran};
    if(psp.status()==Status::good){
      check_Z2j(psp);
    } else { // bad process -> try again
      ++n_psp;
    }
  }
  std::cout << "Z+2j: Took " << n_psp << " to generate "
    << n_psp_base << " successfully PSP (" << 1.*n_psp/n_psp_base << " trials/PSP)" << std::endl;

  std::cout << "All processes passed." << std::endl;
  return EXIT_SUCCESS;
}

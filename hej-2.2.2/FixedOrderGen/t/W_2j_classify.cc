/**
 * \brief      check that the PSP generates only "valid" W + 2 jets events
 *
 *  \authors   The HEJ collaboration (see AUTHORS for details)
 *  \date      2019-2020
 *  \copyright GPLv2 or later
 */

#include <cstdlib>
#include <iostream>
#include <string>
#include <vector>

#include "fastjet/JetDefinition.hh"

#include "HEJ/EWConstants.hh"
#include "HEJ/exceptions.hh"
#include "HEJ/Mixmax.hh"
#include "HEJ/Particle.hh"
#include "HEJ/PDF.hh"
#include "HEJ/PDG_codes.hh"

#include "Decay.hh"
#include "JetParameters.hh"
#include "PhaseSpacePoint.hh"
#include "Process.hh"
#include "Status.hh"
#include "Subleading.hh"

namespace {
  using namespace HEJFOG;
  using namespace HEJ;

  void print_psp(PhaseSpacePoint const & psp){
    std::cerr << "Process:\n"
      << psp.incoming()[0].type << " + "<< psp.incoming()[1].type << " -> ";
    for(auto const & out: psp.outgoing()){
      std::cerr << out.type << " ";
    }
    std::cerr << "\n";
  }

  void bail_out(PhaseSpacePoint const & psp, std::string msg){
    print_psp(psp);
    throw std::logic_error{msg};
  }

  bool is_up_type(Particle const & part){
    return HEJ::is_anyquark(part) && !(std::abs(part.type)%2);
  }
  bool is_down_type(Particle const & part){
    return HEJ::is_anyquark(part) && std::abs(part.type)%2;
  }

  bool check_W2j(PhaseSpacePoint const & psp, ParticleID const W_type){
    bool found_quark = false;
    bool found_anti = false;
    std::vector<Particle> out_partons;
    std::vector<Particle> Wp;
    for(auto const & p: psp.outgoing()){
      if(p.type == W_type) Wp.push_back(p);
      else if(is_parton(p)) out_partons.push_back(p);
      else bail_out(psp, "Found particle with is not "
            +std::to_string(int(W_type))+" or parton");
    }
    if(Wp.size() != 1 || out_partons.size() != 2){
      bail_out(psp, "Found wrong number of outgoing partons");
    }
    for(std::size_t j=0; j<2; ++j){
      auto const & in = psp.incoming()[j];
      auto const & out = out_partons[j];
      if(is_quark(in) || is_antiquark(in)) {
        found_quark = true;
        if(in.type != out.type) { // switch in quark type -> Wp couples to it
          if(found_anti){ // already found qq for coupling to W
            bail_out(psp, "Found second up/down pair");
          } else if(std::abs(in.type)>4 || std::abs(out.type)>4){
            bail_out(psp, "Found bottom/top pair");
          }
          found_anti = true;
          if( is_up_type(in)) { // "up" in
            if(W_type > 0){
              // -> only allowed u -> Wp + d
              if(in.type < 0 || is_up_type(out) || out.type < 0)
                bail_out(psp, "u -/> Wp + d");

            } else {
              // -> only allowed ux -> Wm + dx
              if(in.type > 0 || is_up_type(out) || out.type > 0)
                bail_out(psp, "ux -/> Wm + dx");
            }
          } else { // "down" in
            if(W_type > 0){
              // -> only allowed dx -> Wp + ux
              if(in.type > 0 || is_down_type(out) || out.type > 0)
                bail_out(psp, "dx -/> Wp + ux");
            } else {
              // -> only allowed d -> Wm + u
              if(in.type < 0 || is_down_type(out) || out.type < 0)
                bail_out(psp, "d -/> Wm + u");
            }
          }
        }
      }
    }
    if(!found_quark) {
      bail_out(psp, "Found no initial quarks");
    } else if(!found_anti){
      bail_out(psp, "Found no up/down pair");
    }
    return true;
  }
}

int main(){
  constexpr std::size_t n_psp_base = 1337;
  const JetParameters jet_para{
    fastjet::JetDefinition(fastjet::JetAlgorithm::antikt_algorithm, 0.4), 30, 5, 30};
  PDF pdf(11000, pid::proton, pid::proton);
  constexpr double E_cms = 13000.;
  constexpr double subl_chance = 0.0;
  const Subleading subl_channels = subleading::NONE;
  const ParticlesDecayMap boson_decays{
      {pid::Wp, {Decay{ {pid::e_bar, pid::nu_e}, 1.} }},
      {pid::Wm, {Decay{ {pid::e, pid::nu_e_bar}, 1.} }}
    };
  constexpr EWConstants ew_constants{246.2196508, {80.385, 2.085},
                                                  {91.187, 2.495},
                                                  {125, 0.004165} };
  HEJ::Mixmax ran{};

  // Wp2j
  Process proc {{pid::proton,pid::proton}, 2, pid::Wp, {}};
  std::size_t n_psp = n_psp_base;
  for( std::size_t i = 0; i<n_psp; ++i){
    const PhaseSpacePoint psp{proc,jet_para,pdf,E_cms, subl_chance,subl_channels,
      boson_decays, ew_constants, ran};
    if(psp.status()==Status::good){
      check_W2j(psp, *proc.boson);
    } else { // bad process -> try again
      ++n_psp;
    }
  }
  std::cout << "Wp+2j: Took " << n_psp << " to generate "
    << n_psp_base << " successfully PSP (" << 1.*n_psp/n_psp_base << " trials/PSP)" << std::endl;
  // Wm2j
  proc = Process{{pid::proton,pid::proton}, 2, pid::Wm, {}};
  n_psp = n_psp_base;
  for( std::size_t i = 0; i<n_psp; ++i){
    const PhaseSpacePoint psp{proc,jet_para,pdf,E_cms, subl_chance,subl_channels,
      boson_decays, ew_constants, ran};
    if(psp.status()==Status::good){
      check_W2j(psp, *proc.boson);
    } else { // bad process -> try again
      ++n_psp;
    }
  }
  std::cout << "Wm+2j: Took " << n_psp << " to generate "
    << n_psp_base << " successfully PSP (" << 1.*n_psp/n_psp_base << " trials/PSP)" << std::endl;

  std::cout << "All processes passed." << std::endl;
  return EXIT_SUCCESS;
}

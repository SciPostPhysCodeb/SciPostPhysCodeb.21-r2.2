#include "Subleading.hh"
#include <stdexcept>

namespace HEJFOG {
  using namespace subleading;
  using namespace std::string_view_literals;

  std::string_view name(Channels const c){
    switch(c) {
    case unordered:
      return "unordered"sv;
    case central_qqbar:
      return "central qqbar"sv;
    case extremal_qqbar:
      return "extremal qqbar"sv;
    }
    throw std::logic_error{"Channel not included"};
  }
}

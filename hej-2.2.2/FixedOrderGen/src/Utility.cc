#include "Utility.hh"

#include <HEJ/PDG_codes.hh>
#include <HEJ/exceptions.hh>
#include <stdexcept>
#include <type_traits>

#include "Process.hh"

namespace HEJFOG {
  using HEJ::is_anyquark;

  namespace {
    bool is_up_type(HEJ::pid::ParticleID const part) {
      return HEJ::is_anyquark(part) && ((std::abs(part) % 2) == 0);
    }
    bool is_down_type(HEJ::pid::ParticleID const part){
      return HEJ::is_anyquark(part) && ((std::abs(part)%2) != 0);
    }
  }

  bool parton_can_couple_to_W(
    HEJ::pid::ParticleID const part, HEJ::pid::ParticleID const W_id
  ){
    const int W_charge = W_id>0?1:-1;
    return std::abs(part)<HEJ::pid::b
      && ( (W_charge*part > 0 && is_up_type(part))
           || (W_charge*part < 0 && is_down_type(part)) );
  }

  bool parton_can_couple_to_W(
    HEJ::Particle const & part, HEJ::pid::ParticleID const W_id
  ){
    return parton_can_couple_to_W(part.type, W_id);
  }

  bool is_AWZ_process(Process const & proc){
    return proc.boson && HEJ::is_AWZ_boson(*proc.boson);
  }

  bool vector_boson_can_couple_to(
    HEJ::pid::ParticleID boson,
    HEJ::pid::ParticleID particle
  ) {
    if(!HEJ::is_AWZ_boson(boson)) {
      if(HEJ::is_AWZ_boson(particle)) {
        std::swap(boson, particle);
      } else {
        throw std::invalid_argument{"One argument has to be a vector boson"};
      }
    }
    switch(particle){
    case HEJ::pid::proton:
    case HEJ::pid::antiproton:
      return true;
    case HEJ::pid::gluon:
      return false;
    default:
      if (!is_anyquark(particle)) {
        throw HEJ::not_implemented{
          "Test if boson couples to " + name(particle)
        };
      }
      if(std::abs(boson) == HEJ::pid::ParticleID::Wp) {
        return parton_can_couple_to_W(particle, boson);
      }
      assert(boson == HEJ::pid::Z_photon_mix);
      return true;
    }
  }
} // namespace HEJFOG

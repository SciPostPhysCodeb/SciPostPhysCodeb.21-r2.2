/**
 *  \authors   The HEJ collaboration (see AUTHORS for details)
 *  \date      2019-2022
 *  \copyright GPLv2 or later
 */
#include <algorithm>
#include <cassert>
#include <chrono>
#include <cmath>
#include <cstdint>
#include <cstdlib>
#include <ctime>
#include <iomanip>
#include <iostream>
#include <iterator>
#include <map>
#include <memory>
#include <optional>
#include <string>
#include <utility>
#include <vector>

#include <boost/iterator/filter_iterator.hpp>

#include "fastjet/ClusterSequence.hh"
#include "fastjet/PseudoJet.hh"

#include "HEJ/Analysis.hh"
#include "HEJ/CombinedEventWriter.hh"
#include "HEJ/CrossSectionAccumulator.hh"
#include "HEJ/Event.hh"
#include "HEJ/exceptions.hh"
#include "HEJ/get_analysis.hh"
#include "HEJ/make_RNG.hh"
#include "HEJ/ProgressBar.hh"
#include "HEJ/ScaleFunction.hh"
#include "HEJ/Unweighter.hh"

#include "LHEF/LHEF.h"

#include "config.hh"
#include "EventGenerator.hh"
#include "Status.hh"
#include "Version.hh"

namespace YAML {
  class Node;
}

namespace {
  constexpr auto banner =
    "     __  ___       __       ______                                   __   "
    " __                               \n    / / / (_)___ _/ /_     / ____/___ "
    " ___  _________ ___  __       / /__  / /______                         \n "
    "  / /_/ / / __ `/ __ \\   / __/ / __ \\/ _ \\/ ___/ __ `/ / / /  __  / / _"
    " \\/ __/ ___/                         \n  / __  / / /_/ / / / /  / /___/ /"
    " / /  __/ /  / /_/ / /_/ /  / /_/ /  __/ /_(__  )                         "
    " \n /_/ /_/_/\\__, /_/ /_/  /_____/_/ /_/\\___/_/   \\__, /\\__, /   \\___"
    "_/\\___/\\__/____/                           \n     ____///__/            "
    "__   ____          ///__//____/    ______                           __    "
    "        \n    / ____(_)  _____  ____/ /  / __ \\_________/ /__  _____   / "
    "____/__  ____  ___  _________ _/ /_____  _____\n   / /_  / / |/_/ _ \\/ __"
    "  /  / / / / ___/ __  / _ \\/ ___/  / / __/ _ \\/ __ \\/ _ \\/ ___/ __ `/ "
    "__/ __ \\/ ___/\n  / __/ / />  </  __/ /_/ /  / /_/ / /  / /_/ /  __/ /   "
    "  / /_/ /  __/ / / /  __/ /  / /_/ / /_/ /_/ / /    \n /_/   /_/_/|_|\\___"
    "/\\__,_/   \\____/_/   \\__,_/\\___/_/      \\____/\\___/_/ /_/\\___/_/   "
    "\\__,_/\\__/\\____/_/     \n";

  constexpr double invGeV2_to_pb = 389379292.;
}

HEJFOG::Config load_config(char const * filename){
  try{
    return HEJFOG::load_config(filename);
  }
  catch(std::exception const & exc){
    std::cerr << "Error: " << exc.what() << '\n';
    std::exit(EXIT_FAILURE);
  }
}

std::vector<std::unique_ptr<HEJ::Analysis>> get_analyses(
    std::vector<YAML::Node> const & parameters, LHEF::HEPRUP const & heprup
){
  try{
    return HEJ::get_analyses(parameters, heprup);
  }
  catch(std::exception const & exc){
    std::cerr << "Failed to load analysis: " << exc.what() << '\n';
    std::exit(EXIT_FAILURE);
  }
}

template<class Iterator>
auto make_lowpt_filter(Iterator begin, Iterator end, std::optional<double> peak_pt){
  return boost::make_filter_iterator(
          [peak_pt](HEJ::Event const & ev){
            assert(! ev.jets().empty());
            double min_pt = peak_pt?(*peak_pt):0.;
            const auto softest_jet = fastjet::sorted_by_pt(ev.jets()).back();
            return softest_jet.pt() > min_pt;
          },
          begin, end
        );
}

int main(int argn, char** argv) {
  using namespace std::string_literals;
  if (argn < 2) {
    std::cerr << "\n# Usage:\n." << argv[0] << " config_file\n";
    return EXIT_FAILURE;
  }
  std::cout << banner;
  std::cout << "Version " << HEJFOG::Version::String()
             << ", revision " << HEJFOG::Version::revision() << std::endl;
  fastjet::ClusterSequence::print_banner();
  using clock = std::chrono::system_clock;

  const auto start_time = clock::now();

  auto config = load_config(argv[1]);

  HEJFOG::EventGenerator generator{config};
  auto ran = generator.ran();

  // prepare process information for output
  LHEF::HEPRUP heprup;
  heprup.IDBMUP=std::pair<long,long>(config.beam.particles[0], config.beam.particles[1]);
  heprup.EBMUP=std::make_pair(config.beam.energy, config.beam.energy);
  heprup.PDFGUP=std::make_pair(0,0);
  heprup.PDFSUP=std::make_pair(config.pdf_id,config.pdf_id);
  heprup.NPRUP=1;
  heprup.XSECUP=std::vector<double>(1.);
  heprup.XERRUP=std::vector<double>(1.);
  heprup.LPRUP=std::vector<int>{1};
  heprup.generators.emplace_back(LHEF::XMLTag{});
  heprup.generators.back().name = HEJFOG::Version::package_name();
  heprup.generators.back().version = HEJFOG::Version::String();

  HEJ::CombinedEventWriter writer{config.output, heprup};

  std::vector<std::unique_ptr<HEJ::Analysis>> analyses = get_analyses(
      config.analyses_parameters, heprup
  );
  assert(analyses.empty() || analyses.front() != nullptr);

  // warm-up phase to train unweighter
  std::optional<HEJ::Unweighter> unweighter{};
  std::map<HEJFOG::Status, std::uint64_t> status_counter;

  std::vector<HEJ::Event> events;
  std::uint64_t trials = 0;
  if(config.unweight) {
    std::cout << "Calibrating unweighting ...\n";
    const auto warmup_start = clock::now();
    const std::size_t warmup_events = config.unweight->sample_size;
    HEJ::ProgressBar<std::size_t> warmup_progress{std::cout, warmup_events};
    for(; events.size() < warmup_events; ++trials){
      auto ev = generator.gen_event();
      ++status_counter[generator.status()];
      assert( (generator.status() == HEJFOG::Status::good) == bool(ev) );
      if(generator.status() != HEJFOG::Status::good) continue;
      const bool pass_cuts = analyses.empty() || std::any_of(
        begin(analyses), end(analyses),
        [&ev](auto const & analysis) { return analysis->pass_cuts(*ev, *ev); }
      );
      if(pass_cuts) {
        events.emplace_back(std::move(*ev));
        ++warmup_progress;
      }
    }
    std::cout << std::endl;
    unweighter = HEJ::Unweighter();
    unweighter->set_cut_to_peakwt(
      make_lowpt_filter(events.cbegin(), events.cend(), config.jets.peak_pt),
      make_lowpt_filter(events.cend(),   events.cend(), config.jets.peak_pt),
      config.unweight->max_dev
    );
    std::vector<HEJ::Event> unweighted_events;
    for(auto && ev: events) {
      auto unweighted = unweighter->unweight(std::move(ev), *ran);
      if(unweighted) {
        unweighted_events.emplace_back(std::move(*unweighted));
      }
    }
    events = std::move(unweighted_events);
    if(events.empty()) {
      std::cerr <<
        "Failed to generate events. Please increase \"unweight: sample size\""
        " or reduce \"unweight: max deviation\"\n";
      return EXIT_FAILURE;
    }
    const auto warmup_end = clock::now();
    const double completion = static_cast<double>(events.size())/config.events;
    const std::chrono::duration<double> remaining_time =
      (warmup_end- warmup_start)*(1./completion - 1);
    const auto finish = clock::to_time_t(
        std::chrono::time_point_cast<std::chrono::seconds>(warmup_end + remaining_time)
    );
    std::cout
      << "Generated " << events.size() << "/" << config.events << " events ("
      << static_cast<int>(std::round(100*completion)) << "%)\n"
      << "Estimated remaining generation time: "
      << remaining_time.count() << " seconds ("
      << std::put_time(std::localtime(&finish), "%c") << ")\n\n";
  } // end unweighting warm-up

  // main generation loop
  // event weight is wrong, need to divide by "total number of trials" afterwards
  HEJ::ProgressBar<std::size_t> progress{std::cout, config.events};
  progress.increment(events.size());
  events.reserve(config.events);

  for(; events.size() < config.events; ++trials){
    auto ev = generator.gen_event();
    ++status_counter[generator.status()];
    assert( (generator.status() == HEJFOG::Status::good) == bool(ev) );
    if(generator.status() != HEJFOG::Status::good) continue;
    const bool pass_cuts = analyses.empty() || std::any_of(
      begin(analyses), end(analyses),
      [&ev](auto const & analysis) { return analysis->pass_cuts(*ev, *ev); }
    );
    if(pass_cuts) {
      if(unweighter) {
        auto unweighted = unweighter->unweight(std::move(*ev), *ran);
        if(! unweighted) continue;
        ev = std::move(unweighted);
      }
      events.emplace_back(std::move(*ev));
      ++progress;
    }
  }
  std::cout << std::endl;

  // final run though events with correct weight
  HEJ::CrossSectionAccumulator xs;
  for(auto & ev: events){
    ev.parameters() *= invGeV2_to_pb/trials;
    for(auto const & analysis: analyses) {
      if(analysis->pass_cuts(ev, ev)) {
        analysis->fill(ev, ev);
      }
    }
    writer.write(ev);
    xs.fill(ev);
  }
  for(auto const & analysis: analyses) {
    analysis->finalise();
  }
  writer.finish();

  // Print final informations
  const std::chrono::duration<double> run_time = (clock::now() - start_time);
  std::cout << "\nTask Runtime: " << run_time.count() << " seconds for "
            << events.size() << " Events (" << events.size()/run_time.count()
            << " evts/s)\n" << std::endl;

  std::cout << xs << "\n";

  for(auto && entry: status_counter){
    const double fraction = static_cast<double>(entry.second)/trials;
    const int percent = std::round(100*fraction);
    std::cout << "status "
              << std::left << std::setw(16) << (to_string(entry.first) + ":")
              << " [";
    for(int i = 0; i < percent/2; ++i) std::cout << '#';
    for(int i = percent/2; i < 50; ++i) std::cout << ' ';
    std::cout << "] " << percent << "%" << std::endl;
  }

  return EXIT_SUCCESS;
}

/**
 *  \authors   The HEJ collaboration (see AUTHORS for details)
 *  \date      2019-2020
 *  \copyright GPLv2 or later
 */
#include "EventGenerator.hh"

#include <HEJ/Particle.hh>
#include <cstddef>
#include <iostream>
#include <stdexcept>
#include <utility>

#include "HEJ/Config.hh"
#include "HEJ/EWConstants.hh"
#include "HEJ/Event.hh"
#include "HEJ/HiggsCouplingSettings.hh"
#include "HEJ/event_types.hh"
#include "HEJ/exceptions.hh"
#include "HEJ/make_RNG.hh"
#include "HEJ/PDG_codes.hh"

#include "Beam.hh"
#include "JetParameters.hh"
#include "PhaseSpacePoint.hh"
#include "Process.hh"
#include "Subleading.hh"
#include "Utility.hh"
#include "config.hh"

namespace HEJFOG {
  namespace {

    bool allows_gluon(const HEJ::ParticleID particle_id) {
      return
        std::abs(particle_id) == HEJ::pid::proton
        || particle_id == HEJ::pid::gluon;
    }

    bool allows_anyquark(const HEJ::ParticleID particle_id) {
      return
        std::abs(particle_id) == HEJ::pid::proton
        || HEJ::is_anyquark(particle_id);
      }

    Subleading possible_subl_channels(Process const &process) {
      std::size_t nhiggs_bosons =
        (process.boson && *process.boson == HEJ::pid::Higgs) ? 1 : 0;
      if(process.njets + nhiggs_bosons <= 2){
        return subleading::NONE;
      }
      auto allowed = subleading::ALL;
      if(process.njets + nhiggs_bosons < 4){
        allowed.reset(subleading::central_qqbar);
      }
      if(
        !allows_gluon(process.incoming[0])
        && !allows_gluon(process.incoming[1])
      ){
        allowed.reset(subleading::extremal_qqbar);
      }

      if(
        !allows_anyquark(process.incoming[0])
        && !allows_anyquark(process.incoming[1])
      ){
        allowed.reset(subleading::unordered);
      }

      if(
        process.boson && std::abs(*process.boson) == HEJ::pid::Wp
        && !vector_boson_can_couple_to(process.incoming[0], *process.boson)
        && !vector_boson_can_couple_to(process.incoming[1], *process.boson)
      ){
        allowed.reset(subleading::unordered);
      }
      return allowed;
    }

    bool can_generate_FKL(Process const &process) {
      std::size_t nhiggs_bosons =
        (process.boson && *process.boson == HEJ::pid::Higgs) ? 1 : 0;
      if(process.njets + nhiggs_bosons < 2){
        return false;
      }
      if(
        is_AWZ_process(process)
        && !vector_boson_can_couple_to(process.incoming[0], *process.boson)
        && !vector_boson_can_couple_to(process.incoming[1], *process.boson)
      ) return false;
      return true;
    }

    // TODO: maybe we don't even want to accept inconsistent settings here
    void ensure_valid_subl(
      Process const &process,
      double &subl_chance,
      Subleading &subl_channels
    ){
      if(subl_chance == 0. && subl_channels.any()) {
        std::cerr << "WARNING: Subleading fraction is set to 0,"
          " disabling all subleading channels\n";
        subl_channels = subleading::NONE;
      }
      const Subleading possible_channels = possible_subl_channels(process);
      for (
        unsigned channel = subleading::Channels::first;
        channel <= subleading::Channels::last;
        ++channel
      ){
        if(subl_channels.test(channel) && !possible_channels.test(channel)){
          std::cerr
            << "WARNING: selected subleading channel "
            << name(static_cast<subleading::Channels>(channel))
            << " cannot be generated for the chosen process.\n";
          subl_channels.reset(channel);
        }
      }
      if(subl_channels == subleading::NONE && subl_chance > 0.) {
        if(subl_chance == 1.) {
          throw std::invalid_argument{
            "Cannot generate subleading event configurations"
          };
        }
        std::cerr <<
          "WARNING: Positive fraction of subleading configurations requested,"
          " but cannot generate any.\n";
        subl_chance = 0.;
      }
      if(!can_generate_FKL(process) && subl_chance < 1.) {
        if(subl_chance > 0.) {
          std::cerr <<
            "WARNING: Can only generate subleading configurations.\n";
          subl_chance = 1.;
        } else {
          throw std::invalid_argument{
            "Cannot generate events for given process"
          };
        }
      }
    }
  }

  EventGenerator::EventGenerator(Config const & config):
    EventGenerator{
      config.process,
      config.beam,
      HEJ::ScaleGenerator{
        config.scales.base,
        config.scales.factors,
        config.scales.max_ratio
      },
      config.jets,
      config.pdf_id,
      config.subleading_fraction,
      config.subleading_channels,
      config.particle_decays,
      config.Higgs_coupling,
      config.ew_parameters,
      config.nlo,
      HEJ::make_RNG(config.rng.name, config.rng.seed)
    }
  {}

  EventGenerator::EventGenerator(
      Process process,
      Beam beam,
      HEJ::ScaleGenerator scale_gen,
      JetParameters jets,
      int pdf_id,
      double subl_chance,
      Subleading subl_channels,
      ParticlesDecayMap particle_decays,
      HEJ::HiggsCouplingSettings Higgs_coupling,
      HEJ::EWConstants ew_parameters,
      HEJ::NLOConfig nlo,
      std::shared_ptr<HEJ::RNG> ran
  ):
    pdf_{pdf_id, beam.particles[0], beam.particles[1]},
    ME_{
      [this](double mu){ return pdf_.Halphas(mu); },
      HEJ::MatrixElementConfig{
        false,
        std::move(Higgs_coupling),
        ew_parameters,
        nlo
      }
    },
    scale_gen_{std::move(scale_gen)},
    process_{std::move(process)},
    jets_{std::move(jets)},
    beam_{std::move(beam)},
    status_{Status::good},
    subl_chance_{subl_chance},
    subl_channels_{subl_channels},
    particle_decays_{std::move(particle_decays)},
    ew_parameters_{ew_parameters},
    ran_{std::move(ran)}
  {
    ensure_valid_subl(process_, subl_chance_, subl_channels_);
    assert(ran_ != nullptr);
  }

  std::optional<HEJ::Event> EventGenerator::gen_event(){
    HEJFOG::PhaseSpacePoint psp{
      process_,
      jets_,
      pdf_, beam_.energy,
      subl_chance_, subl_channels_,
      particle_decays_,
      ew_parameters_,
      *ran_
    };
    status_ = psp.status();
    if(status_ != Status::good) return {};

    HEJ::Event ev = scale_gen_(
        HEJ::Event{
          to_EventData( std::move(psp) ).cluster( jets_.def, jets_.min_pt)
        }
    );
    if(!is_resummable(ev.type())){
      throw HEJ::not_implemented("Tried to generate a event type, "
        "which is not yet implemented in HEJ.");
    }

    ev.generate_colours(*ran_);

    const double shat = HEJ::shat(ev);
    const double xa = (ev.incoming()[0].E()-ev.incoming()[0].pz())/(2.*beam_.energy);
    const double xb = (ev.incoming()[1].E()+ev.incoming()[1].pz())/(2.*beam_.energy);

    // evaluate matrix element
    ev.parameters() *= ME_.tree(ev)/(shat*shat);
    // and PDFs
    ev.central().weight *= pdf_.pdfpt(0,xa,ev.central().muf, ev.incoming()[0].type);
    ev.central().weight *= pdf_.pdfpt(0,xb,ev.central().muf, ev.incoming()[1].type);
    for(std::size_t i = 0; i < ev.variations().size(); ++i){
      auto & var = ev.variations(i);
      var.weight *= pdf_.pdfpt(0,xa,var.muf, ev.incoming()[0].type);
      var.weight *= pdf_.pdfpt(0,xb,var.muf, ev.incoming()[1].type);
    }
    return ev;
  }

} // namespace HEJFOG

/**
 *  \authors   The HEJ collaboration (see AUTHORS for details)
 *  \date      2019-2022
 *  \copyright GPLv2 or later
 */
#pragma once

#include <optional>

#include "fastjet/JetDefinition.hh"

namespace HEJFOG {
  struct JetParameters{
    fastjet::JetDefinition def;
    double min_pt{};
    double max_y{};
    std::optional<double> peak_pt;
  };
}

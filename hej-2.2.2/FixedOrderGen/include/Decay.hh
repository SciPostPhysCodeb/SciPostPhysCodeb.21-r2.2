/**
 *  \authors   The HEJ collaboration (see AUTHORS for details)
 *  \date      2019
 *  \copyright GPLv2 or later
 */
#pragma once

#include <unordered_map>
#include <vector>

#include "HEJ/PDG_codes.hh"

namespace HEJFOG {
  struct Decay{
    std::vector<HEJ::ParticleID> products;
    double branching_ratio{};
  };
  #if !defined(__clang__) && defined(__GNUC__) && (__GNUC__ < 6)
  // gcc version < 6 explicitly needs hash function for enum
  // see https://gcc.gnu.org/bugzilla/show_bug.cgi?id=60970
  using ParticlesDecayMap
    = std::unordered_map<HEJ::ParticleID, std::vector<Decay>, std::hash<int>>;
  #else
  using ParticlesDecayMap
    = std::unordered_map<HEJ::ParticleID, std::vector<Decay>>;
  #endif
} // namespace HEJFOG

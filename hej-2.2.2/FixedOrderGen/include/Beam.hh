/**
 *  \authors   The HEJ collaboration (see AUTHORS for details)
 *  \date      2019
 *  \copyright GPLv2 or later
 */
#pragma once

#include <array>

#include "HEJ/PDG_codes.hh"

namespace HEJFOG {
  struct Beam{
    double energy{};
    std::array<HEJ::ParticleID, 2> particles{{
      HEJ::pid::proton, HEJ::pid::proton
    }};
  };
}

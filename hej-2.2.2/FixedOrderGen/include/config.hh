/**
 *  \authors   The HEJ collaboration (see AUTHORS for details)
 *  \date      2019-2022
 *  \copyright GPLv2 or later
 */
#pragma once

#include <cstddef>
#include <optional>
#include <string>
#include <vector>

#include "HEJ/Config.hh"
#include "HEJ/EWConstants.hh"
#include "HEJ/Fraction.hh"
#include "HEJ/HiggsCouplingSettings.hh"
#include "HEJ/output_formats.hh"

#include "yaml-cpp/yaml.h"

#include "Beam.hh"
#include "Decay.hh"
#include "JetParameters.hh"
#include "Process.hh"
#include "Subleading.hh"
#include "UnweightSettings.hh"

namespace HEJFOG {

  struct Config{
    Process process;
    std::size_t events{};
    JetParameters jets;
    Beam beam;
    int pdf_id{};
    HEJ::Fraction<double> subleading_fraction{};
    Subleading subleading_channels; //! < see HEJFOG::Subleading
    ParticlesDecayMap particle_decays;
    std::vector<YAML::Node> analyses_parameters;
    HEJ::ScaleConfig scales;
    std::vector<HEJ::OutputFile> output;
    HEJ::RNGConfig rng;
    HEJ::HiggsCouplingSettings Higgs_coupling;
    HEJ::EWConstants ew_parameters;
    HEJ::NLOConfig nlo;
    std::optional<UnweightSettings> unweight;
  };

  Config load_config(std::string const & config_file);

} // namespace HEJFOG

/**
 *  \authors   The HEJ collaboration (see AUTHORS for details)
 *  \date      2019
 *  \copyright GPLv2 or later
 */
#pragma once

namespace HEJFOG {
  struct UnweightSettings {
    int sample_size;
    double max_dev;
  };
}

/**
 *  \authors   The HEJ collaboration (see AUTHORS for details)
 *  \date      2019-2022
 *  \copyright GPLv2 or later
 */
#pragma once

#include <memory>
#include <optional>

#include "HEJ/EWConstants.hh"
#include "HEJ/MatrixElement.hh"
#include "HEJ/PDF.hh"
#include "HEJ/ScaleFunction.hh"

#include "Beam.hh"
#include "Decay.hh"
#include "JetParameters.hh"
#include "Process.hh"
#include "Status.hh"
#include "Subleading.hh"

namespace HEJ {
  class Event;
  class HiggsCouplingSettings;
  class RNG;
}

//! Namespace for HEJ Fixed Order Generator
namespace HEJFOG {
  class Config;

  class EventGenerator{
  public:
    explicit EventGenerator(Config const & config);

    EventGenerator(
        Process process,
        Beam beam,
        HEJ::ScaleGenerator scale_gen,
        JetParameters jets,
        int pdf_id,
        double subl_chance,
        Subleading subl_channels,
        ParticlesDecayMap particle_decays,
        HEJ::HiggsCouplingSettings Higgs_coupling,
        HEJ::EWConstants ew_parameters,
        HEJ::NLOConfig nlo,
        std::shared_ptr<HEJ::RNG> ran
    );

    std::optional<HEJ::Event> gen_event();

    Status status() const {
      return status_;
    }

    std::shared_ptr<HEJ::RNG> ran() const {
      return ran_;
    }

  private:
    HEJ::PDF pdf_;
    HEJ::MatrixElement ME_;
    HEJ::ScaleGenerator scale_gen_;
    Process process_;
    JetParameters jets_;
    Beam beam_;
    Status status_;
    double subl_chance_;
    Subleading subl_channels_;
    ParticlesDecayMap particle_decays_;
    HEJ::EWConstants ew_parameters_;
    std::shared_ptr<HEJ::RNG> ran_;
  };

} // namespace HEJFOG

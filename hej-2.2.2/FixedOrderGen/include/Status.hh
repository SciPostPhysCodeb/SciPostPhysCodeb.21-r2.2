/**
 *  \authors   The HEJ collaboration (see AUTHORS for details)
 *  \date      2019
 *  \copyright GPLv2 or later
 */
#pragma once

#include <stdexcept>
#include <string>

namespace HEJFOG {
  enum class Status{
    good,
    not_enough_jets,
    too_much_energy
  };

  inline std::string to_string(Status s){
    switch(s){
      case Status::good: return "good";
      case Status::not_enough_jets: return "not enough jets";
      case Status::too_much_energy: return "too much energy";
      default:;
    }
    throw std::logic_error{"unreachable"};
  }
} // namespace HEJFOG

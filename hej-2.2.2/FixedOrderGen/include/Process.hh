/**
 *  \authors   The HEJ collaboration (see AUTHORS for details)
 *  \date      2019-2022
 *  \copyright GPLv2 or later
 */
#pragma once

#include <array>
#include <vector>

#include "HEJ/PDG_codes.hh"

namespace HEJFOG {
  struct Process{
    //! @internal pair to match Event::incoming
    std::array<HEJ::ParticleID, 2> incoming{};
    std::size_t njets{};
    std::optional<HEJ::ParticleID> boson;
    std::vector<HEJ::ParticleID> boson_decay;
  };

}

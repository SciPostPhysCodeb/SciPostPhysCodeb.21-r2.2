/**
 *  \authors   The HEJ collaboration (see AUTHORS for details)
 *  \date      2022
 *  \copyright GPLv2 or later
 */
#pragma once

#include <HEJ/Particle.hh>

namespace HEJFOG {
  class Process;

  bool vector_boson_can_couple_to(
    HEJ::pid::ParticleID vector_boson,
    HEJ::pid::ParticleID particle
  );

  bool parton_can_couple_to_W(
    HEJ::Particle const & part,
    HEJ::pid::ParticleID W_id
  );

  bool parton_can_couple_to_W(
    HEJ::pid::ParticleID part,
    HEJ::pid::ParticleID W_id
  );

  bool is_AWZ_process(Process const & proc);
}

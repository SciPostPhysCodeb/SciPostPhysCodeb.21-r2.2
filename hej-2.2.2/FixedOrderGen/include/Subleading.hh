/**
 *  \authors   The HEJ collaboration (see AUTHORS for details)
 *  \date      2019
 *  \copyright GPLv2 or later
 */
#pragma once

#include <bitset>
#include <string_view>

namespace HEJFOG {
  namespace subleading { //!< @TODO confusing name with capital Subleading
    /**
     * Bit position of different subleading channels
     * e.g. (unsigned int) 1 => only unordered
     */
    enum Channels: unsigned {
      uno,
      unordered = uno,
      cqqbar,
      central_qqbar = cqqbar,
      eqqbar,
      extremal_qqbar = eqqbar,
      first = uno,
      last = eqqbar,
    };
  } // namespace subleading
  std::string_view name(subleading::Channels c);

  using Subleading = std::bitset<subleading::Channels::last+1>;

  namespace subleading {
    static constexpr Subleading ALL{~0u};
    static constexpr Subleading NONE{0u};
  }
} // namespace HEJFOG

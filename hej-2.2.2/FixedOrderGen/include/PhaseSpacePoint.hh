/** \file      PhaseSpacePoint.hh
 *  \brief     Contains the PhaseSpacePoint Class
 *
 *  \authors   The HEJ collaboration (see AUTHORS for details)
 *  \date      2019-2020
 *  \copyright GPLv2 or later
 */
#pragma once

#include <array>
#include <bitset>
#include <cstddef>
#include <unordered_map>
#include <optional>
#include <vector>

#include "boost/iterator/filter_iterator.hpp"

#include "HEJ/Event.hh"
#include "HEJ/PDG_codes.hh"
#include "HEJ/Particle.hh"

#include "fastjet/PseudoJet.hh"

#include "Decay.hh"
#include "Status.hh"
#include "Subleading.hh"

namespace HEJ {
  class EWConstants;
  class PDF;
  class RNG;
}

namespace HEJFOG {
  struct JetParameters;
  struct Process;

  //! A point in resummation phase space
  class PhaseSpacePoint{
  public:

    //! Iterator over partons
    using ConstPartonIterator = boost::filter_iterator<
      bool (*)(HEJ::Particle const &),
      std::vector<HEJ::Particle>::const_iterator
      >;
    //! Reverse Iterator over partons
    using ConstReversePartonIterator = std::reverse_iterator<
                                                ConstPartonIterator>;

    //! Default PhaseSpacePoint Constructor
    PhaseSpacePoint() = delete;

    //! PhaseSpacePoint Constructor
    /**
     * @param proc                  The process to generate
     * @param jet_param             Jet defintion & cuts
     * @param pdf                   The pdf set (used for sampling)
     * @param E_beam                Energie of the beam
     * @param subl_chance           Chance to turn a potentially unordered
     *                              emission into an actual one
     * @param subl_channels         Possible subleading channels.
     *                              see HEJFOG::Subleading
     * @param particle_properties   Properties of producted boson
     *
     * Initially, only FKL phase space points are generated. subl_chance gives
     * the chance of turning one emissions into a subleading configuration,
     * i.e. either unordered or central quark/anti-quark pair. Unordered
     * emissions require that the most extremal emission in any direction is
     * a quark or anti-quark and the next emission is a gluon. Quark/anti-quark
     * pairs are only generated for W processes. At most one subleading
     * emission will be generated in this way.
     */
    PhaseSpacePoint(
      Process const & proc,
      JetParameters const & jet_param,
      HEJ::PDF & pdf, double E_beam,
      double subl_chance,
      Subleading subl_channels,
      ParticlesDecayMap const & particle_decays,
      HEJ::EWConstants const & ew_parameters,
      HEJ::RNG & ran
    );

    //! Get Weight Function
    /**
     * @returns        Weight of Event
     */
    double weight() const{
      return weight_;
    }

    Status status() const{
      return status_;
    }

    //! Get Incoming Function
    /**
     * @returns        Incoming Particles
     */
    std::array<HEJ::Particle, 2> const & incoming() const{
      return incoming_;
    }

    //! Get Outgoing Function
    /**
     * @returns        Outgoing Particles
     */
    std::vector<HEJ::Particle> const & outgoing() const{
      return outgoing_;
    }

    std::unordered_map<std::size_t, std::vector<HEJ::Particle>> const & decays() const{
      return decays_;
    }

    //! Iterator to the first outgoing parton
    ConstPartonIterator begin_partons() const;
    //! Iterator to the first outgoing parton
    ConstPartonIterator cbegin_partons() const;

    //! Iterator to the end of the outgoing partons
    ConstPartonIterator end_partons() const;
    //! Iterator to the end of the outgoing partons
    ConstPartonIterator cend_partons() const;

    //! Reverse Iterator to the first outgoing parton
    ConstReversePartonIterator rbegin_partons() const;
    //! Reverse Iterator to the first outgoing parton
    ConstReversePartonIterator crbegin_partons() const;
    //! Reverse Iterator to the first outgoing parton
    ConstReversePartonIterator rend_partons() const;
    //! Reverse Iterator to the first outgoing parton
    ConstReversePartonIterator crend_partons() const;

  private:
    static constexpr std::size_t NUM_PARTONS = 11; //!< Number of partons
  public:
    using part_mask = std::bitset<NUM_PARTONS>; //!< Selection mask for partons
  private:
    friend HEJ::Event::EventData to_EventData(PhaseSpacePoint psp);
    /**
     * @internal
     * @brief Generate LO parton momentum
     *
     * @param np                Number of partons to generate
     * @param is_pure_jets      If true ensures momentum conservation in x and y
     * @param jet_param         Jet properties to fulfil
     * @param max_pt            max allowed pt for a parton (typically E_CMS)
     * @param ran               Random Number Generator
     *
     * @returns                 Momentum of partons
     *
     * Ensures that each parton is in its own jet.
     * Generation is independent of parton flavour. Output is sorted in rapidity.
     */
    std::vector<fastjet::PseudoJet> gen_LO_partons(
        int np, bool is_pure_jets,
        JetParameters const & jet_param,
        double max_pt,
        HEJ::RNG & ran
    );
    void add_boson(
      Process const & proc,
      ParticlesDecayMap const & particle_decays,
      HEJ::EWConstants const & ew_parameters,
      HEJ::RNG & ran
    );
    HEJ::Particle gen_boson(
        HEJ::ParticleID bosonid, double mass, double width,
        HEJ::RNG & ran
    );
    double gen_V_boson_rapidity(
      HEJ::ParticleID V_boson,
      HEJ::RNG & ran
    );
    double gen_Higgs_rapidity(HEJ::RNG & ran);
    template<class ParticleMomenta>
    fastjet::PseudoJet gen_last_momentum(
        ParticleMomenta const & other_momenta,
        double mass_square, double y
    ) const;

    bool jets_ok(
        std::vector<fastjet::PseudoJet> const & Born_jets,
        std::vector<fastjet::PseudoJet> const & partons
    ) const;
    /**
     * @internal
     * @brief Generate incoming partons according to the PDF
     *
     * @param uf                Scale used in the PDF
     */
    void reconstruct_incoming(
        Process const & proc,
        std::optional<subleading::Channels> channel,
        HEJ::PDF & pdf, double E_beam,
        double uf,
        HEJ::RNG & ran
    );
    std::pair<double, double> xa_xb(double E_beam) const;
    HEJ::ParticleID generate_incoming_id(
        std::size_t beam_idx, double x, double uf, HEJ::PDF & pdf,
        part_mask allowed_partons, HEJ::RNG & ran
    );
    //! @brief Returns list of all allowed initial states partons
    std::array<part_mask,2> allowed_incoming(
        Process const & proc,
        std::optional<subleading::Channels> channel,
        HEJ::RNG & ran
    );

    //! Ensure that incoming state compatible with A/W/Z production
    //! Should not be called for final-state qqbar configurations,
    //! since A/W/Z emission doesn't constrain the initial state there
    std::array<part_mask,2> incoming_AWZ(
        Process const & proc,
        std::array<part_mask,2> allowed_partons,
        HEJ::RNG & ran
    );
    //! Ensure that incoming state compatible with extremal qqbar
    std::array<part_mask,2> incoming_eqqbar(
        std::array<part_mask,2> allowed_partons, HEJ::RNG & ran);
    //! Ensure that incoming state compatible with unordered
    std::array<part_mask,2> incoming_uno(
        std::array<part_mask,2> allowed_partons, HEJ::RNG & ran);

    bool momentum_conserved(double ep) const;

    bool extremal_FKL_ok(
        std::vector<fastjet::PseudoJet> const & partons
    ) const;
    double random_normal(double stddev, HEJ::RNG & ran);
    double random_normal_trunc(
      double stddev,
      HEJ::RNG & ran,
      double min,
      double max
    );
    /**
     * @internal
     * @brief Turns a FKL configuration into a subleading one
     *
     * @param chance            Chance to switch to subleading configuration
     * @param channels          Allowed channels for subleading process
     * @param proc              Process to decide which subleading
     *                          configurations are allowed
     *
     * With a chance of "chance" the FKL configuration is either turned into
     * a unordered configuration or, for A/W/Z bosons, a configuration with
     * a central quark/anti-quark pair.
     */
    void turn_to_subl(
      subleading::Channels channel,
      Process const & proc,
      HEJ::RNG & ran
    );
    void turn_to_uno(bool can_be_uno_backward, bool can_be_uno_forward,
                     HEJ::RNG & ran);
    HEJ::ParticleID select_qqbar_flavour(Process const & proc, HEJ::RNG & ran);
    void turn_to_cqqbar(Process const & proc, HEJ::RNG & ran);
    void turn_to_eqqbar(Process const & proc, HEJ::RNG & ran);
    //! decay where we select the decay channel
    std::vector<HEJ::Particle> decay_channel(
        HEJ::Particle const & parent,
        std::vector<Decay> const & decays,
        HEJ::RNG & ran
    );
    Decay select_decay_channel(
        std::vector<Decay> const & decays,
        HEJ::RNG & ran
    );
    double gen_hard_pt(
        int np, double ptmin, double ptmax, double y,
        HEJ::RNG & ran
    );
    double gen_soft_pt(int np, double max_pt, HEJ::RNG & ran);
    double gen_parton_pt(
        int count, JetParameters const & jet_param, double max_pt, double y,
        HEJ::RNG & ran
    );

    //! Iterator over partons (non-const)
    using PartonIterator = boost::filter_iterator<
      bool (*)(HEJ::Particle const &),
      std::vector<HEJ::Particle>::iterator
      >;
    //! Reverse Iterator over partons (non-const)
    using ReversePartonIterator = std::reverse_iterator<PartonIterator>;

    //! Iterator to the first outgoing parton (non-const)
    PartonIterator begin_partons();
    //! Iterator to the end of the outgoing partons (non-const)
    PartonIterator end_partons();

    //! Reverse Iterator to the first outgoing parton (non-const)
    ReversePartonIterator rbegin_partons();
    //! Reverse Iterator to the first outgoing parton (non-const)
    ReversePartonIterator rend_partons();

    std::optional<subleading::Channels> select_channel(
      Subleading subl_channels,
      double chance,
      HEJ::RNG & ran
    );

    double weight_;

    Status status_;

    std::array<HEJ::Particle, 2> incoming_;
    std::vector<HEJ::Particle> outgoing_;
    //! Particle decays in the format {outgoing index, decay products}
    std::unordered_map<std::size_t, std::vector<HEJ::Particle>> decays_;
  };

  //! Extract HEJ::Event::EventData from PhaseSpacePoint
  HEJ::Event::EventData to_EventData(PhaseSpacePoint psp);
} // namespace HEJFOG

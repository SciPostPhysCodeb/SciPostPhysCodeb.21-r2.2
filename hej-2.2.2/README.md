#            High Energy Jets

High Energy Jets (HEJ) is a Monte Carlo generator for all-order summation of
high-energy logarithms. It can be used as both a C++ library and standalone
executable.

For further information and support please visit

> https://hej.hepforge.org/

The latest version can be downloaded from

> https://hej.hepforge.org/downloads.html

##  Installation

HEJ stable releases can be used without installation using
[Docker](https://www.docker.com/). Manual installation is recommended
mostly for development versions of HEJ or if Docker is not available.

HEJ can be installed via [CMake](https://cmake.org/) version 3.1 or later by
running

```sh
  mkdir build
  cd build
  cmake .. -DCMAKE_INSTALL_PREFIX=target/directory
  make install
```

Replace "target/directory" with the directory where HEJ should be installed.

HEJ depends on multiple external packages, a full list is given in the
user documentation (i.e. https://hej.hepforge.org/doc/current/user/).
The minimal requirements are:

  * A compiler supporting the C++17 standard (e.g. gcc 7 or later)
  * [CLHEP](https://gitlab.cern.ch/CLHEP/CLHEP)
  * [FastJet](http://fastjet.fr/)
  * IOStreams and uBLAS from the [boost libraries](https://boost.org/)
  * [LHAPDF](https://lhapdf.hepforge.org/)
  * [yaml-cpp](https://github.com/jbeder/yaml-cpp)

We also provide a Fixed Order Generator for the HEJ matrix elements as a
separate executable. To install it run the same commands as above in the
[_FixedOrderGen_](FixedOrderGen) directory.

##  Documentation

All documentation is hosted on

> https://hej.hepforge.org/

To generate the user documentation locally run
(requires [sphinx](http://sphinx-doc.org/))

```sh
    cd build
    make sphinx
    xdg-open doc/sphinx/html/index.html
```

The code documentation can be built through [doxygen](http://doxygen.org/);

```sh
    cd build
    make doxygen
    xdg-open doc/doxygen/html/index.html
```

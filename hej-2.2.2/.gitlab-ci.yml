# ---------------------------------
# -        General Setup          -
# ---------------------------------

stages:
  - build
  - test
  - FOG:build
  - FOG:test
  - clean_code
  - FOG:clear_code
  - long_test
  - publish

workflow:
  rules:
    - when: always

variables:
  GIT_SUBMODULE_STRATEGY: recursive
  # build directories
  HEJ_BUILD_DIR: tmp_HEJ/HEJ_build
  HEJ_INSTALL_DIR: tmp_HEJ/HEJ_installed
  FOG_BUILD_DIR: tmp_HEJ/FOG_build
  FOG_INSTALL_DIR: ${HEJ_INSTALL_DIR}
  # docker images
  DOCKER_BASIC: hejdock/hepenv # slc6
  DOCKER_HEJV2ENV: 'hejdock/masterenv:centos7v2'
  DOCKER_CLANG: 'hejdock/hepenv:clang9_ubuntu' # ubuntu with clang
  DOCKER_HEPMC3: hejdock/hepmc3env # centos7
  DOCKER_HIGHFIVE: hejdock/highfiveenv # centos7
  DOCKER_QCDLOOP: 'hejdock/qcdloopenv:centos7v2' # includes rivet
  DOCKER_RIVET_HEPMC2: 'hejdock/rivetenv:hepmc2'
  DOCKER_RIVET: hejdock/rivetenv
  # default name of cmake
  CMAKE: cmake
  CTEST: ctest
  OVERWRITE_BOOST: ""
  HEPSW_INSTALL_SOURCEFILE: '/cvmfs/pheno.egi.eu/HEJ/HEJ_env.sh'

default:
  after_script:
    - date
  tags:
    - docker
  # save complete history of failed tests
  artifacts:
    when: on_failure
    untracked: true
    expire_in: 3d

# ----------- Macros -----------

# default pipeline triggers
.rules_template:
  rules: &rules_def
    - if: $CI_MERGE_REQUEST_ID
      when: on_success
    - if: $CI_COMMIT_TAG
      when: on_success
    - if: '$CI_COMMIT_BRANCH != null && $CI_COMMIT_BRANCH == "master"'
      when: on_success
    - if: '$CI_COMMIT_BRANCH != null && $CI_COMMIT_BRANCH =~ /^v\d+\.\d+$/'
      when: on_success
    - when: manual # non-blocking manual
      allow_failure: true

# ---------------------------------
# -      Script Templates         -
# ---------------------------------

# ----------- Build -----------

.HEJ_build:
  rules: *rules_def
  stage: build
  before_script:
    - date
    - source ${HEPSW_INSTALL_SOURCEFILE} || exit 1
    # prepare build
    - t_HEJ_DIR=${PWD}
    - t_HEJ_INSTALL_DIR=${PWD}/${HEJ_INSTALL_DIR}
    - t_HEJ_BUILD_DIR=${PWD}/${HEJ_BUILD_DIR}
    # hack to allow setting "variable in variable"
    - t_CMAKE_FLAGS=""
    - if [[ -n ${OVERWRITE_BOOST} ]]; then
        declare -n t_boost_root=${OVERWRITE_BOOST};
        t_CMAKE_FLAGS="-DBOOST_ROOT=${t_boost_root}";
      fi
    - echo ${t_CMAKE_FLAGS}
    - mkdir -p ${t_HEJ_BUILD_DIR}
    - cd ${t_HEJ_BUILD_DIR}
    - ${CMAKE} ${t_HEJ_DIR} -DCMAKE_BUILD_TYPE=DEBUG
        -DCMAKE_INSTALL_PREFIX=${t_HEJ_INSTALL_DIR} ${t_CMAKE_FLAGS}
  script:
    - make -j $(nproc --ignore=1)
    - make install
  needs: [] # can run immediately
  artifacts:
    # save build and installed folder
    name: build
    expire_in: 1d
    paths:
      - ${HEJ_INSTALL_DIR}
      - ${HEJ_BUILD_DIR}

# ----------- Test -----------

.HEJ_test:
  rules: *rules_def
  stage: test
  before_script:
    - date
    - source ${HEPSW_INSTALL_SOURCEFILE} || exit 1
    # load HEJ
    - t_HEJ_DIR=${PWD}
    - t_HEJ_INSTALL_DIR=${PWD}/${HEJ_INSTALL_DIR}
    - export LD_LIBRARY_PATH=${t_HEJ_INSTALL_DIR}/lib:${LD_LIBRARY_PATH}
    - export PATH=${t_HEJ_INSTALL_DIR}/bin:${PATH}
    - cd ${HEJ_BUILD_DIR}
    - ${CMAKE} ${t_HEJ_DIR} # rerun cmake to create all test configure files
  script:
    - ${CTEST} --output-on-failure -j $(nproc --ignore=1)

.HEJrivet_test:
  extends: .HEJ_test
  script:
    - ${CTEST} --output-on-failure -j $(nproc --ignore=1)
    - bash -c '[ -f t/tst.yoda ]' && echo "found rivet output"
    - rivet-cmphistos t/tst.yoda
    - bash -c '[ -f MC_XS_XS.dat ]' && echo "yoda not empty"

## ----------- FOG build -----------

.FOG_build:
  rules: *rules_def
  stage: FOG:build
  before_script:
    - date
    - source ${HEPSW_INSTALL_SOURCEFILE} || exit 1
    # load HEJ
    - t_HEJ_INSTALL_DIR=${PWD}/${HEJ_INSTALL_DIR}
    - export LD_LIBRARY_PATH=${t_HEJ_INSTALL_DIR}/lib:${LD_LIBRARY_PATH}
    - export PATH=${t_HEJ_INSTALL_DIR}/bin:${PATH}
    # prepare build
    - t_FOG_DIR=${PWD}/FixedOrderGen
    - t_FOG_INSTALL_DIR=${PWD}/${FOG_INSTALL_DIR}
    - t_FOG_BUILD_DIR=${PWD}/${FOG_BUILD_DIR}
    # hack to allow setting "variable in variable"
    - t_CMAKE_FLAGS=""
    - if [[ -n ${OVERWRITE_BOOST} ]]; then
        declare -n t_boost_root=${OVERWRITE_BOOST};
        t_CMAKE_FLAGS="-DBOOST_ROOT=${t_boost_root}";
      fi
    - mkdir -p ${t_FOG_BUILD_DIR}
    - cd ${t_FOG_BUILD_DIR}
    - ${CMAKE} ${t_FOG_DIR} -DCMAKE_BUILD_TYPE=DEBUG
        -DCMAKE_INSTALL_PREFIX=${t_FOG_INSTALL_DIR} ${t_CMAKE_FLAGS}
  script:
    - make -j $(nproc --ignore=1)
    - make install
  artifacts:
    # save build and installed folder
    name: FOG_build
    expire_in: 1d
    paths:
      - ${HEJ_INSTALL_DIR}
      - ${FOG_INSTALL_DIR}
      - ${FOG_BUILD_DIR}

## ----------- FOG test -----------

.FOG_test:
  rules: *rules_def
  stage: FOG:test
  before_script:
    - date
    - source ${HEPSW_INSTALL_SOURCEFILE} || exit 1
    # load HEJ
    - t_FOG_DIR=${PWD}/FixedOrderGen
    - t_HEJ_INSTALL_DIR=${PWD}/${HEJ_INSTALL_DIR}
    - t_FOG_INSTALL_DIR=${PWD}/${FOG_INSTALL_DIR}
    - export LD_LIBRARY_PATH=${t_HEJ_INSTALL_DIR}/lib:${LD_LIBRARY_PATH}
    - export PATH=${t_HEJ_INSTALL_DIR}/bin:${t_FOG_INSTALL_DIR}/bin:${PATH}
    - t_FOG_BUILD_DIR=${PWD}/${FOG_BUILD_DIR}
    - cd ${t_FOG_BUILD_DIR}
    - ${CMAKE} ${t_FOG_DIR} # rerun cmake to create all test configure files
  script:
    - ${CTEST} --output-on-failure -j $(nproc --ignore=1)

# ---------------------------------
# -         Build & Test          -
# ---------------------------------

# ----------- basic (always run) -----------

build:basic:
  image: ${DOCKER_BASIC}
  extends: .HEJ_build
  rules:
    - when: on_success

test:basic:
  image: ${DOCKER_BASIC}
  extends: .HEJ_test
  needs:
    - job: build:basic
      artifacts: true
  rules:
    - when: on_success

FOG:build:basic:
  image: ${DOCKER_BASIC}
  extends: .FOG_build
  needs:
    - job: build:basic
      artifacts: true
  rules:
    - when: on_success

FOG:test:basic:
  image: ${DOCKER_BASIC}
  extends: .FOG_test
  needs:
    - job: FOG:build:basic
      artifacts: true
  rules:
    - when: on_success

# ----------- HighFive/hdf5 -----------

build:HighFive:
  image: ${DOCKER_HIGHFIVE}
  extends: .HEJ_build
  variables:
    CMAKE: cmake3

test:HighFive:
  image: ${DOCKER_HIGHFIVE}
  extends: .HEJ_test
  variables:
    CMAKE: cmake3
    CTEST: ctest3
  needs:
    - job: build:HighFive
      artifacts: true

# ----------- QCDloop -----------

build:qcdloop:
  image: ${DOCKER_QCDLOOP}
  extends: .HEJ_build
  variables:
    HEPSW_INSTALL_SOURCEFILE: '/cvmfs/pheno.egi.eu/HEJV2/HEJ_env.sh'

test:qcdloop:
  image: ${DOCKER_QCDLOOP}
  extends: .HEJ_test
  variables:
    HEPSW_INSTALL_SOURCEFILE: '/cvmfs/pheno.egi.eu/HEJV2/HEJ_env.sh'
  needs:
    - job: build:qcdloop
      artifacts: true

# ----------- HepMC 3 -----------
# HepMC 3 already in rivet3

build:hepmc3:
  image: ${DOCKER_HEPMC3}
  extends: .HEJ_build
  variables:
    CMAKE: cmake3

test:hepmc3:
  image: ${DOCKER_HEPMC3}
  extends: .HEJ_test
  variables:
    CMAKE: cmake3
    CTEST: ctest3
  needs:
    - job: build:hepmc3
      artifacts: true

# ----------- rivet -----------

build:rivet:
  image: ${DOCKER_RIVET}
  extends: .HEJ_build
  variables:
    HEPSW_INSTALL_SOURCEFILE: '/cvmfs/pheno.egi.eu/HEJV2/HEJ_env.sh'

test:rivet:
  image: ${DOCKER_RIVET}
  extends: .HEJrivet_test
  variables:
    HEPSW_INSTALL_SOURCEFILE: '/cvmfs/pheno.egi.eu/HEJV2/HEJ_env.sh'
  needs:
    - job: build:rivet
      artifacts: true

# ----------- rivet3 & HepMC2 -----------
# This shouldn't change too often

build:rivet3h2:
  image: ${DOCKER_RIVET_HEPMC2}
  variables:
    HEPSW_INSTALL_SOURCEFILE: '/cvmfs/pheno.egi.eu/HEJV2/HEJ_env.sh'
  extends: .HEJ_build

test:rivet3h2:
  image: ${DOCKER_RIVET_HEPMC2}
  extends: .HEJrivet_test
  variables:
    HEPSW_INSTALL_SOURCEFILE: '/cvmfs/pheno.egi.eu/HEJV2/HEJ_env.sh'
  needs:
    - job: build:rivet3h2
      artifacts: true

# ----------- HEJV2 Environment -----------
build:hejv2:
  image: ${DOCKER_HEJV2ENV}
  extends: .HEJ_build
  variables:
    HEPSW_INSTALL_SOURCEFILE: '/cvmfs/pheno.egi.eu/HEJV2/HEJ_env.sh'

test:hejv2:
  image: ${DOCKER_HEJV2ENV}
  extends: .HEJ_test
  variables:
    HEPSW_INSTALL_SOURCEFILE: '/cvmfs/pheno.egi.eu/HEJV2/HEJ_env.sh'
  needs:
    - job: build:hejv2
      artifacts: true

# ---------------------------------
# -          Clean Code           -
# ---------------------------------

No_tabs:
  stage: clean_code
  rules: *rules_def
  image: hejdock/git
  needs: [] # can run immediately
  script:
    - date
    - check_tabs
  artifacts: # don't save anything

# ----------- Template no compiler warnings -----------

.Warning_build:
  extends: .HEJ_build
  stage: clean_code
  script:
    - ${CMAKE} ${t_HEJ_DIR} -DCMAKE_CXX_FLAGS="-Werror"
        -DCMAKE_BUILD_TYPE=RelWithDebInfo
    - make -j $(nproc --ignore=1)
    - make -j $(nproc --ignore=1) install
  artifacts: # don't save anything
  needs: [] # can run immediately

.Warning_FOG:
  extends: .FOG_build
  stage: FOG:clear_code
  script:
    - ${CMAKE} ${t_FOG_DIR} -DCMAKE_CXX_FLAGS="-Werror" ${t_CMAKE_FLAGS}
        -DCMAKE_BUILD_TYPE=RelWithDebInfo
    - make -j $(nproc --ignore=1)
    - make -j $(nproc --ignore=1) install
  artifacts: # don't save anything

# ----------- No gcc warnings -----------

No_Warning:basic:
  image: ${DOCKER_BASIC}
  extends: .Warning_build
  artifacts:
    # save build and installed folder (needed for Long_test:FOG)
    name: build
    expire_in: 1d
    paths:
      - ${HEJ_INSTALL_DIR}
      - ${HEJ_BUILD_DIR}

No_Warning:basic:FOG:
  image: ${DOCKER_BASIC}
  extends: .Warning_FOG
  needs:
    - job: build:basic
      artifacts: true

# ----------- No clang warnings -----------

No_Warning:clang:
  image: ${DOCKER_CLANG}
  extends: .Warning_build
  variables:
    OVERWRITE_BOOST: boost_ROOT_DIR
  artifacts:
    # save installed folder
    name: build
    expire_in: 1d
    paths:
      - ${HEJ_INSTALL_DIR}

No_Warning:clang:FOG:
  image: ${DOCKER_CLANG}
  extends: .Warning_FOG
  needs:
    - job: No_Warning:clang
      artifacts: true
  variables:
    OVERWRITE_BOOST: boost_ROOT_DIR

# ---------------------------------
# -          Long tests           -
# ---------------------------------

.HEJ_test_long:
  extends: .HEJ_build
  stage: long_test
  script:
    - ${CMAKE} ${t_HEJ_DIR} -DCMAKE_BUILD_TYPE=RelWithDebInfo ${t_CMAKE_FLAGS}
        -DTEST_ALL=TRUE
    - make -j $(nproc --ignore=1) install
    - ${CTEST} --output-on-failure -j $(nproc --ignore=1)
  needs: [] # can run immediately

.FOG_test_long:
  extends: .FOG_build
  stage: long_test
  script:
    - ${CMAKE} ${t_FOG_DIR} -DCMAKE_BUILD_TYPE=RelWithDebInfo ${t_CMAKE_FLAGS}
        -DTEST_ALL=TRUE
    - make -j $(nproc --ignore=1) install
    - ${CTEST} --output-on-failure -j $(nproc --ignore=1)

# ----------- QCDloop -----------

Long_test:qcdloop:
  image: ${DOCKER_QCDLOOP}
  extends: .HEJ_test_long
  variables:
    HEPSW_INSTALL_SOURCEFILE: '/cvmfs/pheno.egi.eu/HEJV2/HEJ_env.sh'

# ----------- Basic -----------

Long_test:FOG:
  image: ${DOCKER_BASIC}
  extends: .FOG_test_long
  needs:
    - job: No_Warning:basic
      artifacts: true

# ---------------------------------
# -            Publish            -
# ---------------------------------

Publish_version:
  stage: publish
  image: hejdock/git
  dependencies: []
  artifacts: # don't save anything
  rules:
    - if: '$CI_COMMIT_BRANCH != null && $CI_COMMIT_BRANCH =~ /^v\d+\.\d+$/'
      when: on_success
  before_script:
    - mkdir -p .ssh/ && echo "${SSH_KEY}" > .ssh/id_rsa && chmod 0600 .ssh/id_rsa
    - rm -rf ~/.ssh/id_rsa; mkdir -p ~/.ssh/
    - ln -s $PWD/.ssh/id_rsa ~/.ssh/id_rsa && chmod 0600 ~/.ssh/id_rsa
    - ssh -T ${PUBLIC_GIT_PREFIX} -o "StrictHostKeyChecking no" || echo "added ssh"
  script:
    - git remote add public ${PUBLIC_GIT_PREFIX}${PUBLIC_GIT_POSTFIX}
    - git checkout $CI_COMMIT_REF_NAME
    - git branch
    - git pull
    - git push public
    - git push public --tags
  after_script:
    - rm -f /root/.ssh/id_rsa && rm -fr .ssh
    - git remote rm public

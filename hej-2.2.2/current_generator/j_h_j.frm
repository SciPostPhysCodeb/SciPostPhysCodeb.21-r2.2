*/**
*  \brief Contraction of two FKL currents with central Higgs boson emission
*
*  \authors   The HEJ collaboration (see AUTHORS for details)
*  \date      2020
*  \copyright GPLv2 or later
*
*  The t-channel momenta before and after the Higgs boson emission
*  are decomposed into
*  q1 = q11 - q12
*  q2 = q21 - q22
*  such that the momenta on the right-hand side are lightlike and
*  have positive energy.
*/
#include- include/helspin.frm
#include- include/write.frm

v pb,p2,pa,p1,q1,q2,q11,q12,q21,q22;
i mu,nu;

#do h1={+,-}
   #do h2={+,-}
      l [j_h_j `h1'`h2'] = (
         Current(`h1'1, pa, mu, p1)
         *VH(mu, nu, q1, q2)
         *Current(`h2'1, p2, nu, pb)
      );
   #enddo
#enddo

multiply replace_(
   q1, q11 - q12,
   q2, q21 - q22
);

#call ContractCurrents
.sort
format O4;
format c;
#call WriteHeader(`OUTPUT')
#call WriteOptimised(`OUTPUT',j_h_j,2,pa,p1,pb,p2,q11,q12,q21,q22,COMPLEX:,T1,T2)
#call WriteFooter(`OUTPUT')
.end

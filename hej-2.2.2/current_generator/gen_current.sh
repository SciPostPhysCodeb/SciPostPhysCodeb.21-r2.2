#!/bin/sh

FORM=$1
INPUT=$2
OUTPUT=$3
LOG=$4

mkdir -p `dirname $OUTPUT` || exit 1
$FORM -d OUTPUT=$OUTPUT $INPUT 1> $LOG || exit 1
# fix syntax errors in generated code
perl -i -wpE 's@\b(\d+)(\*|/)@$1\.$2@g' $OUTPUT || exit 1

*/**
*  \brief Contraction of W current with central qqbar emission and FKL current
*
*  \authors   The HEJ collaboration (see AUTHORS for details)
*  \date      2020
*  \copyright GPLv2 or later
*/
#include- include/helspin.frm
#include- include/write.frm

cf Xsym,V3g;
i mu,nu,sigma;
s h,sqqbar,saq,saqbar,s1q,s1qbar,sbq,sbqbar,s4q,s4qbar;
v p,pa,p1,pb,pn,pq,pqbar,pl,plbar,q1,q3,q11,q12;

* cf. eq:Xcen in developer manual
#do h1={+,-}
   #do h2={+,-}
*     eq:Xcrossed in developer manual, normalisation added later on
      l [M_cross `h1'`h2'] = -Current(`h1'1, p1, mu, pa)*Current(
         `h2'1, pq, nu, q1 - pqbar, mu, pqbar
      );
*     eq:Xqprop in developer manual, normalisation added later on
      l [M_qbar `h1'`h2'] = Current(`h1'1, p1, mu, pa)*Current(
         `h2'1, pq, mu, q1 - pq, nu, pqbar
      );
      l [M_sym `h1'`h2'] = -i_*Current(`h1'1, p1, mu, pa)*(
         d_(mu,nu)*Xsym(sigma) + V3g(mu, nu, sigma)
      )/sqqbar*Current(`h2'1, pq, sigma, pqbar);
   #enddo
#enddo
multiply JV(-1, -1, nu, pb, pn, plbar, pl);

id Current(h?, pq?, mu?, p?, nu?, pqbar?) = Current(h, pq, mu, p, nu, pqbar)/m2(p);

* eq:Xsym in developer manual
id Xsym(sigma?) = m2(q1)*(
   pa(sigma)/(saq + saqbar) + p1(sigma)/(s1q + s1qbar)
) - m2(q3)*(
   pb(sigma)/(sbq + sbqbar) + pn(sigma)/(s4q + s4qbar)
);
* eq:V3g in developer manual
id V3g(mu?, nu?, sigma?) = (
   (q1(nu) + pq(nu) + pqbar(nu))*d_(mu, sigma)
   + (q3(mu) - pq(mu) - pqbar(mu))*d_(nu, sigma)
   - (q1(sigma) + q3(sigma))*d_(mu, nu)
);

multiply replace_(q3, q1-pq-pqbar);
* replace q1 by sum of lightlike momenta
multiply replace_(q1, q11-q12);
.sort
#call ContractCurrents()
multiply replace_(
   sqqbar, m2(pq+pqbar),
   saq, m2(pa+pq),
   saqbar, m2(pa+pqbar),
   s1q, m2(p1+pq),
   s1qbar, m2(p1+pqbar),
   sbq, m2(pb+pq),
   sbqbar, m2(pb+pqbar),
   s4q, m2(pn+pq),
   s4qbar, m2(pn+pqbar)
);
.sort
format O4;
format c;
#call WriteHeader(`OUTPUT')
#call WriteOptimised(`OUTPUT',M_qbar,2,pa,p1,pb,pn,pq,pqbar,pl,plbar,q11,q12)
#call WriteOptimised(`OUTPUT',M_cross,2,pa,p1,pb,pn,pq,pqbar,pl,plbar,q11,q12)
#call WriteOptimised(`OUTPUT',M_sym,2,pa,p1,pb,pn,pq,pqbar,pl,plbar,q11,q12)
#call WriteFooter(`OUTPUT')
.end

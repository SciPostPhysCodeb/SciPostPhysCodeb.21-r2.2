*/**
*  \brief Contraction of vector boson currents
*
*  \authors   The HEJ collaboration (see AUTHORS for details)
*  \date      2020
*  \copyright GPLv2 or later
*/
#include- include/helspin.frm
#include- include/write.frm

v pb,p2,pa,p1,pl1,pl1bar,pl2,pl2bar;
i mu;

#do h1={+,-}
   #do hl1={+,-}
      #do h2={+,-}
        #do hl2={+,-}
            l [jV_jV `h1'`hl1'`h2'`hl2'] = JV(`h1'1, `hl1'1, mu, pa, p1, pl1bar, pl1)
                                         * JV(`h2'1, `hl2'1, mu, pb, p2, pl2bar, pl2);
        #enddo
      #enddo
   #enddo
#enddo

#call ContractCurrents
.sort
format O4;
format c;
#call WriteHeader(`OUTPUT')
#call WriteOptimised(`OUTPUT',jV_jV,4,pa,p1,pb,p2,pl1,pl1bar,pl2,pl2bar)
#call WriteFooter(`OUTPUT')
.end

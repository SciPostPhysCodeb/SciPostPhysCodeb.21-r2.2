*/**
*  \brief Contraction of unordered current with g -> h current
*
*  \authors   The HEJ collaboration (see AUTHORS for details)
*  \date      2020
*  \copyright GPLv2 or later
*/
#include- include/helspin.frm
#include- include/write.frm

v pb,pa,p1,pg,q2,q3,p,pr,pr2,pH,pH1,pH2;
i mu,nu,sigma;
s h,foo,bar;

#do h1={+,-}
*  eq:juno in developer manual with p1 -> pg, p2 -> p1
   l [U1_jgh `h1'] = (
      + Current(`h1'1, p1, nu, pg)*Current(`h1'1, pg, mu, pa)
      + 2*p1(nu)*Current(`h1'1, p1, mu, pa)
   )/m2(p1 + pg);
   l [U2_jgh `h1'] = (
      + 2*Current(`h1'1, p1, mu, pa)*pa(nu)
      - Current(`h1'1, p1, mu, pg)*Current(`h1'1, pg, nu, pa)
   )/m2(pa - pg);
   l [L_jgh `h1'] = (
      - (q2(nu) + q3(nu))*Current(`h1'1, p1, mu, pa)
      - 2*pg(mu)*Current(`h1'1, p1, nu, pa)
      + 2*Current(`h1'1, p1, pg, pa)*d_(mu, nu)
      + 2*m2(q2)*Current(`h1'1, p1, mu, pa)*pb(nu)/m2(pb+pg)
   )/m2(q3);
#enddo
multiply replace_(
   q2,p1+pg-pa,
   q3,p1-pa
);
.sort
drop;

* multiply with polarisation vector and other current
#do h1={+,-}
   #do h2={+,-}
      #do hg={+,-}
         #do TENSOR={U1_jgh,U2_jgh,L_jgh}
            l [`TENSOR' `h1'`h2'`hg'] = (
               [`TENSOR' `h1']
               *Eps(`hg'1, nu, pg, pr)
               *VH(sigma, mu, pb, pb-pH)
               *Eps(`h2'1, sigma, pb, pr2)
            );
         #enddo
      #enddo
   #enddo
#enddo

* choice of auxiliary vectors
multiply replace_(
   pr,p1,
   pr2,p1
);

multiply replace_(pH, pH1+pH2);
.sort
#call ContractCurrents
id sqrt(2)*sqrt(2) = 2;
id pa?.pb? = dot(pa, pb);
.sort
format O4;
format c;
#call WriteHeader(`OUTPUT')
#do UNOTENSOR={U1,U2,L}
   #call WriteOptimised(`OUTPUT',`UNOTENSOR'_jgh,3,pg,p1,pa,pH1,pH2,pb,COMPLEX:,T1,T2)
#enddo
#call WriteFooter(`OUTPUT')
.end

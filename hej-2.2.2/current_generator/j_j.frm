*/**
*  \brief Contraction of two FKL currents
*
*  \authors   The HEJ collaboration (see AUTHORS for details)
*  \date      2020
*  \copyright GPLv2 or later
*/
#include- include/helspin.frm
#include- include/write.frm

v pb,p2,pa,p1;
i mu;

#do h1={+,-}
   #do h2={+,-}
      l [j_j `h1'`h2'] = Current(`h1'1, pa, mu, p1)*Current(`h2'1, p2, mu, pb);
   #enddo
#enddo

#call ContractCurrents
.sort
format O4;
format c;
#call WriteHeader(`OUTPUT')
#call WriteOptimised(`OUTPUT',j_j,2,pa,p1,pb,p2)
#call WriteFooter(`OUTPUT')
.end

*/**
*  \brief Contraction of vector boson current with extremal qqbar current
*
*  \authors   The HEJ collaboration (see AUTHORS for details)
*  \date      2020
*  \copyright GPLv2 or later
*/
#include- include/helspin.frm
#include- include/write.frm

* Obtained from jV_juno.frm using crossing symmetry (see eq:crossing in developer manual)

v pqbar,pq,pa,p1,pb,pl,plbar,q2,q3,p,pr;
i mu,nu,sigma;
s h;

#do h2={+,-}
   l [U1X_jV `h2'] = (
      + i_*Current(`h2'1, pq, nu, pb)*i_*i_*Current(`h2'1, pb, mu, pqbar)
      + 2*pq(nu)*i_*Current(`h2'1, pq, mu, pqbar)
   )/m2(pq - pb);
   l [U2X_jV `h2'] = (
      - 2*i_*Current(`h2'1, pq, mu, pqbar)*pqbar(nu)
      - i_*Current(`h2'1, pq, mu, pb)*i_*i_*Current(`h2'1, pb, nu, pqbar)
   )/m2(-pqbar + pb);
   l [LX_jV `h2'] = (
      - (q2(nu) + q3(nu))*i_*Current(`h2'1, pq, mu, pqbar)
      + 2*pb(mu)*i_*Current(`h2'1, pq, nu, pqbar)
      - 2*i_*Current(`h2'1, pq, pb, pqbar)*d_(mu, nu)
      + m2(q2)*i_*Current(`h2'1, pq, mu, pqbar)*(p1(nu)/m2(p1-pb) + pa(nu)/m2(pa-pb))
   )/m2(q3);
#enddo
.sort
drop;

* multiply with polarisation vector and other currents
#do h1={+,-}
   #do hl={+,-}
      #do h2={+,-}
         #do hg={+,-}
            #do TENSOR={U1X_jV,U2X_jV,LX_jV}
               l [`TENSOR' `h1'`hl'`h2'`hg'] = (
                  [`TENSOR' `h2']
                  *JV(`h1'1, `hl'1, mu, pa, p1, plbar, pl)
                  *Eps(`hg'1, nu, pr)
               );
            #enddo
         #enddo
      #enddo
   #enddo
#enddo

* choice of auxiliary vector
id Eps(h?, mu?, pr)*Current(h?, ?a) = Eps(h, mu, pb, pq)*Current(h, ?a);
also Eps(h?, mu?, pr) = Eps(h, mu, pb, pqbar);

multiply replace_(
   q2,pq-pb+pqbar,
   q3,pq+pqbar
);
.sort
#call ContractCurrents
.sort
format O4;
format c;
#call WriteHeader(`OUTPUT')
#call WriteOptimised(`OUTPUT',U1X_jV,4,pa,pb,p1,pq,pqbar,pl,plbar)
#call WriteOptimised(`OUTPUT',U2X_jV,4,pa,pb,p1,pq,pqbar,pl,plbar)
#call WriteOptimised(`OUTPUT',LX_jV,4,pa,pb,p1,pq,pqbar,pl,plbar)
#call WriteFooter(`OUTPUT')
.end

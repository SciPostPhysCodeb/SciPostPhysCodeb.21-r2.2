*/**
*  \brief Contraction of W + extremal qqbar current with FKL current
*
*  TODO: unify conventions with developer manual
*  the current dictionary is as follows:
*
*  code  | manual
*  pq    | p_2
*  pqbar | p_1
*  pg    | p_a
*
*  \authors   The HEJ collaboration (see AUTHORS for details)
*  \date      2020
*  \copyright GPLv2 or later
*/
#include- include/helspin.frm
#include- include/write.frm

s h,sgb,sgn,sqqbarW;
v pq,pqbar,pg,pl,plbar,pn,pb,pW,pr,q1,q1g,q2;
i mu,nu,rho,sigma;
cf m2inv;

#do h1={+,-}
*  eq:U1tensorX in developer manual, up to factors 1/sij, 1/tij
   l [U1X `h1'] = (
      + Current(`h1'1, pq, nu, pg-pq, mu, pqbar+pW, rho, pqbar)
      + Current(`h1'1, pq, nu, pg-pq, rho, pg-pq-pW, mu, pqbar)
      - Current(`h1'1, pq, rho, pq+pW, nu, pg-pq-pW, mu, pqbar)
   );

*  eq:U2tensorX in developer manual, up to factors 1/sij, 1/tij
   l [U2X `h1'] = (
      - Current(`h1'1, pq, mu, pg-pqbar-pW, nu, pqbar+pW, rho, pqbar)
      + Current(`h1'1, pq, mu, pg-pqbar-pW, rho, pg-pqbar, nu, pqbar)
      + Current(`h1'1, pq, rho, pq+pW, mu, pg-pqbar, nu, pqbar)
   );

*  eq:LtensorX in developer manual, up to factors 1/sij, 1/tij
   l [LX `h1'] = (
      - Current(`h1'1, pq, sigma, pqbar+pW, rho, pqbar) +
      + Current(`h1'1, pq, rho, pq+pW, sigma, pqbar)
   )*(
      + (-(pb(nu)/sgb + pn(nu)/sgn)*m2(q1g) + 2*q1(nu) - pg(nu))*d_(mu, sigma)
      - (2*pg(sigma) + q1(sigma))*d_(mu, nu)
      + 2*pg(mu)*d_(nu, sigma)
   )/sqqbarW;
#enddo
.sort
* restore kinematic factors
id Current(h?, pq, mu?, q1?, nu?, q2?, rho?, pqbar) = (
   Current(h, pq, mu, q1, nu, q2, rho, pqbar)*m2inv(q1)*m2inv(q2)
);
id Current(h?, pq, mu?, q1?, nu?, pqbar) = (
   Current(h, pq, mu, q1, nu, pqbar)*m2inv(q1)
);
.sort
drop;

* multiply with polarisation vector and other currents
#do h1={+,-}
   #do h2={+,-}
      #do hg={+,-}
         #do TENSOR={U1X,U2X,LX}
            l [`TENSOR' `h1'`h2'`hg'] = (
               [`TENSOR' `h1']
               *Eps(`hg'1, nu)
               *Current(`h2'1, pn, mu, pb)
               *Current(-1, pl, rho, plbar)
            );
         #enddo
      #enddo
   #enddo
#enddo

* choice of reference vector (pn or pb)
* this is not exactly the optimal choice for all helicities,
* but it's pretty close and the logic is simple
id Eps(h?, nu?)*Current(h?, pn, mu?, pb) = Eps(h, nu, pg, pn)*Current(h, pn, mu, pb);
also Eps(h?, nu?) = Eps(h, nu, pg, pb);

multiply replace_(q1g,q1+pg);
multiply replace_(q1,-(pq+pqbar+pW));
multiply replace_(pW,pl+plbar);
.sort
#call ContractCurrents
multiply replace_(
   sgn,m2(pg+pn),
   sgb,m2(pg+pb),
   sqqbarW,m2(pq+pqbar+pW)
);
id m2inv(q1?) = 1/m2(q1);
multiply replace_(pW,pl+plbar);
.sort
format O4;
format c;
#call WriteHeader(`OUTPUT')
#call WriteOptimised(`OUTPUT',U1X,3,pg,pq,plbar,pl,pqbar,pn,pb)
#call WriteOptimised(`OUTPUT',U2X,3,pg,pq,plbar,pl,pqbar,pn,pb)
#call WriteOptimised(`OUTPUT',LX,3,pg,pq,plbar,pl,pqbar,pn,pb)
#call WriteFooter(`OUTPUT')
.end

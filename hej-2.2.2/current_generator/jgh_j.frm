*/**
*  \brief Contraction of g -> H current with FKL current
*
*  \authors   The HEJ collaboration (see AUTHORS for details)
*  \date      2020
*  \copyright GPLv2 or later
*
*  The Higgs boson momentum is decomposed as pH = pH1 + pH2
*  such that pH1, pH2 are both lightlike with positive energy
*
*  See eq:S_gf_Hf in developer manual.
*/
#include- include/helspin.frm
#include- include/write.frm

v pa,pb,pn,pH,pH1,pH2,pr;
i mu,nu;

#do h1={+,-}
   #do h2={+,-}
      l [jgh_j `h1'`h2'] = (
         Eps(`h1'1, mu, pa, pr)
         *VH(mu, nu, pa, pa-pH)
         *Current(`h2'1, pn, nu, pb)
      );
   #enddo
#enddo

multiply replace_(
   pH, pH1 + pH2,
   pr, pb
);

#call ContractCurrents
.sort
format O4;
format c;
#call WriteHeader(`OUTPUT')
#call WriteOptimised(`OUTPUT',jgh_j,2,pa,pb,pn,pH1,pH2,COMPLEX:,T1,T2)
#call WriteFooter(`OUTPUT')
.end

*/**
*  \brief Contraction of vector boson unordered current with FKL current
*
*  TODO: unify conventions with developer manual
*  the current dictionary is as follows:
*
*  code  | manual
*  pg    | p_1
*  p1    | p_2
*  pa    | p_a
*
*  \authors   The HEJ collaboration (see AUTHORS for details)
*  \date      2020
*  \copyright GPLv2 or later
*/
#include- include/helspin.frm
#include- include/write.frm

s h,s2g,sbg,taV1;
v p,p1,p2,pa,pb,pg,pl,plbar,pV,pr,q1,q1g,q2;
i mu,nu,rho,sigma;
cf m2inv;

#do h1={+,-}
*  eq:U1tensor in developer manual, up to factors 1/sij, 1/tij
   l [U1 `h1'] = (
      + Current(`h1'1, p1, nu, p1+pg, mu, pa-pV, rho, pa)
      + Current(`h1'1, p1, nu, p1+pg, rho, p1+pg+pV, mu, pa)
      + Current(`h1'1, p1, rho, p1+pV, nu, p1+pg+pV, mu, pa)
   );

*  eq:U2tensor in developer manual, up to factors 1/sij, 1/tij
   l [U2 `h1'] = (
      + Current(`h1'1, p1, mu, pa - pV - pg, nu, pa - pV, rho, pa)
      + Current(`h1'1, p1, mu, pa - pV - pg, rho, pa - pg, nu, pa)
      + Current(`h1'1, p1, rho, p1 + pV, mu, pa - pg, nu, pa)
   );

*  eq:Ltensor in developer manual, up to factors 1/sij, 1/tij
   l [L `h1'] = (
      Current(`h1'1, p1, sigma, pa - pV, rho, pa) +
      Current(`h1'1, p1, rho, p1 + pV, sigma, pa)
   )*(
      ((pb(nu)/sbg + p2(nu)/s2g)*m2(q1g) + 2*q1(nu) - pg(nu))*d_(mu, sigma)
      - 2*pg(mu)*d_(nu, sigma)
      + (2*pg(sigma) - q1(sigma))*d_(mu, nu)
   )/taV1;
#enddo
.sort
* restore kinematic factors
id Current(h?, p1, mu?, q1?, nu?, q2?, rho?, pa) = (
   Current(h, p1, mu, q1, nu, q2, rho, pa)*m2inv(q1)*m2inv(q2)
);
id Current(h?, p1, mu?, q1?, nu?, pa) = (
   Current(h, p1, mu, q1, nu, pa)*m2inv(q1)
);
.sort
drop;

* multiply with polarisation vector and other currents
#do h1={+,-}
   #do hl={+,-}
      #do h2={+,-}
         #do hg={+,-}
            #do TENSOR={U1,U2,L}
               l [`TENSOR' `h1'`hl'`h2'`hg'] = (
                  [`TENSOR' `h1']
                  *Eps(`hg'1, nu)
                  *Current(`h2'1, p2, mu, pb)
                  *Current(`hl'1, pl, rho, plbar)
               );
            #enddo
         #enddo
      #enddo
   #enddo
#enddo

* choice of best reference vector (p2 or pb)
id Eps(h?, nu?)*Current(h?, p2, mu?, pb) = Eps(h, nu, pg, p2)*Current(h, p2, mu, pb);
also Eps(h?, mu?) = Eps(h, mu, pg, pb);

multiply replace_(q1g,q1-pg);
multiply replace_(q1,pa-p1-pV);
multiply replace_(pV,pl+plbar);

.sort
#call ContractCurrents
multiply replace_(
   s2g,m2(p2+pg),
   sbg,m2(pb+pg),
   taV1,m2(pa-pl-plbar-p1)
);
id m2inv(q1?) = 1/m2(q1);
multiply replace_(pV,pl+plbar);
.sort
format O4;
format c;
#call WriteHeader(`OUTPUT')
#call WriteOptimised(`OUTPUT',U1,4,p1,p2,pa,pb,pg,pl,plbar)
#call WriteOptimised(`OUTPUT',U2,4,p1,p2,pa,pb,pg,pl,plbar)
#call WriteOptimised(`OUTPUT',L,4,p1,p2,pa,pb,pg,pl,plbar)
#call WriteFooter(`OUTPUT')
.end

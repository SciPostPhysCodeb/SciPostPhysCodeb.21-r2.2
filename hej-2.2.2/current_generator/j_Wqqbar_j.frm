*/**
*  \brief Contraction of FKL current with central qqbar+W emission and FKL current
*
*  TODO: unify conventions with developer manual
*  the current dictionary is as follows:
*
*  code  | manual
*  pq    | p_2
*  pqbar | p_3
*  pl    | p_A
*  plbar | p_B
*
*  \authors   The HEJ collaboration (see AUTHORS for details)
*  \date      2020
*  \copyright GPLv2 or later
*/
#include- include/helspin.frm
#include- include/write.frm

i mu,nu,rho,sigma;
cf T1a, T4b, Xunc, Xcro, X1a, X4b, V3g, jv;
v p1,pa,p4,pb,pq,pqbar,pl,plbar,q1,q3,q11,q12;
s h;
s tunc1,tunc2,tcro1,tcro2,s2AB,s3AB,s23AB;
s t1,t3,s12,s13,s1A,s1B,sa2,sa3,saA,saB,s42,s43,s4A,s4B,sb2,sb3,sbA,sbB;

#do h1={+,-}
   #do h2={+,-}
      l [M_uncross_W `h1'`h2'] = T1a(`h1'1, mu)*T4b(`h2'1, nu)*Xunc(mu, nu);
      l [M_cross_W `h1'`h2'] = T1a(`h1'1, mu)*T4b(`h2'1, nu)*Xcro(mu, nu);
      l [M_sym_W `h1'`h2'] = T1a(`h1'1, mu)*T4b(`h2'1, nu)*(
         X1a(mu, nu, sigma) - X4b(mu, nu, sigma) + V3g(mu, nu, sigma)
      )/s23AB * jv(sigma);
   #enddo
#enddo

id T1a(h?, mu?) = Current(h, p1, mu, pa);
id T4b(h?, mu?) = Current(h, p4, mu, pb);
* see eq:X_Unc in developer manual, multiplied by -t1*t3
id Xunc(mu, nu) = -Current(-1, pl, sigma, plbar)*(
   - Current(-1, pq, sigma, pq + pl + plbar, mu, q3 + pqbar, nu, pqbar)/s2AB/tunc2
   + Current(-1, pq, mu, q1 - pq, sigma, q3 + pqbar, nu, pqbar)/tunc1/tunc2
   + Current(-1, pq, mu, q1 - pq, nu, pqbar + pl + plbar, sigma, pqbar)/tunc1/s3AB
);
* see eq:X_Cro in developer manual, multiplied by -t1*t3
id Xcro(mu, nu) = -Current(-1, pl, sigma, plbar)*(
   - Current(-1, pq, nu, q3 + pq, mu, pqbar + pl + plbar, sigma, pqbar)/tcro1/s3AB
   + Current(-1, pq, nu, q3 + pq, sigma, q1 - pqbar, mu, pqbar)/tcro1/tcro2
   + Current(-1, pq, sigma, pq + pl + plbar, nu, q1 - pqbar, mu, pqbar)/s2AB/tcro2
);
* see eq:X_1a in developer manual, multiplied by -t1*s23AB*t3
id X1a(mu, nu, sigma) = t1*d_(mu, nu)*(
   + p1(sigma)/(s12 + s13 + s1A + s1B)
   + pa(sigma)/(sa2 + sa3 + saA + saB)
);
* see eq:X_4b in developer manual, multiplied by t1*s23AB*t3
id X4b(mu, nu, sigma) = t3*d_(mu, nu)*(
   + p4(sigma)/(s42 + s43 + s4A + s4B)
   + pb(sigma)/(sb2 + sb3 + sbA + sbB)
);
* see eq:3GluonWEmit in developer manual, multiplied by t1*s23AB*t3
id V3g(mu, nu, sigma) = (
   + (q1(nu) + pq(nu) + pqbar(nu) + pl(nu) + plbar(nu))*d_(mu, sigma)
   + (q3(mu) - pq(mu) - pqbar(mu) - pl(mu) - plbar(mu))*d_(nu, sigma)
   - (q1(sigma) + q3(sigma))*d_(mu, nu)
);
* see eq:J_V in developer manual
* this is actually JV (eq:Weffcur1), but we can't use the built-in
* because it cannot deal with two outgoing momenta, i.e. negative-energy spinors
id jv(sigma?) = -i_*(
   + Current(-1, pq, rho, pq + pl + plbar, sigma, pqbar)/s2AB
   - Current(-1, pq, sigma, pqbar + pl + plbar, rho, pqbar)/s3AB
)*Current(-1, pl, rho, plbar);

multiply replace_(
   s2AB, m2(pl+plbar+pq),
   s3AB, m2(pl+plbar+pqbar),
   s23AB, m2(pl+plbar+pq+pqbar),
   tunc1, m2(q1-pq),
   tunc2, m2(q3+pqbar),
   tcro1, m2(q3+pq),
   tcro2, m2(q1-pqbar),
   t1, m2(q1),
   t3, m2(q3),
   s12, m2(p1+pq),
   s13, m2(p1+pqbar),
   s1A, m2(p1+pl),
   s1B, m2(p1+plbar),
   sa2, m2(pa+pq),
   sa3, m2(pa+pqbar),
   saA, m2(pa+pl),
   saB, m2(pa+plbar),
   s42, m2(p4+pq),
   s43, m2(p4+pqbar),
   s4A, m2(p4+pl),
   s4B, m2(p4+plbar),
   sb2, m2(pb+pq),
   sb3, m2(pb+pqbar),
   sbA, m2(pb+pl),
   sbB, m2(pb+plbar)
);
multiply replace_(q3, q1-pl-plbar-pq-pqbar);
* replace q1 by sum of lightlike momenta
* @TODO: choose q11 as collinear to some other vector
multiply replace_(q1, q11-q12);
.sort
#call ContractCurrents()
.sort
format O4;
format c;
#call WriteHeader(`OUTPUT')
#call WriteOptimised(`OUTPUT',M_uncross_W,2,pa,p1,pb,p4,pq,pqbar,pl,plbar,q11,q12)
#call WriteOptimised(`OUTPUT',M_cross_W,2,pa,p1,pb,p4,pq,pqbar,pl,plbar,q11,q12)
#call WriteOptimised(`OUTPUT',M_sym_W,2,pa,p1,pb,p4,pq,pqbar,pl,plbar,q11,q12)
#call WriteFooter(`OUTPUT')
.end

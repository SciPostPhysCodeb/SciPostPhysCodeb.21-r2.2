*/**
*  \brief Contraction of vector boson current with unordered current
*
*  \authors   The HEJ collaboration (see AUTHORS for details)
*  \date      2020
*  \copyright GPLv2 or later
*/
#include- include/helspin.frm
#include- include/write.frm

v pb,p2,pa,p1,pg,pl,plbar,q2,q3,p,pr;
i mu,nu,sigma;
s h;

#do h2={+,-}
*  eq:juno in developer manual with pa -> pb, p1 -> pg
   l [U1_jV `h2'] = (
      + Current(`h2'1, p2, nu, pg)*Current(`h2'1, pg, mu, pb)
      + 2*p2(nu)*Current(`h2'1, p2, mu, pb)
   )/m2(p2 + pg);
   l [U2_jV `h2'] = (
      + 2*Current(`h2'1, p2, mu, pb)*pb(nu)
      - Current(`h2'1, p2, mu, pg)*Current(`h2'1, pg, nu, pb)
   )/m2(pb - pg);
   l [L_jV `h2'] = (
      - (q2(nu) + q3(nu))*Current(`h2'1, p2, mu, pb)
      - 2*pg(mu)*Current(`h2'1, p2, nu, pb)
      + 2*Current(`h2'1, p2, pg, pb)*d_(mu, nu)
      + m2(q2)*Current(`h2'1, p2, mu, pb)*(p1(nu)/m2(p1+pg) + pa(nu)/m2(pa+pg))
   )/m2(q3);
#enddo
.sort
drop;

* multiply with polarisation vector and other currents
#do h1={+,-}
   #do hl={+,-}
      #do h2={+,-}
         #do hg={+,-}
            #do TENSOR={U1_jV,U2_jV,L_jV}
               l [`TENSOR' `h1'`hl'`h2'`hg'] = (
                  [`TENSOR' `h2']
                  *JV(`h1'1, `hl'1, mu, pa, p1, plbar, pl)
                  *Eps(`hg'1, nu, pr)
               );
            #enddo
         #enddo
      #enddo
   #enddo
#enddo

* choice of auxiliary vector
id Eps(h?, mu?, pr)*Current(h?, ?a) = Eps(h, mu, pg, p2)*Current(h, ?a);
also Eps(h?, mu?, pr) = Eps(h, mu, pg, pb);

multiply replace_(
   q2,p2+pg-pb,
   q3,p2-pb
);
.sort
#call ContractCurrents
.sort
format O4;
format c;
#call WriteHeader(`OUTPUT')
#call WriteOptimised(`OUTPUT',U1_jV,4,pa,p1,pb,p2,pg,pl,plbar)
#call WriteOptimised(`OUTPUT',U2_jV,4,pa,p1,pb,p2,pg,pl,plbar)
#call WriteOptimised(`OUTPUT',L_jV,4,pa,p1,pb,p2,pg,pl,plbar)
#call WriteFooter(`OUTPUT')
.end

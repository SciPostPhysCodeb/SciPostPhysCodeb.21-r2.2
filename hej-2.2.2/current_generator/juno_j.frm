*/**
*  \brief Contraction of FKL current with unordered current
*
*  \authors   The HEJ collaboration (see AUTHORS for details)
*  \date      2020
*  \copyright GPLv2 or later
*/
#include- include/helspin.frm
#include- include/write.frm

v pb,p2,pa,p1,pg,q2,q3,p,pr;
i mu,nu,sigma;
s h,foo,bar;

#do h1={+,-}
*  eq:juno in developer manual with p1 -> pg, p2 -> p1
   l [U1_j `h1'] = (
      + Current(`h1'1, p1, nu, pg)*Current(`h1'1, pg, mu, pa)
      + 2*p1(nu)*Current(`h1'1, p1, mu, pa)
   )/m2(p1 + pg);
   l [U2_j `h1'] = (
      + 2*Current(`h1'1, p1, mu, pa)*pa(nu)
      - Current(`h1'1, p1, mu, pg)*Current(`h1'1, pg, nu, pa)
   )/m2(pa - pg);
   l [L_j `h1'] = (
      - (q2(nu) + q3(nu))*Current(`h1'1, p1, mu, pa)
      - 2*pg(mu)*Current(`h1'1, p1, nu, pa)
      + 2*Current(`h1'1, p1, pg, pa)*d_(mu, nu)
      + m2(q2)*Current(`h1'1, p1, mu, pa)*(p2(nu)/m2(p2+pg) + pb(nu)/m2(pb+pg))
   )/m2(q3);
#enddo
.sort
drop;

* multiply with polarisation vector and other currents
#do h1={+,-}
   #do hg={+,-}
      #do TENSOR={U1_j,U2_j,L_j}
         l [`TENSOR' `h1'`hg'] = (
            [`TENSOR' `h1']
            *Current(-1, pb, mu, p2)
            *Eps(`hg'1, nu, pr)
         );
      #enddo
   #enddo
#enddo

* choice of auxiliary vector
id Eps(h?, mu?, pr) = Eps(h, mu, pg, p1);
*id Eps(h?, mu?, pr)*Current(h?, ?a) = Eps(h, mu, pg, p1)*Current(h, ?a);

multiply replace_(
   q2,p1+pg-pa,
   q3,p1-pa
);
.sort
#call ContractCurrents
.sort
format O4;
format c;
#call WriteHeader(`OUTPUT')
#call WriteOptimised(`OUTPUT',U1_j,2,pa,p1,pb,p2,pg)
#call WriteOptimised(`OUTPUT',U2_j,2,pa,p1,pb,p2,pg)
#call WriteOptimised(`OUTPUT',L_j,2,pa,p1,pb,p2,pg)
#call WriteFooter(`OUTPUT')
.end

*/**
*  \brief Contraction of vector boson current with FKL current
*
*  \authors   The HEJ collaboration (see AUTHORS for details)
*  \date      2020
*  \copyright GPLv2 or later
*/
#include- include/helspin.frm
#include- include/write.frm

v pb,p2,pa,p1,pl,plbar;
i mu;

#do h1={+,-}
   #do hl={+,-}
      #do h2={+,-}
         l [jV_j `h1'`hl'`h2'] = JV(`h1'1, `hl'1, mu, pa, p1, plbar, pl)*Current(`h2'1, p2, mu, pb);
      #enddo
   #enddo
#enddo

#call ContractCurrents
.sort
format O4;
format c;
#call WriteHeader(`OUTPUT')
#call WriteOptimised(`OUTPUT',jV_j,3,pa,p1,pb,p2,pl,plbar)
#call WriteFooter(`OUTPUT')
.end

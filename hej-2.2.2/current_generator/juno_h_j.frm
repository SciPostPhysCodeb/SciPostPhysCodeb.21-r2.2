*/**
*  \brief Contraction of unordered current with Higgs emission vertex and FKL current
*
*  \authors   The HEJ collaboration (see AUTHORS for details)
*  \date      2020
*  \copyright GPLv2 or later
*/
#include- include/helspin.frm
#include- include/write.frm

v pb,p2,pa,p1,pg,q2,q3,p,pr,qH1,qH2,q11,q12,q21,q22;
i mu,nu,sigma;
s h,foo,bar;

#do h1={+,-}
*  eq:juno in developer manual with p1 -> pg, p2 -> p1
   l [U1_h_j `h1'] = (
      + Current(`h1'1, p1, nu, pg)*Current(`h1'1, pg, mu, pa)
      + 2*p1(nu)*Current(`h1'1, p1, mu, pa)
   )/m2(p1 + pg);
   l [U2_h_j `h1'] = (
      + 2*Current(`h1'1, p1, mu, pa)*pa(nu)
      - Current(`h1'1, p1, mu, pg)*Current(`h1'1, pg, nu, pa)
   )/m2(pa - pg);
   l [L_h_j `h1'] = (
      - (q2(nu) + q3(nu))*Current(`h1'1, p1, mu, pa)
      - 2*pg(mu)*Current(`h1'1, p1, nu, pa)
      + 2*Current(`h1'1, p1, pg, pa)*d_(mu, nu)
      + m2(q2)*Current(`h1'1, p1, mu, pa)*(p2(nu)/m2(p2+pg) + pb(nu)/m2(pb+pg))
   )/m2(q3);
#enddo
.sort
drop;

* multiply with polarisation vector and other current
#do h1={+,-}
   #do h2={+,-}
      #do hg={+,-}
         #do TENSOR={U1_h_j,U2_h_j,L_h_j}
            l [`TENSOR' `h1'`h2'`hg'] = (
               [`TENSOR' `h1']
               *Current(`h2'1, pb, sigma, p2)
               *Eps(`hg'1, nu, pr)
            );
         #enddo
      #enddo
   #enddo
#enddo

multiply VH(mu, sigma, qH1, qH2);

* TODO: choice of auxiliary vector
id Eps(h?, mu?, pr) = Eps(h, mu, pg, p1);
*id Eps(h?, mu?, pr)*Current(h?, ?a) = Eps(h, mu, pg, p1)*Current(h, ?a);

multiply replace_(
   q2,p1+pg-pa,
   q3,p1-pa,
   qH1, q11 - q12,
   qH2, q21 - q22
);
.sort
#call ContractCurrents
id p1?.p2? = dot(p1, p2);
.sort
format O4;
format c;
#call WriteHeader(`OUTPUT')
#call WriteOptimised(`OUTPUT',U1_h_j,3,pa,p1,pb,p2,pg,q11,q12,q21,q22,COMPLEX:,T1,T2)
#call WriteOptimised(`OUTPUT',U2_h_j,3,pa,p1,pb,p2,pg,q11,q12,q21,q22,COMPLEX:,T1,T2)
#call WriteOptimised(`OUTPUT',L_h_j,3,pa,p1,pb,p2,pg,q11,q12,q21,q22,COMPLEX:,T1,T2)
#call WriteFooter(`OUTPUT')
.end

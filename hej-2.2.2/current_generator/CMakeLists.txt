set(form_folder ${CMAKE_CURRENT_SOURCE_DIR}/form)

include(ExternalProject)
ExternalProject_Add(
  FORM
  SOURCE_DIR ${form_folder}
  INSTALL_DIR ${CMAKE_CURRENT_BINARY_DIR}
  CONFIGURE_COMMAND autoreconf -i ${form_folder}
  COMMAND CC=${CMAKE_C_COMPILER} CXX=${CMAKE_CXX_COMPILER}
    ${form_folder}/configure --without-gmp --disable-native
    --without-zlib --disable-threaded --prefix=<INSTALL_DIR> -q
    # we only run FORM once, don't spend time on optimising it
    COMPILEFLAGS="-O0"

  )
set(form_binary ${CMAKE_CURRENT_BINARY_DIR}/bin/form)

file(GLOB form_headers include/*.frm)
file(GLOB current_files *.frm)

foreach(current ${current_files})
  get_filename_component(header_name ${current} NAME_WE)
  set(header ${PROJECT_BINARY_DIR}/include/HEJ/currents/${header_name}.hh)
  list(APPEND current_headers ${header})
  add_custom_command(
    OUTPUT  ${header}
    COMMAND ./gen_current.sh ${form_binary} ${current} ${header}
      ${CMAKE_CURRENT_BINARY_DIR}/${header_name}.log
    WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
    DEPENDS ${current} ${form_headers} gen_current.sh FORM
    )
endforeach()

add_custom_target(currents DEPENDS ${current_headers})

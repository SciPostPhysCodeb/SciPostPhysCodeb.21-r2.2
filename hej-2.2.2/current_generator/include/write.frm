*/**
*  \brief Procedures for writing current contraction headers
*
*  \authors   The HEJ collaboration (see AUTHORS for details)
*  \date      2020
*  \copyright GPLv2 or later
*/
#ifndef `$HEJWriteIncluded'
#$HEJWriteIncluded = 1;

* Write start of C++ header to `OUTPUT'
#procedure WriteHeader(OUTPUT)

   #write <`OUTPUT'> "#pragma once"
   #write <`OUTPUT'> ""
   #write <`OUTPUT'> "#include <complex>"
   #write <`OUTPUT'> "#include \"HEJ/helicity.hh\""
   #write <`OUTPUT'> "#include \"HEJ/LorentzVector.hh\""
   #write <`OUTPUT'> "#include \"HEJ/utility.hh\""
   #write <`OUTPUT'> ""
   #write <`OUTPUT'> "namespace HEJ {"

#endprocedure

* Write optimised expression to C++ header `OUTPUT'
#procedure WriteOptimised(OUTPUT,EXPRESSION,NUMHELICITIES,?ARGS)

   #write <`OUTPUT'> "   template<%"
   #define FIRST "1"
   #do i=1,`NUMHELICITIES'
      #ifdef `FIRST'
         #undefine FIRST
         #write <`OUTPUT'> "Helicity%"
      #else
         #write <`OUTPUT'> ", Helicity%"
      #endif
   #enddo
   #write <`OUTPUT'> ">"
   #write <`OUTPUT'> "   std::complex<double> `EXPRESSION'("
      #call WriteArgs(`OUTPUT',`?ARGS')
   #write <`OUTPUT'> "\n   );\n"

   #call WriteOptimisedHelper(`OUTPUT',`EXPRESSION',`NUMHELICITIES',`?ARGS')

#endprocedure

*INTERNAL PROCEDURE
#procedure WriteOptimisedHelper(OUTPUT,EXPRESSION,NUMHELICITIES,?REST)

   #if `NUMHELICITIES' > 0
      #do H={+,-}
         #call WriteOptimisedHelper(`OUTPUT',`EXPRESSION',{`NUMHELICITIES'-1},`?REST',`H')
      #enddo

   #else

      #define HELSTRING ""
      #define TEMPLATEARGS ""
      #define FARGS ""
      #do ARG={`?REST'}
         #if ("`ARG'" == "+") || ("`ARG'" == "-")
*           arguments that define helicities
            #redefine HELSTRING "`HELSTRING'`ARG'"
            #if "`TEMPLATEARGS'" != ""
               #redefine TEMPLATEARGS "`TEMPLATEARGS',"
            #endif
            #if "`ARG'" == "+"
               #redefine TEMPLATEARGS "`TEMPLATEARGS'Helicity::plus"
            #else
               #redefine TEMPLATEARGS "`TEMPLATEARGS'Helicity::minus"
            #endif

         #else
*           C++ function arguments
            #if "`FARGS'" != ""
               #redefine FARGS "`FARGS',"
            #endif
            #redefine FARGS "`FARGS'`ARG'"
         #endif
      #enddo
      #optimize [`EXPRESSION'`HELSTRING']
      #write "operations in [`EXPRESSION'`HELSTRING']: `optimvalue_'"
      #write <`OUTPUT'> "   template<>"
      #write <`OUTPUT'> "   inline std::complex<double> `EXPRESSION'<%"
*     we use a loop here because otherwise FORM will insert line breaks
*     if the string is too large
      #define FIRST "1"
      #do TEMPLATEARG={`TEMPLATEARGS'}
         #ifdef `FIRST'
            #undefine FIRST
         #else
            #write <`OUTPUT'> ", %"
         #endif
         #write <`OUTPUT'> "`TEMPLATEARG'%"
      #enddo
      #write <`OUTPUT'> ">("
      #call WriteArgs(`OUTPUT',`FARGS')
      #write <`OUTPUT'> "\n   ) {"
      #write <`OUTPUT'> "        using namespace currents;"
      #write <`OUTPUT'> "        static constexpr std::complex<double> i_{0., 1.};"
      #write <`OUTPUT'> "        static constexpr double pi_{M_PI};"
      #write <`OUTPUT'> "        ignore(i_,pi_%"
      #do ARG={`FARGS'}
         #if ("`ARG'" != "COMPLEX:") && ("`ARG'" != "DOUBLE:") && ("`ARG'" != "VECTORS:")
            #write <`OUTPUT'> ",`ARG'%"
         #endif
      #enddo
      #write <`OUTPUT'> ");"
      #if `optimmaxvar_' > 0
         #write <`OUTPUT'> "     std::complex<double> %"
         #define FIRST "1"
         #do i=1,`optimmaxvar_'
            #ifdef `FIRST'
               #undefine FIRST
               #write <`OUTPUT'> "Z`i'_%"
            #else
               #write <`OUTPUT'> ", Z`i'_%"
            #endif
         #enddo
         #write <`OUTPUT'> ";"
      #endif
      #write <`OUTPUT'> "   %O"
      #write <`OUTPUT'> "      return %E;" [`EXPRESSION'`HELSTRING']
      #write <`OUTPUT'> "   }\n" [`EXPRESSION'`HELSTRING']
      #clearoptimize
   #endif

#endprocedure

*INTERNAL PROCEDURE
* Write C++ function arguments to `OUTPUT'
#procedure WriteArgs(OUTPUT,?ARGS)

   #define TYPE "CLHEP::HepLorentzVector const &"
   #define FIRST "1"
   #do P={`?ARGS'}
      #if "`P'" == "COMPLEX:"
         #redefine TYPE "std::complex<double> const &"
      #elseif "`P'" == "DOUBLE:"
         #redefine TYPE "double const"
      #elseif "`P'" == "VECTORS:"
         #redefine TYPE "CLHEP::HepLorentzVector const &"
      #else
         #ifdef `FIRST'
            #undefine FIRST
            #else
            #write <`OUTPUT'> ","
         #endif
         #write <`OUTPUT'> "      `TYPE' `P'%"
      #endif
   #enddo

#endprocedure

* Write end of C++ header to `OUTPUT'
#procedure WriteFooter(OUTPUT)

   #write <`OUTPUT'> "}"

#endprocedure

#endif

*/**
*  \brief Procedures for dealing with helicity spinors
*
*  \authors   The HEJ collaboration (see AUTHORS for details)
*  \date      2020
*  \copyright GPLv2 or later
*/
#ifndef `$HEJHelspinIncluded'
#$HEJHelspinIncluded = 1;

on shortstats;
* Complex conjugate
cfunction Conjugate;
* Generalised current, see eq:j_gen in the developer manual
cfunction Current;
* Angle and square products
* See eq:angle_product and eq:square_product in the developer manual
cfunction angle,square;
* Generalised angle and square products and
* generalised negative-helicity currents
* See eq:spinors_soinhel and eq:current_spinhel in the developer manual
#define CHAINS "AngleChain,SquareChain,SpinorChain"
ctensors `CHAINS';

* internal symbols are all uppercase
vectors P,Q,K,L;
cfunctions DUM;
ctensors CT;
symbols X,Y;
indices MU1,...,MU50;

#include- include/currents.frm

* Compute all current contractions
* See sec:contr_calc_algo in the developer manual
*
* Example:
*
* vectors p1,...,p4;
* indices mu;
* local foo = Current(-1, p1,mu,p2)*Current(-1, p3,mu,p4);
* #call ContractCurrents
* print;
* .end
*
* Example Output:
*
* foo = - 2*angle(p1,p3)*square(p2,p4);
*
#procedure ContractCurrents

   #call InsSpecialCurrents;
   #call InsCurrent;
   .sort

   #call GetMomentaInChains($MOMENTA)

   #if "`$MOMENTA'" != ""
      #message Rewrite momenta `$MOMENTA' into spinors
      #call ToSpinors(`$MOMENTA')
   #endif

   while(match(SpinorChain(P?, MU1?, Q?)*SpinorChain(K?, MU1?, L?)));
      #call Fierz(MU1?)
   endwhile;
   #call SortArgs
   id AngleChain(P?, Q?) = angle(P, Q);
   id SquareChain(P?, Q?) = square(P, Q);
   argument denom_;
      id AngleChain(P?, Q?) = angle(P, Q);
      id SquareChain(P?, Q?) = square(P, Q);
   endargument;

#endprocedure

* INTERNAL PROCEDURE
* Find all momenta that appear inside
* generalised currents, angle products, and square products
#procedure GetMomentaInChains(MOMENTA)

   id CT?{`CHAINS'}(?a) = DUM(?a)*CT(?a);
   #$TMP = 0;
   #$P = 0;
   argument DUM;
      dropcoefficient;
      $P = term_;
      $TMP = $TMP + $P;
   endargument;
   id DUM(?a) = 1;
   moduleoption local $P;
   moduleoption sum $TMP;
   .sort
   #$TMP = dum_($TMP);
   #inside $TMP
      splitarg dum_;
      argument dum_;
         dropcoefficient;
      endargument;
      repeat id dum_(?a, MU1?index_, ?b) = dum_(?a, ?b);
      id dum_(?a`MOMENTA') = 0;
   #endinside

#endprocedure

* INTERNAL PROCEDURE
* Rewrite Current as SpinorChain
* See eq:jplus_gen, eq:current_spinhel in the developer manual
#procedure InsCurrent

   id Current(+1, ?a) = Current(-1, reverse_(?a));
*  hack
*  with ?a as pattern FORM has problems if the arguments are sums of momenta
   #do N=3,50
      id Current(-1, MU1?,...,MU`N'?) = SpinorChain(MU1,...,MU`N');
   #enddo

#endprocedure

* INTERNAL PROCEDURE
* Sort arguments of angle and square products
* See eq:angle_product, eq:square_product in the developer manual
#procedure SortArgs

   #do F={AngleChain,SquareChain}
      antisymmetrize `F':2;
   #enddo

#endprocedure

* INTERNAL PROCEDURE
* Simplify expressions containing complex conjugates
* See eq:square_product, eq:jplus_gen in the developer manual
#procedure DoConjugate

   splitarg Conjugate;
   repeat id Conjugate(X?, Y?, ?a) = Conjugate(X) + Conjugate(Y, ?a);
   normalize Conjugate;
   id Conjugate(AngleChain(P?, Q?)) = -SquareChain(P, Q);
   id Conjugate(SquareChain(P?, Q?)) = -AngleChain(P, Q);
   id Conjugate(SpinorChain(?a)) = DUM(reverse_(?a));
   id DUM(?a) = SpinorChain(?a);

#endprocedure

* INTERNAL PROCEDURE
* Apply a Fierz transform to currents contracted over index `MU'
* See eq:Fierz in the developer manual
#procedure Fierz(MU)

   once SpinorChain(P?, `MU', Q?)*SpinorChain(K?, `MU', L?) = (
      2*AngleChain(P, K)*SquareChain(L, Q)
   );

#endprocedure

* INTERNAL PROCEDURE
* Rewrite a single momentum `P' inside a SpinorChain into spinors
* See eq:p_in_current in the developer manual
* See ToSpinors for an example
#procedure MomentumToSpinorsInSpinorChain(P)

   id SpinorChain(P?, ?a, `P', ?b, Q?) = DUM(
      mod_(nargs_(P, ?a),2), mod_(nargs_(?b, Q),2),
      SpinorChain(P, ?a, `P', ?b, Q)
   );
   id DUM(1, 1, SpinorChain(P?, ?a, `P', ?b, Q?)) = (
      AngleChain(P, ?a, `P')*SquareChain(`P', ?b, Q)
   );
   id DUM(0, 0, SpinorChain(P?, ?a, `P', ?b, Q?)) = (
      SpinorChain(P, ?a, `P')*SpinorChain(`P', ?b, Q)
   );
   if(count(DUM,1)>0);
      print "even number of arguments in spinor chain in %t";
      exit;
   endif;

#endprocedure

* INTERNAL PROCEDURE
* Rewrite a single momentum `P' inside an AngleChain into spinors
* See eq:p_in_angle in the developer manual
* See ToSpinors for an example
#procedure MomentumToSpinorsInAngleChain(P)

   id AngleChain(P?, ?a, `P', ?b, Q?) = DUM(
      mod_(nargs_(P, ?a),2), mod_(nargs_(?b, Q),2),
      AngleChain(P, ?a, `P', ?b, Q)
   );
   id DUM(1, 0, AngleChain(P?, ?a, `P', ?b, Q?)) = (
      AngleChain(P, ?a, `P')*Conjugate(SpinorChain(`P', ?b, Q))
   );
   id DUM(0, 1, AngleChain(P?, ?a, `P', ?b, Q?)) = (
      SpinorChain(P, ?a, `P')*AngleChain(`P', ?b, Q)
   );
   if(count(DUM,1)>0);
      print "odd number of arguments in angle chain in %t";
      exit;
   endif;

#endprocedure

* INTERNAL PROCEDURE
* Rewrite a single momentum `P' inside a SquareChain into spinors
* See eq:p_in_square in the developer manual
* See ToSpinors for an example
#procedure MomentumToSpinorsInSquareChain(P)

   id SquareChain(P?, ?a, `P', ?b, Q?) = DUM(
      mod_(nargs_(P, ?a),2), mod_(nargs_(?b, Q),2),
      SquareChain(P, ?a, `P', ?b, Q)
   );
   id DUM(1, 0, SquareChain(P?, ?a, `P', ?b, Q?)) = (
      SquareChain(P, ?a, `P')*SpinorChain(`P', ?b, Q)
   );
   id DUM(0, 1, SquareChain(P?, ?a, `P', ?b, Q?)) = (
      Conjugate(SpinorChain(P, ?a, `P'))*SquareChain(`P', ?b, Q)
   );
   if(count(DUM,1)>0);
      print "odd number of arguments in angle chain in %t";
      exit;
   endif;

#endprocedure


* INTERNAL PROCEDURE
* Rewrite a single momentum `P' into spinors
* See ToSpinors for an example
#procedure MomentumToSpinors(P)

   repeat;
      #do CHAIN={`CHAINS'}
         id `CHAIN'(?a, `P', `P', ?b) = 0;
         #call MomentumToSpinorsIn`CHAIN'(`P')
      #enddo
   endrepeat;

#endprocedure

* INTERNAL PROCEDURE
* Successively rewrite all momenta `?PS' into spinors,
* i.e. replace p = <p| [p| + |p> |p]
*
* Example:
*
* v p1,p2,p3;
* l foo = SpinorChain(p1,p2,p3,p2,p1);
* #call ToSpinors(p2,p3)
* print;
* .end
*
* Example Output:
*
* foo =
*   AngleChain(p1,p2)*AngleChain(p3,p2)*SquareChain(p2,p1)*SquareChain(p2,p3)
*
#procedure ToSpinors(?PS)

   #do P={`?PS'}
      #call MomentumToSpinors(`P')
      argument;
         #call MomentumToSpinors(`P')
      endargument;
      factarg Conjugate;
      chainout Conjugate;
      id Conjugate(Conjugate(X?)) = X;
   #enddo
   #call DoConjugate

#endprocedure

#endif

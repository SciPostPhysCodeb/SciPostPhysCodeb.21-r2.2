*/**
*  \brief Predefined current functions and polarisation vectors
*
*  \authors   The HEJ collaboration (see AUTHORS for details)
*  \date      2020
*  \copyright GPLv2 or later
*/
#ifndef `$HEJCurrentsIncluded'
#$HEJCurrentsIncluded = 1;

* FKL current for vector boson
cfunction JV;
* Polarisation vectors
cfunction Eps;
* Polarisation vectors contracted with Higgs emission vertex
cfunction EPSH;
* Higgs emission vertex
cfunction VH;
cfunction m2,plus,minus,perphat,sqrt,conj,dot;
s T1,T2;

* internal symbols are all uppercase
symbols H, H1, HL, M;
indices MU1,...,MU50;
vectors PA,P1,PH,PLBAR,PL,PG,PR,Q1,Q2;

#procedure InsSpecialCurrents

   #call InsJV
   #call InsEps
   #call InsVH

#endprocedure

* Replace vector boson FKL current
#procedure InsJV()

*  we use a loop here to introduce a new dummy index for each occurence
   repeat;
      once JV(H1?, HL?, MU1?, PA?, P1?, PLBAR?, PL?) = Current(HL, PL, MU2, PLBAR)*(
         + Current(H1, P1, MU2, P1+PLBAR+PL, MU1, PA)/m2(P1+PLBAR+PL)
         + Current(H1, P1, MU1, PA-PLBAR-PL, MU2, PA)/m2(PA-PLBAR-PL)
      );
      sum MU2;
   endrepeat;

#endprocedure

* Replace polarisation vectors by currents
#procedure InsEps()

   id Conjugate(Eps(H1?, MU1?, PG?, PR?)) = - Eps(-H1, MU1, PG, PR);
   id Eps(-1, MU1?, PG?, PR?) = sqrt(2)/2*SpinorChain(PR, MU1, PG)/AngleChain(PG,PR);
   id Eps(+1, MU1?, PG?, PR?) = sqrt(2)/2*SpinorChain(PG, MU1, PR)/SquareChain(PG,PR);

#endprocedure

* Higgs emission vertex
* see eq:VH in developer manual, but without overall prefactor
#procedure InsVH()

   id VH(MU1?, MU2?, Q1?, Q2?) = d_(MU1, MU2)*T1 - Q2(MU1)*Q1(MU2)*T2;

#endprocedure

#endif

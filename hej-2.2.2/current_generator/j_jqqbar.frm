*/**
*  \brief Contraction of FKL current with extremal qqbar emission current
*
*  TODO: transcribe formulas to developer manual
*
*  \authors   The HEJ collaboration (see AUTHORS for details)
*  \date      2020
*  \copyright GPLv2 or later
*/
#include- include/helspin.frm
#include- include/write.frm

v pb,p2,p3,pa,p1,p;
i mu,nu,sigma;
s h;

* Equation 3.23 in James Cockburn's Thesis.
l qggm1 = Current(-1, p2, mu, p3-pb, nu, p3);
l qggm2 = -Current(-1, p2, nu, p2-pb, mu, p3);
l qggm3 = -2*i_/m2(p2+p3)*(
   (p2(nu) + p3(nu))*d_(mu, sigma)
   - pb(mu)*d_(nu, sigma)
   + pb(sigma)*d_(mu, nu)
)*Current(-1, p2, sigma, p3);
.sort
drop;

#do i=1,3
   #do h1={+,-}
      #do hg={+,-}
         l [j_qggm`i' `h1'`hg'] = qggm`i'*Eps(`hg'1, nu, pa, pb)*Current(
            `h1'1, pa, mu, p1
         );
      #enddo
   #enddo
#enddo
id Current(h?, p1?, mu?, p?, nu?, p2?) = Current(h, p1, mu, p, nu, p2)/m2(p);

#call ContractCurrents
.sort
format O4;
format c;
#call WriteHeader(`OUTPUT')
#call WriteOptimised(`OUTPUT',j_qggm1,2,pb,p2,p3,pa,p1)
#call WriteOptimised(`OUTPUT',j_qggm2,2,pb,p2,p3,pa,p1)
#call WriteOptimised(`OUTPUT',j_qggm3,2,pb,p2,p3,pa,p1)
#call WriteFooter(`OUTPUT')
.end

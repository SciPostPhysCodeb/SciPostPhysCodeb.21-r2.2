# Distributed under the OSI-approved BSD 3-Clause License.  See accompanying
# file Copyright.txt or https://cmake.org/licensing for details.
# This is a slightly modified version of FindGSL.cmake taken from cmake 3.7

#.rst:
# FindLHAPDF
# --------
#
# Find the native LHAPDF includes and libraries.
#
# LHAPDF package is an object oriented, C++ event record for High Energy
# Physics Monte Carlo generators and simulation. It is free software
# under the GNU General Public License.
#
# Imported Targets
# ^^^^^^^^^^^^^^^^
#
# If LHAPDF is found, this module defines the following
# :prop_tgt:`IMPORTED` target::
#
#  LHAPDF::LHAPDF      - The main LHAPDF library.
#
# Result Variables
# ^^^^^^^^^^^^^^^^
#
# This module will set the following variables in your project::
#
#  LHAPDF_FOUND          - True if LHAPDF found on the local system
#  LHAPDF_INCLUDE_DIRS   - Location of LHAPDF header files.
#  LHAPDF_LIBRARIES      - The LHAPDF libraries.
#  LHAPDF_VERSION        - The version of the discovered LHAPDF install.
#
# Hints
# ^^^^^
#
# Set ``LHAPDF_ROOT_DIR`` to a directory that contains a LHAPDF installation.
#
# This script expects to find libraries at ``$LHAPDF_ROOT_DIR/lib`` and the LHAPDF
# headers at ``$LHAPDF_ROOT_DIR/include/LHAPDF``.  The library directory may
# optionally provide Release and Debug folders.  For Unix-like systems, this
# script will use ``$LHAPDF_ROOT_DIR/bin/LHAPDF-config`` (if found) to aid in the
# discovery LHAPDF.
#
# Cache Variables
# ^^^^^^^^^^^^^^^
#
# This module may set the following variables depending on platform and type
# of LHAPDF installation discovered.  These variables may optionally be set to
# help this module find the correct files::
#
#  LHAPDF_CONFIG_EXECUTABLE   - Location of the ``LHAPDF-config`` script (if any).
#  LHAPDF_LIBRARY             - Location of the LHAPDF library.
#  LHAPDF_LIBRARY_DEBUG       - Location of the debug LHAPDF library (if any).
#

# Include these modules to handle the QUIETLY and REQUIRED arguments.
include(FindPackageHandleStandardArgs)

#=============================================================================
# If the user has provided ``LHAPDF_ROOT_DIR``, use it!  Choose items found
# at this location over system locations.
if( EXISTS "$ENV{LHAPDF_ROOT_DIR}" )
  file( TO_CMAKE_PATH "$ENV{LHAPDF_ROOT_DIR}" LHAPDF_ROOT_DIR )
  set( LHAPDF_ROOT_DIR "${LHAPDF_ROOT_DIR}" CACHE PATH "Prefix for LHAPDF installation." )
endif()
if( NOT EXISTS "${LHAPDF_ROOT_DIR}" )
  set( LHAPDF_USE_PKGCONFIG ON )
endif()

#=============================================================================
# As a first try, use the PkgConfig module.  This will work on many
# *NIX systems.  See :module:`findpkgconfig`
# This will return ``LHAPDF_INCLUDEDIR`` and ``LHAPDF_LIBDIR`` used below.
if( LHAPDF_USE_PKGCONFIG )
  find_package(PkgConfig)
  pkg_check_modules( LHAPDF QUIET LHAPDF )

  if( EXISTS "${LHAPDF_INCLUDEDIR}" )
    get_filename_component( LHAPDF_ROOT_DIR "${LHAPDF_INCLUDEDIR}" DIRECTORY CACHE)
  endif()
endif()

#=============================================================================
# Set LHAPDF_INCLUDE_DIRS and LHAPDF_LIBRARIES. If we skipped the PkgConfig step, try
# to find the libraries at $LHAPDF_ROOT_DIR (if provided), in the directory
# suggested by LHAPDF-config (if available), or in standard system
# locations.  These find_library and find_path calls will prefer custom
# locations over standard locations (HINTS).  If the requested file is found
# neither at the HINTS location nor via LHAPDF-config, standard system locations
# will be still be searched (/usr/lib64 (Redhat), lib/i386-linux-gnu (Debian)).

# If we didn't use PkgConfig, try to find the version via LHAPDF-config
if( NOT LHAPDF_VERSION )
  find_program( LHAPDF_CONFIG_EXECUTABLE
    NAMES lhapdf-config
    HINTS "${LHAPDF_ROOT_DIR}/bin"
    )
  if( EXISTS "${LHAPDF_CONFIG_EXECUTABLE}" )
    execute_process(
      COMMAND "${LHAPDF_CONFIG_EXECUTABLE}" --version
      OUTPUT_VARIABLE LHAPDF_VERSION
      OUTPUT_STRIP_TRAILING_WHITESPACE )
    execute_process(
      COMMAND "${LHAPDF_CONFIG_EXECUTABLE}" --incdir
      OUTPUT_VARIABLE LHAPDF_CONFIG_INCLUDEDIR
      OUTPUT_STRIP_TRAILING_WHITESPACE )
    execute_process(
      COMMAND "${LHAPDF_CONFIG_EXECUTABLE}" --libdir
      OUTPUT_VARIABLE LHAPDF_CONFIG_LIBDIR
      OUTPUT_STRIP_TRAILING_WHITESPACE )
    endif()
endif()
find_path( LHAPDF_INCLUDE_DIR
  NAMES LHAPDF/LHAPDF.h LHAPDF/Version.h
  HINTS ${LHAPDF_ROOT_DIR}/include ${LHAPDF_INCLUDEDIR} ${LHAPDF_CONFIG_INCLUDEDIR}
)
find_library( LHAPDF_LIBRARY
  NAMES LHAPDF
  HINTS ${LHAPDF_ROOT_DIR}/lib ${LHAPDF_LIBDIR} ${LHAPDF_CONFIG_LIBDIR}
  PATH_SUFFIXES Release Debug
)
# Do we also have debug versions?
find_library( LHAPDF_LIBRARY_DEBUG
  NAMES LHAPDF
  HINTS ${LHAPDF_ROOT_DIR}/lib ${LHAPDF_LIBDIR} ${LHAPDF_CONFIG_LIBDIR}
  PATH_SUFFIXES Debug
)
set( LHAPDF_INCLUDE_DIRS ${LHAPDF_INCLUDE_DIR} )
set( LHAPDF_LIBRARIES ${LHAPDF_LIBRARY} )

if( NOT LHAPDF_VERSION )
  if(IS_DIRECTORY ${LHAPDF_INCLUDE_DIR})
    file(STRINGS "${LHAPDF_INCLUDE_DIR}/LHAPDF/Version.h" _LHAPDF_VERS)
    string(
      REGEX MATCH
      "#define *LHAPDF_VERSION *\"[^\"]*\""
      _LHAPDF_VERSION_STR "${_LHAPDF_VERS}"
      )
    string(
      REGEX MATCH
      "[0-9]*\\.[0-9]*\\.[0-9]*"
      LHAPDF_VERSION ${_LHAPDF_VERSION_STR}
      )
  endif()
endif()
if( LHAPDF_VERSION )
  string(REPLACE "." ";" t_list ${LHAPDF_VERSION})
  list(APPEND t_list 0 0) # add a buffer in case supversion not set
  list(GET t_list 0 LHAPDF_VERSION_MAJOR)
  list(GET t_list 1 LHAPDF_VERSION_MINOR)
  list(GET t_list 2 LHAPDF_VERSION_PATCH)
endif()
#=============================================================================
# handle the QUIETLY and REQUIRED arguments and set LHAPDF_FOUND to TRUE if all
# listed variables are TRUE
find_package_handle_standard_args( LHAPDF
  FOUND_VAR
    LHAPDF_FOUND
  REQUIRED_VARS
    LHAPDF_INCLUDE_DIR
    LHAPDF_LIBRARY
  VERSION_VAR
    LHAPDF_VERSION
    )

mark_as_advanced( LHAPDF_ROOT_DIR LHAPDF_VERSION LHAPDF_LIBRARY LHAPDF_INCLUDE_DIR
  LHAPDF_LIBRARY_DEBUG LHAPDF_USE_PKGCONFIG LHAPDF_CONFIG )

#=============================================================================
# Register imported libraries:
# 1. If we can find a Windows .dll file (or if we can find both Debug and
#    Release libraries), we will set appropriate target properties for these.
# 2. However, for most systems, we will only register the import location and
#    include directory.

# Look for dlls, or Release and Debug libraries.
if(WIN32)
  string( REPLACE ".lib" ".dll" LHAPDF_LIBRARY_DLL       "${LHAPDF_LIBRARY}" )
  string( REPLACE ".lib" ".dll" LHAPDF_LIBRARY_DEBUG_DLL "${LHAPDF_LIBRARY_DEBUG}" )
endif()

if( LHAPDF_FOUND AND NOT TARGET LHAPDF::LHAPDF )
  if( EXISTS "${LHAPDF_LIBRARY_DLL}")

    # Windows systems with dll libraries.
    add_library( LHAPDF::LHAPDF      SHARED IMPORTED )
    add_library( LHAPDF::LHAPDFcblas SHARED IMPORTED )

    # Windows with dlls, but only Release libraries.
    set_target_properties( LHAPDF::LHAPDF PROPERTIES
      IMPORTED_LOCATION_RELEASE         "${LHAPDF_LIBRARY_DLL}"
      IMPORTED_IMPLIB                   "${LHAPDF_LIBRARY}"
      INTERFACE_INCLUDE_DIRECTORIES     "${LHAPDF_INCLUDE_DIRS}"
      IMPORTED_CONFIGURATIONS           Release
      IMPORTED_LINK_INTERFACE_LANGUAGES "CXX"
      )

    # If we have both Debug and Release libraries
    if( EXISTS "${LHAPDF_LIBRARY_DEBUG_DLL}")
      set_property( TARGET LHAPDF::LHAPDF APPEND PROPERTY IMPORTED_CONFIGURATIONS Debug )
      set_target_properties( LHAPDF::LHAPDF PROPERTIES
        IMPORTED_LOCATION_DEBUG           "${LHAPDF_LIBRARY_DEBUG_DLL}"
        IMPORTED_IMPLIB_DEBUG             "${LHAPDF_LIBRARY_DEBUG}" )
    endif()

  else()

    # For all other environments (ones without dll libraries), create
    # the imported library targets.
    add_library( LHAPDF::LHAPDF      UNKNOWN IMPORTED )
    set_target_properties( LHAPDF::LHAPDF PROPERTIES
      IMPORTED_LOCATION                 "${LHAPDF_LIBRARY}"
      INTERFACE_INCLUDE_DIRECTORIES     "${LHAPDF_INCLUDE_DIRS}"
      IMPORTED_LINK_INTERFACE_LANGUAGES "CXX"
      )
  endif()
endif()

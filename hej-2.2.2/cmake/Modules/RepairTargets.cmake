## Various small fixes to repair broken/unreliable cmake targets

## Boost target seems to be unreliable and might requires specific versions of
## cmake to work see e.g. https://stackoverflow.com/a/42124857
## Workaround from:
## https://cliutils.gitlab.io/modern-cmake/chapters/packages/Boost.html
if(NOT TARGET Boost::boost)
  message("Setting up boost target \"Boost::boost\"")
  add_library(Boost::boost IMPORTED INTERFACE)
  set_property(TARGET Boost::boost PROPERTY
    INTERFACE_INCLUDE_DIRECTORIES ${Boost_INCLUDE_DIR})
endif()
if(NOT TARGET Boost::iostreams)
  message("Setting up boost target \"Boost::iostreams\"")
  add_library(Boost::iostreams IMPORTED INTERFACE)
  set_property(TARGET Boost::iostreams PROPERTY
    INTERFACE_INCLUDE_DIRECTORIES ${Boost_INCLUDE_DIR})
  set_property(TARGET Boost::iostreams PROPERTY
    INTERFACE_LINK_LIBRARIES ${Boost_LIBRARIES})
endif()
## Boost is stupid: regex is implicitly required for some iostreams headers,
## but CMake can not know which one see https://gitlab.kitware.com/cmake/cmake/issues/18763
## => Create a dummy target to make everyone happy
get_target_property(iostreams_links Boost::iostreams INTERFACE_LINK_LIBRARIES)
if( (NOT TARGET Boost::regex)
    AND (${iostreams_links} MATCHES ".*Boost::regex.*" ))
  add_library(Boost::regex IMPORTED INTERFACE)
endif()

# target yaml-cpp might miss include directory, fixed in (release of 0.6.3)
# https://github.com/jbeder/yaml-cpp/commit/ab5f9259a4e67d3fe0e4832bd407a9e596e2d884
if(NOT $<TARGET_PROPERTY:yaml-cpp,INTERFACE_INCLUDE_DIRECTORIES>
   AND EXISTS ${YAML_CPP_INCLUDE_DIR} )
  # if folder doesn't exist it has to be in some standard location
  set_property(TARGET yaml-cpp APPEND PROPERTY INTERFACE_INCLUDE_DIRECTORIES
      ${YAML_CPP_INCLUDE_DIR})
endif()

# Distributed under the OSI-approved BSD 3-Clause License.  See accompanying
# file Copyright.txt or https://cmake.org/licensing for details.
# This is a slightly modified version of FindGSL.cmake taken from cmake 3.7

#.rst:
# Findfastjet
# --------
#
# Find the native fastjet includes and libraries.
#
# fastjet package is an object oriented, C++ event record for High Energy
# Physics Monte Carlo generators and simulation. It is free software
# under the GNU General Public License.
#
# Imported Targets
# ^^^^^^^^^^^^^^^^
#
# If fastjet is found, this module defines the following
# :prop_tgt:`IMPORTED` target::
#
#  fastjet::fastjet      - The main fastjet library.
#
# Result Variables
# ^^^^^^^^^^^^^^^^
#
# This module will set the following variables in your project::
#
#  fastjet_FOUND          - True if fastjet found on the local system
#  fastjet_INCLUDE_DIRS   - Location of fastjet header files.
#  fastjet_LIBRARIES      - The fastjet libraries.
#  fastjet_VERSION        - The version of the discovered fastjet install.
#
# Hints
# ^^^^^
#
# Set ``fastjet_ROOT_DIR`` to a directory that contains a fastjet installation.
#
# This script expects to find libraries at ``$fastjet_ROOT_DIR/lib`` and the fastjet
# headers at ``$fastjet_ROOT_DIR/include/fastjet``.  The library directory may
# optionally provide Release and Debug folders.  For Unix-like systems, this
# script will use ``$fastjet_ROOT_DIR/bin/fastjet-config`` (if found) to aid in the
# discovery fastjet.
#
# Cache Variables
# ^^^^^^^^^^^^^^^
#
# This module may set the following variables depending on platform and type
# of fastjet installation discovered.  These variables may optionally be set to
# help this module find the correct files::
#
#  fastjet_CONFIG_EXECUTABLE   - Location of the ``fastjet-config`` script (if any).
#  fastjet_LIBRARY             - Location of the fastjet library.
#  fastjet_LIBRARY_DEBUG       - Location of the debug fastjet library (if any).
#

# Include these modules to handle the QUIETLY and REQUIRED arguments.
include(FindPackageHandleStandardArgs)

#=============================================================================
# If the user has provided ``fastjet_ROOT_DIR``, use it!  Choose items found
# at this location over system locations.
if( EXISTS "$ENV{fastjet_ROOT_DIR}" )
  file( TO_CMAKE_PATH "$ENV{fastjet_ROOT_DIR}" fastjet_ROOT_DIR )
  set( fastjet_ROOT_DIR "${fastjet_ROOT_DIR}" CACHE PATH "Prefix for fastjet installation." )
endif()
if( NOT EXISTS "${fastjet_ROOT_DIR}" )
  set( fastjet_USE_PKGCONFIG ON )
endif()

#=============================================================================
# As a first try, use the PkgConfig module.  This will work on many
# *NIX systems.  See :module:`findpkgconfig`
# This will return ``fastjet_INCLUDEDIR`` and ``fastjet_LIBDIR`` used below.
if( fastjet_USE_PKGCONFIG )
  find_package(PkgConfig)
  pkg_check_modules( fastjet QUIET fastjet )

  if( EXISTS "${fastjet_INCLUDEDIR}" )
    get_filename_component( fastjet_ROOT_DIR "${fastjet_INCLUDEDIR}" DIRECTORY CACHE)
  endif()
endif()

#=============================================================================
# Set fastjet_INCLUDE_DIRS and fastjet_LIBRARIES. If we skipped the PkgConfig step, try
# to find the libraries at $fastjet_ROOT_DIR (if provided), in the directory
# suggested by fastjet-config (if available), or in standard system
# locations.  These find_library and find_path calls will prefer custom
# locations over standard locations (HINTS).  If the requested file is found
# neither at the HINTS location nor via fastjet-config, standard system locations
# will be still be searched (/usr/lib64 (Redhat), lib/i386-linux-gnu (Debian)).

# If we didn't use PkgConfig, try to find the version via fastjet-config
if( NOT fastjet_VERSION )
  find_program( fastjet_CONFIG_EXECUTABLE
    NAMES fastjet-config
    HINTS "${fastjet_ROOT_DIR}/bin"
    )
  if( EXISTS "${fastjet_CONFIG_EXECUTABLE}" )
    execute_process(
      COMMAND "${fastjet_CONFIG_EXECUTABLE}" --version
      OUTPUT_VARIABLE fastjet_VERSION
      OUTPUT_STRIP_TRAILING_WHITESPACE )
    execute_process(
      COMMAND "${fastjet_CONFIG_EXECUTABLE}" --prefix
      OUTPUT_VARIABLE fastjet_CONFIG_ROOT_DIR
      OUTPUT_STRIP_TRAILING_WHITESPACE )
    set(fastjet_CONFIG_INCLUDEDIR ${fastjet_CONFIG_ROOT_DIR}/include)
    set(fastjet_CONFIG_LIBDIR ${fastjet_CONFIG_ROOT_DIR}/lib)
    endif()
endif()
find_path( fastjet_INCLUDE_DIR
  NAMES fastjet/config_auto.h fastjet/PseudoJet.hh
  HINTS ${fastjet_ROOT_DIR}/include ${fastjet_INCLUDEDIR} ${fastjet_CONFIG_INCLUDEDIR}
)
find_library( fastjet_LIBRARY
  NAMES fastjet
  HINTS ${fastjet_ROOT_DIR}/lib ${fastjet_LIBDIR} ${fastjet_CONFIG_LIBDIR}
  PATH_SUFFIXES Release Debug
)
# Do we also have debug versions?
find_library( fastjet_LIBRARY_DEBUG
  NAMES fastjet
  HINTS ${fastjet_ROOT_DIR}/lib ${fastjet_LIBDIR} ${fastjet_CONFIG_LIBDIR}
  PATH_SUFFIXES Debug
)
set( fastjet_INCLUDE_DIRS ${fastjet_INCLUDE_DIR} )
set( fastjet_LIBRARIES ${fastjet_LIBRARY} )

if( NOT fastjet_VERSION )
  if(IS_DIRECTORY ${fastjet_INCLUDE_DIR})
    file(STRINGS "${fastjet_INCLUDE_DIR}/fastjet/config_auto.h" _fastjet_DEFS)
    file(STRINGS "${fastjet_INCLUDE_DIR}/fastjet/version.hh" _fastjet_VERS)
    string(
      REGEX MATCH
      "#define *FASTJET_VERSION *\"[^\"]*\""
      _fastjet_VERSION_STR "${_fastjet_VERS} ${_fastjet_DEFS}"
      )
    string(
      REGEX MATCH
      "[0-9]*\\.[0-9]*\\.[0-9]*"
      fastjet_VERSION ${_fastjet_VERSION_STR}
      )
  endif()
endif()
if( fastjet_VERSION )
  string(REPLACE "." ";" t_list ${fastjet_VERSION})
  list(APPEND t_list 0 0) # add a buffer in case supversion not set
  list(GET t_list 0 fastjet_VERSION_MAJOR)
  list(GET t_list 1 fastjet_VERSION_MINOR)
  list(GET t_list 2 fastjet_VERSION_PATCH)
endif()
#=============================================================================
# handle the QUIETLY and REQUIRED arguments and set fastjet_FOUND to TRUE if all
# listed variables are TRUE
find_package_handle_standard_args( fastjet
  FOUND_VAR
    fastjet_FOUND
  REQUIRED_VARS
    fastjet_INCLUDE_DIR
    fastjet_LIBRARY
  VERSION_VAR
    fastjet_VERSION
    )

mark_as_advanced( fastjet_ROOT_DIR fastjet_VERSION fastjet_LIBRARY fastjet_INCLUDE_DIR
  fastjet_LIBRARY_DEBUG fastjet_USE_PKGCONFIG fastjet_CONFIG )

#=============================================================================
# Register imported libraries:
# 1. If we can find a Windows .dll file (or if we can find both Debug and
#    Release libraries), we will set appropriate target properties for these.
# 2. However, for most systems, we will only register the import location and
#    include directory.

# Look for dlls, or Release and Debug libraries.
if(WIN32)
  string( REPLACE ".lib" ".dll" fastjet_LIBRARY_DLL       "${fastjet_LIBRARY}" )
  string( REPLACE ".lib" ".dll" fastjet_LIBRARY_DEBUG_DLL "${fastjet_LIBRARY_DEBUG}" )
endif()

if( fastjet_FOUND AND NOT TARGET fastjet::fastjet )
  if( EXISTS "${fastjet_LIBRARY_DLL}")

    # Windows systems with dll libraries.
    add_library( fastjet::fastjet      SHARED IMPORTED )
    add_library( fastjet::fastjetcblas SHARED IMPORTED )

    # Windows with dlls, but only Release libraries.
    set_target_properties( fastjet::fastjet PROPERTIES
      IMPORTED_LOCATION_RELEASE         "${fastjet_LIBRARY_DLL}"
      IMPORTED_IMPLIB                   "${fastjet_LIBRARY}"
      INTERFACE_INCLUDE_DIRECTORIES     "${fastjet_INCLUDE_DIRS}"
      IMPORTED_CONFIGURATIONS           Release
      IMPORTED_LINK_INTERFACE_LANGUAGES "CXX"
      )

    # If we have both Debug and Release libraries
    if( EXISTS "${fastjet_LIBRARY_DEBUG_DLL}")
      set_property( TARGET fastjet::fastjet APPEND PROPERTY IMPORTED_CONFIGURATIONS Debug )
      set_target_properties( fastjet::fastjet PROPERTIES
        IMPORTED_LOCATION_DEBUG           "${fastjet_LIBRARY_DEBUG_DLL}"
        IMPORTED_IMPLIB_DEBUG             "${fastjet_LIBRARY_DEBUG}" )
    endif()

  else()

    # For all other environments (ones without dll libraries), create
    # the imported library targets.
    add_library( fastjet::fastjet      UNKNOWN IMPORTED )
    set_target_properties( fastjet::fastjet PROPERTIES
      IMPORTED_LOCATION                 "${fastjet_LIBRARY}"
      INTERFACE_INCLUDE_DIRECTORIES     "${fastjet_INCLUDE_DIRS}"
      IMPORTED_LINK_INTERFACE_LANGUAGES "CXX"
      )
  endif()
endif()

# Distributed under the OSI-approved BSD 3-Clause License.  See accompanying
# file Copyright.txt or https://cmake.org/licensing for details.
# This is a slightly modified version of FindGSL.cmake taken from cmake 3.7

#.rst:
# FindCLHEP
# --------
#
# Find the native CLHEP includes and libraries.
#
# CLHEP package is an object oriented, C++ event record for High Energy
# Physics Monte Carlo generators and simulation. It is free software
# under the GNU General Public License.
#
# Imported Targets
# ^^^^^^^^^^^^^^^^
#
# If CLHEP is found, this module defines the following
# :prop_tgt:`IMPORTED` target::
#
#  CLHEP::CLHEP      - The main CLHEP library.
#
# Result Variables
# ^^^^^^^^^^^^^^^^
#
# This module will set the following variables in your project::
#
#  CLHEP_FOUND          - True if CLHEP found on the local system
#  CLHEP_INCLUDE_DIRS   - Location of CLHEP header files.
#  CLHEP_LIBRARIES      - The CLHEP libraries.
#  CLHEP_VERSION        - The version of the discovered CLHEP install.
#
# Hints
# ^^^^^
#
# Set ``CLHEP_ROOT_DIR`` to a directory that contains a CLHEP installation.
#
# This script expects to find libraries at ``$CLHEP_ROOT_DIR/lib`` and the CLHEP
# headers at ``$CLHEP_ROOT_DIR/include/CLHEP``.  The library directory may
# optionally provide Release and Debug folders.  For Unix-like systems, this
# script will use ``$CLHEP_ROOT_DIR/bin/CLHEP-config`` (if found) to aid in the
# discovery CLHEP.
#
# Cache Variables
# ^^^^^^^^^^^^^^^
#
# This module may set the following variables depending on platform and type
# of CLHEP installation discovered.  These variables may optionally be set to
# help this module find the correct files::
#
#  CLHEP_CONFIG_EXECUTABLE   - Location of the ``CLHEP-config`` script (if any).
#  CLHEP_LIBRARY             - Location of the CLHEP library.
#  CLHEP_LIBRARY_DEBUG       - Location of the debug CLHEP library (if any).
#

# Include these modules to handle the QUIETLY and REQUIRED arguments.
include(FindPackageHandleStandardArgs)

#=============================================================================
# If the user has provided ``CLHEP_ROOT_DIR``, use it!  Choose items found
# at this location over system locations.
if( EXISTS "$ENV{CLHEP_ROOT_DIR}" )
  file( TO_CMAKE_PATH "$ENV{CLHEP_ROOT_DIR}" CLHEP_ROOT_DIR )
  set( CLHEP_ROOT_DIR "${CLHEP_ROOT_DIR}" CACHE PATH "Prefix for CLHEP installation." )
endif()
if( NOT EXISTS "${CLHEP_ROOT_DIR}" )
  set( CLHEP_USE_PKGCONFIG ON )
endif()

#=============================================================================
# As a first try, use the PkgConfig module.  This will work on many
# *NIX systems.  See :module:`findpkgconfig`
# This will return ``CLHEP_INCLUDEDIR`` and ``CLHEP_LIBDIR`` used below.
if( CLHEP_USE_PKGCONFIG )
  find_package(PkgConfig)
  pkg_check_modules( CLHEP QUIET CLHEP )

  if( EXISTS "${CLHEP_INCLUDEDIR}" )
    get_filename_component( CLHEP_ROOT_DIR "${CLHEP_INCLUDEDIR}" DIRECTORY CACHE)
  endif()
endif()

#=============================================================================
# Set CLHEP_INCLUDE_DIRS and CLHEP_LIBRARIES. If we skipped the PkgConfig step, try
# to find the libraries at $CLHEP_ROOT_DIR (if provided), in the directory
# suggested by CLHEP-config (if available), or in standard system
# locations.  These find_library and find_path calls will prefer custom
# locations over standard locations (HINTS).  If the requested file is found
# neither at the HINTS location nor via CLHEP-config, standard system locations
# will be still be searched (/usr/lib64 (Redhat), lib/i386-linux-gnu (Debian)).

# If we didn't use PkgConfig, try to find the version via CLHEP-config
if( NOT CLHEP_VERSION )
  find_program( CLHEP_CONFIG_EXECUTABLE
    NAMES clhep-config
    HINTS "${CLHEP_ROOT_DIR}/bin"
    )
  if( EXISTS "${CLHEP_CONFIG_EXECUTABLE}" )
    execute_process(
      COMMAND "${CLHEP_CONFIG_EXECUTABLE}" --version
      OUTPUT_VARIABLE CLHEP_VERSION
      OUTPUT_STRIP_TRAILING_WHITESPACE )
    string(REGEX REPLACE "CLHEP " "" CLHEP_VERSION ${CLHEP_VERSION})
    execute_process(
      COMMAND "${CLHEP_CONFIG_EXECUTABLE}" --prefix
      OUTPUT_VARIABLE CLHEP_CONFIG_ROOT_DIR
      OUTPUT_STRIP_TRAILING_WHITESPACE )
    string (REGEX REPLACE "\"\(.*\)\"[ \t\r\n]*$" "\\1"
      CLHEP_CONFIG_ROOT_DIR ${CLHEP_CONFIG_ROOT_DIR} )
    set(CLHEP_CONFIG_INCLUDEDIR ${CLHEP_CONFIG_ROOT_DIR}/include)
    set(CLHEP_CONFIG_LIBDIR ${CLHEP_CONFIG_ROOT_DIR}/lib)
    endif()
endif()
find_path( CLHEP_INCLUDE_DIR
  NAMES CLHEP/ClhepVersion.h
  HINTS ${CLHEP_ROOT_DIR}/include ${CLHEP_INCLUDEDIR} ${CLHEP_CONFIG_INCLUDEDIR}
)
find_library( CLHEP_LIBRARY
  NAMES CLHEP
  HINTS ${CLHEP_ROOT_DIR}/lib ${CLHEP_LIBDIR} ${CLHEP_CONFIG_LIBDIR}
  PATH_SUFFIXES Release Debug
)
# Do we also have debug versions?
find_library( CLHEP_LIBRARY_DEBUG
  NAMES CLHEP
  HINTS ${CLHEP_ROOT_DIR}/lib ${CLHEP_LIBDIR} ${CLHEP_CONFIG_LIBDIR}
  PATH_SUFFIXES Debug
)
set( CLHEP_INCLUDE_DIRS ${CLHEP_INCLUDE_DIR} )
set( CLHEP_LIBRARIES ${CLHEP_LIBRARY} )

if( NOT CLHEP_VERSION )
  if(IS_DIRECTORY ${CLHEP_INCLUDE_DIR})
    file(STRINGS "${CLHEP_INCLUDE_DIR}/CLHEP/ClhepVersion.h" _CLHEP_VERS)
    string(
      REGEX MATCH
      "static std::string String\\(\\) *{ *return *\"[^\"]*\""
      _CLHEP_VERSION_STR "${_CLHEP_VERS}"
      )
    string(
      REGEX MATCH
      "[0-9]*\\.[0-9]*\\.[0-9]*\\.[0-9]*"
      CLHEP_VERSION ${_CLHEP_VERSION_STR}
      )
  endif()
endif()
if( CLHEP_VERSION )
  string(REPLACE "." ";" t_list ${CLHEP_VERSION})
  list(APPEND t_list 0 0) # add a buffer in case supversion not set
  list(GET t_list 0 CLHEP_VERSION_MAJOR)
  list(GET t_list 1 CLHEP_VERSION_MINOR)
  list(GET t_list 2 CLHEP_VERSION_PATCH)
endif()
#=============================================================================
# handle the QUIETLY and REQUIRED arguments and set CLHEP_FOUND to TRUE if all
# listed variables are TRUE
find_package_handle_standard_args( CLHEP
  FOUND_VAR
    CLHEP_FOUND
  REQUIRED_VARS
    CLHEP_INCLUDE_DIR
    CLHEP_LIBRARY
  VERSION_VAR
    CLHEP_VERSION
    )

mark_as_advanced( CLHEP_ROOT_DIR CLHEP_VERSION CLHEP_LIBRARY CLHEP_INCLUDE_DIR
  CLHEP_LIBRARY_DEBUG CLHEP_USE_PKGCONFIG CLHEP_CONFIG )

#=============================================================================
# Register imported libraries:
# 1. If we can find a Windows .dll file (or if we can find both Debug and
#    Release libraries), we will set appropriate target properties for these.
# 2. However, for most systems, we will only register the import location and
#    include directory.

# Look for dlls, or Release and Debug libraries.
if(WIN32)
  string( REPLACE ".lib" ".dll" CLHEP_LIBRARY_DLL       "${CLHEP_LIBRARY}" )
  string( REPLACE ".lib" ".dll" CLHEP_LIBRARY_DEBUG_DLL "${CLHEP_LIBRARY_DEBUG}" )
endif()

if( CLHEP_FOUND AND NOT TARGET CLHEP::CLHEP )
  if( EXISTS "${CLHEP_LIBRARY_DLL}")

    # Windows systems with dll libraries.
    add_library( CLHEP::CLHEP      SHARED IMPORTED )
    add_library( CLHEP::CLHEPcblas SHARED IMPORTED )

    # Windows with dlls, but only Release libraries.
    set_target_properties( CLHEP::CLHEP PROPERTIES
      IMPORTED_LOCATION_RELEASE         "${CLHEP_LIBRARY_DLL}"
      IMPORTED_IMPLIB                   "${CLHEP_LIBRARY}"
      INTERFACE_INCLUDE_DIRECTORIES     "${CLHEP_INCLUDE_DIRS}"
      IMPORTED_CONFIGURATIONS           Release
      IMPORTED_LINK_INTERFACE_LANGUAGES "CXX"
      )

    # If we have both Debug and Release libraries
    if( EXISTS "${CLHEP_LIBRARY_DEBUG_DLL}")
      set_property( TARGET CLHEP::CLHEP APPEND PROPERTY IMPORTED_CONFIGURATIONS Debug )
      set_target_properties( CLHEP::CLHEP PROPERTIES
        IMPORTED_LOCATION_DEBUG           "${CLHEP_LIBRARY_DEBUG_DLL}"
        IMPORTED_IMPLIB_DEBUG             "${CLHEP_LIBRARY_DEBUG}" )
    endif()

  else()

    # For all other environments (ones without dll libraries), create
    # the imported library targets.
    add_library( CLHEP::CLHEP      UNKNOWN IMPORTED )
    set_target_properties( CLHEP::CLHEP PROPERTIES
      IMPORTED_LOCATION                 "${CLHEP_LIBRARY}"
      INTERFACE_INCLUDE_DIRECTORIES     "${CLHEP_INCLUDE_DIRS}"
      IMPORTED_LINK_INTERFACE_LANGUAGES "CXX"
      )
  endif()
endif()

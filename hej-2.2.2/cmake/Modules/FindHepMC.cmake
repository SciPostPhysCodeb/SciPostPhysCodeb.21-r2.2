# Distributed under the OSI-approved BSD 3-Clause License.  See accompanying
# file Copyright.txt or https://cmake.org/licensing for details.
# This is a slightly modified version of FindGSL.cmake taken from cmake 3.7

#.rst:
# FindHepMC
# --------
#
# Find the native HepMC includes and libraries.
#
# HepMC package is an object oriented, C++ event record for High Energy
# Physics Monte Carlo generators and simulation. It is free software
# under the GNU General Public License.
#
# Imported Targets
# ^^^^^^^^^^^^^^^^
#
# If HepMC is found, this module defines the following
# :prop_tgt:`IMPORTED` target::
#
#  HepMC::HepMC      - The main HepMC library.
#
# Result Variables
# ^^^^^^^^^^^^^^^^
#
# This module will set the following variables in your project::
#
#  HepMC_FOUND          - True if HepMC found on the local system
#  HepMC_INCLUDE_DIRS   - Location of HepMC header files.
#  HepMC_LIBRARIES      - The HepMC libraries.
#  HepMC_VERSION        - The version of the discovered HepMC install.
#
# Hints
# ^^^^^
#
# Set ``HepMC_ROOT_DIR`` to a directory that contains a HepMC installation.
# Set ``HepMC2_ROOT_DIR`` to a directory that contains a HepMC installation.
#
# This script expects to find libraries at ``$HepMC_ROOT_DIR/lib`` and the HepMC
# headers at ``$HepMC_ROOT_DIR/include/HepMC``.  The library directory may
# optionally provide Release and Debug folders.  For Unix-like systems, this
# script will use ``$HepMC_ROOT_DIR/bin/HepMC-config`` (if found) to aid in the
# discovery HepMC.
#
# Cache Variables
# ^^^^^^^^^^^^^^^
#
# This module may set the following variables depending on platform and type
# of HepMC installation discovered.  These variables may optionally be set to
# help this module find the correct files::
#
#  HepMC_CONFIG_EXECUTABLE   - Location of the ``HepMC-config`` script (if any).
#  HepMC_LIBRARY             - Location of the HepMC library.
#  HepMC_LIBRARY_DEBUG       - Location of the debug HepMC library (if any).
#

# Include these modules to handle the QUIETLY and REQUIRED arguments.
include(FindPackageHandleStandardArgs)

#=============================================================================
# If the user has provided ``HepMC_ROOT_DIR``, use it!  Choose items found
# at this location over system locations.
if( EXISTS "$ENV{HepMC_ROOT_DIR}" )
  file( TO_CMAKE_PATH "$ENV{HepMC_ROOT_DIR}" HepMC_ROOT_DIR )
  set( HepMC_ROOT_DIR "${HepMC_ROOT_DIR}" CACHE PATH "Prefix for HepMC installation." )
elseif( EXISTS "$ENV{HepMC2_ROOT_DIR}" )
  file( TO_CMAKE_PATH "$ENV{HepMC2_ROOT_DIR}" HepMC_ROOT_DIR )
  set( HepMC_ROOT_DIR "${HepMC_ROOT_DIR}" CACHE PATH "Prefix for HepMC installation." )
endif()
if( NOT EXISTS "${HepMC_ROOT_DIR}" )
  set( HepMC_USE_PKGCONFIG ON )
endif()

#=============================================================================
# As a first try, use the PkgConfig module.  This will work on many
# *NIX systems.  See :module:`findpkgconfig`
# This will return ``HepMC_INCLUDEDIR`` and ``HepMC_LIBDIR`` used below.
if( HepMC_USE_PKGCONFIG )
  find_package(PkgConfig)
  pkg_check_modules( HepMC QUIET HepMC )

  if( EXISTS "${HepMC_INCLUDEDIR}" )
    get_filename_component( HepMC_ROOT_DIR "${HepMC_INCLUDEDIR}" DIRECTORY CACHE)
  endif()
endif()

#=============================================================================
# Set HepMC_INCLUDE_DIRS and HepMC_LIBRARIES. If we skipped the PkgConfig step, try
# to find the libraries at $HepMC_ROOT_DIR (if provided), in the directory
# suggested by HepMC-config (if available), or in standard system
# locations.  These find_library and find_path calls will prefer custom
# locations over standard locations (HINTS).  If the requested file is found
# neither at the HINTS location nor via HepMC-config, standard system locations
# will be still be searched (/usr/lib64 (Redhat), lib/i386-linux-gnu (Debian)).

# If we didn't use PkgConfig, try to find the version via HepMC-config
if( NOT HepMC_VERSION )
  find_program( HepMC_CONFIG_EXECUTABLE
    NAMES HepMC-config
    HINTS "${HepMC_ROOT_DIR}/bin"
    )
  if( EXISTS "${HepMC_CONFIG_EXECUTABLE}" )
    execute_process(
      COMMAND "${HepMC_CONFIG_EXECUTABLE}" --version
      OUTPUT_VARIABLE HepMC_VERSION
      OUTPUT_STRIP_TRAILING_WHITESPACE )
    execute_process(
      COMMAND "${HepMC_CONFIG_EXECUTABLE}" --incdir
      OUTPUT_VARIABLE HepMC_CONFIG_INCLUDEDIR
      OUTPUT_STRIP_TRAILING_WHITESPACE )
    execute_process(
      COMMAND "${HepMC_CONFIG_EXECUTABLE}" --libdir
      OUTPUT_VARIABLE HepMC_CONFIG_LIBDIR
      OUTPUT_STRIP_TRAILING_WHITESPACE )
    endif()
endif()
find_path( HepMC_INCLUDE_DIR
  NAMES HepMC/HepMCDefs.h
  HINTS ${HepMC_ROOT_DIR}/include ${HepMC_INCLUDEDIR} ${HepMC_CONFIG_INCLUDEDIR}
)
find_library( HepMC_LIBRARY
  NAMES HepMC
  HINTS ${HepMC_ROOT_DIR}/lib ${HepMC_LIBDIR} ${HepMC_CONFIG_LIBDIR}
  PATH_SUFFIXES Release Debug
)
# Do we also have debug versions?
find_library( HepMC_LIBRARY_DEBUG
  NAMES HepMC
  HINTS ${HepMC_ROOT_DIR}/lib ${HepMC_LIBDIR} ${HepMC_CONFIG_LIBDIR}
  PATH_SUFFIXES Debug
)
set( HepMC_INCLUDE_DIRS ${HepMC_INCLUDE_DIR} )
set( HepMC_LIBRARIES ${HepMC_LIBRARY} )

if( NOT HepMC_VERSION )
  if(IS_DIRECTORY ${HepMC_INCLUDE_DIR})
    file(STRINGS "${HepMC_INCLUDE_DIR}/HepMC/HepMCDefs.h" _HepMC_DEFS)
    file(STRINGS "${HepMC_INCLUDE_DIR}/HepMC/Version.h" _HepMC_VERS)
    string(
      REGEX MATCH
      "#define *HEPMC_VERSION *\"[^\"]*\""
      _HepMC_VERSION_STR "${_HepMC_DEFS} ${_HepMC_VERS}"
      )
    string(
      REGEX MATCH
      "[0-9]*\\.[0-9]*\\.[0-9]*"
      HepMC_VERSION ${_HepMC_VERSION_STR}
      )
  endif()
endif()
if( HepMC_VERSION )
  string(REPLACE "." ";" t_list ${HepMC_VERSION})
  list(APPEND t_list 0 0) # add a buffer in case supversion not set
  list(GET t_list 0 HepMC_VERSION_MAJOR)
  list(GET t_list 1 HepMC_VERSION_MINOR)
  list(GET t_list 2 HepMC_VERSION_PATCH)
endif()
#=============================================================================
# handle the QUIETLY and REQUIRED arguments and set HepMC_FOUND to TRUE if all
# listed variables are TRUE
find_package_handle_standard_args( HepMC
  FOUND_VAR
    HepMC_FOUND
  REQUIRED_VARS
    HepMC_INCLUDE_DIR
    HepMC_LIBRARY
  VERSION_VAR
    HepMC_VERSION
    )

mark_as_advanced( HepMC_ROOT_DIR HepMC_VERSION HepMC_LIBRARY HepMC_INCLUDE_DIR
  HepMC_LIBRARY_DEBUG HepMC_USE_PKGCONFIG HepMC_CONFIG )

#=============================================================================
# Register imported libraries:
# 1. If we can find a Windows .dll file (or if we can find both Debug and
#    Release libraries), we will set appropriate target properties for these.
# 2. However, for most systems, we will only register the import location and
#    include directory.

# Look for dlls, or Release and Debug libraries.
if(WIN32)
  string( REPLACE ".lib" ".dll" HepMC_LIBRARY_DLL       "${HepMC_LIBRARY}" )
  string( REPLACE ".lib" ".dll" HepMC_LIBRARY_DEBUG_DLL "${HepMC_LIBRARY_DEBUG}" )
endif()

if( HepMC_FOUND AND NOT TARGET HepMC::HepMC )
  if( EXISTS "${HepMC_LIBRARY_DLL}")

    # Windows systems with dll libraries.
    add_library( HepMC::HepMC      SHARED IMPORTED )
    add_library( HepMC::HepMCcblas SHARED IMPORTED )

    # Windows with dlls, but only Release libraries.
    set_target_properties( HepMC::HepMC PROPERTIES
      IMPORTED_LOCATION_RELEASE         "${HepMC_LIBRARY_DLL}"
      IMPORTED_IMPLIB                   "${HepMC_LIBRARY}"
      INTERFACE_INCLUDE_DIRECTORIES     "${HepMC_INCLUDE_DIRS}"
      IMPORTED_CONFIGURATIONS           Release
      IMPORTED_LINK_INTERFACE_LANGUAGES "CXX"
      )

    # If we have both Debug and Release libraries
    if( EXISTS "${HepMC_LIBRARY_DEBUG_DLL}")
      set_property( TARGET HepMC::HepMC APPEND PROPERTY IMPORTED_CONFIGURATIONS Debug )
      set_target_properties( HepMC::HepMC PROPERTIES
        IMPORTED_LOCATION_DEBUG           "${HepMC_LIBRARY_DEBUG_DLL}"
        IMPORTED_IMPLIB_DEBUG             "${HepMC_LIBRARY_DEBUG}" )
    endif()

  else()

    # For all other environments (ones without dll libraries), create
    # the imported library targets.
    add_library( HepMC::HepMC      UNKNOWN IMPORTED )
    set_target_properties( HepMC::HepMC PROPERTIES
      IMPORTED_LOCATION                 "${HepMC_LIBRARY}"
      INTERFACE_INCLUDE_DIRECTORIES     "${HepMC_INCLUDE_DIRS}"
      IMPORTED_LINK_INTERFACE_LANGUAGES "CXX"
      )
  endif()
endif()

# Distributed under the OSI-approved BSD 3-Clause License.  See accompanying
# file Copyright.txt or https://cmake.org/licensing for details.
# This is a slightly modified version of FindGSL.cmake taken from cmake 3.7

#.rst:
# Findrivet
# --------
#
# Find the native rivet includes and libraries.
#
# rivet package is an object oriented, C++ analysis tool for High Energy
# Physics Monte Carlo generators and simulation. It is free software
# under the GNU General Public License.
#
# Imported Targets
# ^^^^^^^^^^^^^^^^
#
# If rivet is found, this module defines the following
# :prop_tgt:`IMPORTED` target::
#
#  rivet::rivet      - The main rivet library.
#
# Result Variables
# ^^^^^^^^^^^^^^^^
#
# This module will set the following variables in your project::
#
#  rivet_FOUND          - True if rivet found on the local system
#  rivet_INCLUDE_DIRS   - Location of rivet header files.
#  rivet_LIBRARIES      - The rivet libraries.
#  rivet_VERSION        - The version of the discovered rivet install.
#
# Hints
# ^^^^^
#
# Set ``rivet_ROOT_DIR`` to a directory that contains a rivet installation.
#
# This script expects to find libraries at ``$rivet_ROOT_DIR/lib`` and the rivet
# headers at ``$rivet_ROOT_DIR/include/rivet``.  The library directory may
# optionally provide Release and Debug folders.  For Unix-like systems, this
# script will use ``$rivet_ROOT_DIR/bin/rivet-config`` (if found) to aid in the
# discovery rivet.
#
# Cache Variables
# ^^^^^^^^^^^^^^^
#
# This module may set the following variables depending on platform and type
# of rivet installation discovered.  These variables may optionally be set to
# help this module find the correct files::
#
#  rivet_CONFIG_EXECUTABLE   - Location of the ``rivet-config`` script (if any).
#  rivet_LIBRARY             - Location of the rivet library.
#  rivet_LIBRARY_DEBUG       - Location of the debug rivet library (if any).
#

# short cut for config header
set(_rivet_config_header "Rivet/Config/RivetConfig.hh")

# Include these modules to handle the QUIETLY and REQUIRED arguments.
include(FindPackageHandleStandardArgs)

#=============================================================================
# If the user has provided ``rivet_ROOT_DIR``, use it!  Choose items found
# at this location over system locations.
if( EXISTS "$ENV{rivet_ROOT_DIR}" )
  file( TO_CMAKE_PATH "$ENV{rivet_ROOT_DIR}" rivet_ROOT_DIR )
  set( rivet_ROOT_DIR "${rivet_ROOT_DIR}" CACHE PATH "Prefix for rivet installation." )
endif()
if( NOT EXISTS "${rivet_ROOT_DIR}" )
  set( rivet_USE_PKGCONFIG ON )
endif()

#=============================================================================
# As a first try, use the PkgConfig module.  This will work on many
# *NIX systems.  See :module:`findpkgconfig`
# This will return ``rivet_INCLUDEDIR`` and ``rivet_LIBDIR`` used below.
if( rivet_USE_PKGCONFIG )
  find_package(PkgConfig)
  pkg_check_modules( rivet QUIET rivet )

  if( EXISTS "${rivet_INCLUDEDIR}" )
    get_filename_component( rivet_ROOT_DIR "${rivet_INCLUDEDIR}" DIRECTORY CACHE)
  endif()
endif()

#=============================================================================
# Set rivet_INCLUDE_DIRS and rivet_LIBRARIES. If we skipped the PkgConfig step, try
# to find the libraries at $rivet_ROOT_DIR (if provided), in the directory
# suggested by rivet-config (if available), or in standard system
# locations.  These find_library and find_path calls will prefer custom
# locations over standard locations (HINTS).  If the requested file is found
# neither at the HINTS location nor via rivet-config, standard system locations
# will be still be searched (/usr/lib64 (Redhat), lib/i386-linux-gnu (Debian)).

# If we didn't use PkgConfig, try to find the version via rivet-config
if( NOT rivet_VERSION )
  find_program( rivet_CONFIG_EXECUTABLE
    NAMES rivet-config
    HINTS "${rivet_ROOT_DIR}/bin"
    )
  if( EXISTS "${rivet_CONFIG_EXECUTABLE}" )
    execute_process(
      COMMAND "${rivet_CONFIG_EXECUTABLE}" --version
      OUTPUT_VARIABLE rivet_VERSION
      OUTPUT_STRIP_TRAILING_WHITESPACE )
    execute_process(
      COMMAND "${rivet_CONFIG_EXECUTABLE}" --includedir
      OUTPUT_VARIABLE rivet_CONFIG_INCLUDEDIR
      OUTPUT_STRIP_TRAILING_WHITESPACE )
    execute_process(
      COMMAND "${rivet_CONFIG_EXECUTABLE}" --libdir
      OUTPUT_VARIABLE rivet_CONFIG_LIBDIR
      OUTPUT_STRIP_TRAILING_WHITESPACE )
    # get headeres of rivet dependencies
    execute_process(
      COMMAND "${rivet_CONFIG_EXECUTABLE}" --cppflags
      OUTPUT_VARIABLE rivet_INCLUDE_DIRS
      OUTPUT_STRIP_TRAILING_WHITESPACE )
    string(REPLACE " " ";" rivet_INCLUDE_DIRS ${rivet_INCLUDE_DIRS})
    list(FILTER rivet_INCLUDE_DIRS INCLUDE REGEX "-I.*")
    string(REPLACE "-I" "" rivet_INCLUDE_DIRS "${rivet_INCLUDE_DIRS}")
  endif()
endif()
find_path( rivet_INCLUDE_DIR
  NAMES Rivet/Rivet.hh
  HINTS ${rivet_ROOT_DIR}/include ${rivet_INCLUDEDIR} ${rivet_CONFIG_INCLUDEDIR}
)
find_library( rivet_LIBRARY
  NAMES Rivet
  HINTS ${rivet_ROOT_DIR}/lib ${rivet_LIBDIR} ${rivet_CONFIG_LIBDIR}
  PATH_SUFFIXES Release Debug
)
# Do we also have debug versions?
find_library( rivet_LIBRARY_DEBUG
  NAMES Rivet
  HINTS ${rivet_ROOT_DIR}/lib ${rivet_LIBDIR} ${rivet_CONFIG_LIBDIR}
  PATH_SUFFIXES Debug
)
if( NOT rivet_INCLUDE_DIRS )
  set( rivet_INCLUDE_DIRS ${rivet_INCLUDE_DIR} )
endif()
set( rivet_LIBRARIES ${rivet_LIBRARY} )

if( NOT rivet_VERSION )
  if(IS_DIRECTORY ${rivet_INCLUDE_DIR})
    file(STRINGS "${rivet_INCLUDE_DIR}/${_rivet_config_header}"
      _rivet_VERS REGEX "#define *RIVET_VERSION *\"[^\"]*\"")
    string(
      REGEX MATCH
      "[0-9]*\\.[0-9]*\\.[0-9]*"
      rivet_VERSION ${_rivet_VERS}
      )
  endif()
endif()
if( rivet_VERSION )
  string(REPLACE "." ";" t_list ${rivet_VERSION})
  list(APPEND t_list 0 0) # add a buffer in case supversion not set
  list(GET t_list 0 rivet_VERSION_MAJOR)
  list(GET t_list 1 rivet_VERSION_MINOR)
  list(GET t_list 2 rivet_VERSION_PATCH)
endif()
#=============================================================================
# handle the QUIETLY and REQUIRED arguments and set rivet_FOUND to TRUE if all
# listed variables are TRUE
find_package_handle_standard_args( rivet
  FOUND_VAR
    rivet_FOUND
  REQUIRED_VARS
    rivet_INCLUDE_DIR
    rivet_LIBRARY
  VERSION_VAR
    rivet_VERSION
    )

mark_as_advanced( rivet_ROOT_DIR rivet_VERSION rivet_LIBRARY rivet_INCLUDE_DIR
  rivet_LIBRARY_DEBUG rivet_USE_PKGCONFIG rivet_CONFIG )

#=============================================================================
# Register imported libraries:
# 1. If we can find a Windows .dll file (or if we can find both Debug and
#    Release libraries), we will set appropriate target properties for these.
# 2. However, for most systems, we will only register the import location and
#    include directory.

# Look for dlls, or Release and Debug libraries.
if(WIN32)
  string( REPLACE ".lib" ".dll" rivet_LIBRARY_DLL       "${rivet_LIBRARY}" )
  string( REPLACE ".lib" ".dll" rivet_LIBRARY_DEBUG_DLL "${rivet_LIBRARY_DEBUG}" )
endif()

if( rivet_FOUND AND NOT TARGET rivet::rivet )
  if( EXISTS "${rivet_LIBRARY_DLL}")

    # Windows systems with dll libraries.
    add_library( rivet::rivet      SHARED IMPORTED )
    add_library( rivet::rivetcblas SHARED IMPORTED )

    # Windows with dlls, but only Release libraries.
    set_target_properties( rivet::rivet PROPERTIES
      IMPORTED_LOCATION_RELEASE         "${rivet_LIBRARY_DLL}"
      IMPORTED_IMPLIB                   "${rivet_LIBRARY}"
      INTERFACE_INCLUDE_DIRECTORIES     "${rivet_INCLUDE_DIRS}"
      IMPORTED_CONFIGURATIONS           Release
      IMPORTED_LINK_INTERFACE_LANGUAGES "CXX"
      )

    # If we have both Debug and Release libraries
    if( EXISTS "${rivet_LIBRARY_DEBUG_DLL}")
      set_property( TARGET rivet::rivet APPEND PROPERTY IMPORTED_CONFIGURATIONS Debug )
      set_target_properties( rivet::rivet PROPERTIES
        IMPORTED_LOCATION_DEBUG           "${rivet_LIBRARY_DEBUG_DLL}"
        IMPORTED_IMPLIB_DEBUG             "${rivet_LIBRARY_DEBUG}" )
    endif()

  else()

    # For all other environments (ones without dll libraries), create
    # the imported library targets.
    add_library( rivet::rivet      UNKNOWN IMPORTED )
    set_target_properties( rivet::rivet PROPERTIES
      IMPORTED_LOCATION                 "${rivet_LIBRARY}"
      INTERFACE_INCLUDE_DIRECTORIES     "${rivet_INCLUDE_DIRS}"
      IMPORTED_LINK_INTERFACE_LANGUAGES "CXX"
      )
  endif()
endif()

#=============================================================================
# Find if rivet was compiled with HepMC 2 or 3
if(rivet_FOUND)
  if(EXISTS "${rivet_INCLUDE_DIR}/${_rivet_config_header}")
    file(READ "${rivet_INCLUDE_DIR}/${_rivet_config_header}"
      _rivet_config)
    string(
      REGEX MATCH "\n#define *RIVET_ENABLE_HEPMC_3 *1"
      _rivet_config ${_rivet_config})
    if(_rivet_config)
      set(rivet_USE_HEPMC3 true)
      mark_as_advanced(rivet_USE_HEPMC3)
    endif()
  else()
    message(WARNING "Can not determin rivets HepMC version "
              "(Unable to find RivetConfig.hh)")
  endif()

  if(rivet_USE_HEPMC3)
    if(EXCLUDE_HepMC3)
      message(FATAL_ERROR "rivet requires HepMC3, but HepMC3 was explicitly"
        " excluded (EXCLUDE_HepMC3=${EXCLUDE_HepMC3}).")
    elseif(NOT HepMC3_FOUND)
      find_package(HepMC3)
      if(NOT HepMC3_FOUND)
        message(FATAL_ERROR "Unable to find HepMC3 (required by rivet)")
      endif()
    endif()
    set_property(
      TARGET rivet::rivet
      APPEND PROPERTY INTERFACE_LINK_LIBRARIES "${HEPMC3_LIBRARIES}"
    )
    set_property(
      TARGET rivet::rivet
      APPEND PROPERTY INTERFACE_INCLUDE_DIRECTORIES "${HEPMC3_INCLUDE_DIR}"
    )
  else()
    if(EXCLUDE_HepMC)
      message(FATAL_ERROR "rivet requires HepMC, but HepMC was explicitly"
        " excluded (EXCLUDE_HepMC=${EXCLUDE_HepMC}).")
    elseif(NOT HepMC_FOUND)
      find_package(HepMC)
      if(NOT HepMC_FOUND)
        message(FATAL_ERROR "Unable to find HepMC (required by rivet)")
      endif()
    endif()
    set_property(
      TARGET rivet::rivet
      APPEND PROPERTY INTERFACE_LINK_LIBRARIES HepMC::HepMC
    )
  endif()
endif()

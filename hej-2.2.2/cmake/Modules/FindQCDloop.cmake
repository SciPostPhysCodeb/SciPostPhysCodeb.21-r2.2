# Distributed under the OSI-approved BSD 3-Clause License.  See accompanying
# file Copyright.txt or https://cmake.org/licensing for details.
# This is a slightly modified version of FindGSL.cmake taken from cmake 3.7

#.rst:
# FindQCDloop
# --------
#
# Find the native QCDloop includes and libraries.
#
# QCDLoop is a library for one-loop scalar Feynman integrals. It is free
# software under the GNU General Public License.
#
# Imported Targets
# ^^^^^^^^^^^^^^^^
#
# If QCDloop is found, this module defines the following
# :prop_tgt:`IMPORTED` target::
#
#  QCDloop::qcdloop      - The main QCDloop library.
#
# Result Variables
# ^^^^^^^^^^^^^^^^
#
# This module will set the following variables in your project::
#
#  QCDloop_FOUND          - True if QCDloop found on the local system
#  QCDloop_INCLUDE_DIRS   - Location of QCDloop header files.
#  QCDloop_LIBRARIES      - The QCDloop libraries.
#  QCDloop_VERSION        - The version of the discovered QCDloop install.
#
# Hints
# ^^^^^
#
# Set ``QCDloop_ROOT_DIR`` to a directory that contains a QCDloop installation.
#
# This script expects to find libraries at ``$QCDloop_ROOT_DIR/lib`` and the QCDloop
# headers at ``$QCDloop_ROOT_DIR/include/qcdloop``.  The library directory may
# optionally provide Release and Debug folders.  For Unix-like systems, this
# script will use ``$QCDloop_ROOT_DIR/bin/qcdloop-config`` (if found) to aid in the
# discovery QCDloop.
#
# Cache Variables
# ^^^^^^^^^^^^^^^
#
# This module may set the following variables depending on platform and type
# of QCDloop installation discovered.  These variables may optionally be set to
# help this module find the correct files::
#
#  QCDloop_CONFIG_EXECUTABLE   - Location of the ``qcdloop-config`` script (if any).
#  QCDloop_LIBRARY             - Location of the QCDloop library.
#  QCDloop_LIBRARY_DEBUG       - Location of the debug QCDloop library (if any).
#

# Include these modules to handle the QUIETLY and REQUIRED arguments.
include(FindPackageHandleStandardArgs)

#=============================================================================
# If the user has provided ``QCDloop_ROOT_DIR``, use it!  Choose items found
# at this location over system locations.
if( EXISTS "$ENV{QCDloop_ROOT_DIR}" )
  file( TO_CMAKE_PATH "$ENV{QCDloop_ROOT_DIR}" QCDloop_ROOT_DIR )
  set( QCDloop_ROOT_DIR "${QCDloop_ROOT_DIR}" CACHE PATH "Prefix for QCDloop installation." )
endif()
if( NOT EXISTS "${QCDloop_ROOT_DIR}" )
  set( QCDloop_USE_PKGCONFIG ON )
endif()

#=============================================================================
# As a first try, use the PkgConfig module.  This will work on many
# *NIX systems.  See :module:`findpkgconfig`
# This will return ``QCDloop_INCLUDEDIR`` and ``QCDloop_LIBDIR`` used below.
if( QCDloop_USE_PKGCONFIG )
  find_package(PkgConfig)
  pkg_check_modules( QCDloop QUIET qcdloop )

  if( EXISTS "${QCDloop_INCLUDEDIR}" )
    get_filename_component( QCDloop_ROOT_DIR "${QCDloop_INCLUDEDIR}" DIRECTORY CACHE)
  endif()
endif()

#=============================================================================
# Set QCDloop_INCLUDE_DIRS and QCDloop_LIBRARIES. If we skipped the PkgConfig step, try
# to find the libraries at $QCDloop_ROOT_DIR (if provided), in the directory
# suggested by qcdloop-config (if available), or in standard system
# locations.  These find_library and find_path calls will prefer custom
# locations over standard locations (HINTS).  If the requested file is found
# neither at the HINTS location nor via qcdloop-config, standard system locations
# will be still be searched (/usr/lib64 (Redhat), lib/i386-linux-gnu (Debian)).

# If we didn't use PkgConfig, try to find the version via qcdloop-config
if( NOT QCDloop_VERSION )
  find_program( QCDloop_CONFIG_EXECUTABLE
    NAMES qcdloop-config
    HINTS "${QCDloop_ROOT_DIR}/bin"
    )
  if( EXISTS "${QCDloop_CONFIG_EXECUTABLE}" )
    execute_process(
      COMMAND "${QCDloop_CONFIG_EXECUTABLE}" --version
      OUTPUT_VARIABLE QCDloop_VERSION
      OUTPUT_STRIP_TRAILING_WHITESPACE )
    execute_process(
      COMMAND "${QCDloop_CONFIG_EXECUTABLE}" --incdir
      OUTPUT_VARIABLE QCDloop_CONFIG_INCLUDEDIR
      OUTPUT_STRIP_TRAILING_WHITESPACE )
    execute_process(
      COMMAND "${QCDloop_CONFIG_EXECUTABLE}" --libdir
      OUTPUT_VARIABLE QCDloop_CONFIG_LIBDIR
      OUTPUT_STRIP_TRAILING_WHITESPACE )
  endif()

endif()

find_path( QCDloop_INCLUDE_DIR
  NAMES qcdloop/qcdloop.h
  HINTS ${QCDloop_ROOT_DIR}/include ${QCDloop_INCLUDEDIR} ${QCDloop_CONFIG_INCLUDEDIR}
)
find_library( QCDloop_LIBRARY
  NAMES qcdloop
  HINTS ${QCDloop_ROOT_DIR}/lib ${QCDloop_LIBDIR} ${QCDloop_CONFIG_LIBDIR}
  PATH_SUFFIXES Release Debug
)
# Do we also have debug versions?
find_library( QCDloop_LIBRARY_DEBUG
  NAMES qcdloop
  HINTS ${QCDloop_ROOT_DIR}/lib ${QCDloop_LIBDIR} ${QCDloop_CONFIG_LIBDIR}
  PATH_SUFFIXES Debug
)
set( QCDloop_INCLUDE_DIRS ${QCDloop_INCLUDE_DIR} )
set( QCDloop_LIBRARIES ${QCDloop_LIBRARY} )

if( QCDloop_VERSION )# remove "" in QCDloop version
  string(REGEX REPLACE "\"" "" QCDloop_VERSION ${QCDloop_VERSION})
  string(REPLACE "." ";" t_list ${QCDloop_VERSION})
  list(APPEND t_list 0 0) # add a buffer in case supversion not set
  list(GET t_list 0 QCDloop_VERSION_MAJOR)
  list(GET t_list 1 QCDloop_VERSION_MINOR)
  list(GET t_list 2 QCDloop_VERSION_PATCH)
endif()

#=============================================================================
# handle the QUIETLY and REQUIRED arguments and set QCDloop_FOUND to TRUE if all
# listed variables are TRUE
find_package_handle_standard_args( QCDloop
  FOUND_VAR
    QCDloop_FOUND
  REQUIRED_VARS
    QCDloop_INCLUDE_DIR
    QCDloop_LIBRARY
  VERSION_VAR
    QCDloop_VERSION
    )

mark_as_advanced( QCDloop_ROOT_DIR QCDloop_VERSION QCDloop_LIBRARY QCDloop_INCLUDE_DIR
  QCDloop_LIBRARY_DEBUG QCDloop_USE_PKGCONFIG QCDloop_CONFIG )

#=============================================================================
# Register imported libraries:
# 1. If we can find a Windows .dll file (or if we can find both Debug and
#    Release libraries), we will set appropriate target properties for these.
# 2. However, for most systems, we will only register the import location and
#    include directory.

# Look for dlls, or Release and Debug libraries.
if(WIN32)
  string( REPLACE ".lib" ".dll" QCDloop_LIBRARY_DLL       "${QCDloop_LIBRARY}" )
  string( REPLACE ".lib" ".dll" QCDloop_LIBRARY_DEBUG_DLL "${QCDloop_LIBRARY_DEBUG}" )
endif()

if( QCDloop_FOUND AND NOT TARGET QCDloop::qcdloop )
  if( EXISTS "${QCDloop_LIBRARY_DLL}")

    # Windows systems with dll libraries.
    add_library( QCDloop::qcdloop      SHARED IMPORTED )
    add_library( QCDloop::qcdloopcblas SHARED IMPORTED )

    # Windows with dlls, but only Release libraries.
    set_target_properties( QCDloop::qcdloop PROPERTIES
      IMPORTED_LOCATION_RELEASE         "${QCDloop_LIBRARY_DLL}"
      IMPORTED_IMPLIB                   "${QCDloop_LIBRARY}"
      INTERFACE_INCLUDE_DIRECTORIES     "${QCDloop_INCLUDE_DIRS}"
      IMPORTED_CONFIGURATIONS           Release
      IMPORTED_LINK_INTERFACE_LANGUAGES "CXX"
      )

    # If we have both Debug and Release libraries
    if( EXISTS "${QCDloop_LIBRARY_DEBUG_DLL}")
      set_property( TARGET QCDloop::qcdloop APPEND PROPERTY IMPORTED_CONFIGURATIONS Debug )
      set_target_properties( QCDloop::qcdloop PROPERTIES
        IMPORTED_LOCATION_DEBUG           "${QCDloop_LIBRARY_DEBUG_DLL}"
        IMPORTED_IMPLIB_DEBUG             "${QCDloop_LIBRARY_DEBUG}" )
    endif()

  else()

    # For all other environments (ones without dll libraries), create
    # the imported library targets.
    add_library( QCDloop::qcdloop      UNKNOWN IMPORTED )
    set_target_properties( QCDloop::qcdloop PROPERTIES
      IMPORTED_LOCATION                 "${QCDloop_LIBRARY}"
      INTERFACE_INCLUDE_DIRECTORIES     "${QCDloop_INCLUDE_DIRS}"
      IMPORTED_LINK_INTERFACE_LANGUAGES "CXX"
      )
  endif()
endif()
